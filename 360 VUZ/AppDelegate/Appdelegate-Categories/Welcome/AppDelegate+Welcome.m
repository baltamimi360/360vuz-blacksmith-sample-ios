//
//  AppDelegate+Welcome.m
//  360 VUZ
//
//  Created by Mosib on 7/5/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "AppDelegate+Welcome.h"
#import "WelcomeViewController.h"

@implementation AppDelegate (Welcome)

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) presentWelcomeControllerOnRootView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    WelcomeViewController *welcomeViewController = [[WelcomeViewController alloc] init];
    UINavigationController * welcomeNavigationController = [[UINavigationController alloc]
                                                            initWithRootViewController:welcomeViewController];
    [welcomeNavigationController setNavigationBarHidden:YES animated:false];
    
   self.window.rootViewController = [[UIViewController alloc] init];
    
    [self.window.rootViewController presentViewController:welcomeNavigationController animated:false completion:nil];
    
    self.isWelcomeNavigationOnTop = true;
    self.topView = welcomeNavigationController;
    
}

@end
