//
//  AppDelegate+Observers.m
//  360 VUZ
//
//  Created by Mosib on 7/5/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "AppDelegate+Observers.h"

@implementation AppDelegate (Observers)

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) addObservers
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    // Splash Video Finished
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(appdelegateRecievedNotification:)
                                                 name:kNotificationSplashVideoFinished
                                               object:nil];
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) appdelegateRecievedNotification:(NSNotification *) notification
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if ([[notification name] isEqualToString:kNotificationSplashVideoFinished])
    {
        // Present Controller on RootView
        [self presentWelcomeControllerOnRootView];
    }
}

@end
