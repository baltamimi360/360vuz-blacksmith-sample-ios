//
//  AppDelegate+TabBarController.m
//  360 VUZ
//
//  Created by Mosib on 7/5/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "AppDelegate+TabBarController.h"
#import "MainTabBarController.h"

@implementation AppDelegate (TabBarController)

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) setUpTabBarController
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if (self.tabBarController == nil)
    {
      MainTabBarController *tabBar = [[MainTabBarController alloc] init];
        
      self.tabBarController = tabBar;
    }
    
    else
    {
        [self.tabBarController setSelectedIndex:0];
        [self.tabBarController setSelectedIndex:0];
    }
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) makeTabBarAsRootViewController
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
     self.window.rootViewController = self.tabBarController;
}

@end
