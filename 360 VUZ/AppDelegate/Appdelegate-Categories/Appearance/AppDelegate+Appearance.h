//
//  AppDelegate+Appearance.h
//  360 VUZ
//
//  Created by Mosib on 7/9/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (Appearance)

- (void) setNavigationAppearance;

@end
