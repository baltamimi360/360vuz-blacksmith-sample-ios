//
//  AppDelegate+Reachability.h
//  360 VUZ
//
//  Created by Mosib on 7/19/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (Reachability)

- (void) startReachability;
- (BOOL) isInternetReachable;

@end
