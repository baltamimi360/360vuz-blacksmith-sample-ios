//
//  AppDelegate+Reachability.m
//  360 VUZ
//
//  Created by Mosib on 7/19/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "AppDelegate+Reachability.h"


@implementation AppDelegate (Reachability)

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) startReachability
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    // Allocate a reachability object
    Reachability* reach = [Reachability reachabilityWithHostname:kReachableHost];
    
    // Weak Self
    __weak AppDelegate *weakSelf = self;
    
    // Set the blocks
    reach.reachableBlock = ^(Reachability*reach)
    {
        weakSelf.isReachable = true;
    };
    
    reach.unreachableBlock = ^(Reachability*reach)
    {
        weakSelf.isReachable = false;
    };
    
    // Start the notifier, which will cause the reachability object to retain itself!
    [reach startNotifier];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (BOOL) isInternetReachable
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return self.isReachable;
}


@end
