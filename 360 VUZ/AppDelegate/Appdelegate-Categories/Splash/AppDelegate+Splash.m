//
//  AppDelegate+Splash.m
//  360 VUZ
//
//  Created by Mosib on 7/5/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "AppDelegate+Splash.h"
#import "SplashViewController.h"

@implementation AppDelegate (Splash)

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) presentSplashControllerOnRootView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    SplashViewController *splashViewController = [[SplashViewController alloc] init];
    UINavigationController * splashNavigationController = [[UINavigationController alloc]
                                                       initWithRootViewController:splashViewController];
    [splashNavigationController setNavigationBarHidden:YES animated:false];
    
    self.window.rootViewController = splashNavigationController;
    
    [self.window makeKeyAndVisible];

}

@end
