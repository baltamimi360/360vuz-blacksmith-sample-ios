//
//  AppDelegate+PushNotifications.h
//  360 VUZ
//
//  Created by Mosib on 7/19/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (PushNotifications)


- (void) registerForPushNotifications:(UIApplication *)application;
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken;
- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error;
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings;
- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler;
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo;

@end
