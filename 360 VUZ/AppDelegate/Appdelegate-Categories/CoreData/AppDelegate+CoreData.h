//
//  AppDelegate+CoreData.h
//  360 VUZ
//
//  Created by Mosib on 7/5/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (CoreData)

- (void)saveContext;

@end
