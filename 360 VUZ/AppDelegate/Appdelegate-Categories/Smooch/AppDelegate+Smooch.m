//
//  AppDelegate+Smooch.m
//  360 VUZ
//
//  Created by Mosib on 8/16/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "AppDelegate+Smooch.h"
#import <Smooch/Smooch.h>

@implementation AppDelegate (Smooch)

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpSmooch
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    //Smooch chatbot sdk initialize
    SKTSettings* settings = [SKTSettings settingsWithAppId:kSmoochAppToken];
    settings.conversationAccentColor = [UIColor colorWithRed:145.0/255 green:45.0/255 blue:141.0/255 alpha:1.0];
    settings.conversationStatusBarStyle = UIStatusBarStyleDefault;
    [Smooch initWithSettings:settings completionHandler:nil];

    [Smooch setUserFirstName:@"Guest" lastName:@"User"];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) helpDesk
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    NSString *firstName = [[Settings sharedManger] getUserDetails].firstName;
    NSString *lastName = [[Settings sharedManger] getUserDetails].lastName;
    
    if ([firstName isEqualToString:@""] || firstName == nil )
    {
        firstName = @"Guest";
    }
    
    if ([lastName isEqualToString:@""] || lastName == nil )
    {
        firstName = @"User";
    }
    
    [SKTUser currentUser].firstName = firstName;
    [SKTUser currentUser].lastName = lastName;

    [Smooch show];
}

@end
