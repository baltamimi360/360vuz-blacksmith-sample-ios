//
//  AppDelegate+Smooch.h
//  360 VUZ
//
//  Created by Mosib on 8/16/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (Smooch)

- (void) setUpSmooch;
- (void) helpDesk;

@end
