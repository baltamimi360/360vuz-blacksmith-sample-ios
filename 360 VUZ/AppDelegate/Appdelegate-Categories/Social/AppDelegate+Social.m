//
//  AppDelegate+Social.m
//  360 VUZ
//
//  Created by Mosib on 7/25/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "AppDelegate+Social.h"
#import <Fabric/Fabric.h>

@implementation AppDelegate (Social)

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setSocialKeys:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    // Facebook 
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    
    // Twitter Initialize
    [[Twitter sharedInstance] startWithConsumerKey:[SocialKeyConstants twitterConsumerKey] consumerSecret:[SocialKeyConstants twitterConsumerSecret]];
    
    // Fabric
    [Fabric with:@[ [Twitter class]]];
}

@end
