//
//  AppDelegate+Social.h
//  360 VUZ
//
//  Created by Mosib on 7/25/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (Social)

- (void) setSocialKeys:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;

@end
