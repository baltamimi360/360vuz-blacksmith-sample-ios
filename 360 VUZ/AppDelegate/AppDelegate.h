//
//  AppDelegate.h
//  360 VUZ
//
//  Created by Mosib on 7/2/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>



@class MainTabBarController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) MainTabBarController *tabBarController;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

@property (nonatomic,assign) UIInterfaceOrientationMask allowedOrientation;

@property (nonatomic,retain) UINavigationController *topView;

@property (nonatomic) BOOL isWelcomeNavigationOnTop;

@property (nonatomic) BOOL isReachable;

@property (nonatomic) URLType type;

+ (AppDelegate *)appDelegate;


@end

