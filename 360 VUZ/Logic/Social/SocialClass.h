//
//  SocialClass.h
//  360 VUZ
//
//  Created by Mosib on 7/25/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SocialClass : NSObject 

+ (SocialClass *)sharedManger;
- (SocialClass *)init;

-(void) logInWithFacebook:(UIViewController *) viewController;
-(void) logInWithGoogle;
-(void) logInWithTwitter;

@end
