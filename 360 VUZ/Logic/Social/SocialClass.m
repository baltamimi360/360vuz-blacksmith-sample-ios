//
//  SocialClass.m
//  360 VUZ
//
//  Created by Mosib on 7/25/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "SocialClass.h"
#import "ServiceRegisterUser.h"

@interface SocialClass () <GIDSignInDelegate, GIDSignInUIDelegate>
{
    __weak SocialClass* weakSelf;
}
@end

@implementation SocialClass

static SocialClass *sharedInsatce;

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
+(SocialClass *)sharedManger
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInsatce = [[SocialClass alloc] init];
    });
    return sharedInsatce;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(SocialClass *)init
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if(self=[super init])
    {
        weakSelf = self;
    }
    return self;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) logInWithFacebook:(UIViewController *) viewController
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login
     logInWithReadPermissions: @[@"public_profile", @"email", @"user_friends"]
     fromViewController:viewController
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error");
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
         } else {
             NSLog(@"Logged in");
             
             STARTMAINTHREAD
             
            [MBProgressHUD showHUDAddedTo:[AppDelegate appDelegate].topView.view animated:YES];
             
             ENDTHREAD
             
             if ([result.grantedPermissions containsObject:@"email"]) {
                 // Do work
                 [self getDetailsAndLogin];
                 
             }
         }
     }];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void)getDetailsAndLogin
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"first_name, last_name, picture.type(large), email, name, id, gender,friendlists"}]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
         if (!error) {
             
             STARTMAINTHREAD
             
             [MBProgressHUD hideHUDForView:[AppDelegate appDelegate].topView.view animated:YES];
             
             ENDTHREAD
             
             // API call for Login
             [self checkSignUpCredentialsAPI:@{kAPIkeyGender:[[UtilityClass sharedManger] getRandGender],
                                               kAPIkeyFirstName:[result valueForKey:@"first_name"],
                                               kAPIkeyLastName:[result valueForKey:@"last_name"],
                                               kAPIkeyDeviceID:[[Settings sharedManger] getAPNSToken],
                                               kAPIkeyPhoneNumber:@"",
                                               kAPIkeyWebsite:@"",
                                               kAPIkeyBio:@"",
                                               kAPIkeyEmail:[result valueForKey:kFKeyUserEmail],
                                               kFKeyFacebookID:[[FBSDKAccessToken currentAccessToken] userID],
                                               kAPIkeyModel:[[UIDevice currentDevice] platformString]}];
             
         }
         else{
            
             NSLog(@"%@",error.localizedDescription);
         }
     }];
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) logInWithGoogle
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    GIDSignIn* signIn = [GIDSignIn sharedInstance];
    signIn.shouldFetchBasicProfile = YES;
    signIn.clientID = [SocialKeyConstants googleClientID];
    signIn.scopes = @[ @"profile", @"email" ];
    signIn.delegate = self;
    signIn.uiDelegate = self;
    
    [AppDelegate appDelegate].type = URLTypeGoogle;
    [[GIDSignIn sharedInstance] signIn];
}

#pragma mark - GIDSignInDelegate

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if (error) {
        
        NSLog(@"Authentication error: %@", error);
        return;
    }
    
    // API call for Login
    [self checkSignUpCredentialsAPI:@{kAPIkeyGender:[[UtilityClass sharedManger] getRandGender],
                                      kAPIkeyFirstName:user.profile.givenName,
                                      kAPIkeyLastName:user.profile.familyName,
                                      kAPIkeyDeviceID:[[Settings sharedManger] getAPNSToken],
                                      kAPIkeyPhoneNumber:@"",
                                      kAPIkeyWebsite:@"",
                                      kAPIkeyBio:@"",
                                      kAPIkeyEmail:[GIDSignIn sharedInstance].currentUser.profile.email,
                                      kGoogleID:[GIDSignIn sharedInstance].currentUser.userID,
                                      kAPIkeyModel:[[UIDevice currentDevice] platformString]}];

    
}

#pragma mark - Google SignIn Delegate

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
}
// Present a view that prompts the user to sign in with Google

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)signIn:(GIDSignIn *)signIn presentViewController:(UIViewController *)viewController
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [[AppDelegate appDelegate].topView  presentViewController:viewController animated:YES completion:nil];
}
// Dismiss the "Sign in with Google" view

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)signIn:(GIDSignIn *)signIn dismissViewController:(UIViewController *)viewController
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [[AppDelegate appDelegate].topView dismissViewControllerAnimated:YES completion:nil];
    
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) logInWithTwitter
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [[Twitter sharedInstance] logInWithMethods:TWTRLoginMethodWebBased completion:^
     (TWTRSession *session, NSError *error) {
        if (session) {
            
            STARTMAINTHREAD
            
            [MBProgressHUD showHUDAddedTo:[AppDelegate appDelegate].topView.view animated:YES];
            
            ENDTHREAD
          
            [self getTwitterCredentials];
            
        } else {
            NSLog(@"error: %@", [error localizedDescription]);
        }
    }];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) getTwitterCredentials
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    TWTRAPIClient *client = [TWTRAPIClient clientWithCurrentUser];
    NSURLRequest *request = [client URLRequestWithMethod:@"GET"
                                                     URL:@"https://api.twitter.com/1.1/account/verify_credentials.json"
                                              parameters:@{@"include_email": @"true", @"skip_status": @"true",@"scope":@"email"}
                                                   error:nil];
    
    [client sendTwitterRequest:request completion:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
    
        STARTMAINTHREAD
        
        [MBProgressHUD hideHUDForView:[AppDelegate appDelegate].topView.view animated:YES];
        
        ENDTHREAD
        
        if (connectionError) {
            
            [[UtilityClass sharedManger] alertErrorWithMessage:@"Cannot Connect to Twitter server at the moment please try again"];
        }
        
        else
        {
            NSDictionary *dataDictionary = [UtilityClass convertToDictionory:data];
            
            if ([dataDictionary objectForKey:kAPIkeyEmail] == nil) {
                
                [[UtilityClass sharedManger] alertErrorWithMessage:@"Email not found with the current account"];
            }
            else
            {
                
                
                // API call for Login
                [self checkSignUpCredentialsAPI:@{kAPIkeyGender:[[UtilityClass sharedManger] getRandGender],
                                                  kAPIkeyFirstName: [[[UtilityClass sharedManger] splitName:[dataDictionary objectForKey:@"name"]] firstObject],
                                                  kAPIkeyLastName: [[[UtilityClass sharedManger] splitName:[dataDictionary objectForKey:@"name"]] lastObject],
                                                  kAPIkeyDeviceID:[[Settings sharedManger] getAPNSToken],
                                                  kAPIkeyPhoneNumber:@"",
                                                  kAPIkeyWebsite:@"",
                                                  kAPIkeyBio:@"",
                                                  kAPIkeyEmail:[dataDictionary objectForKey:kAPIkeyEmail],
                                                  kTwitterID:[dataDictionary objectForKey:@"id"],
                                                  kAPIkeyModel:[[UIDevice currentDevice] platformString]}];
                
            }
        }
    }];
        
}
     

#pragma mark - API Methods -
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) checkSignUpCredentialsAPI:(NSDictionary *) paramerters
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [MBProgressHUD showHUDAddedTo:[AppDelegate appDelegate].topView.view animated:YES];
    
    STARTBACKGROUNDTHREAD
    ServiceRegisterUser *registerUser = [[ServiceRegisterUser alloc]init];
    
    [registerUser executeRequest:paramerters
                         success:^(NSDictionary  *responseObject)
     {
         [weakSelf SuccessForSignUpCredentialsAPI:responseObject];
         
     }
                         failure:^(NSError * error) {
                             
                             [weakSelf FailureToGetResponceFromServer:error];
                         }];
    ENDTHREAD
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) SuccessForSignUpCredentialsAPI:(NSDictionary *) responseObject
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    NSString * responceObjectStatus = [responseObject objectForKey:kAPIkeyStatus];
    
    if ([responceObjectStatus isEqualToString:kAPIkeySuccess])
    {
        [self successInAPIResponce:responseObject];
    }
    else
    {
        [self failureInAPIResponce:responseObject];
    }
    
    STARTMAINTHREAD
    
    [MBProgressHUD hideHUDForView:[AppDelegate appDelegate].topView.view animated:YES];
    
    ENDTHREAD
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) successInAPIResponce:(NSDictionary *) responseObject
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    STARTMAINTHREAD
    
    [[UtilityClass sharedManger] alertMessage:@"Success" withMessage:[responseObject objectForKey:kAPIkeyMessage]];
    
    [AppDelegate appDelegate].isWelcomeNavigationOnTop = false;
    [AppDelegate appDelegate].topView = nil;
    
    [[AppDelegate appDelegate] makeTabBarAsRootViewController];
    
    ENDTHREAD
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) failureInAPIResponce:(NSDictionary *) responseObject
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    STARTMAINTHREAD
    
    [[UtilityClass sharedManger] alertErrorWithMessage:[NSString stringWithFormat:@"%@",[responseObject objectForKey:kAPIkeyMessage]]];
    
    // Pop to Root View Controller
    [[AppDelegate appDelegate].topView popToRootViewControllerAnimated:true];
    
    ENDTHREAD
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) FailureToGetResponceFromServer:(NSError *)error
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    STARTMAINTHREAD
    
    [MBProgressHUD hideHUDForView:[AppDelegate appDelegate].topView.view animated:YES];
    
    [[UtilityClass sharedManger] alertErrorWithMessage:@"Something went wrong.Please try again later"];
    
    ENDTHREAD
}



@end
