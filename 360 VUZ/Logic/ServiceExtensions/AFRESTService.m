//
//  AFRESTService.m
//  BrownAndWood
//
//  Created by Khawaja Fahad on 08/01/2017.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "AFRESTService.h"

@interface AFRESTService()
{
@private
    /**
     *  The service path used along with the base url in methods such as `GET:parameters:success:failure`
     */
    __strong NSString *_path;
    
    /**
     *  The AFAppDotNetAPIClient (REST Client) which will used to invoke all methods such as `GET:parameters:success:failure` or `POST:parameters:success:failure`
     */
    AFAppDotNetAPIClient *restClient;
    
    /**
     *  Server Response
     */
    id _responseObject;
}
@end

//Sync Request Handling Flag

@implementation AFRESTService

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (id)initWithServicePath:(NSString *)path
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if (self = [super init]) {
        _path = path;
        restClient = [AFAppDotNetAPIClient sharedClient];
        return self;
    }
    return nil;
}


#pragma mark - POST Request Methods

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) executeRequest:(NSDictionary *)parameters  success:(void (^)(id))successBlock failure:(void (^)(NSError *))failureBlock
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [[AFAppDotNetAPIClient sharedClient] POST:_path
                                   parameters:parameters
                                     progress:nil
                                      success:^(NSURLSessionDataTask * __unused task, id JSON){
                                          successBlock(JSON);
                                      }
                                      failure:^(NSURLSessionDataTask *__unused task, NSError *error){
                                          failureBlock(error);
                                      }];
    
}

#pragma mark - POST Request Methods

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) executeRequestGet:(NSDictionary *)parameters  success:(void (^)(id))successBlock failure:(void (^)(NSError *))failureBlock
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
    [[AFAppDotNetAPIClient sharedClient] GET:_path
                                   parameters:parameters
                                    progress:nil
                                      success:^(NSURLSessionDataTask * __unused task, id JSON){
                                          successBlock(JSON);
                                      }
                                      failure:^(NSURLSessionDataTask *__unused task, NSError *error){
                                          failureBlock(error);
                                      }];
}

@end
