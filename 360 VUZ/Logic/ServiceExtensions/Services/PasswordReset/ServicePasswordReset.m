//
//  ServicePasswordReset.m
//  360 VUZ
//
//  Created by Mosib on 7/16/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "ServicePasswordReset.h"

@implementation ServicePasswordReset

-(id) init
{
    self = [super init];
    if (self) {
        self =  [super initWithServicePath:[NSString stringWithFormat:@"%@",[ServerConstants serviceURL:kPostRequestPasswordReset]]];
    }
    
    return self;
}

@end
