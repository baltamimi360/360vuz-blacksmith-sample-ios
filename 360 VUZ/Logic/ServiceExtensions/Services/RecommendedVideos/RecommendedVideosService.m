//
//  RecommendedVideosService.m
//  360 VUZ
//
//  Created by Mosib on 8/27/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "RecommendedVideosService.h"

@implementation RecommendedVideosService

-(id) init
{
    self = [super init];
    if (self) {
        self =  [super initWithServicePath:[NSString stringWithFormat:@"%@",[ServerConstants serviceURL:kPostRequestRecommandedVideos]]];
    }
    
    return self;
}

@end
