//
//  EditProfileService.m
//  360 VUZ
//
//  Created by Mosib on 8/23/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "EditProfileService.h"

@implementation EditProfileService

-(id) init
{
    self = [super init];
    if (self) {
        self =  [super initWithServicePath:[NSString stringWithFormat:@"%@?access_token=%@",[ServerConstants serviceURL:kPostRequestEditProfile],[[Settings sharedManger] getServerToken]]];
    }
    
    return self;
}

@end
