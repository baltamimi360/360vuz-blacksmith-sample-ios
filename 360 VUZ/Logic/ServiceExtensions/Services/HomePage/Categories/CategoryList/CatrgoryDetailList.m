//
//  CatrgoryDetailList.m
//  360 VUZ
//
//  Created by Mosib on 8/27/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "CatrgoryDetailList.h"

@implementation CatrgoryDetailList

-(id) init
{
    self = [super init];
    if (self) {
        self =  [super initWithServicePath:[NSString stringWithFormat:@"%@",[ServerConstants serviceURL:kPostRequestCategoryDetailList]]];
    }
    
    return self;
}


@end
