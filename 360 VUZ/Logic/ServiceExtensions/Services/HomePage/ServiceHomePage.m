//
//  ServiceHomePage.m
//  360 VUZ
//
//  Created by Mosib on 8/6/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "ServiceHomePage.h"

@implementation ServiceHomePage

-(id) init
{
    self = [super init];
    if (self) {
        self =  [super initWithServicePath:[NSString stringWithFormat:@"%@",[ServerConstants serviceURLLive:kPostRequestHomePage]]];
    }
    
    return self;
}

@end
