//
//  ServiceGlobalTredning.m
//  360 VUZ
//
//  Created by Mosib on 8/8/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "ServiceGlobalTredning.h"

@implementation ServiceGlobalTredning

-(id) init
{
    self = [super init];
    if (self) {
        self =  [super initWithServicePath:[NSString stringWithFormat:@"%@",[ServerConstants serviceURL:kPostRequestHPGlobalTredning]]];
    }
    
    return self;
}

@end
