//
//  ServiceVirtualTrending.m
//  360 VUZ
//
//  Created by Mosib on 8/7/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "ServiceVirtualTrending.h"

@implementation ServiceVirtualTrending

-(id) init
{
    self = [super init];
    if (self) {
        self =  [super initWithServicePath:[NSString stringWithFormat:@"%@",[ServerConstants serviceURL:kPostRequestHPVirtualTrending]]];
    }
    
    return self;
}

@end
