//
//  ServiceLive360.m
//  360 VUZ
//
//  Created by Mosib on 8/7/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "ServiceLive360.h"

@implementation ServiceLive360

-(id) init
{
    self = [super init];
    if (self) {
        self =  [super initWithServicePath:[NSString stringWithFormat:@"%@",[ServerConstants serviceURL:kPostRequestHPLive]]];
    }
    
    return self;
}

@end
