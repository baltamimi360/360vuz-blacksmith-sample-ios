//
//  PastLiveEventsService.m
//  360 VUZ
//
//  Created by Mosib on 8/24/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "PastLiveEventsService.h"

@implementation PastLiveEventsService

-(id) init
{
    self = [super init];
    if (self) {
        self =  [super initWithServicePath:[NSString stringWithFormat:@"%@",[ServerConstants serviceURL:kPostRequestPastLiveEvents]]];
    }
    
    return self;
}

@end
