
//
//  ServiceLogin.m
//  360 VUZ
//
//  Created by Mosib on 7/16/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "ServiceLogin.h"

@implementation ServiceLogin

-(id) init
{
    self = [super init];
    if (self) {
        self =  [super initWithServicePath:[NSString stringWithFormat:@"%@",[ServerConstants serviceURL:kPostRequestLogin]]];
    }
    
    return self;
}

@end
