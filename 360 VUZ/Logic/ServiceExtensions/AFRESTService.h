//
//  AFRESTService.h
//  BrownAndWood
//
//  Created by Khawaja Fahad on 08/01/2017.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFAppDotNetAPIClient.h"

@class AFRESTService;
@protocol AFRESTServiceDelegate <NSObject>
- (void) operation:(NSURLSessionDataTask *__unused)task didReceiveError:(id)error;
- (void) operation:(NSURLSessionDataTask *__unused)task didReceiveResponse:(id)jsonObject;
@end

@interface AFRESTService : NSObject
@property (weak, nonatomic) id <AFRESTServiceDelegate> delegate;
- (id)initWithServicePath:(NSString *)path;

#pragma mark - POST Request Methods
- (void) executeRequest:(NSDictionary *)parameters success:(void (^)(id))successBlock failure:(void (^)(NSError *))failureBlock;


#pragma mark - GET Request Methods
- (void) executeRequestGet:(NSDictionary *)parameters  success:(void (^)(id))successBlock failure:(void (^)(NSError *))failureBlock;

@end
