//
//  UtilityClass.h
//  360 VUZ
//
//  Created by Khawaja Fahad on 08/01/2017.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UtilityClass : NSObject
+ (UtilityClass *)sharedManger;
- (UtilityClass *)init;


+ (CGRect) getDeviceBounds;
+ (NSDictionary *) convertToDictionory:(NSData *) object;
+ (BOOL)stringIsNilOrEmpty:(NSString*) string;

- (NSString *) formatTimeFromSeconds:(int)numberOfSeconds;
- (CGFloat) getLabelHeight:(UILabel*)label;
- (CGRect) setFrame:(UILabel*) label;

- (NSDate *) dateFromString:(NSString *) dateString withDateFormat:(NSString *) format;
- (NSString *) stringFromDate:(NSDate *) date withDateFormat:(NSString *) format;

+ (UIImage *)imageFromColor:(UIColor *)color;

+ (CGFloat) getTabBarHeight;
+ (void) setTabBarHeight:(CGFloat) height;

- (NSString *) getErrorResponce:(NSError *) error;

+ (UIViewController*) topMostController;


/**
 *  Email
 */

- (BOOL)validateEmailWithString:(NSString *) email;

/**
 *  Alert
 */

- (void) alertErrorWithMessage:(NSString *) message;
- (void) alertMessage:(NSString *) title withMessage:(NSString *) message;

/**
 *  URL and Paths
 */

- (NSURL *) URLFromFileNameInApp:(NSString *) filename;
- (NSURL *) URLFromFileNameInApp:(NSString *) filename withExtension:(NSString *) ext;
- (NSString *) getFilePathFromFileNameWithApp:(NSString *) fileName withExtension:(NSString *) ext;
- (NSURL *) orionLicenseURL;

/**
 *  Directories
 */

- (NSString *) getDocumentDirectoryPath;
- (NSString *) getDocumentLibraryPath;
- (NSString *) getDocumentCachePath;

- (BOOL) checkIfFileExistsAtDocumentsDirectory:(NSString *) fileName;
- (NSURL *) URLforFileInDocunemtDirectory:(NSString *) fileName;

/**
 *  Download
 */
- (void) downloadFileFromURLString:(NSString *) URLString completedSuccessfully:(void (^)(BOOL success))completionBlock;

/**
 *  Array
 */
- (NSArray *) splitName:(NSString *) name;
- (NSString *) getRandGender;

/**
 *  UIApplication
 */
- (void) UIApplicationOpenURLwithString:(NSString *) url;

/**
 *  Server Video URLs
 */
-(NSURL*) getCDNURL:(NSURL*) videoURL;

/**
 *  Base 64 Encoding
 */
- (NSString *)encodeToBase64String:(UIImage *)image;
- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
@end
