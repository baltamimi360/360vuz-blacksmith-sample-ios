//
//  NSArray+Random.m
//  360 VUZ
//
//  Created by Mosib on 7/25/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "NSArray+Random.h"

@implementation NSArray (Random)


-(id)randomObject {
    NSUInteger myCount = [self count];
    if (myCount)
      return [self objectAtIndex:arc4random() % [self count]];
    
    else
        return nil;
}

@end
