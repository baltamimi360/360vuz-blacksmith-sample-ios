//
//  NSArray+Random.h
//  360 VUZ
//
//  Created by Mosib on 7/25/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (Random)

- (id)randomObject;

@end
