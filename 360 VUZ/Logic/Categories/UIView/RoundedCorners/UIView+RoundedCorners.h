//
//  UIView+RoundedCorners.h
//  360 VUZ
//
//  Created by Mosib on 7/10/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (RoundedCorners)

- (void) roundedCornersAllWithDefaultRaduis;

- (void) roundedRectByRoundingCorners:(UIRectCorner)corners cornerRadii:(CGSize)cornerRadii;

- (void) roundedCornersAllWithCornerRadii:(CGSize)cornerRadii;

@end
