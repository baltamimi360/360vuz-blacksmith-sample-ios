//
//  UIView+RoundedCorners.m
//  360 VUZ
//
//  Created by Mosib on 7/10/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "UIView+RoundedCorners.h"

@implementation UIView (RoundedCorners)

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) roundedCornersAllWithDefaultRaduis
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
        [self roundedRectByRoundingCorners:UIRectCornerTopLeft
         | UIRectCornerTopRight | UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:(CGSize){5.0, 5.0}];
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) roundedCornersAllWithCornerRadii:(CGSize)cornerRadii
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [self roundedRectByRoundingCorners:UIRectCornerTopLeft
     | UIRectCornerTopRight | UIRectCornerBottomLeft | UIRectCornerBottomRight cornerRadii:cornerRadii];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) roundedRectByRoundingCorners:(UIRectCorner)corners cornerRadii:(CGSize)cornerRadii
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect: self.bounds byRoundingCorners: corners cornerRadii: cornerRadii].CGPath;
    
    self.layer.mask = maskLayer;
}



@end
