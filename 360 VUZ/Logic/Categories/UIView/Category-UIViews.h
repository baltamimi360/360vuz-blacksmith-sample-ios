//
//  Category-UIViews.h
//  360 VUZ
//
//  Created by Mosib on 7/10/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#ifndef Category_UIViews_h
#define Category_UIViews_h

#import "UIView+RoundedCorners.h"
#import "UIView+ParentViewController.h"

#endif /* Category_UIViews_h */
