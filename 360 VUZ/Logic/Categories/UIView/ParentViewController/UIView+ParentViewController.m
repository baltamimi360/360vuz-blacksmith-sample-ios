//
//  UIView+ParentViewController.m
//  360 VUZ
//
//  Created by Mosib on 8/13/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "UIView+ParentViewController.h"

@implementation UIView (ParentViewController)

- (UIViewController *)viewController {
    if ([self.nextResponder isKindOfClass:UIViewController.class])
        return (UIViewController *)self.nextResponder;
    else
        return nil;
}

@end
