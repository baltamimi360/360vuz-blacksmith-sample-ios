//
//  UIView+ParentViewController.h
//  360 VUZ
//
//  Created by Mosib on 8/13/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (ParentViewController)

- (UIViewController *)viewController;

@end
