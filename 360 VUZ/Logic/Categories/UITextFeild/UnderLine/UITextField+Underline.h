//
//  UITextField+Underline.h
//  360 VUZ
//
//  Created by Mosib on 7/4/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (Underline)

- (void) setBottomBorder:(UIColor *) shadowColor;

@end
