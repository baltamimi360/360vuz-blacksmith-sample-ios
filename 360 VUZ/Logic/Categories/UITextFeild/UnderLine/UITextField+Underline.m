//
//  UITextField+Underline.m
//  360 VUZ
//
//  Created by Mosib on 7/4/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "UITextField+Underline.h"

@implementation UITextField (Underline)

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setBottomBorder:(UIColor *) shadowColor
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self.borderStyle = UITextBorderStyleNone;
    self.layer.backgroundColor = [UIColor whiteColor].CGColor;
    
    self.layer.masksToBounds = false;
    self.layer.shadowColor = shadowColor.CGColor;
    self.layer.shadowOffset = CGSizeMake(0, 1);
    self.layer.shadowOpacity = 1.0;
    self.layer.shadowRadius = 0.0;
}

@end
