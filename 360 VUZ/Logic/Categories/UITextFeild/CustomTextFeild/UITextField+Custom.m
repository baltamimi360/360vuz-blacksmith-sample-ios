//
//  UITextField+Custom.m
//  360 VUZ
//
//  Created by Mosib on 7/12/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "UITextField+Custom.h"

@implementation UITextField (Custom)

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) applyCustomSettingWithUnderline
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self.borderStyle = UITextBorderStyleNone;
    self.font = [UIFont fontWithName:kFontSFUIDisplayRegular size:16];
    self.autocorrectionType = UITextAutocorrectionTypeNo;
    self.keyboardType = UIKeyboardTypeDefault;
    self.returnKeyType = UIReturnKeyDone;
    self.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    
    [self setBottomBorder:UIColorFromRGB(0xD3D3D3)];
}

@end
