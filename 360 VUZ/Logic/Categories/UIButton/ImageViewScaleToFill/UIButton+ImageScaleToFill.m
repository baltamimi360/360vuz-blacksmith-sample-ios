//
//  UIButton+ImageScaleToFill.m
//  360 VUZ
//
//  Created by Mosib on 7/4/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "UIButton+ImageScaleToFill.h"

@implementation UIButton (ImageScaleToFill)

- (void) scaleToFill
{
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.contentHorizontalAlignment = UIControlContentHorizontalAlignmentFill;
    self.contentVerticalAlignment = UIControlContentVerticalAlignmentFill;
}

@end
