//
//  UILabel+Boldify.h
//  360 VUZ
//
//  Created by Mosib on 7/2/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Boldify)

- (void) boldRange: (NSRange) range;
- (void) boldSubstring: (NSString*) substring;

- (void) fontRange: (NSRange) range withFont:(NSString *) font;
- (void) changeSubstringFont: (NSString*) substring withFont:(NSString *) font;
- (void) fontRange: (NSRange) range withFont:(NSString *) font withSize:(CGFloat) size;

- (void) colorRange: (NSRange) range withColor:(UIColor *) color;
- (void) changeSubstringFont: (NSString*) substring withFont:(NSString *) font withSize:(CGFloat) size;
- (void) colorSubString: (NSString *) substring withcolor:(UIColor *) color;
@end
