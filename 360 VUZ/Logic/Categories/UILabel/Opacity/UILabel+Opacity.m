//
//  UILabel+Opacity.m
//  360 VUZ
//
//  Created by Mosib on 8/10/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "UILabel+Opacity.h"

@implementation UILabel (Opacity)

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) makeBackgroundOpaque
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.1f];
}

@end
