//
//  UILabel+Opacity.h
//  360 VUZ
//
//  Created by Mosib on 8/10/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Opacity)

- (void) makeBackgroundOpaque;

@end
