//
//  Category-UILabels.h
//  360 VUZ
//
//  Created by Mosib on 7/3/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#ifndef Category_UILabels_h
#define Category_UILabels_h

#import "UILabel+Boldify.h"
#import "UILabel+Opacity.h"

#endif /* Category_UILabels_h */
