//
//  UIImageView+Circle.m
//  360 VUZ
//
//  Created by Mosib on 8/2/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "UIImageView+Circle.h"
#import <QuartzCore/QuartzCore.h>
#import <AFNetworking/UIImageView+AFNetworking.h>
#import <AFNetworking/AFImageDownloader.h>

@implementation UIImageView (Circle)

- (void) makeCircle
{
    self.layer.cornerRadius = self.frame.size.width / 2;
    self.layer.masksToBounds = YES;
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)clearImageCacheForURL:(NSURL *)url
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request addValue:@"image/*" forHTTPHeaderField:@"Accept"];
    id <AFImageRequestCache> imageCache = [[self class] sharedImageDownloader].imageCache;
    UIImage *cachedImage = [imageCache imageforRequest:request withAdditionalIdentifier:nil];
    if (cachedImage) {
        [self clearCached:imageCache Request:request];
    }
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)clearCached:(AFAutoPurgingImageCache *)imageCache Request:(NSURLRequest *)request
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if (request) {
        [imageCache removeImageforRequest:request withAdditionalIdentifier:nil];
    }
}

@end
