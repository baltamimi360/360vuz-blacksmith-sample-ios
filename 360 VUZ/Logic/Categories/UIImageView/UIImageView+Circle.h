//
//  UIImageView+Circle.h
//  360 VUZ
//
//  Created by Mosib on 8/2/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFAutoPurgingImageCache.h>

@interface UIImageView (Circle)

- (void) makeCircle;

- (void)clearImageCacheForURL:(NSURL *)url;
- (void)clearCached:(AFAutoPurgingImageCache *)imageCache Request:(NSURLRequest *)request;

@end

