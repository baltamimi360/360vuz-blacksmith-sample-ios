//
//  UIViewController+TitleView.h
//  360 VUZ
//
//  Created by Mosib on 7/6/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (TitleView)

- (void) addTitleLogo;

@end
