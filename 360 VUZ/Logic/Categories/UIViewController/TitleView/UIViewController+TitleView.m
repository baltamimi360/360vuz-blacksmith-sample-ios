//
//  UIViewController+TitleView.m
//  360 VUZ
//
//  Created by Mosib on 7/6/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "UIViewController+TitleView.h"

@implementation UIViewController (TitleView)

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) addTitleLogo
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [self.navigationController.navigationBar setTranslucent:NO];
    
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"360VUZ_logo_Grey"]];
}


@end
