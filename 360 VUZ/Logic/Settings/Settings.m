//
//  Settings.m
//  360 VUZ
//
//  Created by Mosib on 7/4/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "Settings.h"

@implementation Settings

static Settings *sharedInsatce;

NSString *const kUserNameKey            = @"userNameKey";
NSString *const keyUDUserID             = @"UserDefaultUserID";
NSString *const keyUDServerToken        = @"UserDefaultServerToken";
NSString *const keyUDAPNSToken          = @"UserDefaultAPNSToken";
NSString *const keyCalenderEventIDs     = @"UserDefaultCalenderEventIDs";
NSString *const keyUserDetails          = @"UserDetails";
NSString *const keyUserLoggedIn         = @"UserLoggedIn";

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
+(Settings *)sharedManger
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInsatce=[[Settings alloc] init];
    });
    return sharedInsatce;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(Settings *)init
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if(self=[super init])
    {
        [self defaultValuesForStrings];
    }
    return self;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUserName:(NSString *) name
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [[NSUserDefaults standardUserDefaults] setObject:name forKey:kUserNameKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSString *) getUsername
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return [[NSUserDefaults standardUserDefaults] valueForKey:kUserNameKey];
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUserID:(NSString *) userID
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [[NSUserDefaults standardUserDefaults] setObject:userID forKey:keyUDUserID];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSString *) getUserID
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return [[NSUserDefaults standardUserDefaults] valueForKey:keyUDUserID];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setServerToken:(NSString *) serverToken
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [[NSUserDefaults standardUserDefaults] setObject:serverToken forKey:keyUDServerToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSString *) getServerToken
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return [[NSUserDefaults standardUserDefaults] valueForKey:keyUDServerToken];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setAPNSToken:(NSString *) serverToken
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [[NSUserDefaults standardUserDefaults] setObject:serverToken forKey:keyUDAPNSToken];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSString *) getAPNSToken
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if ([[NSUserDefaults standardUserDefaults] valueForKey:keyUDAPNSToken])
    {
    return [[NSUserDefaults standardUserDefaults] valueForKey:keyUDAPNSToken];
    }
    else return [NSString string];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) addEventID:(NSString *) eventID
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    NSMutableArray *copyOfEvents = [[self getEventIDs] mutableCopy];
    [copyOfEvents addObject:eventID];
    
    [[NSUserDefaults standardUserDefaults] setObject:[copyOfEvents copy] forKey:keyCalenderEventIDs];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSArray *) getEventIDs
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if ([[NSUserDefaults standardUserDefaults] objectForKey:keyCalenderEventIDs])
    {
        NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:keyCalenderEventIDs]);
        
        return [[NSUserDefaults standardUserDefaults] objectForKey:keyCalenderEventIDs];
    }
    else return kEmptyArray;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUserDetails:(UserDetailsDataModel *) userDetails
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:userDetails];
    
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:keyUserDetails];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (UserDetailsDataModel *) getUserDetails
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if ([[NSUserDefaults standardUserDefaults] valueForKey:keyUserDetails])
    {
        NSData *data = [NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] valueForKey:keyUserDetails]];
        
        return (UserDetailsDataModel *)data;
    }
    else return [[UserDetailsDataModel alloc] init];
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUserLoggedIn:(BOOL) isLoggedIn
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [[NSUserDefaults standardUserDefaults] setBool:isLoggedIn forKey:keyUserLoggedIn];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (BOOL) getIsUserLoggedIn
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:keyUserLoggedIn];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) defaultValuesForStrings
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
    if (![[NSUserDefaults standardUserDefaults] objectForKey:keyUDAPNSToken ])
    {
    [[NSUserDefaults standardUserDefaults] setObject:kEmptyString forKey:keyUDAPNSToken];
    }
    if (![[NSUserDefaults standardUserDefaults] objectForKey:kUserNameKey ])
    {
         [[NSUserDefaults standardUserDefaults] setObject:kEmptyString forKey:kUserNameKey];
    }
    if (![[NSUserDefaults standardUserDefaults] objectForKey:keyUDUserID ])
    {
       [[NSUserDefaults standardUserDefaults] setObject:kEmptyString forKey:keyUDUserID];
    }
    if (![[NSUserDefaults standardUserDefaults] objectForKey:keyUDServerToken ])
    {
           [[NSUserDefaults standardUserDefaults] setObject:kEmptyString forKey:keyUDServerToken];
    }
    if (![[NSUserDefaults standardUserDefaults] objectForKey:keyCalenderEventIDs ])
    {
        [[NSUserDefaults standardUserDefaults] setObject:kEmptyArray forKey:keyCalenderEventIDs];
    }
    
    if (![[NSUserDefaults standardUserDefaults] objectForKey:keyUserDetails ])
    {
        UserDetailsDataModel *temp = [[UserDetailsDataModel alloc] init];
        
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:temp];
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:keyUserDetails];
    }
    
    [[NSUserDefaults standardUserDefaults] setBool:false forKey:keyUserLoggedIn];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) removeAllDefaults
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[[NSBundle mainBundle] bundleIdentifier]];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
