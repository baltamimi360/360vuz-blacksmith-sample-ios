//
//  Settings.h
//  360 VUZ
//
//  Created by Mosib on 7/4/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserDetailsDataModel.h"

@interface Settings : NSObject

+ (Settings *)sharedManger;
- (Settings *)init;

/**
 *  Setter/Getter for UserName
 */
- (void) setUserName:(NSString *) name;
- (NSString *) getUsername;

/**
 *  Setter/Getter for UserID
 */
- (void) setUserID:(NSString *) userID;
- (NSString *) getUserID;

/**
 *  Setter/Getter for Server Token
 */
- (void) setServerToken:(NSString *) serverToken;
- (NSString *) getServerToken;

/**
 *  Setter/Getter for APNS Token
 */
- (void) setAPNSToken:(NSString *) serverToken;
- (NSString *) getAPNSToken;

/**
 *  Setter/Getter for Event IDs
 */
- (void) addEventID:(NSString *) eventID;
- (NSArray *) getEventIDs;

/**
 *  Setter/Getter for User Details
 */
- (void) setUserDetails:(UserDetailsDataModel *) userDetails;
- (UserDetailsDataModel *) getUserDetails;

/**
 *  Setter/Getter for User Logged In
 */
- (void) setUserLoggedIn:(BOOL) isLoggedIn;
- (BOOL) getIsUserLoggedIn;;

/**
 *  Setter/Getter for User Logged In
 */
- (void) removeAllDefaults;

@end
