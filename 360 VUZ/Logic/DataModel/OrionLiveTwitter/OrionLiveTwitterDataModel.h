//
//  OrionLiveTwitterDataModel.h
//  360 VUZ
//
//  Created by Mosib on 8/21/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrionLiveTwitterDataModel : NSObject

@property (nonatomic,strong) NSString *tweetText;
@property (nonatomic,strong) NSString *profilePictureURL;
@property (nonatomic,strong) NSString *username;
@property (nonatomic,strong) NSString *twitterHandle;
@property (nonatomic,strong) NSString *tweetTime;

@end
