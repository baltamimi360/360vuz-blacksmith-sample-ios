//
//  OrionLiveTwitterDataModel.m
//  360 VUZ
//
//  Created by Mosib on 8/21/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "OrionLiveTwitterDataModel.h"

@implementation OrionLiveTwitterDataModel

-(id)init
{
    self = [super init];
    if (self) {
        _tweetText = @"";
        _profilePictureURL= @"";
        _username= @"";
        _twitterHandle= @"";
        _tweetTime= @"";
    }
    return self;
}


@end
