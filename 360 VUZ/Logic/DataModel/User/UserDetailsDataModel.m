//
//  UserDetailsDataModel.m
//  360 VUZ
//
//  Created by Mosib on 8/23/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "UserDetailsDataModel.h"

@implementation UserDetailsDataModel

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(id)init
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self = [super init];
    if (self) {
        _bio = @"";
        _credits = @"";
        _deviceID = @"";
        _emailID = @"";
        _emailVerified = false;
        _facebookID = @"";
        _gender = @"";
        _googlePlusID = @"";
        _website = @"";
        _emailID = @"";
        _userID = @"";
        _imageUrlPrimary = @"";
        _imageUrlSecondary = @"";
        _inviteCount = @"";
        _lastName = @"";
        _model = @"";
        _phoneNumber = @"";
        _registrationType = @"";
        _twitterId = @"";
        _username = @"";
        _firstName = @"";
        _likeCount = @"";
        _viewCount = @"";
        _videosCount = @"";
        _payPalAmount = @"";
        
    }
    return self;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (id)initWithCoder:(NSCoder *)aDecoder
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if(self = [super init]){
        self.bio = [aDecoder decodeObjectForKey:@"bio"];
        self.credits = [aDecoder decodeObjectForKey:@"credits"];
        self.deviceID = [aDecoder decodeObjectForKey:@"deviceID"];
        self.emailVerified = [aDecoder decodeBoolForKey:@"emailVerified"];
        self.emailID = [aDecoder decodeObjectForKey:@"emailID"];
        self.facebookID = [aDecoder decodeObjectForKey:@"facebookID"];
        self.gender = [aDecoder decodeObjectForKey:@"gender"];
        self.googlePlusID = [aDecoder decodeObjectForKey:@"googlePlusID"];
        self.website = [aDecoder decodeObjectForKey:@"website"];
        self.emailID = [aDecoder decodeObjectForKey:@"emailID"];
        self.userID = [aDecoder decodeObjectForKey:@"userID"];
        self.imageUrlPrimary = [aDecoder decodeObjectForKey:@"imageUrlPrimary"];
        self.imageUrlSecondary = [aDecoder decodeObjectForKey:@"imageUrlSecondary"];
        self.inviteCount = [aDecoder decodeObjectForKey:@"inviteCount"];
        self.lastName = [aDecoder decodeObjectForKey:@"lastName"];
        self.model = [aDecoder decodeObjectForKey:@"model"];
        self.phoneNumber = [aDecoder decodeObjectForKey:@"phoneNumber"];
        self.registrationType = [aDecoder decodeObjectForKey:@"registrationType"];
        self.twitterId = [aDecoder decodeObjectForKey:@"twitterId"];
        self.username = [aDecoder decodeObjectForKey:@"username"];
        self.firstName = [aDecoder decodeObjectForKey:@"firstName"];
        self.likeCount = [aDecoder decodeObjectForKey:@"likeCount"];
        self.viewCount = [aDecoder decodeObjectForKey:@"viewCount"];
        self.videosCount = [aDecoder decodeObjectForKey:@"videosCount"];
        self.payPalAmount = [aDecoder decodeObjectForKey:@"payPalAmount"];
    }
    return self;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)encodeWithCoder:(NSCoder *)aCoder
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [aCoder encodeObject:self.firstName forKey:@"firstName"];
    
     [aCoder encodeObject:self.bio forKey:@"bio"];
    [aCoder encodeObject:self.credits forKey:@"credits"];
     [aCoder encodeObject:self.deviceID forKey:@"deviceID"];
     [aCoder encodeBool:self.emailVerified forKey:@"emailVerified"];
     [aCoder encodeObject:self.emailID forKey:@"emailID"];
    [aCoder encodeObject:self.facebookID forKey:@"facebookID"];
    [aCoder encodeObject:self.gender forKey:@"gender"];
     [aCoder encodeObject:self.googlePlusID forKey:@"googlePlusID"];
    [aCoder encodeObject:self.website forKey:@"website"];
     [aCoder encodeObject:self.emailID forKey:@"emailID"];
     [aCoder encodeObject:self.userID forKey:@"userID"];
    [aCoder encodeObject:self.imageUrlPrimary forKey:@"imageUrlPrimary"];
     [aCoder encodeObject:self.imageUrlSecondary forKey:@"imageUrlSecondary"];
    [aCoder encodeObject:self.inviteCount forKey:@"inviteCount"];
    [aCoder encodeObject:self.lastName forKey:@"lastName"];
     [aCoder encodeObject:self.model forKey:@"model"];
    [aCoder encodeObject:self.phoneNumber forKey:@"phoneNumber"];
     [aCoder encodeObject:self.registrationType forKey:@"registrationType"];
     [aCoder encodeObject:self.twitterId forKey:@"twitterId"];
     [aCoder encodeObject:self.username forKey:@"username"];
     [aCoder encodeObject:self.firstName forKey:@"firstName"];
    [aCoder encodeObject:self.likeCount forKey:@"likeCount"];
    [aCoder encodeObject:self.viewCount forKey:@"viewCount"];
    [aCoder encodeObject:self.videosCount forKey:@"videosCount"];
    [aCoder encodeObject:self.payPalAmount forKey:@"payPalAmount"];
}

@end
