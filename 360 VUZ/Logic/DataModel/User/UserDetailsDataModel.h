//
//  UserDetailsDataModel.h
//  360 VUZ
//
//  Created by Mosib on 8/23/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserDetailsDataModel : NSObject

@property (nonatomic,strong) NSString *bio;
@property (nonatomic,strong) NSString *credits;
@property (nonatomic,strong) NSString *deviceID;
@property (nonatomic,strong) NSString *emailID;
@property (nonatomic) BOOL emailVerified;
@property (nonatomic,strong) NSString *facebookID;
@property (nonatomic,strong) NSString *firstName;
@property (nonatomic,strong) NSString *gender;
@property (nonatomic,strong) NSString *googlePlusID;
@property (nonatomic,strong) NSString *website;
@property (nonatomic,strong) NSString *userID;
@property (nonatomic,strong) NSString *imageUrlPrimary;
@property (nonatomic,strong) NSString *imageUrlSecondary;
@property (nonatomic,strong) NSString *inviteCount;
@property (nonatomic,strong) NSString *lastName;
@property (nonatomic,strong) NSString *model;
@property (nonatomic,strong) NSString *phoneNumber;
@property (nonatomic,strong) NSString *registrationType;
@property (nonatomic,strong) NSString *twitterId;
@property (nonatomic,strong) NSString *username;
@property (nonatomic,strong) NSString *likeCount;
@property (nonatomic,strong) NSString *viewCount;
@property (nonatomic,strong) NSString *videosCount;
@property (nonatomic,strong) NSString *payPalAmount;

@end
