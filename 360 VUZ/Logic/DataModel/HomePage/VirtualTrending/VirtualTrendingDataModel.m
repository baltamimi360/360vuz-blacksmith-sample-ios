//
//  VirtualTrendingDataModel.m
//  360 VUZ
//
//  Created by Mosib on 8/6/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "VirtualTrendingDataModel.h"

@implementation VirtualTrendingDataModel

-(id)init
{
    self = [super init];
    if (self) {
        _videoID = @"";
        _videoCity= @"";
        _categoryID= @"";
        _subCategoryID= @"";
        _videoTitle= @"";
        _videoDescription = @"";
        _websiteLink = @"";
        _videoThumbnail = @"";
        _videoLink = @"";
        _webVideoLink = @"";
        _status = @"";
        _createdDate = @"";
        _metaData = @"";
        _whatsHot = @"";
        _popular = @"";
        _videotype = @"";
        _webVideoType = @"";
        _userID = @"";
        _videoFeature = @"";
        _panoramaImage = @"";
        _notificationFlag = @"";
        _isImage = @"";
        _rating = @"";
        _categoryName = @"";
        _viewCount = @"";
        _likeCount = @"";
        
        
    }
    return self;
}

@end
