//
//  VirtualTrendingDataModel.h
//  360 VUZ
//
//  Created by Mosib on 8/6/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VirtualTrendingDataModel : NSObject

@property (nonatomic,strong) NSString *videoID;
@property (nonatomic,strong) NSString *videoCity;
@property (nonatomic,strong) NSString *categoryID;
@property (nonatomic,strong) NSString *subCategoryID;
@property (nonatomic,strong) NSString *videoTitle;
@property (nonatomic,strong) NSString *videoDescription;
@property (nonatomic,strong) NSString *websiteLink;
@property (nonatomic,strong) NSString *videoThumbnail;
@property (nonatomic,strong) NSString *videoLink;
@property (nonatomic,strong) NSString *webVideoLink;
@property (nonatomic,strong) NSString *status;
@property (nonatomic,strong) NSString *createdDate;
@property (nonatomic,strong) NSString *metaData;
@property (nonatomic,strong) NSString *whatsHot;
@property (nonatomic,strong) NSString *popular;
@property (nonatomic,strong) NSString *videotype;
@property (nonatomic,strong) NSString *webVideoType;
@property (nonatomic,strong) NSString *userID;
@property (nonatomic,strong) NSString *videoFeature;
@property (nonatomic,strong) NSString *panoramaImage;
@property (nonatomic,strong) NSString *notificationFlag;
@property (nonatomic,strong) NSString *isImage;
@property (nonatomic,strong) NSString *rating;
@property (nonatomic,strong) NSString *categoryName;
@property (nonatomic,strong) NSString *viewCount;
@property (nonatomic,strong) NSString *likeCount;

@end
