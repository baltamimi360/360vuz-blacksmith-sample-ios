//
//  SubscribeDataModel.m
//  360 VUZ
//
//  Created by Mosib on 8/6/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "SubscribeDataModel.h"

@implementation SubscribeDataModel

-(id)init
{
    self = [super init];
    if (self) {
        
        _videoID = @"";
        _image = @"";
        _imageShortName = @"";
        _title = @"";
        _liveType = @"";
        _datetime = @"";
        _subscribed = false;
        _androidProductID =  @"";
        _iosProductID =  @"";
        _price =  @"";
        _status =  @"";
        
    }
    return self;
}

@end
