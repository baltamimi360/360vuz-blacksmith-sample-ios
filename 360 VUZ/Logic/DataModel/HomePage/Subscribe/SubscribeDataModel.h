//
//  SubscribeDataModel.h
//  360 VUZ
//
//  Created by Mosib on 8/6/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SubscribeDataModel : NSObject

@property (nonatomic,strong) NSString *videoID;
@property (nonatomic,strong) NSString *image;
@property (nonatomic,strong) NSString *imageShortName;
@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) NSString *liveType;
@property (nonatomic,strong) NSString *datetime;
@property (nonatomic) BOOL subscribed;
@property (nonatomic,strong) NSString *androidProductID;
@property (nonatomic,strong) NSString *iosProductID;
@property (nonatomic,strong) NSString *price;
@property (nonatomic,strong) NSString *status;
@end
