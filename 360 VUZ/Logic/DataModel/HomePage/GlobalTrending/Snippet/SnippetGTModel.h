//
//  SnippetGTModel.h
//  360 VUZ
//
//  Created by Mosib on 8/8/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ResourceIDGTSDataModel.h"
#import "ThumbnailsGTSDataModel.h"

@interface SnippetGTModel : NSObject

@property (nonatomic,strong) NSString *publishedAt;
@property (nonatomic,strong) NSString *channelId;
@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) NSString *videoDescription;
@property (nonatomic,strong) NSString *channelTitle;
@property (nonatomic,strong) NSString *playlistId;
@property (nonatomic,strong) NSNumber *position;
@property (nonatomic,strong) ThumbnailsGTSDataModel *thumbnails;
@property (nonatomic,strong) ResourceIDGTSDataModel *resourceId;

@end
