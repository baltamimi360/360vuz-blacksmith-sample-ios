//
//  ThumbnailsGTSDataModel.h
//  360 VUZ
//
//  Created by Mosib on 8/8/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ThumbnailGTSDatModel.h"

@interface ThumbnailsGTSDataModel : NSObject

@property (nonatomic,strong) ThumbnailGTSDatModel *defaultThumbnail;
@property (nonatomic,strong) ThumbnailGTSDatModel *medium;
@property (nonatomic,strong) ThumbnailGTSDatModel *high;
@property (nonatomic,strong) ThumbnailGTSDatModel *standard;
@property (nonatomic,strong) ThumbnailGTSDatModel *maxres;

@end
