//
//  ThumbnailsGTSDataModel.m
//  360 VUZ
//
//  Created by Mosib on 8/8/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "ThumbnailsGTSDataModel.h"

@implementation ThumbnailsGTSDataModel

-(id)init
{
    self = [super init];
    if (self) {
        _defaultThumbnail = [[ThumbnailGTSDatModel alloc] init];
        _medium = [[ThumbnailGTSDatModel alloc] init];
        _high = [[ThumbnailGTSDatModel alloc] init];
        _standard = [[ThumbnailGTSDatModel alloc] init];
        _maxres = [[ThumbnailGTSDatModel alloc] init];
        
    }
    return self;
}

@end
