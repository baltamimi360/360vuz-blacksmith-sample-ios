//
//  ThumbnailGTSDatModel.m
//  360 VUZ
//
//  Created by Mosib on 8/8/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "ThumbnailGTSDatModel.h"

@implementation ThumbnailGTSDatModel

-(id)init
{
    self = [super init];
    if (self) {
        _url = @"";
        _width = 0;
        _height = 0;
        
    }
    return self;
}

@end
