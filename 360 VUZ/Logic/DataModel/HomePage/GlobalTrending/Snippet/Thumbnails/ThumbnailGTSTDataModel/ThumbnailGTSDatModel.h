//
//  ThumbnailGTSDatModel.h
//  360 VUZ
//
//  Created by Mosib on 8/8/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ThumbnailGTSDatModel : NSObject

@property (nonatomic,strong) NSString *url;
@property (nonatomic,strong) NSNumber *width;
@property (nonatomic,strong) NSNumber *height;

@end
