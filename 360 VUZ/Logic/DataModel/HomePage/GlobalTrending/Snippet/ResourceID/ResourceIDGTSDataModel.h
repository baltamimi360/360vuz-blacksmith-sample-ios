//
//  ResourceIDGTSDataModel.h
//  360 VUZ
//
//  Created by Mosib on 8/8/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ResourceIDGTSDataModel : NSObject

@property (nonatomic,strong) NSString *kind;
@property (nonatomic,strong) NSString *videoID;

@end
