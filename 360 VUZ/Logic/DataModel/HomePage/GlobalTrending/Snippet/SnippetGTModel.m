//
//  SnippetGTModel.m
//  360 VUZ
//
//  Created by Mosib on 8/8/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "SnippetGTModel.h"

@implementation SnippetGTModel

-(id)init
{
    self = [super init];
    if (self) {
        _publishedAt = @"";
        _channelId= @"";
        _title = @"";
        _videoDescription = @"";
        _channelTitle = @"";
        _playlistId = @"";
        _position = 0;
        _thumbnails = [[ThumbnailsGTSDataModel alloc] init];
        _resourceId = [[ResourceIDGTSDataModel alloc] init];
        
    }
    return self;
}

@end
