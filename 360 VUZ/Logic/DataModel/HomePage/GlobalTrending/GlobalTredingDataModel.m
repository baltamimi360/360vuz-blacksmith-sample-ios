//
//  GlobalTredingDataModel.m
//  360 VUZ
//
//  Created by Mosib on 8/6/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "GlobalTredingDataModel.h"

@implementation GlobalTredingDataModel

-(id)init
{
    self = [super init];
    if (self) {
        _kind = @"";
        _etag= @"";
        _youtubeID = @"";
        _snippet = [[SnippetGTModel alloc] init];
        _contentDetails = [[ContentDetailsGTDataModel alloc] init];

    }
    return self;
}

@end
