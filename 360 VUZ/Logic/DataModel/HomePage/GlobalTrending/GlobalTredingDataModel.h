//
//  GlobalTredingDataModel.h
//  360 VUZ
//
//  Created by Mosib on 8/6/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContentDetailsGTDataModel.h"
#import "SnippetGTModel.h"

@interface GlobalTredingDataModel : NSObject

@property (nonatomic,strong) NSString *kind;
@property (nonatomic,strong) NSString *etag;
@property (nonatomic,strong) NSString *youtubeID;
@property (nonatomic,strong) SnippetGTModel *snippet;
@property (nonatomic,strong) ContentDetailsGTDataModel *contentDetails;

@end
