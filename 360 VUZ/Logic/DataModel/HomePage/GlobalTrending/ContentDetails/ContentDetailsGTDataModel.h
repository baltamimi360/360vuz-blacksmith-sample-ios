//
//  ContentDetailsGTDataModel.h
//  360 VUZ
//
//  Created by Mosib on 8/8/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ContentDetailsGTDataModel : NSObject

@property (nonatomic,strong) NSString *videoID;
@property (nonatomic,strong) NSString *videoPublishedAt;

@end
