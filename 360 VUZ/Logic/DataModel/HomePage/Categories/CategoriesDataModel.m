//
//  CategoriesDataModel.m
//  360 VUZ
//
//  Created by Mosib on 8/6/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "CategoriesDataModel.h"

@implementation CategoriesDataModel

-(id)init
{
    self = [super init];
    if (self) {
        _videoID = 0;
        _image= @"";
        _categoryName= @"";
      
    }
    return self;
}

@end
