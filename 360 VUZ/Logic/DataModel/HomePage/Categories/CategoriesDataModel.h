//
//  CategoriesDataModel.h
//  360 VUZ
//
//  Created by Mosib on 8/6/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CategoriesDataModel : NSObject

@property (nonatomic,strong) NSNumber *videoID;
@property (nonatomic,strong) NSString *image;
@property (nonatomic,strong) NSString *categoryName;


@end
