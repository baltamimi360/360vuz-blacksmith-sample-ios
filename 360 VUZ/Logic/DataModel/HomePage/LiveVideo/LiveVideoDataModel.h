//
//  LiveVideoDataModel.h
//  360 VUZ
//
//  Created by Mosib on 8/6/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LiveVideoDataModel : NSObject

@property (nonatomic,strong) NSString *videoID;
@property (nonatomic,strong) NSString *image;
@property (nonatomic,strong) NSString *imageShortName;
@property (nonatomic,strong) NSString *title;
@property (nonatomic,strong) NSString *liveDescription;
@property (nonatomic,strong) NSString *videoLink;
@property (nonatomic,strong) NSString *datetime;
@property (nonatomic,strong) NSString *liveType;
@property (nonatomic,strong) NSString *credit;
@property (nonatomic,strong) NSString *androidProductID;
@property (nonatomic,strong) NSString *iosProductID;
@property (nonatomic,strong) NSString *price;
@property (nonatomic,strong) NSString *twitterTags;
@property (nonatomic,strong) NSString *channelID;
@property (nonatomic,strong) NSString *status;
@property (nonatomic,strong) NSString *notificationFlag;
@property (nonatomic,strong) NSString *dateStatus;
@property (nonatomic,strong) NSString *purchaseStatus;


@end
