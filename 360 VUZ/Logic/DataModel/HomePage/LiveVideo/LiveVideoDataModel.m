//
//  LiveVideoDataModel.m
//  360 VUZ
//
//  Created by Mosib on 8/6/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "LiveVideoDataModel.h"

@implementation LiveVideoDataModel

-(id)init
{
    self = [super init];
    if (self) {
 
        _videoID = @"";
        _image = @"";
        _imageShortName = @"";
        _title = @"";
        _liveDescription = @"";
        _videoLink = @"";
        _datetime = @"";
        _liveType = @"";
        _credit = @"";
        _androidProductID = @"";
        _iosProductID = @"";
        _price = @"";
        _twitterTags = @"";
        _channelID = @"";
        _status = @"";
        _notificationFlag = @"";
        _dateStatus = @"";
        _purchaseStatus = @"";
        
        
    }
    return self;
}

@end
