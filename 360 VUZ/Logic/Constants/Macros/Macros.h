//
//  Macros.h
//  BrownAndWood
//
//  Created by Mosib on 7/5/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#ifndef Macros_h
#define Macros_h

#pragma mark - Threads -

#define STARTBACKGROUNDTHREAD  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
#define STARTMAINTHREAD                             dispatch_async(dispatch_get_main_queue(), ^(){
#define ENDTHREAD                               });

#pragma mark - UIDevice -

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#pragma mark - UIScreen -

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))
#define IS_ZOOMED (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)

#pragma mark - Host Reachable Address -

#define kReachableHost                       @"www.google.com"

#pragma mark - Application URLs -

#define kURLCancelSubscription           @"http://www.360vuz.com/cancelsubscription"
#define kURLLegal                        @"http://www.360vuz.com/legal"
#define kURLYoutube                      @"http://www.youtube.com/watch?v="
#define kCloudURL                        @"https://dj4avls1jaoqn.cloudfront.net"
#define kTwitterFeedURL                        @"https://api.twitter.com/1.1/search/tweets.json?q=%@&result_type=recent"

#pragma mark - Orion 360 -

#define kOrion360LicenseFile                 @"Orion360_SDK_Basic_iOS_com.creativeinnovations.360mea"
#define kExtensionLicense                       @"lic"

#pragma mark - HeightOfViews -

#define heightOfNavigationBar       44.0
#define heightOfTabBar              48.0
#define heightOfCellVideo           225.0
#define RatioOfCellWhatsHot         0.29
#define widthOfCellsHome            280.0


#define kEmptyString                   @""
#define kEmptyArray                    @[]
#define kEmptyDictionary               @{}

#define kTimeFormatISO8601             @"yyyy-MM-dd'T'HH:mm:ss.SSSZ"
#define kTimeFormatYMD                 @"yyyy-mm-dd"
#define kTimeFormatEEE                 @"EEE, MMM d, ”yy"
#define kTimeFormatDayMonth            @"dd MMMM"
#define kTimeFormatHourSecsMeridian    @"hh:mm a"

typedef NS_ENUM(NSInteger, URLType)
{
    URLTypeFacebook,
    URLTypeGoogle,
    URLTypeTwitter
};

typedef NS_ENUM(NSInteger, CellTypeHomeViewController)
{
    WhatsHot,
    Live360,
    Subscribe,
    Trending,
    Categories,
    GlobalTrending,
    VirtualToursTrending
};

enum
{
    SENSORS = 0,
    VR
};
typedef int ControlMode;

#define kURLMethodGET           @"GET"



#endif /* Macros_h */
