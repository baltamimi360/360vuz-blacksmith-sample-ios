//
//  APIDictionaryKeys.h
//  360 VUZ
//
//  Created by Mosib on 7/18/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#ifndef APIDictionaryKeys_h
#define APIDictionaryKeys_h

/**
 *  Others
 */

#define kkeyData                               @"data"
#define kkeNextPageToken                       @"next_page_token"
#define kkeyItems                              @"items"

/**
 *  Login Keys
 */

#define kAPIkeyEmail                           @"email"
#define kAPIkeyPassword                        @"password"
#define kAPIkeyServerToken                     @"id"
#define kAPIkeyUserID                          @"userId"

/**
 *  Sign Up Keys
 */

#define kAPIkeyGender                            @"gender"
#define kAPIkeyFirstName                         @"first_name"
#define kAPIkeyLastName                          @"last_name"
#define kAPIkeyDeviceID                          @"device_id"
#define kAPIkeyPhoneNumber                       @"phone_number"
#define kAPIkeyWebsite                           @"website"
#define kAPIkeyBio                               @"bio"
#define kAPIkeyModel                             @"model"

/**
 *  Responce Keys
 */
#define kAPIkeyStatus                            @"status"
#define kAPIkeyfailure                           @"failure"
#define kAPIkeyMessage                           @"message"
#define kAPIkeySuccess                           @"success"


/**
 *  Twitter Keys
 */
#define kTKeyUserImage                           @"rofile_image_url"
#define kTKeyUserName                            @"screen_name"
#define kTKeyUserEmail                           @"email"
#define kTKeyUserGender                          @"gender"
#define kTKeyUserID                              @"id"


/**
 *  Facebook Keys
 */
#define kFKeyUserImage                           @"rofile_image_url"
#define kFKeyUserName                            @"screen_name"
#define kFKeyUserEmail                           @"email"
#define kFKeyUserGender                          @"gender"
#define kFKeyUserID                              @"id"
#define kFKeyFacebookID                          @"facebook_id"

/**
 *  Google Keys
 */
#define kGoogleID                                  @"google_plus_id"
#define kTwitterID                                 @"twitter_id"


/**
 *  Whats Hot Keys
 */
#define kHPKeyWhatsHot                               @"whathot_video"
#define kHPKeyLiveVideo                              @"live_video"
#define kHPKeyTrendingVirtualVideo                   @"trending_virtual_video"
#define kHPKeyChannelList                            @"channel_list"
#define kHPKeyTrendingVideo                          @"trending_360_video"
#define kHPKeyYoutube                                @"youtube"
#define kHPKeyCategory                               @"category"


/**
 *  Whats Hot Keys
 */
#define kWHKeyID                                     @"id"
#define kWHKeyVideoCity                              @"videoCity"
#define kWHKeyCategoryID                             @"categoryId"
#define kWHKeySubCategoryID                          @"subcategoryId"
#define kWHKeyVideoTitle                             @"videoTitle"
#define kWHKeyVideoDescription                       @"videoDescription"
#define kWHKeyWebsiteLink                            @"websiteLink"
#define kWHKeyVideoThumbnail                         @"videoThumbnail"
#define kWHKeyVideoLink                              @"videoLink"
#define kWHKeyWebVideoLink                           @"webVideoLink"
#define kWHKeyStatus                                 @"status"
#define kWHKeyCreatedDate                            @"createdDate"
#define kWHKeyMetaData                               @"metaData"
#define kWHKeyWhatsHot                               @"whatsHot"
#define kWHKeyPopular                                @"popular"
#define kWHKeyVideoType                              @"videoType"
#define kWHKeyWebVideoType                           @"webVideoType"
#define kWHKeyUserID                                 @"userID"
#define kWHKeyVideoFeature                           @"videoFeature"
#define kWHKeyPanoramImage                           @"panoramaImage"
#define kWHKeyNotificationFlag                       @"notificationFlag"
#define kWHKeyIsImage                                @"isImage"
#define kWHKeyRating                                 @"rating"
#define kWHKeyCategoryName                           @"categoryName"
#define kWHKeyViewCount                              @"video"
#define kWHKeyLikeCount                              @"like"


/**
 *  Live Video Keys
 */
#define kLVKeyID                                     @"id"
#define kLVKeyImage                                  @"image"
#define kLVKeyImageName                              @"imageShortName"
#define kLVKeyTitle                                  @"title"
#define kLVKeyDescription                            @"description"
#define kLVKeyVideoLink                              @"videoLink"
#define kLVKeyDateTime                               @"dateTime"
#define kLVKeyLiveType                               @"videoThumbnail"
#define kLVKeyCredit                                 @"credit"
#define kLVKeyAndriodProductID                       @"androidProductId"
#define kLVKeyiOSProductID                           @"iosProductId"
#define kLVKeyPrice                                  @"price"
#define kLVKeyTwitterTags                            @"twitterTags"
#define kLVKeyChannelID                              @"channelId"
#define kLVKeyStatus                                 @"status"
#define kLVKeyNotificationFlag                       @"notificationFlag"
#define kLVKeyDateStatus                             @"dateStatus"
#define kLVKeyPurchaseStatus                         @"purchaseStatus"

/**
 *  Subscribe Keys
 */
#define kSKeyID                                      @"id"
#define kSKeyImage                                   @"image"
#define kSKeyImageName                               @"image_short_name"
#define kSKeyTitle                                   @"title"
#define kSKeySubscribed                              @"subscribed"
#define kSKeyDateTime                               @"datetime"
#define kSKeyAndriodProductID                       @"android_product_id"
#define kSKeyiOSProductID                           @"ios_product_id"
#define kSKeyPrice                                  @"price"
#define kSKeyStatus                                 @"status"

/**
 *  Trending Videos Keys
 */
#define kTVKeyID                                     @"id"
#define kTVKeyVideoCity                              @"videoCity"
#define kTVKeyCategoryID                             @"categoryId"
#define kTVKeySubCategoryID                          @"subCategoryId"
#define kTVKeyVideoTitle                             @"videoTitle"
#define kTVKeyVideoDescription                       @"videoDescription"
#define kTVKeyWebsiteLink                            @"websiteLink"
#define kTVKeyVideoThumbnail                         @"videoThumbnail"
#define kTVKeyVideoLink                              @"videoLink"
#define kTVKeyWebVideoLink                           @"webVideoLink"
#define kTVKeyStatus                                 @"status"
#define kTVKeyCreatedDate                            @"createdDate"
#define kTVKeyMetaData                               @"metaData"
#define kTVKeyWhatsHot                               @"whatsHot"
#define kTVKeyPopular                                @"popular"
#define kTVKeyVideoType                              @"videoType"
#define kTVKeyWebVideoType                           @"webVideoType"
#define kTVKeyUserID                                 @"userId"
#define kTVKeyVideoFeature                           @"videoFeature"
#define kTVKeyPanoramImage                           @"panoramaImage"
#define kTVKeyNotificationFlag                       @"notificationFlag"
#define kTVKeyIsImage                                @"isImage"
#define kTVKeyRating                                 @"rating"
#define kTVKeyCategoryName                           @"categoryName"
#define kTVKeyViewCount                              @"video"
#define kTVKeyLikeCount                              @"like"

/**
 *  Category Keys
 */
#define kCKeyID                                      @"id"
#define kCKeyCategoryName                            @"categoryName"
#define kCKeyImage                                   @"image"


/**
 *  Global Trending Videos Keys
 */
#define kGTVKeyID                                     @"id"
#define kGTVKeyETag                                   @"etag"
#define kGTVKeyVideoKind                              @"kind"
#define kGTVKeySnippet                                @"snippet"
#define kGTVKeyContentDetails                         @"contentDetails"
#define kGTVKeyVideoID                                @"videoId"
#define kGTVKeyVideoPublished                         @"videoPublishedAt"
#define kGTVKeyPublishedAt                            @"publishedAt"
#define kGTVKeyChannedlID                             @"channelId"
#define kGTVKeyTitle                                  @"title"
#define kGTVKeyDescription                            @"description"
#define kGTVKeyChannelTitle                           @"channelTitle"
#define kGTVKeyPlayListID                             @"playlistId"
#define kGTVKeyPosition                               @"position"
#define kGTVKeyResourceId                             @"resourceId"
#define kGTVKeyThumbnails                             @"thumbnails"
#define kGTVKeyDefault                                @"default"
#define kGTVKeyMedium                                 @"medium"
#define kGTVKeyHigh                                   @"high"
#define kGTVKeyStandard                               @"standard"
#define kGTVKeyMaxres                                 @"maxres"
#define kGTVKeyKind                                   @"kind"
#define kGTVKeyURL                                    @"url"
#define kGTVKeyWidth                                   @"width"
#define kGTVKeyHeight                                 @"height"


/**
 *  Virtual Trending Videos Keys
 */
#define kVTVKeyID                                     @"id"
#define kVTVKeyVideoCity                              @"videoCity"
#define kVTVKeyCategoryID                             @"categoryId"
#define kVTVKeySubCategoryID                          @"subcategoryId"
#define kVTVKeyVideoTitle                             @"videoTitle"
#define kVTVKeyVideoDescription                       @"videoDescription"
#define kVTVKeyWebsiteLink                            @"websiteLink"
#define kVTVKeyVideoThumbnail                         @"videoThumbnail"
#define kVTVKeyVideoLink                              @"videoLink"
#define kVTVKeyWebVideoLink                           @"webVideoLink"
#define kVTVKeyStatus                                 @"status"
#define kVTVKeyCreatedDate                            @"createdDate"
#define kVTVKeyMetaData                               @"metaData"
#define kVTVKeyWhatsHot                               @"whatsHot"
#define kVTVKeyPopular                                @"popular"
#define kVTVKeyVideoType                              @"videoType"
#define kVTVKeyWebVideoType                           @"webVideoType"
#define kVTVKeyUserID                                 @"useeID"
#define kVTVKeyVideoFeature                           @"videoFeature"
#define kVTVKeyPanoramImage                           @"panoramaImage"
#define kVTVKeyNotificationFlag                       @"notificationFlag"
#define kVTVKeyIsImage                                @"isImage"
#define kVTVKeyRating                                 @"rating"
#define kVTVKeyCategoryName                           @"categoryName"
#define kVTVKeyViewCount                              @"video"
#define kVTVKeyLikeCount                              @"like"


/**
 *  User Details Videos Keys
 */

#define kUserDetailKeyUser                                   @"user"
#define kUserDetailKeyBio                                    @"bio"
#define kUserDetailKeyCredits                                @"credits"
#define kUserDetailKeyDeviceID                               @"deviceId"
#define kUserDetailKeyEmail                                  @"email"
#define kUserDetailKeyEmailVerified                          @"emailVerified"
#define kUserDetailKeyFacebookID                             @"facebookId"
#define kUserDetailKeyFirstName                              @"firstName"
#define kUserDetailKeyGender                                 @"gender"
#define kUserDetailKeyGooglePlusID                           @"googlePlusId"
#define kUserDetailKeyID                                     @"id"
#define kUserDetailKeyImageURLP                               @"imageUrlPrimary"
#define kUserDetailKeyImageURLS                               @"imageUrlSecondary"
#define kUserDetailKeyInviteCount                             @"inviteCount"
#define kUserDetailKeyLastName                                @"lastName"
#define kUserDetailKeyModel                                   @"model"
#define kUserDetailKeyPhoneNumber                             @"phoneNumber"
#define kUserDetailKeyRegistrationType                        @"registrationType"
#define kUserDetailKeyTwitterID                               @"twitterId"
#define kUserDetailKeyUsername                                @"username"
#define kUserDetailKeyWebsite                                 @"website"
#define kUserDetailKeyLikeCount                               @"likedCount"
#define kUserDetailKeyViewCount                               @"viewedCount"
#define kUserDetailKeyVideosCount                             @"videosCount"
#define kUserDetailKeyPayPalAmount                            @"payPalAmount"

/**
 *  Orion Player Keys
 */

#define kOrionPlayerKeyStatus                                   @"statuses"
#define kOrionPlayerKeyUser                                   @"user"
#define kOrionPlayerKeyText                                @"text"
#define kOrionPlayerKeyScreenName                               @"screen_name"
#define kOrionPlayerKeyName                                  @"name"
#define kOrionPlayerKeyProfileImage                          @"profile_image_url"
#define kOrionPlayerKeyCreatedAt                             @"created_at"

/**
 *   Category Detail Keys
 */
#define kCDKeyID                                     @"id"
#define kCDKeyVideoCity                              @"videoCity"
#define kCDKeyCategoryID                             @"categoryId"
#define kCDKeySubCategoryID                          @"subcategoryId"
#define kCDKeyVideoTitle                             @"videoTitle"
#define kCDKeyVideoDescription                       @"videoDescription"
#define kCDKeyWebsiteLink                            @"websiteLink"
#define kCDKeyVideoThumbnail                         @"videoThumbnail"
#define kCDKeyVideoLink                              @"videoLink"
#define kCDKeyWebVideoLink                           @"webVideoLink"
#define kCDKeyStatus                                 @"status"
#define kCDKeyCreatedDate                            @"createdDate"
#define kCDKeyMetaData                               @"metaData"
#define kCDKeyWhatsHot                               @"whatsHot"
#define kCDKeyPopular                                @"popular"
#define kCDKeyVideoType                              @"videoType"
#define kCDKeyWebVideoType                           @"webVideoType"
#define kCDKeyUserID                                 @"userID"
#define kCDKeyVideoFeature                           @"videoFeature"
#define kCDKeyPanoramImage                           @"panoramaImage"
#define kCDKeyNotificationFlag                       @"notificationFlag"
#define kCDKeyIsImage                                @"isImage"
#define kCDKeyRating                                 @"rating"
#define kCDKeyCategoryName                           @"categoryName"
#define kCDKeyViewCount                              @"video"
#define kCDKeyLikeCount                              @"like"


#endif /* APIDictionaryKeys_h */
