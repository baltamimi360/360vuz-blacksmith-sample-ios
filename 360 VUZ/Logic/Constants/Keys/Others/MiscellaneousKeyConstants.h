//
//  MiscellaneousKeyConstants.h
//  360 VUZ
//
//  Created by Mosib on 8/16/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const kGIDSignInClientID;
extern NSString * const kOneSignalID;
extern NSString * const kGADMobileAdsID;
extern NSString * const kGAITrackerID;
extern NSString * const kSmoochAppToken;
