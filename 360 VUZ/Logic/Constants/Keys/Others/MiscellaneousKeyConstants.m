//
//  MiscellaneousKeyConstants.m
//  360 VUZ
//
//  Created by Mosib on 8/16/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "MiscellaneousKeyConstants.h"

NSString * const kGIDSignInClientID  = @"656862675130-sthktoisq96707u0ks079ou8jadcilhr.apps.googleusercontent.com";
NSString * const kOneSignalID        = @"2bc41934-c238-40ee-a43a-f11b6222ce42";
NSString * const kGADMobileAdsID     = @"ca-app-pub-6858314599865339/2484811409";
NSString * const kGAITrackerID       = @"UA-77322417-1";
NSString * const kSmoochAppToken     = @"0x81e2f04hlxfgw5qna92mn7e";
