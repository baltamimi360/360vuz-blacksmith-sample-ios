//
//  SocialKeyConstants.h
//  360 VUZ
//
//  Created by Mosib on 7/25/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <Foundation/Foundation.h>

/****************** Social Constants *******************/
extern NSString * const kTwitterConsumerKey;
extern NSString * const kTwitterConsumerSecret;
extern NSString * const kGoogleClientID;
extern NSString * const kFacebookAppID;

@interface SocialKeyConstants : NSObject

#pragma mark - Social Keys Strings

+ (NSString *) twitterConsumerKey;
+ (NSString *) twitterConsumerSecret;
+ (NSString *) googleClientID;
+ (NSString *) facebookAppID;

@end
