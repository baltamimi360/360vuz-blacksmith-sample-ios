//
//  SocialKeyConstants.m
//  360 VUZ
//
//  Created by Mosib on 7/25/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "SocialKeyConstants.h"

/****************** Social Constants *******************/
NSString * const kTwitterConsumerKey      = @"vGoJzsapXN33nK7QAUycaT5Af";
NSString * const kTwitterConsumerSecret   = @"aJnIDW1iem0fgW70ovGETXN0Pp7rYhfHsChqZXxo22JZQsc9U9";
NSString * const kGoogleClientID   = @"656862675130-sthktoisq96707u0ks079ou8jadcilhr.apps.googleusercontent.com";
NSString * const kFacebookAppID   =  @"334936870033137";

@implementation SocialKeyConstants

#pragma mark - Social Keys Strings

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
+ (NSString *) twitterConsumerKey
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return kTwitterConsumerKey;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
+ (NSString *) twitterConsumerSecret
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return kTwitterConsumerSecret;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
+ (NSString *) googleClientID
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return kGoogleClientID;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
+ (NSString *) facebookAppID
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return kFacebookAppID;
}

@end
