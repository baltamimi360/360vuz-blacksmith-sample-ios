//
//  ApplicationFonts.h
//  360 VUZ
//
//  Created by Mosib on 7/2/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#ifndef ApplicationConstants_h
#define ApplicationConstants_h


#pragma mark - SF UI Text -
#define kFontSFUITextMedium                       @"SFUIText-Medium"
#define kFontSFUITextLight                        @"SFUIText-Light"
#define kFontSFUITextRegular                      @"SFUIText-Regular"
#define kFontSFUITextSemiboldItalic               @"SFUIText-SemiboldItalic"
#define kFontSFUITextHeavy                        @"SFUIText-Heavy"
#define kFontSFUITextBold                         @"SFUIText-Bold"
#define kFontSFUITextMediumItalic                 @"SFUIText-MediumItalic"
#define kFontSFUITextItalic                       @"SFUIText-Italic"
#define kFontSFUITextBoldItalic                   @"SFUIText-BoldItalic"
#define kFontSFUITextSemibold                     @"SFUIText-Semibold"
#define kFontSFUITextLightItalic                  @"SFUIText-LightItalic"
#define kFontSFUITextHeavyItalic                  @"SFUIText-HeavyItalic"

#pragma mark - SF UI Text -
#define kFontSFUIDisplayRegular                   @"SFUIDisplay-Regular"
#define kFontSFUIDisplayBold                      @"SFUIDisplay-Bold"
#define kFontSFUIDisplayThin                      @"SFUIDisplay-Thin"
#define kFontSFUIDisplayMedium                    @"SFUIDisplay-Medium"
#define kFontSFUIDisplayHeavy                     @"SFUIDisplay-Heavy"
#define kFontSFUIDisplayUltralight                @"SFUIDisplay-Ultralight"
#define kFontSFUIDisplaySemibold                  @"SFUIDisplay-Semibold"
#define kFontSFUIDisplayLight                     @"SFUIDisplay-Light"
#define kFontSFUIDisplayBlack                     @"SFUIDisplay-Black"


#endif /* ApplicationConstants_h */
