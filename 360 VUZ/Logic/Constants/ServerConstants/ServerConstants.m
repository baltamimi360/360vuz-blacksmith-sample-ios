//
//  ServerConstants.m
//  360 VUZ
//
//  Created by Mosib on 7/3/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "ServerConstants.h"


/****************** Server Constants *******************/

NSString * const kDomainName     = @"166.62.84.69:3000/api";// s3.amazonaws.com
NSString * const kDomainLive     = @"www.360mea.com/360admin/api";

@implementation ServerConstants


#pragma mark - Web Services
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
+ (NSString *) serverURL
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return [NSString stringWithFormat:@"%@%@", kURLScheme, kDomainName];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
+ (NSString *) serverURLLive
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return [NSString stringWithFormat:@"%@%@", kURLScheme, kDomainLive];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
+ (NSString *) serviceURL:(NSString *)servicePath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return [NSString stringWithFormat:@"%@%@",[ServerConstants serverURL], servicePath];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
+ (NSString *) serviceURLLive:(NSString *)servicePath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return [NSString stringWithFormat:@"%@%@",[ServerConstants serverURLLive], servicePath];
}


@end
