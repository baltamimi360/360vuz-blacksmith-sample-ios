//
//  ServerConstants.h
//  360 VUZ
//
//  Created by Mosib on 7/3/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <Foundation/Foundation.h>

/****************** Server Constants *******************/
extern NSString * const kDomainName;
extern NSString * const kDomainLive;

@interface ServerConstants : NSObject

#pragma mark - Web Services

+ (NSString *) serverURL;
+ (NSString *) serverURLLive;
+ (NSString *) serviceURL:(NSString *)servicePath;
+ (NSString *) serviceURLLive:(NSString *)servicePath;




@end
