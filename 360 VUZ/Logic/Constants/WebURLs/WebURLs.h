//
//  WebURLs.h
//  BrownAndWood
//
//  Created by Khawaja Fahad on 08/01/2017.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#ifndef Neeomail_WebURLs_h
#define Neeomail_WebURLs_h

/*
 *	HTTP_PROTOCOL Value
 *	0 = HTTP
 *	1 = HTTPS
 */
#define HTTP_PROTOCOL 0
#if HTTP_PROTOCOL
#define kURLScheme @"https://"
#else
#define kURLScheme @"http://"
#endif


#pragma mark - POST Requests -

/**
 *  Login 
 */
#define kPostRequestLogin     @"/MobileUsers/user-login"

/**
 *  Register User
 */
#define kPostRequestRegisterUser     @"/MobileUsers/register-user"

/**
 *  Password Reset
 */
#define kPostRequestPasswordReset     @"/MobileUsers/request-password-reset"

/**
 *  Update Password
 */
#define kPostRequestUpdatePassword     @"/MobileUsers/update-password"

/**
 *  Home Page
 */
#define kPostRequestHomePage          @"/homepage_video"

/**
 *  Categories
 */
#define kPostRequestHPCategories        @"/Categories/get-trending-video"

/**
 *  Live 360
 */
#define kPostRequestHPLive        @"/MeaLiveStreamings/get-live-video"

/**
 *  Trending
 */
#define kPostRequestHPTrending        @"/Videos/get-trending-360-video"

/**
 *  Whats Hot
 */
#define kPostRequestHPWhatsHot        @"/Videos/get-whats-hot-video"

/**
 *  Subscribe
 */
#define kPostRequestHPSubscribeList     @"/MeaChannels/get-channel-list"

/**
 *  Global
 */
#define kPostRequestHPVirtualTrending     @"/Videos/get-trending-video"

/**
 *  Youtube
 */
#define kPostRequestHPGlobalTredning     @"/Videos/get-youtube-video"

/**
 *  Edit Profile
 */
#define kPostRequestEditProfile         @"/MobileUsers/update-user-profile"

/**
 *  Past LIve Events
 */
#define kPostRequestPastLiveEvents      @"/MeaLiveStreamings/get-past-live-video"

/**
 *  Past LIve Events
 */
#define kPostRequestUpcomingLiveEvents      @"/MeaLiveStreamings/get-upcoming-live-video"

/**
 *  Category list Detail
 */
#define kPostRequestCategoryDetailList     @"/Videos/get-category-videos"


/**
 *  Recommanded Video list
 */
#define kPostRequestRecommandedVideos     @"/Videos/get-related-videos"

#endif
