//
//  UINotificationNames.h
//  360 VUZ
//
//  Created by Mosib on 7/3/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#ifndef UINotificationNames_h
#define UINotificationNames_h


/**
 *  Splash Movie Finished
 */
#define kNotificationSplashVideoFinished @"NotificationSplashVideoFinished"

#define kNotificationTextFeildUpdatedInEditProfile @"NotificationTextFeildUpdatedInEditProfile"

#endif /* UINotificationNames_h */
