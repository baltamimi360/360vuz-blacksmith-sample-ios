//
//  ApplicationIdentifiers.h
//  360 VUZ
//
//  Created by Mosib on 7/10/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <Foundation/Foundation.h>


extern NSString * const kRIDCategoriesCollectionViewCell;
extern NSString * const kRIDCategoriesHeaderCollectionView;
extern NSString * const kRIDVideoTableViewCell;
extern NSString * const kRIDWhatsHotTableViewCell;
extern NSString * const kRIDWhatsHotCollectionViewCell;
extern NSString * const kRIDCategoriesTableViewCell;
extern NSString * const kRIDLive360CollectionViewCell;
extern NSString * const kRIDLive360TableViewCell;
extern NSString * const kRIDTrendingCollectionViewCell;
extern NSString * const kRIDTrendingHomeTableViewCell;
extern NSString * const kRIDSubscribeCollectionViewCell;
extern NSString * const kRIDSubscribeTableViewCell;
extern NSString * const kRIDPast360TableViewCell;
extern NSString * const kRIDEditProfileDetailTableViewCell;
extern NSString * const kRIDEditProfileSocialTableViewCell;
extern NSString * const kRIDGlobalHomeCollectionViewCell;
extern NSString * const kRIDGlobalHomeTableViewCell;
extern NSString * const kRIDVirtualCollectionViewCell;
extern NSString * const kRIDVirtualTrendingTableViewCell;
extern NSString * const kRIDSocialShareDetailVideoTableViewCell;
extern NSString * const kRIDRecommendedVideoDetailTableViewCell;
extern NSString * const kRIDVideoDescriptionVideoDetailTableViewCell;
extern NSString * const kRIDVirtualDetailTableViewCell;
extern NSString * const kRIDCategoriesYoutubeTableViewCell;
extern NSString * const kRIDOrionLiveTwitterTableViewCell;
