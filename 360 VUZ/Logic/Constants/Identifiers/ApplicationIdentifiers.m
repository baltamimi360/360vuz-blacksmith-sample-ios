//
//  ApplicationIdentifiers.m
//  360 VUZ
//
//  Created by Mosib on 7/10/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "ApplicationIdentifiers.h"

NSString * const kRIDCategoriesCollectionViewCell     = @"CategoriesCollectionViewCell";
NSString * const kRIDCategoriesHeaderCollectionView   = @"CategoriesHeaderCollectionView";
NSString * const kRIDVideoTableViewCell   = @"VideoTableViewCell";
NSString * const kRIDWhatsHotTableViewCell   = @"WhatsHotTableViewCell";
NSString * const kRIDWhatsHotCollectionViewCell   = @"WhatsHotCollectionViewCell";
NSString * const kRIDCategoriesTableViewCell   = @"CategoriesTableViewCell";
NSString * const kRIDLive360CollectionViewCell   = @"Live360CollectionViewCell";
NSString * const kRIDLive360TableViewCell   = @"Live360TableViewCell";
NSString * const kRIDTrendingCollectionViewCell   = @"TrendingCollectionViewCell";
NSString * const kRIDTrendingHomeTableViewCell   = @"TrendingHomeTableViewCell";
NSString * const kRIDSubscribeCollectionViewCell   = @"SubscribeCollectionViewCell";
NSString * const kRIDSubscribeTableViewCell   = @"SubscribeTableViewCell";
NSString * const kRIDPast360TableViewCell   = @"Past360TableViewCell";
NSString * const kRIDEditProfileDetailTableViewCell = @"EditProfileDetailTableViewCell";
NSString * const kRIDEditProfileSocialTableViewCell = @"EditProfileSocialTableViewCell";
NSString * const kRIDGlobalHomeCollectionViewCell = @"GlobalHomeCollectionViewCell";
NSString * const kRIDGlobalHomeTableViewCell = @"GlobalTrendingTableViewCell";
NSString * const kRIDVirtualCollectionViewCell = @"VirtualCollectionViewCell";
NSString * const kRIDVirtualTrendingTableViewCell = @"VirtualTrendingTableViewCell";
NSString * const kRIDSocialShareDetailVideoTableViewCell = @"SocialShareDetailVideoTableViewCell";
NSString * const kRIDRecommendedVideoDetailTableViewCell = @"RecommendedVideoDetailTableViewCell";
NSString * const kRIDVideoDescriptionVideoDetailTableViewCell = @"VideoDescriptionVideoDetailTableViewCell";
NSString * const kRIDVirtualDetailTableViewCell = @"VirtualDetailTableViewCell";
NSString * const kRIDCategoriesYoutubeTableViewCell = @"CategoriesYoutubeTableViewCell";
NSString * const kRIDOrionLiveTwitterTableViewCell = @"OrionLiveTwitterTableViewCell";
