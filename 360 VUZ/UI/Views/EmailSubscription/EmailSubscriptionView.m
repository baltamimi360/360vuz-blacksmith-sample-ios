//
//  EmailSubscriptionView.m
//  360 VUZ
//
//  Created by Mosib on 7/12/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "EmailSubscriptionView.h"
#import <MessageUI/MessageUI.h>

@interface EmailSubscriptionView() <MFMailComposeViewControllerDelegate>
{
    UITextField * textFieldEmail;
}

@end

@implementation EmailSubscriptionView

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (id)initWithFrame:(CGRect)frame
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self = [super initWithFrame:frame];
    //You code here
    
    if (self)
    {
        // TextField Email
        textFieldEmail = [[UITextField alloc] init];
        CGSize sizeOfTextFieldEmail = CGSizeMake(frame.size.width * 0.80, 40.0);
        CGPoint PointTextFieldEmail = CGPointMake(frame.size.width/2 - sizeOfTextFieldEmail.width/2, frame.size.height/2);
        textFieldEmail.frame = CGRectMake(PointTextFieldEmail.x, PointTextFieldEmail.y, sizeOfTextFieldEmail.width, sizeOfTextFieldEmail.height);
        textFieldEmail.placeholder = @"Type your email ..";
        [textFieldEmail applyCustomSettingWithUnderline];
        textFieldEmail.keyboardType = UIKeyboardTypeEmailAddress;
        
        // Button Login Frame
        UIButton *buttonSubmit = [UIButton buttonWithType:UIButtonTypeCustom];
        [buttonSubmit addTarget:self
                        action:@selector(actionButtonSubmit:)
              forControlEvents:UIControlEventTouchUpInside];
        [buttonSubmit setTitle:@"Submit" forState:UIControlStateNormal];
        buttonSubmit.backgroundColor = UIColorFromRGB(0x29AEE3);
        CGSize sizeOfbuttonLogin = CGSizeMake(frame.size.width * 0.80, 55.0);
        CGPoint pointOfbuttonLogin = CGPointMake(frame.size.width/2 - sizeOfbuttonLogin.width/2, PointTextFieldEmail.y + sizeOfTextFieldEmail.height + 20);
        buttonSubmit.frame = CGRectMake(pointOfbuttonLogin.x, pointOfbuttonLogin.y, sizeOfbuttonLogin.width, sizeOfbuttonLogin.height);
        [buttonSubmit.titleLabel setFont:[UIFont fontWithName:kFontSFUIDisplayRegular size:14]];
        buttonSubmit.layer.cornerRadius = 4;
        buttonSubmit.clipsToBounds = YES;
        
        // Title Label
        UILabel *labelDonNotMiss = [[UILabel alloc] init];
        CGSize sizeOflabelDonNotMiss = CGSizeMake(frame.size.width * 0.80, 55.0);
        CGPoint pointOflabelDonNotMiss = CGPointMake(PointTextFieldEmail.x, PointTextFieldEmail.y - 100);
        labelDonNotMiss.frame = CGRectMake(pointOflabelDonNotMiss.x, pointOflabelDonNotMiss.y, sizeOflabelDonNotMiss.width, sizeOflabelDonNotMiss.height);
        labelDonNotMiss.text = @"Do not miss the next 360 live !";
        labelDonNotMiss.font = [UIFont fontWithName:kFontSFUIDisplayBold size:22];
        labelDonNotMiss.adjustsFontSizeToFitWidth = true;
        labelDonNotMiss.textColor = UIColorFromRGB(0xD3D3D3);
        
        // ImageView
        UIImageView *imageView360Live = [[UIImageView alloc] init];
        CGSize sizeOfimageView360Live = CGSizeMake(180, 45.0);
        CGPoint pointOfimageView360Live = CGPointMake(PointTextFieldEmail.x + sizeOfTextFieldEmail.width/2 - sizeOfimageView360Live.width/2, pointOflabelDonNotMiss.y - 50);
        imageView360Live.frame = CGRectMake(pointOfimageView360Live.x, pointOfimageView360Live.y, sizeOfimageView360Live.width, sizeOfimageView360Live.height);
        imageView360Live.image = [UIImage imageNamed:@"360live-icon"];
        imageView360Live.contentMode = UIViewContentModeScaleAspectFit;
        
        [self addSubview:textFieldEmail];
        [self addSubview:buttonSubmit];
        [self addSubview:labelDonNotMiss];
        [self addSubview:imageView360Live];
        
    }
    
    return self;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonSubmit:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if (![[UtilityClass sharedManger] validateEmailWithString:textFieldEmail.text])
    {
        NSArray *toRecipents = [NSArray arrayWithObject:@"info@360mea.com"];
        
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        if ([MFMailComposeViewController canSendMail]) {
            
            [mc setToRecipients:toRecipents];
            [mc setSubject:@"Live Broadcast Notification."];
            if([sender tag] == 100){
                [mc setMessageBody:[NSString stringWithFormat:@"My email is %@. Please keep me notified of the next live broadcast.",textFieldEmail.text ] isHTML:NO];
            }
            else{
                [mc setMessageBody:[NSString stringWithFormat:@"My email is %@. Please keep me notified of the next live broadcast.",textFieldEmail.text ] isHTML:NO];
            }
            
            [[self viewController] presentViewController:mc animated:YES completion:NULL];
        }
        
    }else{
        UIAlertView *alert11 = [[UIAlertView alloc] initWithTitle:@"360 VUZ" message:@"Email is not valid!" delegate:nil cancelButtonTitle:@"OK"otherButtonTitles:nil,nil];
        [alert11 show];
    }
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    [[self viewController] dismissViewControllerAnimated:YES completion:NULL];
}


@end
