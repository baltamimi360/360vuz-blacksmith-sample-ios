//
//  SubcribeOverlayView.m
//  360 VUZ
//
//  Created by Mosib on 7/24/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "SubcribeOverlayView.h"


@implementation SubcribeOverlayView

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (id)initWithFrame:(CGRect)frame
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self = [super initWithFrame:frame];
    //You code here
    
    if (self)
    {
        // Set Up UI
        [self setUpUI];
        
    }
    
    return self;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpUI
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
    CGRect frame = self.frame;
    
    // ImageView Play
    UIImageView *imageViewBackground = [[UIImageView alloc] initWithFrame:frame];
    imageViewBackground.image = [UIImage imageNamed:@"Subcribe_Overlay"];
    
    CGSize sizeOfimageViewLock = CGSizeMake(19, 26.0);
    CGPoint pointOfimageViewLock = CGPointMake(self.frame.size.width/2 - sizeOfimageViewLock.width/2, self.frame.size.height/2 - sizeOfimageViewLock.height/2);
    UIImageView *imageViewLock = [[UIImageView alloc] initWithFrame:CGRectMake(pointOfimageViewLock.x, pointOfimageViewLock.y, sizeOfimageViewLock.width, sizeOfimageViewLock.height)];
    imageViewLock.image = [UIImage imageNamed:@"lock-icon"];
    imageViewLock.contentMode = UIViewContentModeScaleAspectFit;
    
    
    UILabel *labelClick = [[UILabel alloc] init];
    CGSize sizeOflabelClick = CGSizeMake(frame.size.width - 25, 14.0);
    CGPoint pointOflabelClick = CGPointMake(self.frame.size.width/2 - sizeOflabelClick.width/2, pointOfimageViewLock.y + sizeOfimageViewLock.height + 5);
    labelClick.frame = CGRectMake(pointOflabelClick.x, pointOflabelClick.y, sizeOflabelClick.width, sizeOflabelClick.height);
    labelClick.text = @"Click to Unlock";
    labelClick.adjustsFontSizeToFitWidth = YES;
    labelClick.font = [UIFont fontWithName:kFontSFUIDisplayMedium size:12];
    labelClick.textAlignment = NSTextAlignmentCenter;
    labelClick.textColor = [UIColor whiteColor];
    
    [self addSubview:imageViewBackground];
    [self addSubview:imageViewLock];
    [self addSubview:labelClick];
  
}

@end
