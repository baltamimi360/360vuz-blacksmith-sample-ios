//
//  TimeLeftView.m
//  360 VUZ
//
//  Created by Mosib on 7/13/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "TimeLeftView.h"

@implementation TimeLeftView

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (id)initWithFrame:(CGRect)frame
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self = [super initWithFrame:frame];
    //You code here
    
    if (self)
    {
        // Set Up UI
        [self setUpUI];
        
    }
    
    return self;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpUI
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    UILabel *labelTimeUntilLiveStarts = [[UILabel alloc] init];
    CGSize sizeOflabelTimeUntilLiveStarts = CGSizeMake(160, 30.0);
    CGPoint pointOflabelTimeUntilLiveStarts = CGPointMake(self.frame.size.width/2 - sizeOflabelTimeUntilLiveStarts.width/2, 0);
    labelTimeUntilLiveStarts.frame = CGRectMake(pointOflabelTimeUntilLiveStarts.x, pointOflabelTimeUntilLiveStarts.y, sizeOflabelTimeUntilLiveStarts.width, sizeOflabelTimeUntilLiveStarts.height);
    labelTimeUntilLiveStarts.text = @"Time until live starts";
    labelTimeUntilLiveStarts.font = [UIFont fontWithName:kFontSFUIDisplayRegular size:16];
    labelTimeUntilLiveStarts.textAlignment = NSTextAlignmentCenter;
    labelTimeUntilLiveStarts.textColor = [UIColor colorWithRed:178/255.0 green:178/255.0 blue:178/255.0 alpha:1/1.0];
    
    _labelValueDays = [[UILabel alloc] init];
    CGSize sizeOflabelValueDays = CGSizeMake(55, 40.0);
    CGPoint pointOflabelValueDays = CGPointMake(5 , 30);
    _labelValueDays.frame = CGRectMake(pointOflabelValueDays.x, pointOflabelValueDays.y, sizeOflabelValueDays.width, sizeOflabelValueDays.height);
    _labelValueDays.text = @"00";
    _labelValueDays.font = [UIFont fontWithName:kFontSFUIDisplayBold size:40];
    _labelValueDays.textAlignment = NSTextAlignmentCenter;
    _labelValueDays.textColor = [UIColor colorWithRed:19/255.0 green:174/255.0 blue:229/255.0 alpha:1/1.0];
    
    _labelValueHours = [[UILabel alloc] init];
    CGSize sizeOflabelValueHours = CGSizeMake(55, 40.0);
    CGPoint pointOflabelValueHours = CGPointMake(85 , 30);
    _labelValueHours.frame = CGRectMake(pointOflabelValueHours.x, pointOflabelValueHours.y, sizeOflabelValueHours.width, sizeOflabelValueHours.height);
    _labelValueHours.text = @"00";
    _labelValueHours.font = [UIFont fontWithName:kFontSFUIDisplayBold size:40];
    _labelValueHours.textAlignment = NSTextAlignmentCenter;
    _labelValueHours.textColor = [UIColor colorWithRed:19/255.0 green:174/255.0 blue:229/255.0 alpha:1/1.0];
    
    _labelValueMinutes = [[UILabel alloc] init];
    CGSize sizeOflabelValueMinutes = CGSizeMake(55, 40.0);
    CGPoint pointOflabelValueMinutes = CGPointMake(170 , 30);
    _labelValueMinutes.frame = CGRectMake(pointOflabelValueMinutes.x, pointOflabelValueMinutes.y, sizeOflabelValueMinutes.width, sizeOflabelValueMinutes.height);
    _labelValueMinutes.text = @"00";
    _labelValueMinutes.font = [UIFont fontWithName:kFontSFUIDisplayBold size:40];
    _labelValueMinutes.textAlignment = NSTextAlignmentCenter;
    _labelValueMinutes.textColor = [UIColor colorWithRed:19/255.0 green:174/255.0 blue:229/255.0 alpha:1/1.0];
    
    _labelValueSeconds = [[UILabel alloc] init];
    CGSize sizeOflabelValueSeconds = CGSizeMake(55, 40.0);
    CGPoint pointOflabelValueSeconds = CGPointMake(255 , 30);
    _labelValueSeconds.frame = CGRectMake(pointOflabelValueSeconds.x, pointOflabelValueSeconds.y, sizeOflabelValueSeconds.width, sizeOflabelValueSeconds.height);
    _labelValueSeconds.text = @"00";
    _labelValueSeconds.font = [UIFont fontWithName:kFontSFUIDisplayBold size:40];
    _labelValueSeconds.textAlignment = NSTextAlignmentCenter;
    _labelValueSeconds.textColor = [UIColor colorWithRed:19/255.0 green:174/255.0 blue:229/255.0 alpha:1/1.0];
    
    
    UILabel *labelDays = [[UILabel alloc] init];
    CGSize sizeOflabelDays = CGSizeMake(55, 20.0);
    CGPoint pointOflabelDays = CGPointMake(pointOflabelValueDays.x +  sizeOflabelValueDays.width/2 - sizeOflabelDays.width/2, pointOflabelValueDays.y + sizeOflabelValueDays.height );
    labelDays.frame = CGRectMake(pointOflabelDays.x, pointOflabelDays.y, sizeOflabelDays.width, sizeOflabelDays.height);
    labelDays.text = @"DAYS";
    labelDays.font = [UIFont fontWithName:kFontSFUIDisplayLight size:14];
    labelDays.textAlignment = NSTextAlignmentCenter;
    labelDays.textColor = [UIColor colorWithRed:178/255.0 green:178/255.0 blue:178/255.0 alpha:1/1.0];
    
    UILabel *labelHours = [[UILabel alloc] init];
    CGSize sizeOflabelHours = CGSizeMake(55, 20.0);
    CGPoint pointOflabelHours = CGPointMake(pointOflabelValueHours.x +  sizeOflabelValueHours.width/2 - sizeOflabelHours.width/2, pointOflabelValueHours.y + sizeOflabelValueHours.height );
    labelHours.frame = CGRectMake(pointOflabelHours.x, pointOflabelHours.y, sizeOflabelHours.width, sizeOflabelHours.height);
    labelHours.text = @"HOURS";
    labelHours.font = [UIFont fontWithName:kFontSFUIDisplayLight size:14];
    labelHours.textAlignment = NSTextAlignmentCenter;
    labelHours.textColor = [UIColor colorWithRed:178/255.0 green:178/255.0 blue:178/255.0 alpha:1/1.0];
    
    UILabel *labelMinutes = [[UILabel alloc] init];
    CGSize sizeOflabelMinutes = CGSizeMake(60, 20.0);
    CGPoint pointOflabelMinutes = CGPointMake(pointOflabelValueMinutes.x +  sizeOflabelValueMinutes.width/2 - sizeOflabelMinutes.width/2, pointOflabelValueMinutes.y + sizeOflabelValueMinutes.height );
    labelMinutes.frame = CGRectMake(pointOflabelMinutes.x, pointOflabelMinutes.y, sizeOflabelMinutes.width, sizeOflabelMinutes.height);
    labelMinutes.text = @"MINUTES";
    labelMinutes.font = [UIFont fontWithName:kFontSFUIDisplayLight size:14];
    labelMinutes.textAlignment = NSTextAlignmentCenter;
    labelMinutes.textColor = [UIColor colorWithRed:178/255.0 green:178/255.0 blue:178/255.0 alpha:1/1.0];
    
    UILabel *labelSeconds = [[UILabel alloc] init];
    CGSize sizeOflabelSeconds = CGSizeMake(70, 20.0);
    CGPoint pointOflabelSeconds = CGPointMake(pointOflabelValueSeconds.x +  sizeOflabelValueSeconds.width/2 - sizeOflabelSeconds.width/2, pointOflabelValueSeconds.y + sizeOflabelValueSeconds.height );
    labelSeconds.frame = CGRectMake(pointOflabelSeconds.x, pointOflabelSeconds.y, sizeOflabelSeconds.width, sizeOflabelSeconds.height);
    labelSeconds.text = @"SECONDS";
    labelSeconds.font = [UIFont fontWithName:kFontSFUIDisplayLight size:14];
    labelSeconds.textAlignment = NSTextAlignmentCenter;
    labelSeconds.textColor = [UIColor colorWithRed:178/255.0 green:178/255.0 blue:178/255.0 alpha:1/1.0];
    
    [self addSubview:labelTimeUntilLiveStarts];
    [self addSubview:_labelValueDays];
    [self addSubview:_labelValueHours];
    [self addSubview:_labelValueMinutes];
    [self addSubview:_labelValueSeconds];
    [self addSubview:labelDays];
    [self addSubview:labelHours];
    [self addSubview:labelMinutes];
    [self addSubview:labelSeconds];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonLogin:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
}

@end
