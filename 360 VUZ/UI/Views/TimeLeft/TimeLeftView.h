//
//  TimeLeftView.h
//  360 VUZ
//
//  Created by Mosib on 7/13/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimeLeftView : UIView

@property (nonatomic) UILabel *labelValueDays;
@property (nonatomic) UILabel *labelValueHours;
@property (nonatomic) UILabel *labelValueMinutes;
@property (nonatomic) UILabel *labelValueSeconds;

@end
