//
//  TopBarLive360View.h
//  360 VUZ
//
//  Created by Mosib on 7/17/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopBarLive360View : UIView

@property (nonatomic, copy)void(^pageControlBlock)(int);

- (void) scrollByMainView:(int) page;

@end


