//
//  TopBarLive360View.m
//  360 VUZ
//
//  Created by Mosib on 7/17/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "TopBarLive360View.h"

@interface TopBarLive360View()
{
UIButton *  buttonUpcoming;
UIButton *  buttonPast;
    UIView *viewSelected;
}

@end

@implementation TopBarLive360View

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (id)initWithFrame:(CGRect)frame
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self = [super initWithFrame:frame];
    //You code here
    
    if (self)
    {
        // Set Up UI
        [self setUpUI];
        
    }
    
    return self;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpUI
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
    buttonUpcoming = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonUpcoming.frame = CGRectMake(0, 0, [UtilityClass getDeviceBounds].size.width/2, 40);
    [buttonUpcoming setTitleColor:UIColorFromRGB(0x13AEE5) forState:UIControlStateNormal];
    buttonUpcoming.titleLabel.font = [UIFont fontWithName:kFontSFUIDisplayBold size:16.0];
    [buttonUpcoming setTitle:@"Upcoming 360VUZ" forState:UIControlStateNormal];
    [buttonUpcoming addTarget:self
                    action:@selector(actionButtonUpcoming:)
          forControlEvents:UIControlEventTouchUpInside];
    [buttonUpcoming setSelected:true];

    
    buttonPast = [UIButton buttonWithType:UIButtonTypeCustom];
    buttonPast.frame = CGRectMake([UtilityClass getDeviceBounds].size.width/2, 0, [UtilityClass getDeviceBounds].size.width/2, 40);
    [buttonPast setTitleColor:UIColorFromRGB(0xB2B2B2) forState:UIControlStateNormal];
    buttonPast.titleLabel.font = [UIFont fontWithName:kFontSFUIDisplayBold size:16.0];
    [buttonPast setTitle:@"Past 360VUZ" forState:UIControlStateNormal];
    [buttonPast addTarget:self
                    action:@selector(actionButtonPast:)
          forControlEvents:UIControlEventTouchUpInside];
    
    
    UIView *viewSolid = [[UIView alloc] init];
    viewSolid.frame = CGRectMake(0, buttonUpcoming.frame.size.height + 5, [UtilityClass getDeviceBounds].size.width, 2);
    viewSolid.backgroundColor = UIColorFromRGB(0xB2B2B2);
    
    viewSelected = [[UIView alloc] init];
    viewSelected.frame = CGRectMake(0, buttonUpcoming.frame.size.height + 5, [UtilityClass getDeviceBounds].size.width/2, 2);
    viewSelected.backgroundColor = UIColorFromRGB(0x13AEE5);
    
    [self addSubview:viewSolid];
    [self addSubview:viewSelected];
    [self addSubview:buttonUpcoming];
    [self addSubview:buttonPast];
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonPast:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if (!sender.isSelected)
    {
        // Adjust Selected Control
        [self selectPast];
        
        // Page Control to Main View
        _pageControlBlock(1);
    }
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonUpcoming:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if (!sender.isSelected)
    {
        // Adjust Selected Control
        [self selectUpcoming];
        
        // Page Control to Main View
        _pageControlBlock(0);
    }
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) animateSliderwithFrame:(CGRect) frame
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
    [UIView animateWithDuration:0.25
                          delay:0.0
                        options: UIViewAnimationOptionLayoutSubviews
                     animations:^{
                         viewSelected.frame = frame;
                     }completion:nil];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) selectUpcoming
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [buttonUpcoming setSelected:!buttonUpcoming.isSelected];
    [buttonUpcoming setTitleColor:UIColorFromRGB(0x13AEE5) forState:UIControlStateNormal];
    [buttonPast setTitleColor:UIColorFromRGB(0xB2B2B2) forState:UIControlStateNormal];
    [buttonPast setSelected:false];
    
    CGRect temp = viewSelected.frame;
    temp.origin.x = 0 ;
    
    [self animateSliderwithFrame:temp];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) selectPast
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [buttonPast setSelected:!buttonPast.isSelected];
    [buttonPast setTitleColor:UIColorFromRGB(0x13AEE5) forState:UIControlStateNormal];
    [buttonUpcoming setTitleColor:UIColorFromRGB(0xB2B2B2) forState:UIControlStateNormal];
    [buttonUpcoming setSelected:false];
    
    CGRect temp = viewSelected.frame;
    temp.origin.x = [[UIScreen mainScreen] bounds].size.width/2 ;
    
    [self animateSliderwithFrame:temp];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) scrollByMainView:(int) page
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if (page)
    {
        [self selectPast];
    }
    
    else
    {
        [self selectUpcoming];
    }
}

@end
