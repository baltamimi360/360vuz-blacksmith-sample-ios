//
//  CategoriesHeaderCollectionView.m
//  360 VUZ
//
//  Created by Mosib on 7/10/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "CategoriesHeaderCollectionView.h"

@implementation CategoriesHeaderCollectionView

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (id)initWithFrame:(CGRect)frame
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self = [super initWithFrame:frame];
    //You code here
    
    if (self)
    {
        // Categories ImageView
        UIImageView *imageViewCategories = [[UIImageView alloc] initWithFrame:CGRectMake(20, 12.5, 25, 25)];
        [imageViewCategories setImage:[UIImage imageNamed:@"category-Selected"]];
        
        // Title Label
        UILabel *labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(65, 15, 200, 20)];
        labelTitle.text = @"360° Categories";
        labelTitle.font = [UIFont fontWithName:kFontSFUIDisplayBold size:18];
        labelTitle.textColor = UIColorFromRGB(0x4A4A4A);
        
        [self addSubview:imageViewCategories];
        [self addSubview:labelTitle];
        
    }
    
    return self;
}

@end
