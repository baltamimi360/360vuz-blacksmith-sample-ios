//
//  UpComing360VUZ.m
//  360 VUZ
//
//  Created by Mosib on 7/13/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "UpComing360VUZ.h"
#import "TimeLeftView.h"
#import "OrionViewControllerLive.h"

@interface UpComing360VUZ ()
{
    UIButton *buttonShareIcon;
    UIImageView *imageShareIcon;
    UIButton * buttonWhatsApp;
    UIButton * buttonEmail;
    UIButton * buttonfacebook;
    UIButton * buttontwitter;
    CGRect deviceBounds;
    UIImageView *imageViewCalendar;
    UILabel *labelAddEventToCalendar;
    UIButton *buttonAddEventToCalender;
    UIButton *buttonLiveButton;
}
@end

@implementation UpComing360VUZ

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (id)initWithFrame:(CGRect)frame
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self = [super initWithFrame:frame];
    //You code here
    
    if (self)
    {
        // Set Up UI
        [self setUpUIForDetails];
        
        // Round Corners
        [_imageMoviePoster roundedCornersAllWithDefaultRaduis];
        
        // Set Up Frame for Timer
        [self setUpUIForTimer:frame];
        
        // Set Live Button Frame
        [self addLiveButton];
    }
    
    return self;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) setUpUIForDetails
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    deviceBounds = [UtilityClass getDeviceBounds];
    
    self.backgroundColor = [UIColor whiteColor];
    
    // Back Ground Image
    self.imageMoviePoster = [[UIImageView alloc] initWithFrame:CGRectMake(10, 35, deviceBounds.size.width - 20 , ((heightOfCellVideo - 25) / 568 ) * deviceBounds.size.height)];
    _imageMoviePoster.backgroundColor = [UIColor blackColor];
    _imageMoviePoster.contentMode = UIViewContentModeScaleAspectFit;
    
    // Add Shade
    UIImageView *imageViewShade = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, _imageMoviePoster.frame.size.width , _imageMoviePoster.frame.size.height)];
    imageViewShade.image = [UIImage imageNamed:@"bg_shade"];
    [_imageMoviePoster addSubview:imageViewShade];
    
    // ImageView Play
    UIImageView *imagePlayIcon = [[UIImageView alloc] initWithFrame:CGRectMake(10, _imageMoviePoster.frame.size.height - 60, 40 , 40)];
    imagePlayIcon.image = [UIImage imageNamed:@"play-icon"];
    
    //
    imageShareIcon = [[UIImageView alloc] initWithFrame:CGRectMake(_imageMoviePoster.frame.size.width - 35, _imageMoviePoster.frame.size.height - 50, 20 , 20)];
    imageShareIcon.image = [UIImage imageNamed:@"share-icon"];
    imageShareIcon.contentMode = UIViewContentModeScaleAspectFit;
    
    
    // Button Open Live
    buttonLiveButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonLiveButton addTarget:self
                       action:@selector(actionButtonLive:)
             forControlEvents:UIControlEventTouchUpInside];
    buttonLiveButton.frame = _imageMoviePoster.frame;
    
    // Button Share Share
    buttonShareIcon = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonShareIcon addTarget:self
                        action:@selector(actionButtonShare:)
              forControlEvents:UIControlEventTouchUpInside];
    buttonShareIcon.frame = CGRectMake(_imageMoviePoster.frame.size.width - 50, _imageMoviePoster.frame.size.height - 65, 60 , 60);
    //    buttonShareIcon.backgroundColor = [UIColor blueColor];
    
    [_imageMoviePoster addSubview:imagePlayIcon];
    [_imageMoviePoster addSubview:imageShareIcon];
    
    
    // Label Title
    self.labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(imagePlayIcon.frame.origin.x + imagePlayIcon.frame.size.width + 5, imagePlayIcon.frame.origin.y, imageShareIcon.frame.origin.x - imagePlayIcon.frame.origin.x -imagePlayIcon.frame.size.width - 20, 22)];
    [self.labelTitle setTextColor:[UIColor whiteColor]];
    self.labelTitle.adjustsFontSizeToFitWidth = YES;
    self.labelTitle.font = [UIFont fontWithName:kFontSFUIDisplayBold size:18];
    [_imageMoviePoster addSubview:self.labelTitle];
    
    // Label 360 Video
    self.label360video = [[UILabel alloc] initWithFrame:CGRectMake(self.labelTitle.frame.origin.x , self.labelTitle.frame.origin.y + self.labelTitle.frame.size.height, _labelTitle.frame.size.width, 14)];
    [self.label360video setNumberOfLines:1];
    self.label360video.font = [UIFont fontWithName:kFontSFUIDisplayRegular size:12];
    [self.label360video setTextAlignment:NSTextAlignmentLeft];
    [self.label360video setTextColor:[UIColor colorWithRed:19/255.0 green:174/255.0 blue:229/255.0 alpha:1/1.0]];
    [self.label360video setBackgroundColor:[UIColor clearColor]];
    self.label360video.adjustsFontSizeToFitWidth = YES;
    [_imageMoviePoster addSubview:self.label360video];
    

    // Button Share WhatsApp
    buttonWhatsApp = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonWhatsApp addTarget:self
                       action:@selector(actionButtonWhatsApp:)
             forControlEvents:UIControlEventTouchUpInside];
    buttonWhatsApp.frame = CGRectMake(deviceBounds.size.width - 50, _imageMoviePoster.frame.origin.y +  _imageMoviePoster.frame.size.height - 30
                                      , 30 , 30);
    [buttonWhatsApp setImage:[UIImage imageNamed:@"whatsapp-share-icon"] forState:UIControlStateNormal];
    [buttonWhatsApp scaleToFill];
    
    
    // Button Share Email
    buttonEmail = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonEmail addTarget:self
                    action:@selector(actionButtonEmail:)
          forControlEvents:UIControlEventTouchUpInside];
    buttonEmail.frame = CGRectMake(buttonWhatsApp.frame.origin.x - 55, _imageMoviePoster.frame.origin.y +  _imageMoviePoster.frame.size.height - 30, 30 , 30);
    [buttonEmail setImage:[UIImage imageNamed:@"email-share-icon"] forState:UIControlStateNormal];
    [buttonEmail scaleToFill];
    
    
    // Button Facebook
    buttonfacebook = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonfacebook addTarget:self
                       action:@selector(actionButtonFacebook:)
             forControlEvents:UIControlEventTouchUpInside];
    buttonfacebook.frame = CGRectMake(buttonEmail.frame.origin.x - 55, _imageMoviePoster.frame.origin.y +  _imageMoviePoster.frame.size.height - 30, 30 , 30);
    [buttonfacebook setImage:[UIImage imageNamed:@"facebook-share-icon"] forState:UIControlStateNormal];
    [buttonfacebook scaleToFill];
    
    
    // Button Twitter
    buttontwitter = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttontwitter addTarget:self
                      action:@selector(actionButtonTwitter:)
            forControlEvents:UIControlEventTouchUpInside];
    buttontwitter.frame = CGRectMake(buttonfacebook.frame.origin.x - 55, _imageMoviePoster.frame.origin.y +  _imageMoviePoster.frame.size.height - 30, 30 , 30);
    [buttontwitter setImage:[UIImage imageNamed:@"twitter-share-icon"] forState:UIControlStateNormal];
    [buttontwitter scaleToFill];
    
  
    
    [self addSubview:buttonWhatsApp];
    [self addSubview:buttonEmail];
    [self addSubview:buttonfacebook];
    [self addSubview:buttontwitter];
    [self addSubview:self.imageMoviePoster];
    [self addSubview:buttonLiveButton];
    [self addSubview:buttonShareIcon];
    

    // ImageView Clock
    UIImageView *imageViewClock = [[UIImageView alloc] initWithFrame:CGRectMake(self.imageMoviePoster.frame.origin.x  , self.imageMoviePoster.frame.origin.y + self.imageMoviePoster.frame.size.height + 10  + 7, 14, 14)];
    imageViewClock.image = [UIImage imageNamed:@"Clock"];
    
    // Label Date
    _labelDate = [[UILabel alloc] init];
    _labelDate.frame = CGRectMake(imageViewClock.frame.origin.x + imageViewClock.frame.size.width + 5 , imageViewClock.frame.origin.y, 50, 14);
    _labelDate.font = [UIFont fontWithName:kFontSFUIDisplayMedium size:10];
    _labelDate.textColor = [UIColor colorWithRed:178/255.0 green:178/255.0 blue:178/255.0 alpha:1/1.0];
    
    // Label Time
    _labelTime = [[UILabel alloc] init];
    _labelTime.frame = CGRectMake(_labelDate.frame.origin.x + _labelDate.frame.size.width + 15 , imageViewClock.frame.origin.y, 50, 14);
    _labelTime.font = [UIFont fontWithName:kFontSFUIDisplayMedium size:10];
    _labelTime.textColor = [UIColor colorWithRed:178/255.0 green:178/255.0 blue:178/255.0 alpha:1/1.0];
   
    // ImageView Calendar
    CGSize sizeOfImageCalendar = CGSizeMake(30, 30);
    imageViewCalendar = [[UIImageView alloc] initWithFrame:CGRectMake(self.imageMoviePoster.frame.origin.x + self.imageMoviePoster.frame.size.width - sizeOfImageCalendar.width  , self.imageMoviePoster.frame.origin.y + self.imageMoviePoster.frame.size.height + 10, sizeOfImageCalendar.width, sizeOfImageCalendar.height)];
    imageViewCalendar.image = [UIImage imageNamed:@"Calendar_Icon"];
    
    // Label Time
    CGSize sizeOflabelAddEventToCalendar = CGSizeMake(105, 16);
    labelAddEventToCalendar = [[UILabel alloc] init];
    labelAddEventToCalendar.frame = CGRectMake(imageViewCalendar.frame.origin.x -  sizeOflabelAddEventToCalendar.width - 5 , imageViewCalendar.frame.origin.y + 7, sizeOflabelAddEventToCalendar.width, sizeOflabelAddEventToCalendar.height);
    labelAddEventToCalendar.font = [UIFont fontWithName:kFontSFUIDisplayMedium size:14];
    labelAddEventToCalendar.textColor = [UIColor colorWithRed:19/255.0 green:174/255.0 blue:229/255.0 alpha:1/1.0];
    labelAddEventToCalendar.text = @"Add to Calender";
    
    // Button Share Share
    buttonAddEventToCalender = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonAddEventToCalender addTarget:self
                        action:@selector(actionButtonAddEvent:)
              forControlEvents:UIControlEventTouchUpInside];
    buttonAddEventToCalender.frame = CGRectMake(labelAddEventToCalendar.frame.origin.x, imageViewCalendar.frame.origin.y - 10, imageViewCalendar.frame.size.width + sizeOflabelAddEventToCalendar.width + 10 , 50);
    
    [self addSubview:imageViewClock];
    [self addSubview:_labelDate];
    [self addSubview:_labelTime];
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) addLiveButton
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    // Button Login Frame
    _buttonLive = [UIButton buttonWithType:UIButtonTypeCustom];
    [_buttonLive addTarget:self
                   action:@selector(actionButtonLive:)
         forControlEvents:UIControlEventTouchUpInside];
    [_buttonLive setTitle:@"NOT LIVE" forState:UIControlStateNormal];
    [_buttonLive setTitleColor:UIColorFromRGB(0xD3D3D3) forState:UIControlStateNormal];
    
    CGSize labelSizeOfLogin = CGSizeMake(deviceBounds.size.width * 0.85, 55.0);
    
    if(IS_IPHONE_5)
    {
        labelSizeOfLogin = CGSizeMake(deviceBounds.size.width * 0.85, 44.0);
    }

    CGPoint labelPointOfLogin = CGPointMake(deviceBounds.size.width/2 - labelSizeOfLogin.width/2,  _viewTimeLeft.frame.origin.y + _viewTimeLeft.frame.size.height + 5);
    
    if (IS_IPAD)
    {
       labelPointOfLogin = CGPointMake(deviceBounds.size.width/2 - labelSizeOfLogin.width/2,  _viewTimeLeft.frame.origin.y + _viewTimeLeft.frame.size.height + 175);
    }
    
    
    _buttonLive.frame = CGRectMake(labelPointOfLogin.x, labelPointOfLogin.y, labelSizeOfLogin.width, labelSizeOfLogin.height);
    [_buttonLive.titleLabel setFont:[UIFont fontWithName:kFontSFUIDisplayRegular size:14]];
    _buttonLive.layer.borderWidth = 1.0f;
    _buttonLive.layer.borderColor = UIColorFromRGB(0xD3D3D3).CGColor;
    _buttonLive.layer.cornerRadius = 4; // this value vary as per your desire
    _buttonLive.clipsToBounds = YES;
    
    [self addSubview:_buttonLive];
}

# pragma mark - UIButton Actions

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonShare:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if ([buttonShareIcon isSelected])
    {
        [buttonShareIcon setSelected:false];
        
        CGRect frame = sender.frame;
        frame.origin.y = frame.origin.y + 25;
        sender.frame = frame;
        
        [self annimateOut];
        
        imageShareIcon.image = [UIImage imageNamed:@"share-icon"];
    }
    
    else
    {
        [buttonShareIcon setSelected:true];
        
        CGRect frame = sender.frame;
        frame.origin.y = frame.origin.y - 25;
        sender.frame = frame;
        
        imageShareIcon.image = [UIImage imageNamed:@"close-icon"];
        
        [self annimateIn];
    }
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpUIForTimer:(CGRect)frame
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    CGSize sizeOfviewTimer = CGSizeMake(320, 100.0);
    CGPoint pointOfviewTimer = CGPointMake(deviceBounds.size.width/2 -  sizeOfviewTimer.width/2 , frame.size.height * 0.55);
    _viewTimeLeft = [[TimeLeftView alloc] initWithFrame:CGRectMake(pointOfviewTimer.x, pointOfviewTimer.y, sizeOfviewTimer.width, sizeOfviewTimer.height)];
    

    [self addSubview:_viewTimeLeft];
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonWhatsApp:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonEmail:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonFacebook:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonTwitter:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    NSLog(@"Hello");
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonLive:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [AppDelegate appDelegate].allowedOrientation = UIInterfaceOrientationMaskLandscapeLeft;
    
    OrionViewControllerLive *vc = [[OrionViewControllerLive alloc] init];
    vc.dataModel = _modal;
    
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:vc];
     [nc setNavigationBarHidden:YES animated:false];
    
    [[self viewController].navigationController presentViewController:nc animated:false completion:nil];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) actionButtonAddEvent:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
            EKEventStore *store = [EKEventStore new];
            [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
                if (!granted) { return; }
                EKEvent *event = [EKEvent eventWithEventStore:store];
                event.title = _modal.title;
                event.startDate = [_liveEventDate dateByAddingTimeInterval:-10*60];
                event.endDate = [event.startDate dateByAddingTimeInterval:60*60];
                event.calendar = [store defaultCalendarForNewEvents];
                
                
                STARTBACKGROUNDTHREAD
                
                NSError *err = nil;
                [store saveEvent:event span:EKSpanThisEvent commit:YES error:&err];
    
                [[Settings sharedManger] addEventID:event.title];
                
                NSString * alertTitle;
                NSString *  msg;
                
                if (err) {
                    
                  alertTitle = @"Calendar was not set";
                   msg = @"Please set default calendar in settings.";
                }
                else
                {
                    alertTitle = @"Event added";
                    msg = @"Event has been added in your calendar.";
                }
                
                STARTMAINTHREAD
                
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:alertTitle message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
               
                [imageViewCalendar removeFromSuperview];
                [labelAddEventToCalendar removeFromSuperview];
                [buttonAddEventToCalender removeFromSuperview];
                
                ENDTHREAD
                
                ENDTHREAD
            }];
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) annimateIn
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
    [self.imageMoviePoster layer].anchorPoint = CGPointMake(0.5f, 0.65f);
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) annimateOut
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [self.imageMoviePoster layer].anchorPoint = CGPointMake(0.5f, 0.5f);
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) checkToShowAddToCalendar
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    BOOL shouldMakeNewEvent = true;
    
    for (NSString * eventTitle in [[Settings sharedManger] getEventIDs] )
    {
        if ([eventTitle isEqualToString:_modal.title])
        {
            shouldMakeNewEvent = false;
        }
    }
    
    if (shouldMakeNewEvent)
    {
        [self addSubview:imageViewCalendar];
        [self addSubview:labelAddEventToCalendar];
        [self addSubview:buttonAddEventToCalender];
    }  
}


@end
