//
//  UpComing360VUZ.h
//  360 VUZ
//
//  Created by Mosib on 7/13/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TimeLeftView.h"
#import "LiveVideoDataModel.h"

@interface UpComing360VUZ : UIView

@property (strong, nonatomic) UILabel *labelTitle;
@property (strong, nonatomic) UILabel *label360video;
@property (strong, nonatomic) UIImageView *imageMoviePoster;
@property (strong, nonatomic) TimeLeftView *viewTimeLeft;
@property (strong, nonatomic) UILabel *labelDate;
@property (strong, nonatomic) UILabel *labelTime;
@property (strong, nonatomic) UIButton *buttonLive;
@property (strong, nonatomic) LiveVideoDataModel *modal;
@property (strong, nonatomic) NSDate *liveEventDate;

- (void) checkToShowAddToCalendar;
@end
