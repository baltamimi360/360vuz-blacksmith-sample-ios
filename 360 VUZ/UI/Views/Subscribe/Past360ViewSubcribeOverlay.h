//
//  Past360ViewSubcribeOverlay.h
//  360 VUZ
//
//  Created by Mosib on 8/1/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Past360ViewSubcribeOverlay : UIView

@property (nonatomic) UILabel *labelDNT;

@end
