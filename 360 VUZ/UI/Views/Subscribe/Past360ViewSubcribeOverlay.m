//
//  Past360ViewSubcribeOverlay.m
//  360 VUZ
//
//  Created by Mosib on 8/1/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "Past360ViewSubcribeOverlay.h"
#import <MessageUI/MessageUI.h>

@interface Past360ViewSubcribeOverlay() <MFMailComposeViewControllerDelegate>
{
    UITextField * textfeildEmail;
}

@end

@implementation Past360ViewSubcribeOverlay

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (id)initWithFrame:(CGRect)frame
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self = [super initWithFrame:frame];
    //You code here
    
    if (self)
    {
        self.backgroundColor = UIColorFromRGB(0x13AEE5);
        
        // Label Subscribe
        UILabel *labelSubscribe = [[UILabel alloc] init];
        labelSubscribe.frame = CGRectMake(0, 10, self.frame.size.width, 20);
        labelSubscribe.text = @"SUBSCRIBE";
        labelSubscribe.textColor = [UIColor whiteColor];
        labelSubscribe.font = [UIFont fontWithName:kFontSFUIDisplaySemibold size:16];
        labelSubscribe.textAlignment = NSTextAlignmentCenter;
        
        // Label Do not Miss
        _labelDNT = [[UILabel alloc] init];
        _labelDNT.frame = CGRectMake(0, 50, self.frame.size.width, 20);
        _labelDNT.text = @", Do not miss next 360 live!";
        _labelDNT.textColor = [UIColor whiteColor];
        _labelDNT.font = [UIFont fontWithName:kFontSFUIDisplayRegular size:14];
        _labelDNT.textAlignment = NSTextAlignmentCenter;
        
        // Label Get Notified
        UILabel *labelGN = [[UILabel alloc] init];
        labelGN.frame = CGRectMake(0, 70, self.frame.size.width, 20);
        labelGN.text = @"Get notified on the next live!";
        labelGN.font = [UIFont fontWithName:kFontSFUIDisplayRegular size:14];
        labelGN.textColor = [UIColor whiteColor];
        labelGN.textAlignment = NSTextAlignmentCenter;
        
        // TextFeild Email
        textfeildEmail = [[UITextField alloc] init];
        textfeildEmail.frame = CGRectMake(20, 105, self.frame.size.width - 40, 20);
        textfeildEmail.font = [UIFont fontWithName:kFontSFUIDisplayRegular size:14];
        UIColor *color = [UIColor whiteColor];
        textfeildEmail.attributedPlaceholder =
        [[NSAttributedString alloc]
         initWithString:@"Type your email..."
         attributes:@{NSForegroundColorAttributeName:color}];
        textfeildEmail.textColor = [UIColor whiteColor];
        textfeildEmail.backgroundColor = [UIColor clearColor];
        textfeildEmail.keyboardType = UIKeyboardTypeEmailAddress;
        CALayer *border = [CALayer layer];
        CGFloat borderWidth = 1;
        border.borderColor = [UIColor whiteColor].CGColor;
        border.frame = CGRectMake(0, textfeildEmail.frame.size.height - borderWidth, textfeildEmail.frame.size.width, textfeildEmail.frame.size.height);
        border.borderWidth = borderWidth;
        [textfeildEmail.layer addSublayer:border];
        textfeildEmail.layer.masksToBounds = YES;
        
        // Button Submit
        UIButton *buttonSubmit = [UIButton buttonWithType:UIButtonTypeCustom];
        [buttonSubmit addTarget:self
                         action:@selector(actionButtonSubmit:)
               forControlEvents:UIControlEventTouchUpInside];
        [buttonSubmit setTitle:@"Submit" forState:UIControlStateNormal];
        buttonSubmit.backgroundColor = [UIColor whiteColor];
        CGSize SizeOfButtonSubmit = CGSizeMake(100, 40.0);
        CGPoint PointOfButtonSubmit = CGPointMake(self.frame.size.width/2 - SizeOfButtonSubmit.width/2, 140);
        buttonSubmit.frame = CGRectMake(PointOfButtonSubmit.x, PointOfButtonSubmit.y, SizeOfButtonSubmit.width, SizeOfButtonSubmit.height);
        [buttonSubmit.titleLabel setFont:[UIFont fontWithName:kFontSFUIDisplayBold size:14]];
        [buttonSubmit setTitleColor:UIColorFromRGB(0x13AEE5) forState:UIControlStateNormal];
        buttonSubmit.layer.cornerRadius = 4;
        buttonSubmit.clipsToBounds = YES;
        
        
        [self addSubview:labelSubscribe];
        [self addSubview:labelGN];
        [self addSubview:textfeildEmail];
        [self addSubview:_labelDNT];
        [self addSubview:buttonSubmit];
        
    }
    
    return self;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonSubmit:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if ([[UtilityClass sharedManger] validateEmailWithString:textfeildEmail.text])
    {
        NSArray *toRecipents = [NSArray arrayWithObject:@"info@360mea.com"];
        
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        if ([MFMailComposeViewController canSendMail]) {
            
            [mc setToRecipients:toRecipents];
            [mc setSubject:@"Live Broadcast Notification."];
            if([sender tag] == 100){
                [mc setMessageBody:[NSString stringWithFormat:@"My email is %@. Please keep me notified of the next live broadcast.",textfeildEmail.text ] isHTML:NO];
            }
            else{
                [mc setMessageBody:[NSString stringWithFormat:@"My email is %@. Please keep me notified of the next live broadcast.",textfeildEmail.text ] isHTML:NO];
            }
            
            [[self viewController] presentViewController:mc animated:YES completion:NULL];
        }
        
    }else{
        UIAlertView *alert11 = [[UIAlertView alloc] initWithTitle:@"360 VUZ" message:@"Email is not valid!" delegate:nil cancelButtonTitle:@"OK"otherButtonTitles:nil,nil];
        [alert11 show];
    }
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            break;
        default:
            break;
    }
    
    [[self viewController] dismissViewControllerAnimated:YES completion:NULL];
}



@end
