//
//  PVCNoVideosAdded.m
//  360 VUZ
//
//  Created by Mosib on 7/9/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "PVCNoVideosAdded.h"

@implementation PVCNoVideosAdded

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (id)initWithFrame:(CGRect)frame
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if ((self = [super initWithFrame:frame])) {
        [self setUpUI];
    }
    return self;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpUI
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    CGRect frameOfView = self.frame;
    
    // ImageView No Videos Added
    UIImageView *imageViewNoVideosAdded = [[UIImageView alloc] init];
    CGSize sizeOfimageViewNoVideosAdded = CGSizeMake(100.0, 100.0);
    CGPoint PointOfimageViewNoVideosAdded = CGPointMake(frameOfView.size.width/2 - sizeOfimageViewNoVideosAdded.width/2, frameOfView.size.height/3 - sizeOfimageViewNoVideosAdded.height/2);
    imageViewNoVideosAdded.image = [UIImage imageNamed:@"no-videos-icon"];
    imageViewNoVideosAdded.frame = CGRectMake(PointOfimageViewNoVideosAdded.x, PointOfimageViewNoVideosAdded.y, sizeOfimageViewNoVideosAdded.width, sizeOfimageViewNoVideosAdded.height);
    imageViewNoVideosAdded.contentMode = UIViewContentModeScaleAspectFit;
    
    // Label Lives Value
    UILabel *labelLivesValue = [[UILabel alloc] init];
    CGSize sizeOflabelLivesValue = CGSizeMake(frameOfView.size.width, 24.0);
    CGPoint PointOflabelLivesValue = CGPointMake(0,PointOfimageViewNoVideosAdded.y + sizeOfimageViewNoVideosAdded.height + 10);
    labelLivesValue.frame = CGRectMake(PointOflabelLivesValue.x, PointOflabelLivesValue.y, sizeOflabelLivesValue.width, sizeOflabelLivesValue.height);
    labelLivesValue.textAlignment = NSTextAlignmentCenter;
    labelLivesValue.minimumScaleFactor = 6;
    labelLivesValue.adjustsFontSizeToFitWidth = YES;
    labelLivesValue.text = @"No videos added";
    labelLivesValue.font = [UIFont fontWithName:kFontSFUIDisplayBold size:18];
    labelLivesValue.textColor = UIColorFromRGB(0x13AEE5);
    

    [self addSubview:imageViewNoVideosAdded];
    [self addSubview:labelLivesValue];
   
    
}

@end
