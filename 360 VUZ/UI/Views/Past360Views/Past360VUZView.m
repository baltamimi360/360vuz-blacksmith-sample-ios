//
//  Past360VUZView.m
//  360 VUZ
//
//  Created by Mosib on 7/30/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "Past360VUZView.h"
#import "Past360ViewSubcribeOverlay.h"
#import "Past360TableViewCell.h"
#import "ServiceLive360.h"
#import "PastLiveEventsService.h"
#import "LiveHomeViewController.h"

@interface Past360VUZView () <UITableViewDataSource, UITableViewDelegate>
{
    CGRect deviceBounds;
    int pageNumber;
    BOOL lastResultFetchedEmpty;
    __weak Past360VUZView * weakSelf;
}
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *dataArray;

@end

@implementation Past360VUZView

static BOOL fetchInProgress;

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (id)initWithFrame:(CGRect)frame
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self = [super initWithFrame:frame];
    //You code here
    
    if (self)
    {
        
        self.backgroundColor = [UIColor whiteColor];
    
        // Initialize TableView
        [self setUpTableView];
        
        //
        [self initializeVariables];
        
        //
        [self setUpSubscribeView];
        
        //
        [self serviceCategoriesAPI];

    }
    
    return self;
}


#pragma mark - API Methods -
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) serviceCategoriesAPI
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    STARTBACKGROUNDTHREAD
    PastLiveEventsService *live360 = [[PastLiveEventsService alloc]init];
    
    [live360 executeRequest:@{@"limit" : @"5" , @"page_no": [@(pageNumber) stringValue]}
                    success:^(NSDictionary  *responseObject)
     {
         
         [weakSelf SuccessForHomePageAPI:responseObject];
         
     }
                    failure:^(NSError * error) {
                        
                        [weakSelf FailureForHomePageAPI:error];
                        
                        
                        
                    }];
    ENDTHREAD
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) SuccessForHomePageAPI:(NSDictionary *) responseObject
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
    if ([[responseObject objectForKey:@"data"] count] == 0)
    {
        lastResultFetchedEmpty = true;
    }
    
    else
    {
        pageNumber++;
    }
    
    if (!lastResultFetchedEmpty)
    {
        STARTMAINTHREAD
        
        [_tableView beginUpdates];
        
        int resultsSize = (int)[self.dataArray count];
   
        [self.dataArray addObjectsFromArray:[self  makeDataValuesForLiveVideo:[responseObject objectForKey:@"data"]]];
        
        NSMutableArray *arrayWithIndexPaths = [NSMutableArray array];
        
        for (int i = resultsSize; i < resultsSize + [[responseObject objectForKey:@"data"] count]; i++)
            [arrayWithIndexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
        
        [self.tableView insertRowsAtIndexPaths:arrayWithIndexPaths withRowAnimation:UITableViewRowAnimationTop];
        
        [_tableView endUpdates];
        
        fetchInProgress = false;
        
        ENDTHREAD
        
    }
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) FailureForHomePageAPI:(NSError *)error
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    STARTMAINTHREAD
    
    fetchInProgress = false;
    
    ENDTHREAD
}


#pragma mark - Private Methods -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) initializeVariables
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    weakSelf = self;
    
    fetchInProgress = false;
    
    pageNumber = 0;
    
    _dataArray = [[NSMutableArray alloc] init];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if(self.tableView.contentOffset.y >= (self.tableView.contentSize.height - self.tableView.bounds.size.height)) {

        
        if (fetchInProgress)
            return;
        
        fetchInProgress = TRUE;
        
        // Service API
        [self serviceCategoriesAPI];
    }
}



#pragma mark - UITableView DataSource Methods -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
  
    
    Past360TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kRIDPast360TableViewCell];
            
            if (cell == nil) {
                cell = [[Past360TableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:kRIDPast360TableViewCell];
                
            }
    
    cell.dataModel = [self.dataArray objectAtIndex:indexPath.row];
    
    __weak Past360TableViewCell *weakCell = cell;
    
    [cell.imageMoviePoster setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:cell.dataModel.image]]
                                 placeholderImage:[UtilityClass imageFromColor:[UIColor blackColor]]
                                          success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                              
                                              weakCell.imageMoviePoster.image = image;
                                              [weakCell setNeedsLayout];
                                              
                                          } failure:nil];

            
            cell.labelTitle.text = cell.dataModel.title;
            cell.label360video.text = cell.dataModel.liveDescription;
    
    cell.labelDate.text = [[UtilityClass sharedManger] stringFromDate:[[UtilityClass sharedManger] dateFromString:cell.dataModel.datetime withDateFormat:kTimeFormatISO8601] withDateFormat:kTimeFormatDayMonth];
    
    cell.labelTime.text = [[UtilityClass sharedManger] stringFromDate:[[UtilityClass sharedManger] dateFromString:cell.dataModel.datetime withDateFormat:kTimeFormatISO8601] withDateFormat:kTimeFormatHourSecsMeridian];
    
    
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor = [UIColor whiteColor];
            
            return cell;
            
    
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return [_dataArray count];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return 1;
}

#pragma mark - UITableView Delegate Methods -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    LiveHomeViewController *vc = [[LiveHomeViewController alloc] init];
    vc.modal = (LiveVideoDataModel *)[self.dataArray objectAtIndex:indexPath.row];
    [[self viewController].navigationController pushViewController:vc animated:true];
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return MAX(heightOfCellVideo,RatioOfCellWhatsHot * [UtilityClass getDeviceBounds].size.height) + 40;
}

#pragma mark - Private Methods -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) setUpTableView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    CGFloat x = 0;
    CGFloat y = 0;
    CGFloat width = self.frame.size.width;
    CGFloat height = self.frame.size.height;
    CGRect tableFrame = CGRectMake(x, y, width, height);
    
    
    UITableView *tableView = [[UITableView alloc]initWithFrame:tableFrame style:UITableViewStylePlain];
    tableView.scrollEnabled = YES;
    tableView.userInteractionEnabled = YES;
    tableView.bounces = YES;
    [tableView setShowsHorizontalScrollIndicator:NO];
    [tableView setShowsVerticalScrollIndicator:NO];
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    
    tableView.contentInset = UIEdgeInsetsMake(0, 0., 245, 0);
    
    if([tableView respondsToSelector:@selector(setCellLayoutMarginsFollowReadableWidth:)])
    {
        tableView.cellLayoutMarginsFollowReadableWidth = NO;
    }
    
    tableView.delegate = self;
    tableView.dataSource = self;
    
    self.tableView = tableView;
    
    [self addSubview:self.tableView];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpSubscribeView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    Past360ViewSubcribeOverlay * viewPast360ViewOverlay = [[Past360ViewSubcribeOverlay alloc] initWithFrame:CGRectMake(8,self.frame.size.height - 250,[UtilityClass getDeviceBounds].size.width - 16 , 200)];

    viewPast360ViewOverlay.labelDNT.text = [NSString stringWithFormat:@"Khaled %@",viewPast360ViewOverlay.labelDNT.text];
    
    [self addSubview:viewPast360ViewOverlay];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSArray *) makeDataValuesForLiveVideo:(NSArray *) dataArray
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    NSMutableArray *dataArrayWithDataModel = [[NSMutableArray alloc] init];
    
    for (NSDictionary * object in dataArray)
    {
        LiveVideoDataModel *liveVideoDataModel = [[LiveVideoDataModel alloc] init];
        
        liveVideoDataModel.videoID = [object objectForKey:kLVKeyID];
        liveVideoDataModel.image = [object objectForKey:kLVKeyImage];
        liveVideoDataModel.imageShortName = [object objectForKey:kLVKeyImageName];
        liveVideoDataModel.title = [object objectForKey:kLVKeyTitle];
        liveVideoDataModel.liveDescription = [object objectForKey:kLVKeyDescription];
        liveVideoDataModel.videoLink = [object objectForKey:kLVKeyVideoLink];
        liveVideoDataModel.datetime = [object objectForKey:kLVKeyDateTime];
        liveVideoDataModel.liveType = [object objectForKey:kLVKeyLiveType];
        liveVideoDataModel.credit = [object objectForKey:kLVKeyCredit];
        liveVideoDataModel.androidProductID = [object objectForKey:kLVKeyAndriodProductID];
        liveVideoDataModel.iosProductID = [object objectForKey:kLVKeyiOSProductID];
        liveVideoDataModel.price = [object objectForKey:kLVKeyPrice];
        liveVideoDataModel.twitterTags = [object objectForKey:kLVKeyTwitterTags];
        liveVideoDataModel.channelID = [object objectForKey:kLVKeyChannelID];
        liveVideoDataModel.status = [object objectForKey:kLVKeyStatus];
        liveVideoDataModel.notificationFlag = [object objectForKey:kLVKeyNotificationFlag];
        liveVideoDataModel.dateStatus = [object objectForKey:kLVKeyDateStatus];
        liveVideoDataModel.purchaseStatus = [object objectForKey:kLVKeyPurchaseStatus];
        
        [dataArrayWithDataModel addObject:liveVideoDataModel];
    }
    
    return [dataArrayWithDataModel copy];
}


@end
