//
//  PVCProfileBannerView.m
//  360 VUZ
//
//  Created by Mosib on 7/9/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "PVCProfileBannerView.h"

@interface PVCProfileBannerView ()

@end

@implementation PVCProfileBannerView

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (id)initWithFrame:(CGRect)frame
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if ((self = [super initWithFrame:frame])) {
        self.backgroundColor = [UIColor whiteColor];
        [self setUpUI];
    }
    return self;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpUI
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    CGRect frameOfView = self.frame;
    
    // Top Seperator
    UIView *seperatorViewTop = [[UIView alloc] init];
    seperatorViewTop.backgroundColor = UIColorFromRGB(0xF3F4F3);
    seperatorViewTop.frame = CGRectMake(10, 0, frameOfView.size.width - 20, 1);
    
    // Bottom Seperator
    UIView *seperatorViewBottom = [[UIView alloc] init];
    seperatorViewBottom.backgroundColor = UIColorFromRGB(0xF3F4F3);
    seperatorViewBottom.frame = CGRectMake(10, frameOfView.size.height - 1, frameOfView.size.width - 20, 1);
    
    // Center Seperator
    UIView *seperatorViewCentre = [[UIView alloc] init];
    seperatorViewCentre.backgroundColor = UIColorFromRGB(0xF3F4F3);
    seperatorViewCentre.frame = CGRectMake(frameOfView.size.width/2 - 1, frameOfView.size.height/4 , 1, frameOfView.size.height/2);
    
    // Left Seperator
    UIView *seperatorViewLeft = [[UIView alloc] init];
    seperatorViewLeft.backgroundColor = UIColorFromRGB(0xF3F4F3);
    seperatorViewLeft.frame = CGRectMake(frameOfView.size.width/4 * 1 - 1, frameOfView.size.height/4 , 1, frameOfView.size.height/2);
    
    // Right Seperator
    UIView *seperatorViewRight = [[UIView alloc] init];
    seperatorViewRight.backgroundColor = UIColorFromRGB(0xF3F4F3);
    seperatorViewRight.frame = CGRectMake(frameOfView.size.width/4 * 3, frameOfView.size.height/4 , 1, frameOfView.size.height/2);
    
    
    // Label Spins
    UILabel *labelSpins = [[UILabel alloc] init];
    CGSize sizeOflabelSpins = CGSizeMake(seperatorViewLeft.frame.origin.x, 14.0);
    CGPoint PointOflabelSpins = CGPointMake(0, 30);
    labelSpins.frame = CGRectMake(PointOflabelSpins.x, PointOflabelSpins.y, sizeOflabelSpins.width, sizeOflabelSpins.height);
    labelSpins.textAlignment = NSTextAlignmentCenter;
    labelSpins.minimumScaleFactor = 6;
    labelSpins.adjustsFontSizeToFitWidth = YES;
    labelSpins.text = @"SPINS";
    labelSpins.font = [UIFont fontWithName:kFontSFUIDisplayLight size:10];
    labelSpins.textColor = UIColorFromRGB(0x9B9B9B);
    
    // Label Cash
    UILabel *labelCash = [[UILabel alloc] init];
    CGSize sizeOflabelCash = CGSizeMake(seperatorViewLeft.frame.origin.x, 14.0);
    CGPoint PointOflabelCash = CGPointMake(seperatorViewLeft.frame.origin.x,30);
    labelCash.frame = CGRectMake(PointOflabelCash.x, PointOflabelCash.y, sizeOflabelCash.width, sizeOflabelCash.height);
    labelCash.textAlignment = NSTextAlignmentCenter;
    labelCash.minimumScaleFactor = 6;
    labelCash.adjustsFontSizeToFitWidth = YES;
    labelCash.text = @"CASH";
    labelCash.font = [UIFont fontWithName:kFontSFUIDisplayLight size:10];
    labelCash.textColor = UIColorFromRGB(0x9B9B9B);
    
    // Label 360s
    UILabel *label360s = [[UILabel alloc] init];
    CGSize sizeOflabel360s = CGSizeMake(seperatorViewLeft.frame.origin.x, 14.0);
    CGPoint PointOflabel360s = CGPointMake(seperatorViewCentre.frame.origin.x,30);
    label360s.frame = CGRectMake(PointOflabel360s.x, PointOflabel360s.y, sizeOflabel360s.width, sizeOflabel360s.height);
    label360s.textAlignment = NSTextAlignmentCenter;
    label360s.minimumScaleFactor = 6;
    label360s.adjustsFontSizeToFitWidth = YES;
    label360s.text = @"360s";
    label360s.font = [UIFont fontWithName:kFontSFUIDisplayLight size:10];
    label360s.textColor = UIColorFromRGB(0x9B9B9B);
    
    // Label Lives
    UILabel *labelLives = [[UILabel alloc] init];
    CGSize sizeOflabelLives = CGSizeMake(seperatorViewLeft.frame.origin.x, 14.0);
    CGPoint PointOflabelLives = CGPointMake(seperatorViewRight.frame.origin.x,30);
    labelLives.frame = CGRectMake(PointOflabelLives.x, PointOflabelLives.y, sizeOflabelLives.width, sizeOflabelLives.height);
    labelLives.textAlignment = NSTextAlignmentCenter;
    labelLives.minimumScaleFactor = 6;
    labelLives.adjustsFontSizeToFitWidth = YES;
    labelLives.text = @"LIVES";
    labelLives.font = [UIFont fontWithName:kFontSFUIDisplayLight size:10];
    labelLives.textColor = UIColorFromRGB(0x9B9B9B);
    
    // Label Spins Value
    _labelSpinsValue = [[UILabel alloc] init];
    CGSize sizeOflabelSpinsValue = CGSizeMake(seperatorViewLeft.frame.origin.x, 14.0);
    CGPoint PointOflabelSpinsValue = CGPointMake(0, 10);
    _labelSpinsValue.frame = CGRectMake(PointOflabelSpinsValue.x, PointOflabelSpinsValue.y, sizeOflabelSpinsValue.width, sizeOflabelSpinsValue.height);
    _labelSpinsValue.textAlignment = NSTextAlignmentCenter;
    _labelSpinsValue.minimumScaleFactor = 6;
    _labelSpinsValue.adjustsFontSizeToFitWidth = YES;
    _labelSpinsValue.font = [UIFont fontWithName:kFontSFUIDisplayBold size:12];
    _labelSpinsValue.textColor = UIColorFromRGB(0x13AEE5);
    
    // Label Cash Value
    _labelCashValue = [[UILabel alloc] init];
    CGSize sizeOflabelCashValue = CGSizeMake(seperatorViewLeft.frame.origin.x, 14.0);
    CGPoint PointOflabelCashValue = CGPointMake(seperatorViewLeft.frame.origin.x,10);
    _labelCashValue.frame = CGRectMake(PointOflabelCashValue.x, PointOflabelCashValue.y, sizeOflabelCashValue.width, sizeOflabelCashValue.height);
    _labelCashValue.textAlignment = NSTextAlignmentCenter;
    _labelCashValue.minimumScaleFactor = 6;
    _labelCashValue.adjustsFontSizeToFitWidth = YES;
    _labelCashValue.font = [UIFont fontWithName:kFontSFUIDisplayBold size:12];
    _labelCashValue.textColor = UIColorFromRGB(0x13AEE5);
    
    // Label 360s Value
    _label360sValue = [[UILabel alloc] init];
    CGSize sizeOflabel360sValue = CGSizeMake(seperatorViewLeft.frame.origin.x, 14.0);
    CGPoint PointOflabel360sValue = CGPointMake(seperatorViewCentre.frame.origin.x,10);
    _label360sValue.frame = CGRectMake(PointOflabel360sValue.x, PointOflabel360sValue.y, sizeOflabel360sValue.width, sizeOflabel360sValue.height);
    _label360sValue.textAlignment = NSTextAlignmentCenter;
    _label360sValue.minimumScaleFactor = 6;
    _label360sValue.adjustsFontSizeToFitWidth = YES;
    _label360sValue.font = [UIFont fontWithName:kFontSFUIDisplayBold size:12];
    _label360sValue.textColor = UIColorFromRGB(0x13AEE5);
    
    // Label Lives Value
    _labelLivesValue = [[UILabel alloc] init];
    CGSize sizeOflabelLivesValue = CGSizeMake(seperatorViewLeft.frame.origin.x, 14.0);
    CGPoint PointOflabelLivesValue = CGPointMake(seperatorViewRight.frame.origin.x,10);
    _labelLivesValue.frame = CGRectMake(PointOflabelLivesValue.x, PointOflabelLivesValue.y, sizeOflabelLivesValue.width, sizeOflabelLivesValue.height);
    _labelLivesValue.textAlignment = NSTextAlignmentCenter;
    _labelLivesValue.minimumScaleFactor = 6;
    _labelLivesValue.adjustsFontSizeToFitWidth = YES;
    _labelLivesValue.font = [UIFont fontWithName:kFontSFUIDisplayBold size:12];
    _labelLivesValue.textColor = UIColorFromRGB(0x13AEE5);
    

    [self addSubview:seperatorViewTop];
    [self addSubview:seperatorViewBottom];
    [self addSubview:seperatorViewCentre];
    [self addSubview:seperatorViewLeft];
    [self addSubview:seperatorViewRight];
    [self addSubview:labelLives];
    [self addSubview:labelSpins];
    [self addSubview:labelCash];
    [self addSubview:label360s];
    [self addSubview:_labelLivesValue];
    [self addSubview:_labelSpinsValue];
    [self addSubview:_labelCashValue];
    [self addSubview:_label360sValue];
    
}

@end
