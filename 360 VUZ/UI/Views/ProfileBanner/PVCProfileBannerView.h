//
//  PVCProfileBannerView.h
//  360 VUZ
//
//  Created by Mosib on 7/9/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PVCProfileBannerView : UIView

@property (nonatomic) UILabel *labelSpinsValue;
// Label Cash Value
@property (nonatomic)UILabel *labelCashValue;
// Label 360s Value
@property (nonatomic) UILabel *label360sValue;
// Label Lives Value
@property (nonatomic) UILabel *labelLivesValue;


@end
