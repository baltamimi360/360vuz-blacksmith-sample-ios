//
//  TrendingCollectionViewCell.m
//  360 VUZ
//
//  Created by Mosib on 7/23/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "TrendingCollectionViewCell.h"

@implementation TrendingCollectionViewCell

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (id)initWithFrame:(CGRect)frame
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self = [super initWithFrame:frame];
    //You code here
    
    if (self)
    {
        // Set Up UI
        [self setUpUI];
        
        // Round Corners
        [self roundedCornersAllWithDefaultRaduis];
    }
    
    return self;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpUI
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    CGRect frame = self.frame;
    
    self.backgroundColor = [UIColor whiteColor];
    
    // Back Ground Image
    self.imageMoviePoster = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width , frame.size.height)];
    
    // Add Shade
    UIImageView *imageViewShade = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width , frame.size.height)];
    imageViewShade.image = [UIImage imageNamed:@"bg_shade"];
    [_imageMoviePoster addSubview:imageViewShade];
    
    // ImageView Play
    UIImageView *imagePlayIcon = [[UIImageView alloc] initWithFrame:CGRectMake(10, _imageMoviePoster.frame.size.height - 40, 25 , 25)];
    imagePlayIcon.image = [UIImage imageNamed:@"play-icon"];
    
    [_imageMoviePoster addSubview:imagePlayIcon];

    
    // Label Title
    self.labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(imagePlayIcon.frame.origin.x + imagePlayIcon.frame.size.width + 5, imagePlayIcon.frame.origin.y - 5, frame.size.width - imagePlayIcon.frame.origin.x -imagePlayIcon.frame.size.width - 20, 20)];
    [self.labelTitle setTextColor:[UIColor whiteColor]];
    self.labelTitle.adjustsFontSizeToFitWidth = YES;
    self.labelTitle.minimumScaleFactor = 0.8;
    self.labelTitle.font = [UIFont fontWithName:kFontSFUIDisplayRegular size:18];
    [_imageMoviePoster addSubview:self.labelTitle];
    
    // Label 360 Video
    self.label360video = [[UILabel alloc] initWithFrame:CGRectMake(self.labelTitle.frame.origin.x , self.labelTitle.frame.origin.y + self.labelTitle.frame.size.height, 60, 14)];
    [self.label360video setNumberOfLines:1];
    self.label360video.font = [UIFont fontWithName:kFontSFUIDisplayRegular size:12];
    [self.label360video setTextAlignment:NSTextAlignmentLeft];
    [self.label360video setTextColor:[UIColor colorWithRed:19/255.0 green:174/255.0 blue:229/255.0 alpha:1/1.0]];
    [self.label360video setBackgroundColor:[UIColor clearColor]];
    self.label360video.adjustsFontSizeToFitWidth = YES;
    [_imageMoviePoster addSubview:self.label360video];
    
    // ImageView Spin
    UIImageView *imageSpin = [[UIImageView alloc] initWithFrame:CGRectMake(_label360video.frame.origin.x + _label360video.frame.size.width + 5, _label360video.frame.origin.y, 16, 15)];
    imageSpin.image = [UIImage imageNamed:@"spin-icon"];
    [_imageMoviePoster addSubview:imageSpin];
    
    // Label Spins
    self.labelSpins = [[UILabel alloc] initWithFrame:CGRectMake(imageSpin.frame.origin.x + imageSpin.frame.size.width + 5, _label360video.frame.origin.y, self.labelTitle.frame.size.width/2 - 10, 14)];
    [self.labelSpins setNumberOfLines:1];
    self.labelSpins.font = [UIFont fontWithName:kFontSFUIDisplayRegular size:12];;
    [self.labelSpins setTextColor:[UIColor colorWithRed:19/255.0 green:174/255.0 blue:229/255.0 alpha:1/1.0]];
    [self.labelSpins setBackgroundColor:[UIColor clearColor]];
    self.labelSpins.adjustsFontSizeToFitWidth = YES;
    [_imageMoviePoster addSubview:self.labelSpins];
    
    [self.contentView addSubview:self.imageMoviePoster];
}

@end
