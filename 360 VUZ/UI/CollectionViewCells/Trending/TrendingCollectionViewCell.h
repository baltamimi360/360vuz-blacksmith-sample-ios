//
//  TrendingCollectionViewCell.h
//  360 VUZ
//
//  Created by Mosib on 7/23/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TrendingVideosDataModel.h"

@interface TrendingCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) UILabel *labelTitle;
@property (strong, nonatomic) UILabel *label360video;
@property (strong, nonatomic) UILabel *labelSpins;
@property (strong, nonatomic) UIImageView *imageMoviePoster;
@property (nonatomic) TrendingVideosDataModel *dataModel;

@end
