//
//  Live360CollectionViewCell.h
//  360 VUZ
//
//  Created by Mosib on 7/23/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiveVideoDataModel.h"

@interface Live360CollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) UILabel *labelTitle;
@property (strong, nonatomic) UIImageView *imageMoviePoster;
@property (strong) LiveVideoDataModel *dataModel;

@end

