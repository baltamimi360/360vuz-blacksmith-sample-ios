//
//  CategoriesCollectionViewCell.m
//  360 VUZ
//
//  Created by Mosib on 7/10/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "CategoriesCollectionViewCell.h"

@implementation CategoriesCollectionViewCell

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (id)initWithFrame:(CGRect)frame
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self = [super initWithFrame:frame];
    //You code here
    
    if (self)
    {
        // Set Up UI
        [self setUpUI];
        
        // Round Corners
        [self roundedCornersAllWithDefaultRaduis];
    }
    
    return self;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpUI
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    CGRect frame = self.frame;
    
    // ImageView Background Frame
    _imageViewBackground = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
    _imageViewBackground.backgroundColor = [UIColor whiteColor];
    
    // Label Title Frame
    _labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, frame.size.height - 35, frame.size.width - 20, 25)];
    _labelTitle.font = [UIFont fontWithName:kFontSFUIDisplayBold size:20];
    _labelTitle.minimumScaleFactor = 0.8;
    _labelTitle.adjustsFontSizeToFitWidth = YES;
    _labelTitle.textColor = UIColorFromRGB(0xFFFFFF);

    // Label Spins Frame
    _labelSpins = [[UILabel alloc] initWithFrame:CGRectMake(10, _labelTitle.frame.origin.y + _labelTitle.frame.size.height , frame.size.width - 20, 12)];
    _labelSpins.font = [UIFont fontWithName:kFontSFUIDisplayRegular size:10];
    _labelTitle.minimumScaleFactor = 0.5;
    _labelTitle.adjustsFontSizeToFitWidth = YES;
    _labelSpins.textColor = UIColorFromRGB(0xFFFFFF);
    
    
    [self addSubview:_imageViewBackground];
    [self addSubview:_labelTitle];
    [self addSubview:_labelSpins];
}

@end
