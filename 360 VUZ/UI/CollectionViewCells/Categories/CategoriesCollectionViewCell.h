//
//  CategoriesCollectionViewCell.h
//  360 VUZ
//
//  Created by Mosib on 7/10/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CategoriesDataModel.h"

@interface CategoriesCollectionViewCell : UICollectionViewCell

@property (nonatomic) UIImageView * imageViewBackground;
@property (nonatomic) UILabel * labelTitle;
@property (nonatomic) UILabel * labelSpins;
@property (nonatomic) CategoriesDataModel *dataModel;

@end
