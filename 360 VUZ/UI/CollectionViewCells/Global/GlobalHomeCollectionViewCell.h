//
//  GlobalHomeCollectionViewCell.h
//  360 VUZ
//
//  Created by Mosib on 8/6/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GlobalTredingDataModel.h"

@interface GlobalHomeCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) UILabel *labelTitle;
@property (strong, nonatomic) UILabel *label360video;
@property (strong, nonatomic) UIImageView *imageMoviePoster;
@property (nonatomic) GlobalTredingDataModel *dataModel;

@end
