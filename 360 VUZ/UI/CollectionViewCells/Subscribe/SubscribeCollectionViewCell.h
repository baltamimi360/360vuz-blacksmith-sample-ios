//
//  SubscribeCollectionViewCell.h
//  360 VUZ
//
//  Created by Mosib on 7/24/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubscribeDataModel.h"

typedef NS_ENUM(NSInteger, Subscription)
{
    Disabled,
    Enabled
};

@interface SubscribeCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) UILabel *labelTitle;
@property (strong, nonatomic) UIButton *buttonSubscribe;
@property (strong, nonatomic) UILabel *labelSubscribe;
@property (strong, nonatomic) UIImageView *imageBackground;
@property (nonatomic, assign) Subscription type;
@property (strong) SubscribeDataModel *dataModel;

-(void) actionButtonSubcribe;

@end
