//
//  SubscribeCollectionViewCell.m
//  360 VUZ
//
//  Created by Mosib on 7/24/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "SubscribeCollectionViewCell.h"
#import "SubcribeOverlayView.h"


@interface SubscribeCollectionViewCell()
{
    SubcribeOverlayView * subscribeOverlay;
}
@end

@implementation SubscribeCollectionViewCell

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (id)initWithFrame:(CGRect)frame
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self = [super initWithFrame:frame];
    //You code here
    
    if (self)
    {
        // Set Up UI
        [self setUpUI];
        
    }
    
    return self;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpUI
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    CGRect frame = self.frame;
    
    self.backgroundColor = [UIColor whiteColor];
    
    // Back Ground Image
    _imageBackground = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width , frame.size.width)];
    
    // Round Corners
    [_imageBackground roundedCornersAllWithCornerRadii:CGSizeMake(15, 15)];
    
    // Label Title
    self.labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(1, _imageBackground.frame.size.height + 3, frame.size.width -2, 25)];
    [self.labelTitle setTextColor:UIColorFromRGB(0x4A4A4A)];
    self.labelTitle.adjustsFontSizeToFitWidth = YES;
    self.labelTitle.numberOfLines = 2;
    self.labelTitle.textAlignment = NSTextAlignmentCenter;
    self.labelTitle.font = [UIFont fontWithName:kFontSFUIDisplayRegular size:16];
   
    
    subscribeOverlay = [[SubcribeOverlayView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width , frame.size.width)];
    
    // Round Corners
    [subscribeOverlay roundedCornersAllWithCornerRadii:CGSizeMake(15, 15)];
    
    _labelSubscribe = [[UILabel alloc] initWithFrame:CGRectMake(10,_labelTitle.frame.origin.y + _labelTitle.frame.size.height + 5 , frame.size.width - 20, 30)];
    [_labelSubscribe setTextColor:[UIColor whiteColor]];
    _labelSubscribe.text = @"Subcribe";
    _labelSubscribe.adjustsFontSizeToFitWidth = YES;
    _labelSubscribe.textAlignment = NSTextAlignmentCenter;
    _labelSubscribe.font = [UIFont fontWithName:kFontSFUIDisplayRegular size:14];
    _labelSubscribe.backgroundColor = UIColorFromRGB(0x13AEE5);
    // Round Corners
    [_labelSubscribe roundedCornersAllWithDefaultRaduis];
 
    [self.contentView addSubview:_labelSubscribe];
    [self.contentView addSubview:self.imageBackground];
    [self.contentView addSubview:self.labelTitle];
    [self.contentView addSubview:subscribeOverlay];

}

#pragma mark - Private Methods -
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonSubcribe
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    switch(_type)
    {
            case Enabled:
        {
            [self disableSubscription];
            
            break;
        }
            
            case Disabled:
        {
            
            [self enableSubscription];
            
            break;
        }
    }
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) enableSubscription
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    _type = Enabled;
    
    [subscribeOverlay removeFromSuperview];
    
     _labelSubscribe.text = @"Subcribed";
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) disableSubscription
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
     _type = Disabled;
    
    [self.contentView addSubview:subscribeOverlay];
    
    _labelSubscribe.text = @"Subcribe";
}


@end
