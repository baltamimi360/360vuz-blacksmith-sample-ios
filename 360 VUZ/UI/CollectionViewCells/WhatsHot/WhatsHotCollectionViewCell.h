//
//  WhatsHotCollectionViewCell.h
//  360 VUZ
//
//  Created by Mosib on 7/20/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WhatsHotDataModel.h"

@interface WhatsHotCollectionViewCell : UICollectionViewCell <Orion1ViewDelegate>

@property (strong, nonatomic) UILabel *labelTitle;
@property (strong, nonatomic) UILabel *label360video;
@property (strong, nonatomic) UILabel *labelSpins;
@property (nonatomic) WhatsHotDataModel *dataModel;
@property (nonatomic) NSString * panaromaFileName;
@property (nonatomic) Orion1View* orionView;
@property (nonatomic) NSString *urlString;
@property (nonatomic) BOOL isReadyToPlayVideo;

-(void) playVideo;

@end
