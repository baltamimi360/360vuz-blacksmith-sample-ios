//
//  WhatsHotCollectionViewCell.m
//  360 VUZ
//
//  Created by Mosib on 7/20/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "WhatsHotCollectionViewCell.h"
#import "DetailVideoViewController.h"

@implementation WhatsHotCollectionViewCell

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (id)initWithFrame:(CGRect)frame
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        // Set Up UI
        [self setUpUI];
        
    }
    
    return self;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpUI
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    CGRect  deviceBounds = [UtilityClass getDeviceBounds];
    
    self.backgroundColor = [UIColor whiteColor];
    
    _orionView = [[Orion1View alloc] initWithFrame:CGRectMake(0, 0, deviceBounds.size.width, RatioOfCellWhatsHot * deviceBounds.size.height)];
    _orionView.delegate = self;
    _orionView.overrideSilentSwitch = YES;
    
    [self.contentView addSubview:_orionView];
    
    // Add Shade
    UIImageView *imageViewShade = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, deviceBounds.size.width, RatioOfCellWhatsHot * deviceBounds.size.height)];
    imageViewShade.image = [UIImage imageNamed:@"bg_shade"];
    [self.contentView addSubview:imageViewShade];
    
    // Back Ground Image
    UIView *viewTop = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, deviceBounds.size.width , RatioOfCellWhatsHot * deviceBounds.size.height)];
    
    // ImageView Play
    UIImageView *imagePlayIcon = [[UIImageView alloc] initWithFrame:CGRectMake(15, viewTop.frame.size.height - 40, 25 , 25)];
    imagePlayIcon.image = [UIImage imageNamed:@"play-icon"];
    
    [viewTop addSubview:imagePlayIcon];
    
    // Label Title
    self.labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(imagePlayIcon.frame.origin.x + imagePlayIcon.frame.size.width + 5, imagePlayIcon.frame.origin.y - 10    , deviceBounds.size.width - imagePlayIcon.frame.origin.x - imagePlayIcon.frame.size.width - 40, 28)];
    [self.labelTitle setTextColor:[UIColor whiteColor]];
    self.labelTitle.adjustsFontSizeToFitWidth = YES;
    self.labelTitle.minimumScaleFactor = 0.8;
    self.labelTitle.font = [UIFont fontWithName:kFontSFUIDisplayRegular size:24];
    [viewTop addSubview:self.labelTitle];
    
    // Label Title
    UILabel *labelWhatsHot = [[UILabel alloc] initWithFrame:CGRectMake(imagePlayIcon.frame.origin.x, imagePlayIcon.frame.origin.y - 45, deviceBounds.size.width - 150, 35)];
    labelWhatsHot.text = @"Whats Hot!";
    [labelWhatsHot setTextColor:UIColorFromRGB(0xF5A623)];
    labelWhatsHot.adjustsFontSizeToFitWidth = YES;
    labelWhatsHot.font = [UIFont fontWithName:kFontSFUIDisplayLight size:28];
    [viewTop addSubview:labelWhatsHot];
    
    // Label 360 Video
    self.label360video = [[UILabel alloc] initWithFrame:CGRectMake(self.labelTitle.frame.origin.x , self.labelTitle.frame.origin.y + self.labelTitle.frame.size.height, 80, 14)];
    [self.label360video setNumberOfLines:1];
    self.label360video.font = [UIFont fontWithName:kFontSFUIDisplayRegular size:12];
    [self.label360video setTextAlignment:NSTextAlignmentLeft];
    [self.label360video setTextColor:[UIColor colorWithRed:19/255.0 green:174/255.0 blue:229/255.0 alpha:1/1.0]];
    [self.label360video setBackgroundColor:[UIColor clearColor]];
    self.label360video.adjustsFontSizeToFitWidth = YES;
    [viewTop addSubview:self.label360video];
    
    // ImageView Spin
    UIImageView *imageSpin = [[UIImageView alloc] initWithFrame:CGRectMake(_label360video.frame.origin.x + _label360video.frame.size.width + 5, _label360video.frame.origin.y, 16, 15)];
    imageSpin.image = [UIImage imageNamed:@"spin-icon"];
    [viewTop addSubview:imageSpin];
    
    // Label Spins
    self.labelSpins = [[UILabel alloc] initWithFrame:CGRectMake(imageSpin.frame.origin.x + imageSpin.frame.size.width + 10, _label360video.frame.origin.y, self.labelTitle.frame.size.width/2 - 10, 14)];
    [self.labelSpins setNumberOfLines:1];
    self.labelSpins.font = [UIFont fontWithName:kFontSFUIDisplayRegular size:12];;
    [self.labelSpins setTextColor:[UIColor colorWithRed:19/255.0 green:174/255.0 blue:229/255.0 alpha:1/1.0]];
    [self.labelSpins setBackgroundColor:[UIColor clearColor]];
    self.labelSpins.adjustsFontSizeToFitWidth = YES;
    [viewTop addSubview:self.labelSpins];
    
    [self.contentView addSubview:viewTop];
    
    
    UIButton * buttonWatch = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonWatch addTarget:self
                    action:@selector(actionButtonWatch:)
          forControlEvents:UIControlEventTouchUpInside];
    buttonWatch.frame = CGRectMake(0, 0, deviceBounds.size.width , RatioOfCellWhatsHot * deviceBounds.size.height);
    
    buttonWatch.backgroundColor = [UIColor clearColor];
    
    [self.contentView addSubview:buttonWatch];
    
}

#pragma - Orino1View delegate functions

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)orion1ViewReadyToPlayVideo:(Orion1View*)orion1View
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    _isReadyToPlayVideo = true;
    
    [_orionView play:0.0f];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) playVideo
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [_orionView initVideoWithUrl:[[UtilityClass sharedManger] URLforFileInDocunemtDirectory:_panaromaFileName] previewImageUrl:nil licenseFileUrl:[[UtilityClass sharedManger] orionLicenseURL]];
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonWatch:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    DetailVideoViewController * vc =[[DetailVideoViewController alloc] init];
    vc.CellType = WhatsHot;
    vc.modal = _dataModel;
    
    UINavigationController *parentNavigationController = [self viewController].navigationController;
    
    [parentNavigationController pushViewController:vc animated:true];
}


@end
