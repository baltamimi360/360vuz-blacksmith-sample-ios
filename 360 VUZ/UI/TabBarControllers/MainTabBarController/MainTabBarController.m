//
//  MainTabBarController.m
//  360 VUZ
//
//  Created by Mosib on 7/5/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "MainTabBarController.h"
#import "ProfileViewController.h"
#import "Live360ViewController.h"
#import "AddVideosViewController.h"
#import "CategoriesViewController.h"
#import "HomeViewController.h"

@interface MainTabBarController ()
@property(nonatomic)  UIButton *centerButton;

@end

@implementation MainTabBarController
@synthesize centerButton;

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)viewDidLoad
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super viewDidLoad];
    
    // Set Up Navigation Controllers for Tab Bar
    [self setUpTabBarControllers];
    
    // Create a custom UIButton and add it to the center of our tab bar
    [self addCenterButtonWithImage:[UIImage imageNamed:@"upload-360-file"] highlightImage:[UIImage imageNamed:@"upload-360-file"] target:self action:@selector(buttonPressed:)];
    
    // Set Up Tab Bar Item Images
    [self setUpTabBarItemImages];
    
    // Set Tab Bar properties like shadow and Tint Color
    [self setUpTabBarProperties];
    
    // Adjust Bar button Items Insets
    [self.viewControllers enumerateObjectsUsingBlock:^(UIViewController *vc, NSUInteger idx, BOOL *stop) {
        vc.tabBarItem.title = nil;
        vc.tabBarItem.imageInsets = UIEdgeInsetsMake(5, 0, -5, 0);
    }];
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)didReceiveMemoryWarning
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpTabBarControllers
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    // Set Tab Bar Frame
    [UtilityClass setTabBarHeight:self.tabBar.frame.size.height];
    
    // Set Home View Controller
    HomeViewController *homeViewController = [[HomeViewController alloc] init];
    homeViewController.view.backgroundColor = [UIColor whiteColor];
    UINavigationController *homeNavigation = [[UINavigationController alloc]
                                              initWithRootViewController:homeViewController];
    
    // Set Categories Controller
    CategoriesViewController *categoriesViewController = [[CategoriesViewController alloc] init];
    categoriesViewController.view.backgroundColor = [UIColor yellowColor];
    UINavigationController *categoriesNavigation = [[UINavigationController alloc]
                                                    initWithRootViewController:categoriesViewController];
    
    // Set Add Videos Controller
    AddVideosViewController *addVideosViewController = [[AddVideosViewController alloc] init];
    addVideosViewController.view.backgroundColor = [UIColor grayColor];
    UINavigationController* addVidoesNavigation = [[UINavigationController alloc]
                                                   initWithRootViewController:addVideosViewController];
    
    // Set Live360 Controller
    Live360ViewController *LiveViewController = [[Live360ViewController alloc] init];
    LiveViewController.view.backgroundColor = [UIColor whiteColor];
    UINavigationController* liveNavigation = [[UINavigationController alloc]
                                              initWithRootViewController:LiveViewController];

    

    // Set Profile Controller
    ProfileViewController *accountViewController = [[ProfileViewController alloc] init];
    accountViewController.view.backgroundColor = [UIColor whiteColor];
    UINavigationController* accountNavigation = [[UINavigationController alloc]
                                                 initWithRootViewController:accountViewController];
    [accountNavigation setNavigationBarHidden:YES animated:YES];
    
   
    // Set Tab Bar View Controllers
    self.viewControllers = [NSArray arrayWithObjects:homeNavigation, categoriesNavigation, addVidoesNavigation, liveNavigation  , accountNavigation , nil];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpTabBarItemImages
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [[self.tabBar.items objectAtIndex:0] setImage:[[UIImage imageNamed:@"Home"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    [[self.tabBar.items objectAtIndex:0] setSelectedImage:[[UIImage imageNamed:@"Home-Selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];

    [[self.tabBar.items objectAtIndex:1] setImage:[[UIImage imageNamed:@"category"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    [[self.tabBar.items objectAtIndex:1] setSelectedImage:[[UIImage imageNamed:@"category-Selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    [[self.tabBar.items objectAtIndex:3] setImage:[[UIImage imageNamed:@"360live"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [[self.tabBar.items objectAtIndex:3] setSelectedImage:[[UIImage imageNamed:@"360live-Selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    

    [[self.tabBar.items objectAtIndex:4] setImage:[[UIImage imageNamed:@"Profile"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    [[self.tabBar.items objectAtIndex:4] setSelectedImage:[[UIImage imageNamed:@"Profile-Selected"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpTabBarProperties
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [self.tabBar setBackgroundImage:[UtilityClass imageFromColor:[UIColor whiteColor]]];
    self.tabBar.layer.borderWidth = 0.50;
    self.tabBar.layer.borderColor = [UIColor clearColor].CGColor;
    self.tabBar.tintColor = [UIColor clearColor];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)addCenterButtonWithImage:(UIImage *)buttonImage highlightImage:(UIImage *)highlightImage target:(id)target action:(SEL)action
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
    button.frame = CGRectMake(0.0, 0.0, buttonImage.size.width, buttonImage.size.height);
    [button setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [button setBackgroundImage:highlightImage forState:UIControlStateHighlighted];
    
    CGFloat heightDifference = buttonImage.size.height - self.tabBar.frame.size.height;
    if (heightDifference < 0) {
        button.center = self.tabBar.center;
    } else {
        CGPoint center = self.tabBar.center;
        center.y = center.y - heightDifference/2.0 - 10;
        button.center = center;
    }
    
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:button];
    self.centerButton = button;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)buttonPressed:(id)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if (![[Settings sharedManger] getIsUserLoggedIn])
    {
        // Post Notification For Splash Finished
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSplashVideoFinished object:nil];
    }
    
    else
    {
    
    [self setSelectedIndex:2];
    [self performSelector:@selector(doHighlight:) withObject:sender afterDelay:0];
    }
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)doHighlight:(UIButton*)b
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [b setHighlighted:YES];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)doNotHighlight:(UIButton*)b
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [b setHighlighted:NO];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if(self.tabBarController.selectedIndex != 2){
        [self performSelector:@selector(doNotHighlight:) withObject:centerButton afterDelay:0];
    }
    
    NSUInteger indexOfTab = [[tabBar items] indexOfObject:item];
    NSLog(@"Tab index = %u", (int)indexOfTab);
    
    int index = (int)indexOfTab;
    
    if (index > 1 && ![[Settings sharedManger] getIsUserLoggedIn])
    {
        // Post Notification For Splash Finished
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSplashVideoFinished object:nil];
    }
}

@end
