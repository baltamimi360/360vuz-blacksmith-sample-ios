//
//  SpinCountDetailVideoTableViewCell.m
//  360 VUZ
//
//  Created by Mosib on 8/8/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "SpinCountDetailVideoTableViewCell.h"

@interface SpinCountDetailVideoTableViewCell()
{
    UIView *viewSeperator;
}

@end


@implementation SpinCountDetailVideoTableViewCell

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        // Set Up UI
        [self setUpUI];
        
    }
    
    return self;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) setUpUI
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self.backgroundColor = [UIColor whiteColor];
    CGRect  deviceBounds = [UtilityClass getDeviceBounds];
    
    // ImageView Spin
    UIImageView *imageSpin = [[UIImageView alloc] initWithFrame:CGRectMake(15, 15, 16, 15)];
    imageSpin.image = [UIImage imageNamed:@"spin-icon"];
    [self.contentView addSubview:imageSpin];
    
    
    // Label Spins
    _labelSpins = [[UILabel alloc] init];
    _labelSpins.frame = CGRectMake(imageSpin.frame.origin.x + imageSpin.frame.size.width + 5, 15, 50, 20);
    
    // Align Center
    CGPoint center = _labelSpins.center;
    center.y = imageSpin.center.y;
    _labelSpins.center = center;
    
    _labelSpins.font = [UIFont fontWithName:kFontSFUIDisplayRegular size:16];
    _labelSpins.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:_labelSpins];
    
    // ImageView Spin
    viewSeperator = [[UIView alloc] initWithFrame:CGRectMake(_labelSpins.frame.origin.x + _labelSpins.frame.size.width + 5, center.y - 0.5, deviceBounds.size.width - (_labelSpins.frame.origin.x + _labelSpins.frame.size.width + 10), 1)];
    viewSeperator.backgroundColor = [UIColor lightGrayColor];
    
    [self.contentView addSubview:viewSeperator];

}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) adjustFrameForSperator
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    CGRect  deviceBounds = [UtilityClass getDeviceBounds];
    
    CGRect labelFrame = _labelSpins.frame;
    
    labelFrame.size.width = _labelSpins.intrinsicContentSize.width;
    
    _labelSpins.frame =  labelFrame;
    
    CGRect viewSeperatorFrame =  viewSeperator.frame;
    
   viewSeperatorFrame.origin.x = _labelSpins.frame.origin.x + _labelSpins.frame.size.width + 5,
    viewSeperatorFrame.size.width = deviceBounds.size.width - (_labelSpins.frame.origin.x + _labelSpins.frame.size.width + 10);
    
    viewSeperator.frame =  viewSeperatorFrame;
    
}


@end
