//
//  RecommendedVideoDetailTableViewCell.m
//  360 VUZ
//
//  Created by Mosib on 8/8/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "RecommendedVideoDetailTableViewCell.h"

@implementation RecommendedVideoDetailTableViewCell

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        // Set Up UI
        [self setUpUI];
        
    }
    
    return self;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpUI
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self.backgroundColor = [UIColor whiteColor];
    CGRect  deviceBounds = [UtilityClass getDeviceBounds];
    
    // Back Ground Image
    self.imageMoviePoster = [[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 160 , 80)];
    
    self.imageMoviePoster.contentMode = UIViewContentModeScaleToFill;
    
    // ImageView Play
    UIImageView *imagePlayIcon = [[UIImageView alloc] initWithFrame:CGRectMake(10, _imageMoviePoster.frame.size.height - 30, 25 , 25)];
    imagePlayIcon.image = [UIImage imageNamed:@"play-icon"];
    [_imageMoviePoster addSubview:imagePlayIcon];

    
    // Label Title
    self.labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(_imageMoviePoster.frame.origin.x + _imageMoviePoster.frame.size.width + 10, 0, deviceBounds.size.width - (_imageMoviePoster.frame.origin.x + _imageMoviePoster.frame.size.width + 20), 36)];
    [self.labelTitle setTextColor:[UIColor blackColor]];
    self.labelTitle.adjustsFontSizeToFitWidth = YES;
    self.labelTitle.minimumScaleFactor = 0.8;
    self.labelTitle.numberOfLines = 2;
    self.labelTitle.font = [UIFont fontWithName:kFontSFUIDisplayLight size:18];
    [self.contentView addSubview:self.labelTitle];
    
    // Label 360 Video
    self.labelDescription = [[UILabel alloc] initWithFrame:CGRectMake(self.labelTitle.frame.origin.x - 5  , self.labelTitle.frame.origin.y + self.labelTitle.frame.size.height - 8, self.labelTitle.frame.size.width + 10, 70)];
    self.labelDescription.font = [UIFont fontWithName:kFontSFUIDisplayBold size:12];
    [self.labelDescription setTextAlignment:NSTextAlignmentLeft];
    [self.labelDescription setTextColor:[UIColor blackColor]];
    [self.labelDescription setBackgroundColor:[UIColor clearColor]];
    [self.contentView addSubview:self.labelDescription];
    
    
    [self.contentView addSubview:self.imageMoviePoster];
    
}

@end
