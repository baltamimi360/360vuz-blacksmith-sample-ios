//
//  RecommendedVideoDetailTableViewCell.h
//  360 VUZ
//
//  Created by Mosib on 8/8/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WhatsHotDataModel.h"


@interface RecommendedVideoDetailTableViewCell : UITableViewCell
@property (strong, nonatomic) UILabel *labelTitle;
@property (strong, nonatomic) UILabel *labelDescription;
@property (strong, nonatomic) UIImageView *imageMoviePoster;
@property (nonatomic) WhatsHotDataModel *dataModel;

@end
