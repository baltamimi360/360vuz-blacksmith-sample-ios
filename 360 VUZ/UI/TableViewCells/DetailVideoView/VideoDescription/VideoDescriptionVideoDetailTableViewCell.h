//
//  VideoDescriptionVideoDetailTableViewCell.h
//  360 VUZ
//
//  Created by Mosib on 8/8/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoDescriptionVideoDetailTableViewCell : UITableViewCell

@property (strong, nonatomic) UILabel *labelShare;

- (void) adjustFrame;

@end
