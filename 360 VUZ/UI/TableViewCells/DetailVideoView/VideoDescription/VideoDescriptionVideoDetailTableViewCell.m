//
//  VideoDescriptionVideoDetailTableViewCell.m
//  360 VUZ
//
//  Created by Mosib on 8/8/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "VideoDescriptionVideoDetailTableViewCell.h"

@interface VideoDescriptionVideoDetailTableViewCell ()
{

}
@end

@implementation VideoDescriptionVideoDetailTableViewCell

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        // Set Up UI
        [self setUpUI];
        
    }
    
    return self;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) setUpUI
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self.backgroundColor = [UIColor whiteColor];
    CGRect  deviceBounds = [UtilityClass getDeviceBounds];
    
    // Label Title
    _labelShare = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, deviceBounds.size.width - 40, 30)];
    _labelShare.adjustsFontSizeToFitWidth = YES;
    _labelShare.minimumScaleFactor = 0.8;
    _labelShare.numberOfLines = 0;
    _labelShare.textColor = UIColorFromRGB(0xBBBBBB);

    _labelShare.font = [UIFont fontWithName:kFontSFUIDisplayRegular size:14];
    
    
    [self.contentView addSubview:_labelShare];

    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) adjustFrame
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    CGRect  deviceBounds = [UtilityClass getDeviceBounds];
    
    CGRect frame = _labelShare.frame;
    
    CGRect adjustedFrame = [_labelShare.text boundingRectWithSize:CGSizeMake(deviceBounds.size.width - 40, 0)
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:@{NSFontAttributeName:[UIFont fontWithName:kFontSFUIDisplayRegular size:14]}
                                              context:nil];
    
    frame.size.height = adjustedFrame.size.height;
    
    _labelShare.frame = frame;

}

@end
