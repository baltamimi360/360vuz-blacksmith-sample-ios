//
//  SocialShareDetailVideoTableViewCell.m
//  360 VUZ
//
//  Created by Mosib on 8/8/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "SocialShareDetailVideoTableViewCell.h"

@interface SocialShareDetailVideoTableViewCell ()
{
    UIButton * buttonShareIcon;
    UIButton * buttonWhatsApp;
    UIButton * buttonfacebook;
    UIButton * buttontwitter;
}
@end

@implementation SocialShareDetailVideoTableViewCell

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        // Set Up UI
        [self setUpUI];
        
    }
    
    return self;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) setUpUI
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self.backgroundColor = [UIColor whiteColor];
    CGRect  deviceBounds = [UtilityClass getDeviceBounds];
    
    // ImageView Spin
    UIView *viewSeperatorTop = [[UIView alloc] initWithFrame:CGRectMake(10, 2, deviceBounds.size.width - 20, 1)];
    viewSeperatorTop.backgroundColor = UIColorFromRGB(0xBBBBBB);
    [self.contentView addSubview:viewSeperatorTop];
    
    // Label Title
    UILabel *labelShare = [[UILabel alloc] initWithFrame:CGRectMake(20, 17, 60, 30)];
    labelShare.adjustsFontSizeToFitWidth = YES;
    labelShare.text = @"Share";
    labelShare.font = [UIFont fontWithName:kFontSFUIDisplayBold size:18];
    [self.contentView addSubview:labelShare];
  
    CGPoint labelShareCenter = labelShare.center;
    
    CGFloat availibeLenght = deviceBounds.size.width - ( labelShare.frame.origin.x + labelShare.frame.size.width + 5 );
    
    // Button Twitter
    buttontwitter = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttontwitter addTarget:self
                      action:@selector(actionButtonTwitter:)
            forControlEvents:UIControlEventTouchUpInside];
    buttontwitter.frame = CGRectMake(labelShare.frame.origin.x + labelShare.frame.size.width + 5, 7, availibeLenght * 0.25 , 30);
    [buttontwitter setImage:[UIImage imageNamed:@"twitter-share-icon"] forState:UIControlStateNormal];
    [self recenter:labelShareCenter toCenter:buttontwitter];
    
    // Button Facebook
    buttonfacebook = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonfacebook addTarget:self
                       action:@selector(actionButtonFacebook:)
             forControlEvents:UIControlEventTouchUpInside];
    buttonfacebook.frame = CGRectMake(buttontwitter.frame.origin.x + buttontwitter.frame.size.width + 1, 7, availibeLenght * 0.25 , 30);
    [buttonfacebook setImage:[UIImage imageNamed:@"facebook-share-icon"] forState:UIControlStateNormal];
    [self recenter:labelShareCenter toCenter:buttonfacebook];
    
    // Button Share WhatsApp
    buttonWhatsApp = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonWhatsApp addTarget:self
                       action:@selector(actionButtonWhatsApp:)
             forControlEvents:UIControlEventTouchUpInside];
    buttonWhatsApp.frame = CGRectMake(buttonfacebook.frame.origin.x + buttonfacebook.frame.size.width + 1, 7, availibeLenght * 0.25 , 30);
    [buttonWhatsApp setImage:[UIImage imageNamed:@"whatsapp-share-icon"] forState:UIControlStateNormal];
    [self recenter:labelShareCenter toCenter:buttonWhatsApp];
    
    // Button Share Email
    buttonShareIcon = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonShareIcon addTarget:self
                    action:@selector(actionButtonEmail:)
          forControlEvents:UIControlEventTouchUpInside];
    buttonShareIcon.frame = CGRectMake(buttonWhatsApp.frame.origin.x + buttonWhatsApp.frame.size.width + 1, 7, availibeLenght * 0.25 , 30);
    [buttonShareIcon setImage:[UIImage imageNamed:@"email-share-icon"] forState:UIControlStateNormal];
    [self recenter:labelShareCenter toCenter:buttonShareIcon];
    
    UIView *viewSeperatorleft = [[UIView alloc] initWithFrame:CGRectMake( labelShare.frame.origin.x + labelShare.frame.size.width + 5 + (availibeLenght * 0.25), 10, 1, 25)];
    viewSeperatorleft.backgroundColor = UIColorFromRGB(0xBBBBBB);
    [self.contentView addSubview:viewSeperatorleft];
    [self recenter:labelShareCenter toCenter:viewSeperatorleft];
    
    UIView *viewSeperatorCenter = [[UIView alloc] initWithFrame:CGRectMake( labelShare.frame.origin.x + labelShare.frame.size.width + 5 + (availibeLenght * 0.5), 10,1, 25)];
    viewSeperatorCenter.backgroundColor = UIColorFromRGB(0xBBBBBB);
    [self.contentView addSubview:viewSeperatorCenter];
    [self recenter:labelShareCenter toCenter:viewSeperatorCenter];
    
    UIView *viewSeperatorRight = [[UIView alloc] initWithFrame:CGRectMake( labelShare.frame.origin.x + labelShare.frame.size.width + 5 + (availibeLenght * 0.75), 10, 1, 25)];
    viewSeperatorRight.backgroundColor = UIColorFromRGB(0xBBBBBB);
    [self.contentView addSubview:viewSeperatorRight];
    [self recenter:labelShareCenter toCenter:viewSeperatorRight];
    

    UIView *viewSeperatorBottom = [[UIView alloc] initWithFrame:CGRectMake(10, 60, deviceBounds.size.width - 20, 1)];
    viewSeperatorBottom.backgroundColor = UIColorFromRGB(0xBBBBBB);
    [self.contentView addSubview:viewSeperatorBottom];

    // Label Title
    UILabel *labelRecommanded = [[UILabel alloc] initWithFrame:CGRectMake(10, viewSeperatorBottom.frame.origin.y + viewSeperatorBottom.frame.size.height, 150, 25)];
    labelRecommanded.text = @"Recommended for you";
    labelRecommanded.textColor = UIColorFromRGB(0x4A4A4A);
    labelRecommanded.font = [UIFont fontWithName:kFontSFUIDisplayMedium size:14];
    [self.contentView addSubview:labelRecommanded];
    
    [self.contentView addSubview:buttonWhatsApp];
    [self.contentView addSubview:buttonShareIcon];
    [self.contentView addSubview:buttonfacebook];
    [self.contentView addSubview:buttontwitter];
    
}

#pragma mark - Action Buttons
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonWhatsApp:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonEmail:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonFacebook:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonWatch:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonTwitter:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) recenter:(CGPoint) point toCenter:(UIView *) view
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    CGPoint viewcenter = view.center;
    
    viewcenter.y = point.y;
    
    view.center = viewcenter;
}

@end
