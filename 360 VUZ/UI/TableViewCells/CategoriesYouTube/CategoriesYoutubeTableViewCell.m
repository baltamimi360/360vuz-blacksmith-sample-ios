//
//  CategoriesYoutubeTableViewCell.m
//  360 VUZ
//
//  Created by Mosib on 8/16/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "CategoriesYoutubeTableViewCell.h"

@interface CategoriesYoutubeTableViewCell ()
{
    UIButton * buttonWhatsApp;
    UIButton * buttonfacebook;
    UIButton * buttontwitter;
}
@end

@implementation CategoriesYoutubeTableViewCell

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        // Set Up UI
        [self setUpUI];
        
    }
    
    return self;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpUI
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self.backgroundColor = [UIColor whiteColor];
    CGRect  deviceBounds = [UtilityClass getDeviceBounds];
    
    // Back Ground Image
    self.imageMoviePoster = [[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 180 , 90)];
    
    self.imageMoviePoster.contentMode = UIViewContentModeScaleToFill;
    
    // ImageView Play
    UIImageView *imagePlayIcon = [[UIImageView alloc] initWithFrame:CGRectMake(_imageMoviePoster.frame.size.width/2 - 25/2, _imageMoviePoster.frame.size.height/2 - 25/2, 25 , 25)];
    imagePlayIcon.image = [UIImage imageNamed:@"play-icon"];
    [_imageMoviePoster addSubview:imagePlayIcon];
    
    
    // Label Title
    self.labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(_imageMoviePoster.frame.origin.x + _imageMoviePoster.frame.size.width + 5, 10, deviceBounds.size.width - (_imageMoviePoster.frame.origin.x + _imageMoviePoster.frame.size.width + 20), 36)];
    [self.labelTitle setTextColor:[UIColor blackColor]];
    self.labelTitle.adjustsFontSizeToFitWidth = YES;
    self.labelTitle.minimumScaleFactor = 0.8;
    self.labelTitle.numberOfLines = 2;
    self.labelTitle.font = [UIFont fontWithName:kFontSFUIDisplaySemibold size:18];
    [self.contentView addSubview:self.labelTitle];
    
    // Button Share WhatsApp
    buttonWhatsApp = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonWhatsApp addTarget:self
                       action:@selector(actionButtonWhatsApp:)
             forControlEvents:UIControlEventTouchUpInside];
    buttonWhatsApp.frame = CGRectMake(deviceBounds.size.width - 50, 60, 30 , 30);
    [buttonWhatsApp setImage:[UIImage imageNamed:@"whatsapp-share-icon"] forState:UIControlStateNormal];
    buttonWhatsApp.backgroundColor = [UIColor whiteColor];
    [buttonWhatsApp scaleToFill];
    
    [self.contentView addSubview:buttonWhatsApp];

    // Button Facebook
    buttonfacebook = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonfacebook addTarget:self
                       action:@selector(actionButtonFacebook:)
             forControlEvents:UIControlEventTouchUpInside];
    buttonfacebook.frame = CGRectMake(buttonWhatsApp.frame.origin.x - 55, 60, 30 , 30);
    [buttonfacebook setImage:[UIImage imageNamed:@"facebook-share-icon"] forState:UIControlStateNormal];
    [buttonfacebook scaleToFill];
    
     [self.contentView addSubview:buttonfacebook];
    
    // Button Twitter
    buttontwitter = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttontwitter addTarget:self
                      action:@selector(actionButtonTwitter:)
            forControlEvents:UIControlEventTouchUpInside];
    buttontwitter.frame = CGRectMake(buttonfacebook.frame.origin.x - 55, 60, 30 , 30);
    [buttontwitter setImage:[UIImage imageNamed:@"twitter-share-icon"] forState:UIControlStateNormal];
    [buttontwitter scaleToFill];

    [self.contentView addSubview:buttontwitter];
       
    [self.contentView addSubview:self.imageMoviePoster];
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonWhatsApp:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonEmail:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonFacebook:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonTwitter:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
}


@end
