//
//  CategoriesYoutubeTableViewCell.h
//  360 VUZ
//
//  Created by Mosib on 8/16/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GlobalTredingDataModel.h"

@interface CategoriesYoutubeTableViewCell : UITableViewCell

@property (strong, nonatomic) UILabel *labelTitle;
@property (strong, nonatomic) UIImageView *imageMoviePoster;
@property (nonatomic) GlobalTredingDataModel *dataModel;

@end
