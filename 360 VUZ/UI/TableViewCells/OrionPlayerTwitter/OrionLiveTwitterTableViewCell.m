//
//  OrionLiveTwitterTableViewCell.m
//  360 VUZ
//
//  Created by Mosib on 8/21/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "OrionLiveTwitterTableViewCell.h"

@implementation OrionLiveTwitterTableViewCell

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        // Set Up UI
        [self setUpUI];
        
    }
    
    return self;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpUI
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self.backgroundColor = [UIColor whiteColor];
    
    CGFloat actualWidth = [UtilityClass getDeviceBounds].size.width * 0.60;
    
    // Back Ground Image
    self.imageViewPicture = [[UIImageView alloc] initWithFrame:CGRectMake(15, 15,60   , 60)];
    
    self.imageViewPicture.contentMode = UIViewContentModeScaleToFill;
    
    [self.contentView addSubview:self.imageViewPicture];
    
    // Label Title
    self.labelName = [[UILabel alloc] initWithFrame:CGRectMake(self.imageViewPicture.frame.origin.x + self.imageViewPicture.frame.size.width + 8 , 15, actualWidth - (self.imageViewPicture.frame.origin.x + self.imageViewPicture.frame.size.width + 20), 20)];
    [self.labelName setTextColor:[UIColor blackColor]];
    self.labelName.adjustsFontSizeToFitWidth = YES;
    self.labelName.minimumScaleFactor = 0.8;
    self.labelName.font = [UIFont fontWithName:kFontSFUIDisplayLight size:12];

    self.labelName.textColor = [UIColor whiteColor];
    [self.contentView addSubview:self.labelName];
    
    // Label Tweet
    self.labelTweet = [[UILabel alloc] initWithFrame:CGRectMake(self.imageViewPicture.frame.origin.x + self.imageViewPicture.frame.size.width + 10 , self.labelName.frame.origin.y +  self.labelName.frame.size.height + 5 , actualWidth - (self.imageViewPicture.frame.origin.x + self.imageViewPicture.frame.size.width + 20 + 60) , 60)];
    [self.labelTweet setTextColor:[UIColor blackColor]];
    self.labelTweet.adjustsFontSizeToFitWidth = YES;
    self.labelTweet.minimumScaleFactor = 0.60;
    self.labelTweet.numberOfLines = 3;
    self.labelTweet.font = [UIFont fontWithName:kFontSFUIDisplayLight size:18];
    self.labelTweet.textColor = [UIColor whiteColor];
    [self.contentView addSubview:self.labelTweet];
    
    // Label Tweet
    self.labelTime = [[UILabel alloc] initWithFrame:CGRectMake(actualWidth - 60 , 5 , 50, 20)];
    [self.labelTime setTextColor:[UIColor blackColor]];
    self.labelTime.adjustsFontSizeToFitWidth = YES;
    self.labelTime.minimumScaleFactor = 0.8;
    self.labelTime.numberOfLines = 2;
    self.labelTime.font = [UIFont fontWithName:kFontSFUIDisplayLight size:15];
    self.labelTime.textColor = [UIColor whiteColor];
    [self.contentView addSubview:self.labelTime];
    
}

@end
