//
//  OrionLiveTwitterTableViewCell.h
//  360 VUZ
//
//  Created by Mosib on 8/21/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrionLiveTwitterTableViewCell : UITableViewCell

@property (nonatomic) UIImageView *imageViewPicture;
@property (nonatomic) UILabel *labelName;
@property (nonatomic) UILabel *labelTweet;
@property (nonatomic) UILabel *labelTime;
@end
