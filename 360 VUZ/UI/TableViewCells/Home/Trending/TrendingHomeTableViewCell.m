//
//  TrendingHomeTableViewCell.m
//  360 VUZ
//
//  Created by Mosib on 7/23/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "TrendingHomeTableViewCell.h"
#import "TrendingCollectionViewCell.h"
#import "ServiceTrending.h"
#import "DetailVideoViewController.h"

@interface TrendingHomeTableViewCell ()
{
    CGRect deviceBounds;
    int pageNumber;
    BOOL lastResultFetchedEmpty;
    __weak TrendingHomeTableViewCell * weakSelf;
}
@end

@implementation TrendingHomeTableViewCell

static BOOL fetchInProgress;

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        // Initiailze Variables
        [self initializeVariables];
        
        //  Add Header to View
        [self setUpHeader];
        
        // Add CollectionView to View
        [self setUpCollectionView];
        
        // Service API
        [self serviceTrendingAPI];
        
    }
    
    return self;
}

#pragma mark - API Methods -
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) serviceTrendingAPI
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    STARTBACKGROUNDTHREAD
    ServiceTrending *tredning = [[ServiceTrending alloc]init];
    
    [tredning executeRequest:@{@"limit" : @"5" , @"page_no": [@(pageNumber) stringValue]}
                    success:^(NSDictionary  *responseObject)
     {
         
         [weakSelf SuccessForHomePageAPI:responseObject];
         
     }
                    failure:^(NSError * error) {
                        
                        [weakSelf FailureForHomePageAPI:error];
                        
                        
                        
                    }];
    ENDTHREAD
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) SuccessForHomePageAPI:(NSDictionary *) responseObject
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
    if ([[responseObject objectForKey:@"data"] count] == 0)
    {
        lastResultFetchedEmpty = true;
    }
    
    else
    {
        pageNumber++;
    }
    
    if (!lastResultFetchedEmpty)
    {
        
        STARTMAINTHREAD
        
        [_collectionView performBatchUpdates:^{
            
            int resultsSize = (int)[self.dataArray count];
            
            [self.dataArray addObjectsFromArray:[self  makeDataValuesForTrendingVideos:[responseObject objectForKey:@"data"]]];
            
            NSMutableArray *arrayWithIndexPaths = [NSMutableArray array];
            
            for (int i = resultsSize; i < resultsSize + [[responseObject objectForKey:@"data"] count]; i++)
                [arrayWithIndexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
            
            [_collectionView insertItemsAtIndexPaths:arrayWithIndexPaths];
            
        } completion:^(BOOL finished) {
            
            if(finished)
            {
                fetchInProgress = false;
            }
        }];
        
        ENDTHREAD
        
    }
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) FailureForHomePageAPI:(NSError *)error
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    STARTMAINTHREAD
    
    fetchInProgress = false;
    
    ENDTHREAD
}


#pragma mark - Private Methods -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) initializeVariables
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    weakSelf = self;
    
    fetchInProgress = false;
    
    pageNumber = 0;
    
    _dataArray = [[NSMutableArray alloc] init];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpCollectionView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    UICollectionViewFlowLayout * flowLayout =  [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    flowLayout.minimumInteritemSpacing = 0;
    flowLayout.minimumLineSpacing = 3;
    
    // Initialize Collection View
    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 25, deviceBounds.size.width, 135) collectionViewLayout:flowLayout];
    
    // Set DataSource and Delegate
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
    
    // Register Classes for Usage
    [_collectionView registerClass:[TrendingCollectionViewCell class] forCellWithReuseIdentifier:kRIDTrendingCollectionViewCell];
    
    // Add CollectionView to View
    _collectionView.backgroundColor = [UIColor whiteColor];
    _collectionView.showsHorizontalScrollIndicator = false;
    [self.contentView addSubview:_collectionView];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpHeader
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    deviceBounds = [UtilityClass getDeviceBounds];
    
    // Back Ground Image
    _imageViewHeader = [[UIImageView alloc] initWithFrame:CGRectMake(10, 3, 20 , 20)];
    _imageViewHeader.contentMode = UIViewContentModeScaleAspectFit;
    
    // Label Title
    _labelTitle = [[UILabel alloc] init];
    CGSize sizeOflabelTitle = CGSizeMake(200, 22.0);
    CGPoint pointOflabelTitle = CGPointMake(_imageViewHeader.frame.origin.x + _imageViewHeader.frame.size.width + 5 , 3 );
    _labelTitle.frame = CGRectMake(pointOflabelTitle.x, pointOflabelTitle.y, sizeOflabelTitle.width, sizeOflabelTitle.height);
    _labelTitle.font = [UIFont fontWithName:kFontSFUIDisplaySemibold size:20];
    _labelTitle.textColor = UIColorFromRGB(0x4A4A4A);
    
    [self.contentView addSubview:_imageViewHeader];
    [self.contentView addSubview:_labelTitle];
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSArray *) makeDataValuesForTrendingVideos:(NSArray *) dataArray
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    NSMutableArray *dataArrayWithDataModel = [[NSMutableArray alloc] init];
    
    for (NSDictionary * object in dataArray)
    {
        TrendingVideosDataModel *trendingVideosDataModel = [[TrendingVideosDataModel alloc] init];
        
        trendingVideosDataModel.videoID = [[object objectForKey:kTVKeyID] stringValue];
        trendingVideosDataModel.videoCity = [object objectForKey:kTVKeyVideoCity];
        trendingVideosDataModel.categoryID = [[object objectForKey:kTVKeyCategoryID] stringValue];
        trendingVideosDataModel.subCategoryID = [object objectForKey:kTVKeySubCategoryID];
        trendingVideosDataModel.videoTitle = [object objectForKey:kTVKeyVideoTitle];
        trendingVideosDataModel.videoDescription = [object objectForKey:kTVKeyVideoDescription];
        trendingVideosDataModel.websiteLink = [object objectForKey:kTVKeyWebsiteLink];
        trendingVideosDataModel.videoThumbnail = [object objectForKey:kTVKeyVideoThumbnail];
        trendingVideosDataModel.videoLink = [object objectForKey:kTVKeyVideoLink];
        trendingVideosDataModel.webVideoLink = [object objectForKey:kTVKeyWebVideoLink];
        trendingVideosDataModel.status = [object objectForKey:kTVKeyStatus];
        trendingVideosDataModel.createdDate = [object objectForKey:kTVKeyCreatedDate];
        trendingVideosDataModel.metaData = [object objectForKey:kTVKeyMetaData];
        trendingVideosDataModel.whatsHot = [object objectForKey:kTVKeyWhatsHot];
        trendingVideosDataModel.popular = [object objectForKey:kTVKeyPopular];
        trendingVideosDataModel.videotype = [object objectForKey:kTVKeyVideoType];
        trendingVideosDataModel.webVideoType = [object objectForKey:kTVKeyWebVideoType];
        trendingVideosDataModel.userID = [object objectForKey:kTVKeyUserID];
        trendingVideosDataModel.videoFeature = [object objectForKey:kTVKeyVideoFeature];
        trendingVideosDataModel.panoramaImage = [object objectForKey:kTVKeyPanoramImage];
        trendingVideosDataModel.notificationFlag = [object objectForKey:kTVKeyNotificationFlag];
        trendingVideosDataModel.isImage = [object objectForKey:kTVKeyIsImage];
        trendingVideosDataModel.rating = [object objectForKey:kTVKeyRating];
        trendingVideosDataModel.categoryName = [object objectForKey:kTVKeyCategoryID];
        trendingVideosDataModel.viewCount = [[object objectForKey:kTVKeyViewCount] stringValue];
        trendingVideosDataModel.likeCount = [[object objectForKey:kTVKeyLikeCount] stringValue];
        
        [dataArrayWithDataModel addObject:trendingVideosDataModel];
    }
    
    return [dataArrayWithDataModel copy];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if (scrollView.contentOffset.x == roundf(scrollView.contentSize.width -scrollView.frame.size.width)) {
        
        if (fetchInProgress)
            return;
        
        fetchInProgress = TRUE;
        
        // Service API
        [self serviceTrendingAPI];
    }
}


#pragma mark - UICollectionViewDataSource -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    TrendingCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kRIDTrendingCollectionViewCell forIndexPath:indexPath];
    
    cell.dataModel = [self.dataArray objectAtIndex:indexPath.row];
    
    __weak TrendingCollectionViewCell *weakCell = cell;
    
    [cell.imageMoviePoster setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:cell.dataModel.videoThumbnail]]
                                placeholderImage:[UtilityClass imageFromColor:[UIColor blackColor]]
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                             
                                             weakCell.imageMoviePoster.image = image;
                                             [weakCell setNeedsLayout];
                                             
                                         } failure:nil];
    
    cell.labelTitle.text = cell.dataModel.videoTitle;
    cell.labelSpins.text = cell.dataModel.viewCount;
    cell.label360video.text = @"360 video";
    
    return cell;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return [_dataArray count];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return 1;
}

#pragma mark - UICollectionViewDelegateFlowLayout -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return UIEdgeInsetsMake(5,5,0,0);  // top, left, bottom, right
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return CGSizeMake(widthOfCellsHome , 125);
}

#pragma mark - UICollectionViewDelegate-
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    DetailVideoViewController * vc =[[DetailVideoViewController alloc] init];
    vc.CellType = WhatsHot;
    vc.modal = [self.dataArray objectAtIndex:indexPath.row];
    
    UINavigationController *parentNavigationController = [self viewController].navigationController;
    
    [parentNavigationController pushViewController:vc animated:true];
}

@end
