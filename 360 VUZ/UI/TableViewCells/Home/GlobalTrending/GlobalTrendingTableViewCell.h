//
//  GlobalTrendingTableViewCell.h
//  360 VUZ
//
//  Created by Mosib on 8/6/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GlobalTrendingTableViewCell : UITableViewCell<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic, retain) UICollectionView *collectionView;
@property (nonatomic) UILabel *labelTitle;
@property (nonatomic) UIImageView * imageViewHeader;
@property (nonatomic) NSMutableArray *dataArray;

@end
