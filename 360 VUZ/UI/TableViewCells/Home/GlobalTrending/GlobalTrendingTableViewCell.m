//
//  GlobalTrendingTableViewCell.m
//  360 VUZ
//
//  Created by Mosib on 8/6/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "GlobalTrendingTableViewCell.h"
#import "GlobalHomeCollectionViewCell.h"
#import "GlobalTredingDataModel.h"
#import "ServiceGlobalTredning.h"
#import "ContentDetailsGTDataModel.h"
#import "SnippetGTModel.h"
#import "ResourceIDGTSDataModel.h"
#import "ThumbnailsGTSDataModel.h"
#import "ThumbnailGTSDatModel.h"

@interface GlobalTrendingTableViewCell ()
{
    CGRect deviceBounds;
    NSString *pageToken;
    BOOL lastResultFetchedEmpty;
    __weak GlobalTrendingTableViewCell * weakSelf;
}
@end

@implementation GlobalTrendingTableViewCell

static BOOL fetchInProgress;

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        // Initiailze Variables
        [self initializeVariables];
        
        //  Add Header to View
        [self setUpHeader];
        
        // Add CollectionView to View
        [self setUpCollectionView];
        
        // Service API
        [self serviceGlobalTrendingAPI];
        
    }
    
    return self;
}



#pragma mark - API Methods -
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) serviceGlobalTrendingAPI
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    STARTBACKGROUNDTHREAD
    ServiceGlobalTredning *subscribe = [[ServiceGlobalTredning alloc]init];
    
    [subscribe executeRequest:@{@"next_page_token" : pageToken }
                      success:^(NSDictionary  *responseObject)
     {
         
         [weakSelf SuccessForHomePageAPI:responseObject];
         
     }
                      failure:^(NSError * error) {
                          
                          [weakSelf FailureForHomePageAPI:error];
                          
                          
                          
                      }];
    ENDTHREAD
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) SuccessForHomePageAPI:(NSDictionary *) responseObject
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
    if ([[[responseObject objectForKey:kkeyData] objectForKey:kkeyItems] count] == 0)
    {
        lastResultFetchedEmpty = true;
    }
    
    if (!lastResultFetchedEmpty)
    {
        STARTMAINTHREAD
        
        [_collectionView performBatchUpdates:^{
            
            int resultsSize = (int)[self.dataArray count];
            
            pageToken = [[responseObject objectForKey:kkeyData] objectForKey:kkeNextPageToken];
            
            [self.dataArray addObjectsFromArray:[self  makeDataValuesForGlobalTrendingVideos:[[responseObject objectForKey:kkeyData] objectForKey:kkeyItems]]];
            
            NSMutableArray *arrayWithIndexPaths = [NSMutableArray array];
            
            for (int i = resultsSize; i < resultsSize + [[[responseObject objectForKey:kkeyData] objectForKey:kkeyItems] count]; i++)
                [arrayWithIndexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
            
            [_collectionView insertItemsAtIndexPaths:arrayWithIndexPaths];
            
        } completion:^(BOOL finished) {
            
            if(finished)
            {
                fetchInProgress = false;
            }
        }];
        
        ENDTHREAD
        
    }
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) FailureForHomePageAPI:(NSError *)error
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    STARTMAINTHREAD
    
    fetchInProgress = false;
    
    ENDTHREAD
}


#pragma mark - Private Methods -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) initializeVariables
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    weakSelf = self;
    
    fetchInProgress = false;
    
    pageToken = @"";
    
    _dataArray = [[NSMutableArray alloc] init];
}



/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpCollectionView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    UICollectionViewFlowLayout * flowLayout =  [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    flowLayout.minimumInteritemSpacing = 0;
    flowLayout.minimumLineSpacing = 3;
    
    // Initialize Collection View
    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 25, deviceBounds.size.width, 135) collectionViewLayout:flowLayout];
    
    // Set DataSource and Delegate
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
    
    // Register Classes for Usage
    [_collectionView registerClass:[GlobalHomeCollectionViewCell class] forCellWithReuseIdentifier:kRIDGlobalHomeCollectionViewCell];
    
    // Add CollectionView to View
    _collectionView.backgroundColor = [UIColor whiteColor];
    _collectionView.showsHorizontalScrollIndicator = false;
    [self.contentView addSubview:_collectionView];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpHeader
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    deviceBounds = [UtilityClass getDeviceBounds];
    
    // Back Ground Image
    _imageViewHeader = [[UIImageView alloc] initWithFrame:CGRectMake(10, 3, 20 , 20)];
    _imageViewHeader.contentMode = UIViewContentModeScaleAspectFit;
    
    // Label Title
    _labelTitle = [[UILabel alloc] init];
    CGSize sizeOflabelTitle = CGSizeMake(200, 22.0);
    CGPoint pointOflabelTitle = CGPointMake(_imageViewHeader.frame.origin.x + _imageViewHeader.frame.size.width + 5 , 3 );
    _labelTitle.frame = CGRectMake(pointOflabelTitle.x, pointOflabelTitle.y, sizeOflabelTitle.width, sizeOflabelTitle.height);
    _labelTitle.font = [UIFont fontWithName:kFontSFUIDisplaySemibold size:20];
    _labelTitle.textColor = UIColorFromRGB(0x4A4A4A);
    
    [self.contentView addSubview:_imageViewHeader];
    [self.contentView addSubview:_labelTitle];
    
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if (scrollView.contentOffset.x == roundf(scrollView.contentSize.width -scrollView.frame.size.width)) {
        
        if (fetchInProgress)
            return;
        
        fetchInProgress = TRUE;
        
        // Service API
        [self serviceGlobalTrendingAPI];
    }
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSArray *) makeDataValuesForGlobalTrendingVideos:(NSArray *) dataArray
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    NSMutableArray *dataArrayWithDataModel = [[NSMutableArray alloc] init];
    
    for (NSDictionary * object in dataArray)
    {
        GlobalTredingDataModel *gobalTrendingVideosDataModel = [[GlobalTredingDataModel alloc] init];
        
        gobalTrendingVideosDataModel.kind = [object objectForKey:kGTVKeyID];
        gobalTrendingVideosDataModel.etag = [object objectForKey:kGTVKeyETag];
        gobalTrendingVideosDataModel.youtubeID = [object objectForKey:kGTVKeyVideoKind];
        
        NSDictionary *dicSnippets = [object objectForKey:kGTVKeySnippet];
        
        
        // Make Snippet Object
        SnippetGTModel *snippet = [[SnippetGTModel alloc] init];
        
         snippet.publishedAt = [dicSnippets objectForKey:kGTVKeyPublishedAt];
         snippet.channelId = [dicSnippets objectForKey:kGTVKeyChannedlID];
         snippet.title = [dicSnippets objectForKey:kGTVKeyTitle];
         snippet.videoDescription = [dicSnippets objectForKey:kGTVKeyDescription];
         snippet.channelTitle = [dicSnippets objectForKey:kGTVKeyChannelTitle];
         snippet.playlistId = [dicSnippets objectForKey:kGTVKeyPlayListID];
         snippet.position = [dicSnippets objectForKey:kGTVKeyPosition];
        
        NSDictionary *dicResourceId = [dicSnippets objectForKey:kGTVKeyResourceId];
        
        // Make Resource ID Object
        ResourceIDGTSDataModel *resourceId = [[ResourceIDGTSDataModel alloc] init];
        resourceId.kind = [dicResourceId objectForKey:kGTVKeyKind];
        resourceId.videoID = [dicResourceId objectForKey:kGTVKeyVideoID];
      
        NSDictionary *dicThumbnails = [dicSnippets objectForKey:kGTVKeyThumbnails];
        
        // Make Thumbnail Objects
        ThumbnailsGTSDataModel * thumbnails = [[ThumbnailsGTSDataModel alloc] init];
        
        NSDictionary *dicDefaultThumbnail = [dicThumbnails objectForKey:kGTVKeyDefault];
        
        // Make default Thumbnail Object
        ThumbnailGTSDatModel * defaultThumbnail = [[ThumbnailGTSDatModel alloc] init];
        defaultThumbnail.url = [dicDefaultThumbnail objectForKey:kGTVKeyURL];
        defaultThumbnail.width = [dicDefaultThumbnail objectForKey:kGTVKeyWidth];
        defaultThumbnail.height = [dicDefaultThumbnail objectForKey:kGTVKeyHeight];
        
        thumbnails.defaultThumbnail = defaultThumbnail;
        
        NSDictionary *dicmediumThumbnail = [dicThumbnails objectForKey:kGTVKeyMedium];
        
        // Make medium Thumbnail Object
        ThumbnailGTSDatModel * medium = [[ThumbnailGTSDatModel alloc] init];
        medium.url = [dicmediumThumbnail objectForKey:kGTVKeyURL];
        medium.width = [dicmediumThumbnail objectForKey:kGTVKeyWidth];
        medium.height = [dicmediumThumbnail objectForKey:kGTVKeyHeight];
        
        thumbnails.medium = medium;
        
         NSDictionary *dichighThumbnail = [dicThumbnails objectForKey:kGTVKeyHigh];
        
        // Make high Thumbnail Object
        ThumbnailGTSDatModel * high = [[ThumbnailGTSDatModel alloc] init];
        high.url = [dichighThumbnail objectForKey:kGTVKeyURL];
        high.width = [dichighThumbnail objectForKey:kGTVKeyWidth];
        high.height = [dichighThumbnail objectForKey:kGTVKeyHeight];
        
        thumbnails.high = high;
        
         NSDictionary *dicstandardThumbnail = [dicThumbnails objectForKey:kGTVKeyStandard];
        
        // Make standard Thumbnail Object
        ThumbnailGTSDatModel * standard = [[ThumbnailGTSDatModel alloc] init];
        standard.url = [dicstandardThumbnail objectForKey:kGTVKeyURL];
        standard.width = [dicstandardThumbnail objectForKey:kGTVKeyWidth];
        standard.height = [dicstandardThumbnail objectForKey:kGTVKeyHeight];
        
        thumbnails.standard = standard;
        
         NSDictionary *dicmaxresThumbnail = [dicThumbnails objectForKey:kGTVKeyMaxres];
        
        // Make maxres Thumbnail Object
        ThumbnailGTSDatModel * maxres = [[ThumbnailGTSDatModel alloc] init];
        maxres.url = [dicmaxresThumbnail objectForKey:kGTVKeyURL];
        maxres.width = [dicmaxresThumbnail objectForKey:kGTVKeyWidth];
        maxres.height = [dicmaxresThumbnail objectForKey:kGTVKeyHeight];
        thumbnails.maxres = maxres;
        
        snippet.thumbnails = thumbnails;
        snippet.resourceId = resourceId;
        
        NSDictionary *dicContentDetails = [object objectForKey:kGTVKeyContentDetails];
        
        // Make Content Details Object
        ContentDetailsGTDataModel * contentDetails = [[ContentDetailsGTDataModel alloc] init];
        contentDetails.videoID = [dicContentDetails objectForKey:kGTVKeyVideoID];
        contentDetails.videoPublishedAt = [dicContentDetails objectForKey:kGTVKeyVideoPublished];
        
        gobalTrendingVideosDataModel.snippet = snippet;
        gobalTrendingVideosDataModel.contentDetails = contentDetails;
        
        [dataArrayWithDataModel addObject:gobalTrendingVideosDataModel];
    }
    
    return [dataArrayWithDataModel copy];
}


#pragma mark - UICollectionViewDataSource -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    GlobalHomeCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kRIDGlobalHomeCollectionViewCell forIndexPath:indexPath];
    
    cell.dataModel = [self.dataArray objectAtIndex:indexPath.row];
    
    __weak GlobalHomeCollectionViewCell *weakCell = cell;
    
    [cell.imageMoviePoster setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:cell.dataModel.snippet.thumbnails.medium.url]]
                                 placeholderImage:[UtilityClass imageFromColor:[UIColor blackColor]]
                                          success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                              
                                              weakCell.imageMoviePoster.image = image;
                                              [weakCell setNeedsLayout];
                                              
                                          } failure:nil];
    
    cell.labelTitle.text = cell.dataModel.snippet.title;
    cell.label360video.text = @"360 video";
    
    return cell;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return [_dataArray count];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return 1;
}

#pragma mark - UICollectionViewDelegateFlowLayout -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return UIEdgeInsetsMake(5,5,0,0);  // top, left, bottom, right
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return CGSizeMake(widthOfCellsHome , 125);
}

#pragma mark - UICollectionViewDelegate-
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    GlobalHomeCollectionViewCell *cell = (GlobalHomeCollectionViewCell *) [collectionView cellForItemAtIndexPath:indexPath];
    
    [[UtilityClass sharedManger] UIApplicationOpenURLwithString:[NSString stringWithFormat:@"%@%@",kURLYoutube,cell.dataModel.snippet.resourceId.videoID]];
}

@end
