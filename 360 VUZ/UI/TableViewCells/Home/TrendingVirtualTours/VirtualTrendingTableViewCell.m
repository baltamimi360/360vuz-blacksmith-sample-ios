//
//  VirtualTrendingTableViewCell.m
//  360 VUZ
//
//  Created by Mosib on 8/7/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "VirtualTrendingTableViewCell.h"
#import "VirtualCollectionViewCell.h"
#import "ServiceVirtualTrending.h"
#import "VirtualHomeViewController.h"

@interface VirtualTrendingTableViewCell ()
{
    CGRect deviceBounds;
    int pageNumber;
    BOOL lastResultFetchedEmpty;
    __weak VirtualTrendingTableViewCell * weakSelf;
}
@end

@implementation VirtualTrendingTableViewCell

static BOOL fetchInProgress;

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        // Initiailze Variables
        [self initializeVariables];
        
        //  Add Header to View
        [self setUpHeader];
        
        // Add CollectionView to View
        [self setUpCollectionView];
        
        // Service API
        [self serviceVirtualTrendingAPI];
        
    }
    
    return self;
}

#pragma mark - API Methods -
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) serviceVirtualTrendingAPI
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    STARTBACKGROUNDTHREAD
    ServiceVirtualTrending *subscribe = [[ServiceVirtualTrending alloc]init];
    
    [subscribe executeRequest:@{@"limit" : @"5" , @"page_no": [@(pageNumber) stringValue]}
                      success:^(NSDictionary  *responseObject)
     {
         
         [weakSelf SuccessForHomePageAPI:responseObject];
         
     }
                      failure:^(NSError * error) {
                          
                          [weakSelf FailureForHomePageAPI:error];
                          
                          
                          
                      }];
    ENDTHREAD
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) SuccessForHomePageAPI:(NSDictionary *) responseObject
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
    if ([[responseObject objectForKey:kkeyData] count] == 0)
    {
        lastResultFetchedEmpty = true;
    }
    
    else
    {
        pageNumber++;
    }
    
    if (!lastResultFetchedEmpty)
    {
        
        STARTMAINTHREAD
        
        [_collectionView performBatchUpdates:^{
            
            int resultsSize = (int)[self.dataArray count];
            
            [self.dataArray addObjectsFromArray:[self  makeDataValuesForVirtualTrendingVideos:[responseObject objectForKey:kkeyData]]];
            
            NSMutableArray *arrayWithIndexPaths = [NSMutableArray array];
            
            for (int i = resultsSize; i < resultsSize + [[responseObject objectForKey:kkeyData] count]; i++)
                [arrayWithIndexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
            
            [_collectionView insertItemsAtIndexPaths:arrayWithIndexPaths];
            
        } completion:^(BOOL finished) {
            
            if(finished)
            {
                fetchInProgress = false;
            }
        }];
        
        ENDTHREAD
        
    }
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) FailureForHomePageAPI:(NSError *)error
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    STARTMAINTHREAD
    
    fetchInProgress = false;
    
    ENDTHREAD
}


#pragma mark - Private Methods -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) initializeVariables
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    weakSelf = self;
    
    fetchInProgress = false;
    
    pageNumber = 0;
    
    _dataArray = [[NSMutableArray alloc] init];
}


#pragma mark - Private Methods -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpCollectionView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    UICollectionViewFlowLayout * flowLayout =  [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    flowLayout.minimumInteritemSpacing = 0;
    flowLayout.minimumLineSpacing = 3;
    
    // Initialize Collection View
    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 25, deviceBounds.size.width, 135) collectionViewLayout:flowLayout];
    
    // Set DataSource and Delegate
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
    
    // Register Classes for Usage
    [_collectionView registerClass:[VirtualCollectionViewCell class] forCellWithReuseIdentifier:kRIDVirtualCollectionViewCell];
    
    // Add CollectionView to View
    _collectionView.backgroundColor = [UIColor whiteColor];
    _collectionView.showsHorizontalScrollIndicator = false;
    [self.contentView addSubview:_collectionView];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpHeader
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    deviceBounds = [UtilityClass getDeviceBounds];
    
    // Back Ground Image
    _imageViewHeader = [[UIImageView alloc] initWithFrame:CGRectMake(10, 3, 20 , 20)];
    _imageViewHeader.contentMode = UIViewContentModeScaleAspectFit;
    
    // Label Title
    _labelTitle = [[UILabel alloc] init];
    CGSize sizeOflabelTitle = CGSizeMake(200, 22.0);
    CGPoint pointOflabelTitle = CGPointMake(_imageViewHeader.frame.origin.x + _imageViewHeader.frame.size.width + 5 , 3 );
    _labelTitle.frame = CGRectMake(pointOflabelTitle.x, pointOflabelTitle.y, sizeOflabelTitle.width, sizeOflabelTitle.height);
    _labelTitle.font = [UIFont fontWithName:kFontSFUIDisplaySemibold size:20];
    _labelTitle.textColor = UIColorFromRGB(0x4A4A4A);
    
    [self.contentView addSubview:_imageViewHeader];
    [self.contentView addSubview:_labelTitle];
    
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if (scrollView.contentOffset.x == roundf(scrollView.contentSize.width -scrollView.frame.size.width)) {
    
        
        if (fetchInProgress)
            return;
        
        fetchInProgress = TRUE;
        
        // Service API
        [self serviceVirtualTrendingAPI];
    }
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSArray *) makeDataValuesForVirtualTrendingVideos:(NSArray *) dataArray
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    NSMutableArray *dataArrayWithDataModel = [[NSMutableArray alloc] init];
    
    for (NSDictionary * object in dataArray)
    {
        VirtualTrendingDataModel *virtualTrendingDataModel = [[VirtualTrendingDataModel alloc] init];
        
        virtualTrendingDataModel.videoID = [object objectForKey:kVTVKeyID];
        virtualTrendingDataModel.videoCity = [object objectForKey:kVTVKeyVideoCity];
        virtualTrendingDataModel.categoryID = [object objectForKey:kVTVKeyCategoryID];
        virtualTrendingDataModel.subCategoryID = [object objectForKey:kVTVKeySubCategoryID];
        virtualTrendingDataModel.videoTitle = [object objectForKey:kVTVKeyVideoTitle];
        virtualTrendingDataModel.videoDescription = [object objectForKey:kVTVKeyVideoDescription];
        virtualTrendingDataModel.websiteLink = [object objectForKey:kVTVKeyWebsiteLink];
        virtualTrendingDataModel.videoThumbnail = [object objectForKey:kVTVKeyVideoThumbnail];
        virtualTrendingDataModel.videoLink = [object objectForKey:kVTVKeyVideoLink];
        virtualTrendingDataModel.webVideoLink = [object objectForKey:kVTVKeyWebVideoLink];
        virtualTrendingDataModel.status = [object objectForKey:kVTVKeyStatus];
        virtualTrendingDataModel.createdDate = [object objectForKey:kVTVKeyCreatedDate];
        virtualTrendingDataModel.metaData = [object objectForKey:kVTVKeyMetaData];
        virtualTrendingDataModel.whatsHot = [object objectForKey:kVTVKeyWhatsHot];
        virtualTrendingDataModel.popular = [object objectForKey:kVTVKeyPopular];
        virtualTrendingDataModel.videotype = [object objectForKey:kVTVKeyVideoType];
        virtualTrendingDataModel.webVideoType = [object objectForKey:kVTVKeyWebVideoType];
        virtualTrendingDataModel.userID = [object objectForKey:kVTVKeyUserID];
        virtualTrendingDataModel.videoFeature = [object objectForKey:kVTVKeyVideoFeature];
        virtualTrendingDataModel.panoramaImage = [object objectForKey:kVTVKeyPanoramImage];
        virtualTrendingDataModel.notificationFlag = [object objectForKey:kVTVKeyNotificationFlag];
        virtualTrendingDataModel.isImage = [object objectForKey:kVTVKeyIsImage];
        virtualTrendingDataModel.rating = [object objectForKey:kVTVKeyRating];
        virtualTrendingDataModel.categoryName = [object objectForKey:kVTVKeyCategoryName];
        virtualTrendingDataModel.viewCount = [[object objectForKey:kVTVKeyViewCount] stringValue];
        virtualTrendingDataModel.likeCount = [[object objectForKey:kVTVKeyLikeCount] stringValue];
        
        [dataArrayWithDataModel addObject:virtualTrendingDataModel];
    }
    
    return [dataArrayWithDataModel copy];
}


#pragma mark - UICollectionViewDataSource -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    VirtualCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kRIDVirtualCollectionViewCell forIndexPath:indexPath];
    
    cell.dataModel = [self.dataArray objectAtIndex:indexPath.row];
    
    __weak VirtualCollectionViewCell *weakCell = cell;
    
    [cell.imageMoviePoster setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:cell.dataModel.videoThumbnail]]
                                 placeholderImage:[UtilityClass imageFromColor:[UIColor blackColor]]
                                          success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                              
                                              weakCell.imageMoviePoster.image = image;
                                              [weakCell setNeedsLayout];
                                              
                                          } failure:nil];
    
    cell.labelTitle.text = cell.dataModel.videoTitle;
    cell.labelSpins.text = cell.dataModel.viewCount;
    cell.label360video.text = @"360 video";
    
    return cell;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return [_dataArray count];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return 1;
}

#pragma mark - UICollectionViewDelegateFlowLayout -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return UIEdgeInsetsMake(5,5,0,0);  // top, left, bottom, right
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return CGSizeMake(widthOfCellsHome , 125);
}

#pragma mark - UICollectionViewDelegate-
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    VirtualHomeViewController *vc =[[VirtualHomeViewController alloc] init];
    vc.dataModel = [self.dataArray objectAtIndex:indexPath.row];
    [[self viewController].navigationController pushViewController:vc animated:true];
}
@end
