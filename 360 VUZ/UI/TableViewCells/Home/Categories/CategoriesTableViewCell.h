//
//  CategoriesTableViewCell.h
//  360 VUZ
//
//  Created by Mosib on 7/23/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoriesTableViewCell : UITableViewCell<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic, retain) UICollectionView *collectionView;
@property (nonatomic) UILabel *labelTitle;
@property (nonatomic) UIImageView * imageViewHeader;
@property (nonatomic) NSArray *dataArray;

@end
