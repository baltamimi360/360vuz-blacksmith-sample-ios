//
//  CategoriesTableViewCell.m
//  360 VUZ
//
//  Created by Mosib on 7/23/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "CategoriesTableViewCell.h"
#import "CategoriesCollectionViewCell.h"
#import "ServiceCategories.h"
#import "CategoriesDetailPageViewController.h"
#import "CategoriesYoutubeViewController.h"

#define TRANSFORM_CELL_VALUE CGAffineTransformMakeScale(0.8, 0.8)
#define ANIMATION_SPEED 0.2

@interface CategoriesTableViewCell ()
{
    CGRect deviceBounds;
    BOOL isfirstTimeTransform;
    __weak CategoriesTableViewCell * weakSelf;
}
@end


@implementation CategoriesTableViewCell

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        isfirstTimeTransform = TRUE;
        
        weakSelf = self;
        
        //  Add Header to View
        [self setUpHeader];
        
        // Add CollectionView to View
        [self setUpCollectionView];
        
        // Service API
        [self serviceCategoriesAPI];
        
    }
    
    return self;
}

#pragma mark - API Methods -
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) serviceCategoriesAPI
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    STARTBACKGROUNDTHREAD
    
    ServiceCategories *categories = [[ServiceCategories alloc]init];
    
    [categories executeRequest:nil
                        success:^(NSDictionary  *responseObject)
     {
         

         [weakSelf SuccessForHomePageAPI:responseObject];
         
     }
                        failure:^(NSError * error) {
                            
                            [weakSelf FailureForHomePageAPI:error];
                            
                            
                            
                        }];
    ENDTHREAD
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) SuccessForHomePageAPI:(NSDictionary *) responseObject
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
    self.dataArray = [self  makeDataValuesForCategory:[responseObject objectForKey:kkeyData]];
    
    STARTMAINTHREAD
    
    // Reload Data
    [_collectionView reloadData];
    
    ENDTHREAD
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) FailureForHomePageAPI:(NSError *)error
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    STARTMAINTHREAD
    
//    [[UtilityClass sharedManger] alertErrorWithMessage:@"Something went wrong Please try again later"];
    
    ENDTHREAD
}


#pragma mark - Private Methods -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpCollectionView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    UICollectionViewFlowLayout * flowLayout =  [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    flowLayout.minimumInteritemSpacing = 0;
    flowLayout.minimumLineSpacing = 3;
    
    // Initialize Collection View
    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 25, deviceBounds.size.width, 135) collectionViewLayout:flowLayout];
    
    // Set DataSource and Delegate
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
    
    // Register Classes for Usage
    [_collectionView registerClass:[CategoriesCollectionViewCell class] forCellWithReuseIdentifier:kRIDCategoriesCollectionViewCell];
    
    // Add CollectionView to View
    _collectionView.backgroundColor = [UIColor whiteColor];
    _collectionView.showsHorizontalScrollIndicator = false;
    [self.contentView addSubview:_collectionView];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpHeader
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    deviceBounds = [UtilityClass getDeviceBounds];
    
    // Back Ground Image
    _imageViewHeader = [[UIImageView alloc] initWithFrame:CGRectMake(10, 3, 20 , 20)];
    _imageViewHeader.contentMode = UIViewContentModeScaleAspectFit;
    
    // Label Title
    _labelTitle = [[UILabel alloc] init];
    CGSize sizeOflabelTitle = CGSizeMake(200, 22.0);
    CGPoint pointOflabelTitle = CGPointMake(_imageViewHeader.frame.origin.x + _imageViewHeader.frame.size.width + 5 , 3 );
    _labelTitle.frame = CGRectMake(pointOflabelTitle.x, pointOflabelTitle.y, sizeOflabelTitle.width, sizeOflabelTitle.height);
    _labelTitle.font = [UIFont fontWithName:kFontSFUIDisplaySemibold size:20];
    _labelTitle.textColor = UIColorFromRGB(0x4A4A4A);
    
    [self.contentView addSubview:_imageViewHeader];
    [self.contentView addSubview:_labelTitle];
    
}


#pragma mark - UICollectionViewDataSource -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    CategoriesCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kRIDCategoriesCollectionViewCell forIndexPath:indexPath];
    
    cell.dataModel = [self.dataArray objectAtIndex:indexPath.row];
    
    __weak CategoriesCollectionViewCell *weakCell = cell;
    
    [cell.imageViewBackground setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:cell.dataModel.image]]
                                 placeholderImage:[UtilityClass imageFromColor:[UIColor blackColor]]
                                          success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                              
                                              weakCell.imageViewBackground.image = image;
                                              [weakCell setNeedsLayout];
                                              
                                          } failure:nil];
    

    cell.labelTitle.text = cell.dataModel.categoryName;
    cell.labelSpins.text = @"";
    
//    if (indexPath.row == 0 && isfirstTimeTransform) { // make a bool and set YES initially, this check will prevent fist load transform
//        isfirstTimeTransform = NO;
//    }else{
//        cell.transform = TRANSFORM_CELL_VALUE; // the new cell will always be transform and without animation
//    }
    
    return cell;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return [self.dataArray count];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return 1;
}

#pragma mark - UICollectionViewDelegateFlowLayout -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return UIEdgeInsetsMake(5,5,0,0);  // top, left, bottom, right
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return CGSizeMake(widthOfCellsHome , 125);
}

#pragma mark - UICollectionViewDelegate-
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    CategoriesCollectionViewCell *cell = (CategoriesCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    
    if ([cell.dataModel.videoID intValue]  == 33)
    {
        CategoriesYoutubeViewController *vc = [[CategoriesYoutubeViewController alloc] init];
        
         [[self viewController].navigationController pushViewController:vc animated:true];
    }
    else
    {
        // Normal Controller
        CategoriesDetailPageViewController * vc = [[CategoriesDetailPageViewController alloc] init];
        vc.videoID = [cell.dataModel.videoID stringValue];
        vc.categoryTitle = cell.labelTitle.text;
        [[self viewController].navigationController pushViewController:vc animated:true];
    }
    
}

///*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
//- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
///*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
//{
//    float pageWidth = widthOfCellsHome ; // width + space
//    
//    float currentOffset = scrollView.contentOffset.x;
//    float targetOffset = targetContentOffset->x;
//    float newTargetOffset = 0;
//    
//    if (targetOffset > currentOffset)
//        newTargetOffset = ceilf(currentOffset / pageWidth) * pageWidth;
//    else
//        newTargetOffset = floorf(currentOffset / pageWidth) * pageWidth;
//    
//    if (newTargetOffset < 0)
//        newTargetOffset = 0;
//    else if (newTargetOffset > scrollView.contentSize.width)
//        newTargetOffset = scrollView.contentSize.width;
//    
//    targetContentOffset->x = currentOffset;
//    [scrollView setContentOffset:CGPointMake(newTargetOffset, scrollView.contentOffset.y) animated:YES];
//    
//    int index = newTargetOffset / pageWidth;
//    
//    if (index == 0) { // If first index
//        UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index  inSection:0]];
//        
//        [UIView animateWithDuration:ANIMATION_SPEED animations:^{
//            cell.transform = CGAffineTransformIdentity;
//        }];
//        cell = [self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index + 1  inSection:0]];
//        [UIView animateWithDuration:ANIMATION_SPEED animations:^{
//            cell.transform = TRANSFORM_CELL_VALUE;
//        }];
//    }else{
//        UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
//        [UIView animateWithDuration:ANIMATION_SPEED animations:^{
//            cell.transform = CGAffineTransformIdentity;
//        }];
//        
//        index --; // left
//        cell = [self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
//        [UIView animateWithDuration:ANIMATION_SPEED animations:^{
//            cell.transform = TRANSFORM_CELL_VALUE;
//        }];
//        
//        index ++;
//        index ++; // right
//        cell = [self.collectionView cellForItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0]];
//        [UIView animateWithDuration:ANIMATION_SPEED animations:^{
//            cell.transform = TRANSFORM_CELL_VALUE;
//        }];
//}
//}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSArray *) makeDataValuesForCategory:(NSArray *) dataArray
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    NSMutableArray *dataArrayWithDataModel = [[NSMutableArray alloc] init];
    
    for (NSDictionary * object in dataArray)
    {
        CategoriesDataModel *categoriesDataModel = [[CategoriesDataModel alloc] init];
        
        categoriesDataModel.videoID = [object objectForKey:kCKeyID];
        categoriesDataModel.image = [object objectForKey:kCKeyImage];
        categoriesDataModel.categoryName = [object objectForKey:kCKeyCategoryName];
        
        [dataArrayWithDataModel addObject:categoriesDataModel];
    }
    
    return [dataArrayWithDataModel copy];
}

@end
