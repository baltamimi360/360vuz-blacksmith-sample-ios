//
//  Live360TableViewCell.m
//  360 VUZ
//
//  Created by Mosib on 7/23/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "Live360TableViewCell.h"
#import "Live360CollectionViewCell.h"
#import "ServiceLive360.h"
#import "LiveHomeViewController.h"

@interface Live360TableViewCell ()
{
    CGRect deviceBounds;
    int pageNumber;
    BOOL lastResultFetchedEmpty;
    __weak Live360TableViewCell * weakSelf;
}
@end

@implementation Live360TableViewCell

static BOOL fetchInProgress;

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        // Initiailze Variables
        [self initializeVariables];
        
        //  Add Header to View
        [self setUpHeader];
        
        // Add CollectionView to View
        [self setUpCollectionView];
        
        // Service API
        [self serviceCategoriesAPI];
        
    }
    
    return self;
}


#pragma mark - API Methods -
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) serviceCategoriesAPI
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    STARTBACKGROUNDTHREAD
    ServiceLive360 *live360 = [[ServiceLive360 alloc]init];
    
    [live360 executeRequest:@{@"limit" : @"5" , @"page_no": [@(pageNumber) stringValue]}
                       success:^(NSDictionary  *responseObject)
     {
    
         [weakSelf SuccessForHomePageAPI:responseObject];
         
     }
                       failure:^(NSError * error) {
                           
                           [weakSelf FailureForHomePageAPI:error];
                           
                           
                           
                       }];
    ENDTHREAD
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) SuccessForHomePageAPI:(NSDictionary *) responseObject
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{

    if ([[responseObject objectForKey:@"data"] count] == 0)
    {
        lastResultFetchedEmpty = true;
    }
    
    else
    {
        pageNumber++;
    }
    
    if (!lastResultFetchedEmpty)
    {
    
    STARTMAINTHREAD
    
    [_collectionView performBatchUpdates:^{
        
        int resultsSize = (int)[self.dataArray count];
        
        [self.dataArray addObjectsFromArray:[self  makeDataValuesForLiveVideo:[responseObject objectForKey:@"data"]]];
        
        NSMutableArray *arrayWithIndexPaths = [NSMutableArray array];
        
        for (int i = resultsSize; i < resultsSize + [[responseObject objectForKey:@"data"] count]; i++)
            [arrayWithIndexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
        
        [_collectionView insertItemsAtIndexPaths:arrayWithIndexPaths];
        
    } completion:^(BOOL finished) {
        
        if(finished)
        {
             fetchInProgress = false;
        }
    }];
   
    ENDTHREAD
        
    }
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) FailureForHomePageAPI:(NSError *)error
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    STARTMAINTHREAD
    
     fetchInProgress = false;
    
    ENDTHREAD
}


#pragma mark - Private Methods -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) initializeVariables
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    weakSelf = self;
    
    fetchInProgress = false;
    
    pageNumber = 0;
    
    _dataArray = [[NSMutableArray alloc] init];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpCollectionView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    UICollectionViewFlowLayout * flowLayout =  [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    flowLayout.minimumInteritemSpacing = 0;
    flowLayout.minimumLineSpacing = 3;
    
    // Initialize Collection View
    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 25, deviceBounds.size.width, 135) collectionViewLayout:flowLayout];
    
    // Set DataSource and Delegate
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
    
    // Register Classes for Usage
    [_collectionView registerClass:[Live360CollectionViewCell class] forCellWithReuseIdentifier:kRIDLive360CollectionViewCell];
    
    // Add CollectionView to View
    _collectionView.backgroundColor = [UIColor whiteColor];
    _collectionView.showsHorizontalScrollIndicator = false;
    [self.contentView addSubview:_collectionView];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpHeader
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    deviceBounds = [UtilityClass getDeviceBounds];
    
    // Back Ground Image
    _imageViewHeader = [[UIImageView alloc] initWithFrame:CGRectMake(10, 8, 60 , 15)];
    _imageViewHeader.contentMode = UIViewContentModeScaleAspectFit;
    
    // Label Title
    _labelTitle = [[UILabel alloc] init];
    CGSize sizeOflabelTitle = CGSizeMake(200, 22.0);
    CGPoint pointOflabelTitle = CGPointMake(_imageViewHeader.frame.origin.x + _imageViewHeader.frame.size.width + 5 , 3 );
    _labelTitle.frame = CGRectMake(pointOflabelTitle.x, pointOflabelTitle.y, sizeOflabelTitle.width, sizeOflabelTitle.height);
    _labelTitle.font = [UIFont fontWithName:kFontSFUIDisplaySemibold size:20];
    _labelTitle.textColor = UIColorFromRGB(0x4A4A4A);
    
    [self.contentView addSubview:_imageViewHeader];
    [self.contentView addSubview:_labelTitle];
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if (scrollView.contentOffset.x == roundf(scrollView.contentSize.width -scrollView.frame.size.width)) {
    
        
        if (fetchInProgress)
            return;
        
         fetchInProgress = TRUE;
        
        // Service API
        [self serviceCategoriesAPI];
    }
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSArray *) makeDataValuesForLiveVideo:(NSArray *) dataArray
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    NSMutableArray *dataArrayWithDataModel = [[NSMutableArray alloc] init];
    
    for (NSDictionary * object in dataArray)
    {
        LiveVideoDataModel *liveVideoDataModel = [[LiveVideoDataModel alloc] init];
        
        liveVideoDataModel.videoID = [object objectForKey:kLVKeyID];
        liveVideoDataModel.image = [object objectForKey:kLVKeyImage];
        liveVideoDataModel.imageShortName = [object objectForKey:kLVKeyImageName];
        liveVideoDataModel.title = [object objectForKey:kLVKeyTitle];
        liveVideoDataModel.liveDescription = [object objectForKey:kLVKeyDescription];
        liveVideoDataModel.videoLink = [object objectForKey:kLVKeyVideoLink];
        liveVideoDataModel.datetime = [object objectForKey:kLVKeyDateTime];
        liveVideoDataModel.liveType = [object objectForKey:kLVKeyLiveType];
        liveVideoDataModel.credit = [object objectForKey:kLVKeyCredit];
        liveVideoDataModel.androidProductID = [object objectForKey:kLVKeyAndriodProductID];
        liveVideoDataModel.iosProductID = [object objectForKey:kLVKeyiOSProductID];
        liveVideoDataModel.price = [object objectForKey:kLVKeyPrice];
        liveVideoDataModel.twitterTags = [object objectForKey:kLVKeyTwitterTags];
        liveVideoDataModel.channelID = [object objectForKey:kLVKeyChannelID];
        liveVideoDataModel.status = [object objectForKey:kLVKeyStatus];
        liveVideoDataModel.notificationFlag = [object objectForKey:kLVKeyNotificationFlag];
        liveVideoDataModel.dateStatus = [object objectForKey:kLVKeyDateStatus];
        liveVideoDataModel.purchaseStatus = [object objectForKey:kLVKeyPurchaseStatus];
        
        [dataArrayWithDataModel addObject:liveVideoDataModel];
    }
    
    return [dataArrayWithDataModel copy];
}


#pragma mark - UICollectionViewDataSource -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    Live360CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kRIDLive360CollectionViewCell forIndexPath:indexPath];
    
    cell.dataModel = [self.dataArray objectAtIndex:indexPath.row];
    
    __weak Live360CollectionViewCell *weakCell = cell;
    
    [cell.imageMoviePoster setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:cell.dataModel.image]]
                          placeholderImage:[UtilityClass imageFromColor:[UIColor blackColor]]
                                   success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                      
                                       weakCell.imageMoviePoster.image = image;
                                       [weakCell setNeedsLayout];
                                       
                                   } failure:nil];
    
    cell.labelTitle.text = cell.dataModel.title;
    
    return cell;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return [_dataArray count];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return 1;
}

#pragma mark - UICollectionViewDelegateFlowLayout -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return UIEdgeInsetsMake(5,5,0,0);  // top, left, bottom, right
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return CGSizeMake(widthOfCellsHome , 125);
}

#pragma mark - UICollectionViewDelegate-
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    LiveHomeViewController *vc = [[LiveHomeViewController alloc] init];
    vc.modal = (LiveVideoDataModel *)[self.dataArray objectAtIndex:indexPath.row];
    [[self viewController].navigationController pushViewController:vc animated:true];
}

@end
