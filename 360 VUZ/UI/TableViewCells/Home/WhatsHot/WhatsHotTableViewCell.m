//
//  WhatsHotTableViewCell.m
//  360 VUZ
//
//  Created by Mosib on 7/20/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "WhatsHotTableViewCell.h"
#import "WhatsHotCollectionViewCell.h"
#import "ServiceWhatsHot.h"
#import "DetailVideoViewController.h"

@interface WhatsHotTableViewCell ()
{
    CGRect deviceBounds;
    BOOL isfirstTimeTransform;
    __weak WhatsHotTableViewCell * weakSelf;
}
@end

@implementation WhatsHotTableViewCell

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        weakSelf = self;
        
        // Add CollectionView to View
        [self setUpCollectionView];
        
        // Service API
        [self serviceWhatsHotAPI];
        
    }
    
    return self;
}

#pragma mark - API Methods -
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) serviceWhatsHotAPI
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    STARTBACKGROUNDTHREAD
    
    ServiceWhatsHot *whatsHot = [[ServiceWhatsHot alloc]init];
    
    [whatsHot executeRequest:nil
                       success:^(NSDictionary  *responseObject)
     {
         
         [weakSelf SuccessForHomePageAPI:responseObject];
         
     }
                       failure:^(NSError * error) {
                           
                           [weakSelf FailureForHomePageAPI:error];
                           
                           
                           
                       }];
    ENDTHREAD
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) SuccessForHomePageAPI:(NSDictionary *) responseObject
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self.dataArray = [self  makeDataValuesForWhatsHot:[responseObject objectForKey:kkeyData]];
    
    self.isDataLoaded = true;
    
    STARTMAINTHREAD
    
    // Reload Data
    [_collectionView reloadData];
    
    // Start Timer
    [self startTimer];
    
    ENDTHREAD
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) FailureForHomePageAPI:(NSError *)error
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    STARTMAINTHREAD
    
    //    [[UtilityClass sharedManger] alertErrorWithMessage:@"Something went wrong Please try again later"];
    
    ENDTHREAD
}


#pragma mark - Private Methods -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpCollectionView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    deviceBounds = [UtilityClass getDeviceBounds];
    
    UICollectionViewFlowLayout * flowLayout =  [[UICollectionViewFlowLayout alloc] init];
    
     [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
     flowLayout.minimumInteritemSpacing = 0;
     flowLayout.minimumLineSpacing = 0;
    
    // Initialize Collection View
    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, deviceBounds.size.width, RatioOfCellWhatsHot * deviceBounds.size.height) collectionViewLayout:flowLayout];
    
    // Set DataSource and Delegate
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
    
    
    // Register Classes for Usage
    [_collectionView registerClass:[WhatsHotCollectionViewCell class] forCellWithReuseIdentifier:kRIDWhatsHotCollectionViewCell];
    
    // Add CollectionView to View
    _collectionView.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:_collectionView];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) startTimer
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    _nextWhatsHotTimer = [NSTimer scheduledTimerWithTimeInterval: 6.0 target: self
                                                    selector: @selector(callAfterFiveSecond) userInfo: nil repeats: YES];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) stopTimer
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
        [_nextWhatsHotTimer invalidate];
        _nextWhatsHotTimer = nil;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSArray *) makeDataValuesForWhatsHot:(NSArray *) dataArray
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    NSMutableArray *dataArrayWithDataModel = [[NSMutableArray alloc] init];
    
    for (NSDictionary * object in dataArray)
    {
        WhatsHotDataModel *whatsHotDataModel = [[WhatsHotDataModel alloc] init];
        
        whatsHotDataModel.videoID = [[object objectForKey:kWHKeyID] stringValue] ;
        whatsHotDataModel.videoCity = [object objectForKey:kWHKeyVideoCity];
        whatsHotDataModel.categoryID = [[object objectForKey:kWHKeyCategoryID]stringValue] ; 
        whatsHotDataModel.subCategoryID = [object objectForKey:kWHKeySubCategoryID];
        whatsHotDataModel.videoTitle = [object objectForKey:kWHKeyVideoTitle];
        whatsHotDataModel.videoDescription = [object objectForKey:kWHKeyVideoDescription];
        whatsHotDataModel.websiteLink = [object objectForKey:kWHKeyWebsiteLink];
        whatsHotDataModel.videoThumbnail = [object objectForKey:kWHKeyVideoThumbnail];
        whatsHotDataModel.videoLink = [object objectForKey:kWHKeyVideoLink];
        whatsHotDataModel.webVideoLink = [object objectForKey:kWHKeyWebVideoLink];
        whatsHotDataModel.status = [object objectForKey:kWHKeyStatus];
        whatsHotDataModel.createdDate = [object objectForKey:kWHKeyCreatedDate];
        whatsHotDataModel.metaData = [object objectForKey:kWHKeyMetaData];
        whatsHotDataModel.whatsHot = [object objectForKey:kWHKeyWhatsHot];
        whatsHotDataModel.popular = [object objectForKey:kWHKeyPopular];
        whatsHotDataModel.videotype = [object objectForKey:kWHKeyVideoType];
        whatsHotDataModel.webVideoType = [object objectForKey:kWHKeyWebVideoType];
        whatsHotDataModel.userID = [object objectForKey:kWHKeyUserID];
        whatsHotDataModel.videoFeature = [object objectForKey:kWHKeyVideoFeature];
        whatsHotDataModel.panoramaImage = [object objectForKey:kWHKeyPanoramImage];
        whatsHotDataModel.notificationFlag = [object objectForKey:kWHKeyNotificationFlag];
        whatsHotDataModel.isImage = [object objectForKey:kWHKeyIsImage];
        whatsHotDataModel.rating = [object objectForKey:kWHKeyRating];
        whatsHotDataModel.categoryName = [object objectForKey:kWHKeyCategoryID];
        whatsHotDataModel.viewCount = [[object objectForKey:kWHKeyViewCount] stringValue];
        whatsHotDataModel.likeCount = [[object objectForKey:kWHKeyLikeCount] stringValue];
        
        [dataArrayWithDataModel addObject:whatsHotDataModel];
    }
    
    return [dataArrayWithDataModel copy];
}

#pragma mark - UICollectionViewDataSource -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    WhatsHotCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kRIDWhatsHotCollectionViewCell forIndexPath:indexPath];
    
       //  Set Cell Data Model
    cell.dataModel = [self.dataArray objectAtIndex:indexPath.row];
    cell.panaromaFileName = [self getSuggestedFilename:cell.dataModel.panoramaImage];
    
    
#warning TODO: fix this later... Remove this if when we go live
    
    
    if (indexPath.row == 0)
    {
        cell.panaromaFileName = @"1bf818a9593eeb78dcde41d813de8c8c.mp4";
    }
    
    // Load File
     if ([[UtilityClass sharedManger] checkIfFileExistsAtDocumentsDirectory:cell.panaromaFileName])
        {
            
            if (cell.orionView.isPaused)
            {
                [cell.orionView play:0.0f];
            }
            else
            {
            
              [cell playVideo];
                
            }
        }
        
        else
        {
            STARTBACKGROUNDTHREAD
            [[UtilityClass sharedManger] downloadFileFromURLString:cell.dataModel.panoramaImage completedSuccessfully:^(BOOL success) {
                if (success)
                {
                    STARTMAINTHREAD
                    [cell playVideo];
                    ENDTHREAD
                }
            }];
            
            ENDTHREAD
        }
    
    cell.labelTitle.text = cell.dataModel.videoTitle;
    cell.labelSpins.text = cell.dataModel.viewCount;
    cell.label360video.text = @"360° Video";
    
   
    return cell;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return [_dataArray count];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return 1;
}

#pragma mark - UICollectionViewDelegateFlowLayout -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return UIEdgeInsetsMake(0,0,0,0);  // top, left, bottom, right
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return CGSizeMake(deviceBounds.size.width, RatioOfCellWhatsHot * deviceBounds.size.height);
}

#pragma mark - UICollectionViewDelegate-
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    DetailVideoViewController * vc =[[DetailVideoViewController alloc] init];
    vc.CellType = WhatsHot;
    vc.modal = [self.dataArray objectAtIndex:indexPath.row];
    
    UINavigationController *parentNavigationController = [self viewController].navigationController;
    
    [parentNavigationController pushViewController:vc animated:true];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
    float pageWidth = deviceBounds.size.width; // width + space
    
    float currentOffset = scrollView.contentOffset.x;
    float targetOffset = targetContentOffset->x;
    float newTargetOffset = 0;
    
    if (targetOffset > currentOffset)
        newTargetOffset = ceilf(currentOffset / pageWidth) * pageWidth;
    else
        newTargetOffset = floorf(currentOffset / pageWidth) * pageWidth;
    
    if (newTargetOffset < 0)
        newTargetOffset = 0;
    else if (newTargetOffset > scrollView.contentSize.width)
        newTargetOffset = scrollView.contentSize.width;
    
    targetContentOffset->x = currentOffset;
    [scrollView setContentOffset:CGPointMake(newTargetOffset, scrollView.contentOffset.y) animated:YES];
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) callAfterFiveSecond
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    NSArray *visibleItems = [self.collectionView indexPathsForVisibleItems];
    if ([visibleItems count] > 0)
    {
    NSIndexPath *currentItem = [visibleItems objectAtIndex:0];
    NSIndexPath *nextItem = [NSIndexPath indexPathForItem:currentItem.item + 1 inSection:currentItem.section];
    
    if (nextItem.row  == [_dataArray count])
    {
        nextItem = [NSIndexPath indexPathForRow:0 inSection:currentItem.section];
    }
    
    [self.collectionView scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    }

}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSString *) getSuggestedFilename:(NSString *) fileName
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
   return  [fileName lastPathComponent];
}

@end
