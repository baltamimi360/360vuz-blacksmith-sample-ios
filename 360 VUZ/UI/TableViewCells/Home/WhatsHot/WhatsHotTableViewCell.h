//
//  WhatsHotTableViewCell.h
//  360 VUZ
//
//  Created by Mosib on 7/20/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WhatsHotTableViewCell : UITableViewCell <UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic, retain) UICollectionView *collectionView;
@property (nonatomic)  NSTimer *nextWhatsHotTimer;
@property (nonatomic)  NSArray * dataArray;
@property (nonatomic)  BOOL isDataLoaded;

- (void) stopTimer;
- (void) startTimer;

@end
