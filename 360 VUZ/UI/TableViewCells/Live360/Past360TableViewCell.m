//
//  VideoTableViewCell.m
//  360 VUZ
//
//  Created by Mosib on 7/11/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "Past360TableViewCell.h"

@interface Past360TableViewCell ()
{
    UIButton *buttonShareIcon;
    UIImageView *imageShareIcon;
    UIButton * buttonWhatsApp;
    UIButton * buttonEmail;
    UIButton * buttonfacebook;
    UIButton * buttontwitter;
}
@end

@implementation Past360TableViewCell

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        // Set Up UI
        [self setUpUI];
        
    }
    
    return self;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) setUpUI
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    CGRect  deviceBounds = [UtilityClass getDeviceBounds];
    self.backgroundColor = [UIColor whiteColor];
    
    // Back Ground Image
    self.imageMoviePoster = [[UIImageView alloc] initWithFrame:CGRectMake(10, 5, deviceBounds.size.width - 20 , MAX(heightOfCellVideo,RatioOfCellWhatsHot * deviceBounds.size.height) - 10)];
    _imageMoviePoster.backgroundColor = [UIColor blackColor];
    _imageMoviePoster.contentMode = UIViewContentModeScaleAspectFit;
    
    // Add Shade
    UIImageView *imageViewShade = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, _imageMoviePoster.frame.size.width , _imageMoviePoster.frame.size.height)];
    imageViewShade.image = [UIImage imageNamed:@"bg_shade"];
    [_imageMoviePoster addSubview:imageViewShade];
    
    // ImageView Play
    UIImageView *imagePlayIcon = [[UIImageView alloc] initWithFrame:CGRectMake(10, _imageMoviePoster.frame.size.height - 60, 40 , 40)];
    imagePlayIcon.image = [UIImage imageNamed:@"play-icon"];
    
    //
    imageShareIcon = [[UIImageView alloc] initWithFrame:CGRectMake(_imageMoviePoster.frame.size.width - 35, _imageMoviePoster.frame.size.height - 50, 20 , 20)];
    imageShareIcon.image = [UIImage imageNamed:@"share-icon"];
    imageShareIcon.contentMode = UIViewContentModeScaleAspectFit;
    
    // Button Share Share
    buttonShareIcon = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonShareIcon addTarget:self
                        action:@selector(actionButtonShare:)
              forControlEvents:UIControlEventTouchUpInside];
    buttonShareIcon.frame = CGRectMake(_imageMoviePoster.frame.size.width - 50, _imageMoviePoster.frame.size.height - 65, 60 , 60);
    //    buttonShareIcon.backgroundColor = [UIColor blueColor];
    
    [_imageMoviePoster addSubview:imagePlayIcon];
    [_imageMoviePoster addSubview:imageShareIcon];
    
    
    // Label Title
    self.labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(imagePlayIcon.frame.origin.x + imagePlayIcon.frame.size.width + 5, imagePlayIcon.frame.origin.y, imageShareIcon.frame.origin.x - imagePlayIcon.frame.origin.x -imagePlayIcon.frame.size.width - 20, 22)];
    [self.labelTitle setTextColor:[UIColor whiteColor]];
    self.labelTitle.adjustsFontSizeToFitWidth = YES;
    self.labelTitle.font = [UIFont fontWithName:kFontSFUIDisplayBold size:18];
    [_imageMoviePoster addSubview:self.labelTitle];
    
    // Label 360 Video
    self.label360video = [[UILabel alloc] initWithFrame:CGRectMake(self.labelTitle.frame.origin.x , self.labelTitle.frame.origin.y + self.labelTitle.frame.size.height, _labelTitle.frame.size.width, 14)];
    [self.label360video setNumberOfLines:1];
    self.label360video.font = [UIFont fontWithName:kFontSFUIDisplayRegular size:12];
    [self.label360video setTextAlignment:NSTextAlignmentLeft];
    [self.label360video setTextColor:[UIColor colorWithRed:19/255.0 green:174/255.0 blue:229/255.0 alpha:1/1.0]];
    [self.label360video setBackgroundColor:[UIColor clearColor]];
    self.label360video.adjustsFontSizeToFitWidth = YES;
    [_imageMoviePoster addSubview:self.label360video];
    
    
    // Button Share WhatsApp
    buttonWhatsApp = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonWhatsApp addTarget:self
                       action:@selector(actionButtonWhatsApp:)
             forControlEvents:UIControlEventTouchUpInside];
    buttonWhatsApp.frame = CGRectMake(deviceBounds.size.width - 50, MAX(heightOfCellVideo,RatioOfCellWhatsHot * deviceBounds.size.height) - 35, 30 , 30);
    [buttonWhatsApp setImage:[UIImage imageNamed:@"whatsapp-share-icon"] forState:UIControlStateNormal];
    [buttonWhatsApp scaleToFill];
    
    
    // Button Share Email
    buttonEmail = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonEmail addTarget:self
                    action:@selector(actionButtonEmail:)
          forControlEvents:UIControlEventTouchUpInside];
    buttonEmail.frame = CGRectMake(buttonWhatsApp.frame.origin.x - 55, MAX(heightOfCellVideo,RatioOfCellWhatsHot * deviceBounds.size.height) - 35, 30 , 30);
    [buttonEmail setImage:[UIImage imageNamed:@"email-share-icon"] forState:UIControlStateNormal];
    [buttonEmail scaleToFill];
    
    
    // Button Facebook
    buttonfacebook = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonfacebook addTarget:self
                       action:@selector(actionButtonFacebook:)
             forControlEvents:UIControlEventTouchUpInside];
    buttonfacebook.frame = CGRectMake(buttonEmail.frame.origin.x - 55, MAX(heightOfCellVideo,RatioOfCellWhatsHot * deviceBounds.size.height) - 35, 30 , 30);
    [buttonfacebook setImage:[UIImage imageNamed:@"facebook-share-icon"] forState:UIControlStateNormal];
    [buttonfacebook scaleToFill];
    
    
    // Button Twitter
    buttontwitter = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttontwitter addTarget:self
                      action:@selector(actionButtonTwitter:)
            forControlEvents:UIControlEventTouchUpInside];
    buttontwitter.frame = CGRectMake(buttonfacebook.frame.origin.x - 55, MAX(heightOfCellVideo,RatioOfCellWhatsHot * deviceBounds.size.height) - 35, 30 , 30);
    [buttontwitter setImage:[UIImage imageNamed:@"twitter-share-icon"] forState:UIControlStateNormal];
    [buttontwitter scaleToFill];
    
    // ImageView Clock
    UIImageView *imageViewClock = [[UIImageView alloc] initWithFrame:CGRectMake(self.imageMoviePoster.frame.origin.x + 15  , self.imageMoviePoster.frame.origin.y + self.imageMoviePoster.frame.size.height + 15, 14, 14)];
    imageViewClock.image = [UIImage imageNamed:@"Clock"];
    
    // Label Date
    _labelDate = [[UILabel alloc] init];
    _labelDate.frame = CGRectMake(imageViewClock.frame.origin.x + imageViewClock.frame.size.width + 10 , imageViewClock.frame.origin.y, 50, 14);
    _labelDate.text = @"01 January";
    _labelDate.font = [UIFont fontWithName:kFontSFUIDisplayRegular size:10];
    _labelDate.textColor = [UIColor colorWithRed:178/255.0 green:178/255.0 blue:178/255.0 alpha:1/1.0];
    
    // Label Time
    _labelTime = [[UILabel alloc] init];
    _labelTime.frame = CGRectMake(_labelDate.frame.origin.x + _labelDate.frame.size.width + 15 , imageViewClock.frame.origin.y, 50, 14);
    _labelTime.text = @"04:00 PM";
    _labelTime.font = [UIFont fontWithName:kFontSFUIDisplayRegular size:10];
    _labelTime.textColor = [UIColor colorWithRed:178/255.0 green:178/255.0 blue:178/255.0 alpha:1/1.0];
    
    UIButton *buttonWatch = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonWatch addTarget:self
                    action:@selector(actionButtonWatch:)
          forControlEvents:UIControlEventTouchUpInside];
    [buttonWatch setTitle:@"Watch" forState:UIControlStateNormal];
    buttonWatch.backgroundColor = UIColorFromRGB(0x29AEE3);
    CGSize sizeOfbuttonWatch = CGSizeMake(80, 30.0);
    CGPoint pointOfbuttonLogin = CGPointMake(_imageMoviePoster.frame.size.width - sizeOfbuttonWatch.width - 10, self.imageMoviePoster.frame.origin.y + self.imageMoviePoster.frame.size.height + 7.5);
    buttonWatch.frame = CGRectMake(pointOfbuttonLogin.x, pointOfbuttonLogin.y, sizeOfbuttonWatch.width, sizeOfbuttonWatch.height);
    [buttonWatch.titleLabel setFont:[UIFont fontWithName:kFontSFUIDisplayRegular size:14]];
    buttonWatch.layer.cornerRadius = 4;
    buttonWatch.clipsToBounds = YES;
    
    
    [self.contentView addSubview:buttonWhatsApp];
    [self.contentView addSubview:buttonEmail];
    [self.contentView addSubview:buttonfacebook];
    [self.contentView addSubview:buttontwitter];
    [self.contentView addSubview:self.imageMoviePoster];
    [self.contentView addSubview:buttonShareIcon];
    [self.contentView addSubview:buttonWatch];
    [self.contentView addSubview:_labelTime];
    [self.contentView addSubview:_labelDate];
    [self.contentView addSubview:imageViewClock];
}

# pragma mark - UIButton Actions

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonShare:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if ([buttonShareIcon isSelected])
    {
        [buttonShareIcon setSelected:false];
        
        CGRect frame = sender.frame;
        frame.origin.y = frame.origin.y + 25;
        sender.frame = frame;
        
        [self annimateOut];
        
        imageShareIcon.image = [UIImage imageNamed:@"share-icon"];
    }
    
    else
    {
        [buttonShareIcon setSelected:true];
        
        CGRect frame = sender.frame;
        frame.origin.y = frame.origin.y - 25;
        sender.frame = frame;
        
        imageShareIcon.image = [UIImage imageNamed:@"close-icon"];
        
        [self annimateIn];
        
    }
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonWhatsApp:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonEmail:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonFacebook:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonWatch:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonTwitter:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) annimateIn
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
    [self.imageMoviePoster layer].anchorPoint = CGPointMake(0.5f, 0.65f);
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) annimateOut
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
    [self.imageMoviePoster layer].anchorPoint = CGPointMake(0.5f, 0.5f);
    
}
@end
