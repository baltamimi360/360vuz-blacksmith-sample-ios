//
//  Past360TableViewCell.h
//  360 VUZ
//
//  Created by Mosib on 7/30/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiveVideoDataModel.h"

@interface Past360TableViewCell : UITableViewCell

@property (strong, nonatomic) UILabel *labelTitle;
@property (strong, nonatomic) UILabel *label360video;
@property (strong, nonatomic) UIImageView *imageMoviePoster;
@property (strong, nonatomic) UILabel *labelDate;
@property (strong, nonatomic) UILabel *labelTime;
@property (strong) LiveVideoDataModel *dataModel;

@end
