//
//  VirtualDetailTableViewCell.h
//  360 VUZ
//
//  Created by Mosib on 8/15/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VirtualDetailTableViewCell : UITableViewCell
@property (strong, nonatomic) UILabel *labelTitle;
@property (strong, nonatomic) UILabel *labelDescription;

/*
 * Adjust UI Elements
 */
- (void) adjustFrame;

/*
 * Show Rating View
 */
- (void) showRatingView;

/*
 * Hide Rating View
 */
- (void) hideRatingView;

@end
