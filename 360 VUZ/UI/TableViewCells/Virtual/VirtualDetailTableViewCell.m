//
//  VirtualDetailTableViewCell.m
//  360 VUZ
//
//  Created by Mosib on 8/15/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "VirtualDetailTableViewCell.h"
#import "RatingView.h"

@interface VirtualDetailTableViewCell()
{
     RatingView *ratingView;
}

@end


@implementation VirtualDetailTableViewCell

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        // Set Up UI
        [self setUpUI];
        
    }
    
    return self;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpUI
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self.backgroundColor = [UIColor whiteColor];
    CGRect  deviceBounds = [UtilityClass getDeviceBounds];
    

    // Label Title
    self.labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, deviceBounds.size.width - 30, 15)];
    [self.labelTitle setTextColor:[UIColor blackColor]];
    self.labelTitle.adjustsFontSizeToFitWidth = YES;
    self.labelTitle.minimumScaleFactor = 0.8;
    self.labelTitle.numberOfLines = 2;
    self.labelTitle.font = [UIFont fontWithName:kFontSFUIDisplaySemibold size:14];
    [self.contentView addSubview:self.labelTitle];
    
    // Label 360 Video
    self.labelDescription = [[UILabel alloc] initWithFrame:CGRectMake(self.labelTitle.frame.origin.x + 10  , self.labelTitle.frame.origin.y + self.labelTitle.frame.size.height + 5 , self.labelTitle.frame.size.width  - 20, 15)];
    self.labelDescription.font = [UIFont fontWithName:kFontSFUIDisplayLight size:12];
    [self.labelDescription setTextAlignment:NSTextAlignmentLeft];
    [self.labelDescription setTextColor:[UIColor blackColor]];
    [self.labelDescription setBackgroundColor:[UIColor clearColor]];
    self.labelDescription.numberOfLines = 0;
    [self.contentView addSubview:self.labelDescription];
    
    ratingView = [[RatingView alloc] initWithFrame:CGRectMake(self.labelTitle.frame.origin.x + 10  , 16 , 200, 40)
                                      selectedImageName:@"selected.png"
                                        unSelectedImage:@"unSelected.png"
                                               minValue:0
                                               maxValue:5
                                          intervalValue:0.5
                                             stepByStep:NO];
    
     [self.contentView addSubview:self.labelTitle];
     [self.contentView addSubview:self.labelDescription];
     [self.contentView addSubview:ratingView];
    
     [self hideRatingView];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) adjustFrame
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
    CGRect frame = _labelDescription.frame;
    
    CGRect adjustedFrame = [_labelDescription.text boundingRectWithSize:CGSizeMake(_labelDescription.frame.size.width, 0)
                                                          options:NSStringDrawingUsesLineFragmentOrigin
                                                       attributes:@{NSFontAttributeName:[UIFont fontWithName:kFontSFUIDisplayLight size:12]}
                                                          context:nil];
    
    frame.size.height = adjustedFrame.size.height;
    
    _labelDescription.frame = frame;
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) showRatingView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    ratingView.hidden = false;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) hideRatingView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
   ratingView.hidden = true;
}

@end
