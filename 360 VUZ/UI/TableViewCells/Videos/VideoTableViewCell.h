//
//  VideoTableViewCell.h
//  360 VUZ
//
//  Created by Mosib on 7/11/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CategoryDetailDataModel.h"

@interface VideoTableViewCell : UITableViewCell

@property (strong, nonatomic) UILabel *labelTitle;
@property (strong, nonatomic) UILabel *label360video;
@property (strong, nonatomic) UILabel *labelSpins;
@property (strong, nonatomic) UIImageView *imageMoviePoster;
@property (strong, nonatomic) CategoryDetailDataModel *dataModel;

@end
