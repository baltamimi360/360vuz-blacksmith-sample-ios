//
//  EditProfileDetailTableViewCell.m
//  360 VUZ
//
//  Created by Mosib on 8/1/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "EditProfileDetailTableViewCell.h"

@implementation EditProfileDetailTableViewCell

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        // Set Up UI
        [self setUpUI];
        
    }
    
    return self;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) setUpUI
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self.backgroundColor = [UIColor whiteColor];
    CGRect  deviceBounds = [UtilityClass getDeviceBounds];
    
    // TextFeild Email
    _textfeildEmail = [[UITextField alloc] init];
    _textfeildEmail.frame = CGRectMake(20, 15, deviceBounds.size.width - 40, 30);
    _textfeildEmail.font = [UIFont fontWithName:kFontSFUIDisplayRegular size:18];
    _textfeildEmail.backgroundColor = [UIColor clearColor];
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1;
    border.borderColor = [UIColor lightGrayColor].CGColor;
    border.frame = CGRectMake(0, _textfeildEmail.frame.size.height - borderWidth, _textfeildEmail.frame.size.width, _textfeildEmail.frame.size.height);
    border.borderWidth = borderWidth;
    [_textfeildEmail.layer addSublayer:border];
    _textfeildEmail.layer.masksToBounds = YES;
    [_textfeildEmail addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];

    [self.contentView addSubview:_textfeildEmail];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void)textFieldDidChange :(UITextField *) textField
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    NSString * type;
    
    if (_indexPath.section == 0)
    {
    
    if (_indexPath.row == 0)
    {
        type = @"FirstName";
    }
    
    else if (_indexPath.row == 1)
    {
       type = @"LastName";
    }
    
    else if (_indexPath.row == 2)
    {
        type = @"Website";
    }
    
    else if (_indexPath.row == 3)
    {
        type = @"Bio";
    }
    }
    
    else if (_indexPath.section == 1)
    {
        if (_indexPath.row == 0)
        {
            type = @"EmailID";
        }
        
        else if (_indexPath.row == 1)
        {
            type = @"PhoneNumber";
        }
    }
    
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:kNotificationTextFeildUpdatedInEditProfile
     object:nil userInfo:@{@"Type":type,@"value":textField.text}];
}

@end
