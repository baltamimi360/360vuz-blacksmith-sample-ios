//
//  OrionViewController.h
//  360 VUZ
//
//  Created by Mosib on 8/17/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiveVideoDataModel.h"

@interface OrionViewControllerLive : UIViewController

@property (nonatomic) LiveVideoDataModel *dataModel;

@end
