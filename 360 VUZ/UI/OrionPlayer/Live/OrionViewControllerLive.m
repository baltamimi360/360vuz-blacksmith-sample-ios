//
//  OrionViewController.m
//  360 VUZ
//
//  Created by Mosib on 8/17/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "OrionViewControllerLive.h"
#import "OrionLiveTwitterTableViewCell.h"
#import "OrionLiveTwitterDataModel.h"

@interface OrionViewControllerLive ()<UITableViewDataSource, UITableViewDelegate, Orion1ViewDelegate,UIGestureRecognizerDelegate>
{
    BOOL _isSeeking;
    BOOL _controlsHidden;
  
    NSMutableArray *arrTwitterFeeds;
    BOOL _isUsingCardboard,isFirstTime;
    //
    
    ControlMode controlMode;
    UIImageView *imageViewLiveChatIcon;
    UIView *textFeildBorder;
    UITextField *textFeildComments;
    UIButton * buttonBack;
    UIButton * buttonAddFriends;
    UIView *seperatorViewLeft;
    UIButton * buttonTwitter;
    UIView *seperatorViewCenter;
    UIButton * buttonVR;
    UIView *seperatorViewRight;
    UIButton * buttonRefresh;
    UITapGestureRecognizer *tapGr;
}

@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *dataArray;
@property (nonatomic) Orion1View* orionView;
@property (nonatomic) UISlider  *timeSlider;
@property (nonatomic) UIActivityIndicatorView *bufferIndicator;
@property (nonatomic) UIImageView *imageViewLiveProfile;

@end

@implementation OrionViewControllerLive

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)viewDidLoad
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super viewDidLoad];
    
    // Initialize Variables
    [self initializeVariables];
    
    // Set Up View
    [self setUpView];
    
    // Set Up TableVIew
    [self setUpTableView];

}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonBack:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [AppDelegate appDelegate].allowedOrientation = UIInterfaceOrientationMaskPortrait;
    
    [self.navigationController dismissViewControllerAnimated:false completion:nil];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonTwitter:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
   self.tableView.hidden = false;
    
    [self hideButtons];
    
    // Load Twitter Feed
    [self loadTwitterFeed];
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonRefresh:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [_orionView play:0.0f];
    [self showHideControlBars:!_controlsHidden];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonVR:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if (controlMode == SENSORS)
    {
         [self hideButtons];
        
        _orionView.vrModeEnabled = YES;
        controlMode = VR;
        
        buttonVR.hidden = false;
        
        tapGr.enabled = false;
    }
    
    else
    {
         [self showButtons];
        
        _orionView.vrModeEnabled = false;
        controlMode = SENSORS;
        
        tapGr.enabled = true;
    }
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonAddFriends:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
}

#pragma mark Orientation changed
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void)orientationChanged:(id)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
    if ([[UIDevice currentDevice]orientation] == UIInterfaceOrientationLandscapeLeft){
        //do something or rather
        NSLog(@"landscape left");
    }
    if ([[UIDevice currentDevice]orientation] == UIInterfaceOrientationLandscapeRight){
      
        NSLog(@"landscape right");
    }
    if ([[UIDevice currentDevice]orientation] == UIInterfaceOrientationPortrait){
        
        NSLog(@"portrait");
    }
    
    if ([[UIDevice currentDevice]orientation] == UIInterfaceOrientationPortraitUpsideDown){
        
        NSLog(@"UIInterfaceOrientationPortraitUpsideDown");
    }
    
    if ([[UIDevice currentDevice]orientation] == UIInterfaceOrientationUnknown){
        
        NSLog(@"UIInterfaceOrientationUnknown");
    }

    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) viewWillAppear:(BOOL)animated
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super viewWillAppear:animated];
    
   UserDetailsDataModel * user =  [[Settings sharedManger] getUserDetails];
    
    __weak OrionViewControllerLive *weakSelf = self;
    
    [_imageViewLiveProfile setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:user.imageUrlPrimary]]
                             placeholderImage:[UIImage imageNamed:@"profile-photo"]
                                      success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                          
                                          weakSelf.imageViewLiveProfile.image = image;
                                          weakSelf.imageViewLiveProfile.layer.cornerRadius = weakSelf.imageViewLiveProfile.frame.size.height /2;
                                          weakSelf.imageViewLiveProfile.layer.masksToBounds = YES;
                                          weakSelf.imageViewLiveProfile.layer.borderWidth = 0;
                                          
                                      } failure:nil];
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) viewDidAppear:(BOOL)animated
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super viewDidAppear:animated];
    
    // Load Video
    [self loadOrionVideo];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)didReceiveMemoryWarning
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private Methods -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) initializeVariables
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
        _dataArray = [[NSMutableArray alloc] init];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) loadOrionVideo
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
    [_orionView initVideoWithUrl:[[UtilityClass sharedManger] getCDNURL:[NSURL URLWithString:_dataModel.videoLink]] previewImageUrl:nil licenseFileUrl:[[UtilityClass sharedManger] orionLicenseURL]];
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
     self.view.backgroundColor = [UIColor blackColor];
    
    _orionView = [[Orion1View alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    _orionView.delegate = self;
    _orionView.overrideSilentSwitch = YES;
    
    [self.view addSubview:_orionView];
    
    // Tap gesture regocnizer
    tapGr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected:)];
    tapGr.delegate = self;
    [_orionView addGestureRecognizer:tapGr];
    
    // button Back
    buttonBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonBack addTarget:self
                   action:@selector(actionButtonBack:)
         forControlEvents:UIControlEventTouchUpInside];
    buttonBack.frame = CGRectMake(10, [UtilityClass getDeviceBounds].size.height - 35, 40 , 30);
    [buttonBack setImage:[UIImage imageNamed:@"back-button-White"] forState:UIControlStateNormal];
    
    [self.view addSubview:buttonBack];
    
    // Button Add Friends
    buttonAddFriends = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonAddFriends addTarget:self
                         action:@selector(actionButtonAddFriends:)
               forControlEvents:UIControlEventTouchUpInside];
    buttonAddFriends.frame = CGRectMake(10, 10, 40 , 40);
    [buttonAddFriends.imageView setContentMode:UIViewContentModeScaleAspectFit];
    [buttonAddFriends setImage:[UIImage imageNamed:@"add-friends-white"] forState:UIControlStateNormal];
    
    [self.view addSubview:buttonAddFriends];
    
    // Seperator View Left
    seperatorViewLeft = [[UIView alloc] initWithFrame:CGRectMake(54.5, 15, 1, 30)];
    seperatorViewLeft.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:seperatorViewLeft];
    
    // Button Twitter Feed
    buttonTwitter = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonTwitter addTarget:self
                      action:@selector(actionButtonTwitter:)
            forControlEvents:UIControlEventTouchUpInside];
    [buttonTwitter.imageView setContentMode:UIViewContentModeScaleAspectFit];
    buttonTwitter.frame = CGRectMake(60, 10, 40 , 40);
    [buttonTwitter setImage:[UIImage imageNamed:@"twitter-icon-white"] forState:UIControlStateNormal];
    
    [self.view addSubview:buttonTwitter];
    
    // Seperator View Center
    seperatorViewCenter = [[UIView alloc] initWithFrame:CGRectMake(104.5, 15, 1, 30)];
    seperatorViewCenter.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:seperatorViewCenter];
    
    // Button VR
    buttonVR = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonVR addTarget:self
                 action:@selector(actionButtonVR:)
       forControlEvents:UIControlEventTouchUpInside];
    [buttonVR.imageView setContentMode:UIViewContentModeScaleAspectFit];
    [buttonVR setImageEdgeInsets:UIEdgeInsetsMake(4, 4, 4, 4)];
    buttonVR.frame = CGRectMake(110, 10, 40 , 40);
    [buttonVR setImage:[UIImage imageNamed:@"select_VR"] forState:UIControlStateNormal];
    
    [self.view addSubview:buttonVR];
    
    // Seperator View Right
    seperatorViewRight = [[UIView alloc] initWithFrame:CGRectMake(154.5, 15, 1, 30)];
    seperatorViewRight.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:seperatorViewRight];
    
    // Button Refresh
    buttonRefresh = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonRefresh addTarget:self
                      action:@selector(actionButtonRefresh:)
            forControlEvents:UIControlEventTouchUpInside];
    [buttonRefresh.imageView setContentMode:UIViewContentModeScaleAspectFit];
    buttonRefresh.frame = CGRectMake(160, 10, 40 , 40);
    [buttonRefresh setImageEdgeInsets:UIEdgeInsetsMake(2, 2, 2, 2)];
    [buttonRefresh setImage:[UIImage imageNamed:@"refresh"] forState:UIControlStateNormal];
    
    [self.view addSubview:buttonRefresh];
    
    // Live Chat Icon Image
    imageViewLiveChatIcon = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.view.frame) - 90, 19.5, 74, 11)];
    imageViewLiveChatIcon.image = [UIImage imageNamed:@"live-chat-icon"];
    
    [self.view addSubview:imageViewLiveChatIcon];
    
    // TextField Password
    textFeildComments = [[UITextField alloc] init];
    CGSize sizeOfTextFeildComments = CGSizeMake(CGRectGetWidth( self.view.frame)/2, 20.0);
    CGPoint PointTextFeildComments = CGPointMake(CGRectGetWidth( self.view.frame)/2 - sizeOfTextFeildComments.width/2, CGRectGetHeight( self.view.frame) - 40 );
    textFeildComments.frame = CGRectMake(PointTextFeildComments.x, PointTextFeildComments.y, sizeOfTextFeildComments.width, sizeOfTextFeildComments.height);
    textFeildComments.borderStyle = UITextBorderStyleNone;
    textFeildComments.font = [UIFont fontWithName:kFontSFUIDisplayThin size:16];
    textFeildComments.autocorrectionType = UITextAutocorrectionTypeNo;
    textFeildComments.keyboardType = UIKeyboardTypeDefault;
    textFeildComments.returnKeyType = UIReturnKeyDone;
    textFeildComments.clearButtonMode = UITextFieldViewModeWhileEditing;
    textFeildComments.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    textFeildComments.enabled = NO;
    UIColor *colorPlaceHolder = [UIColor whiteColor];
    textFeildComments.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Say Something.." attributes:@{NSForegroundColorAttributeName: colorPlaceHolder}];
    
    textFeildBorder = [[UIView alloc] initWithFrame:CGRectMake(PointTextFeildComments.x, PointTextFeildComments.y + sizeOfTextFeildComments.height + 3, textFeildComments.frame.size.width, 1)];
    textFeildBorder.backgroundColor = [UIColor whiteColor];
    
    [self.view addSubview:textFeildBorder];

    [self.view addSubview:textFeildComments];
    
    // ImageView Profile
    _imageViewLiveProfile = [[UIImageView alloc] initWithFrame:CGRectMake(textFeildComments.frame.origin.x - 50, CGRectGetHeight( self.view.frame) - 40, 30, 30)];
    _imageViewLiveProfile.image = [UIImage imageNamed:@"profile-photo"];
    
    [self.view addSubview:_imageViewLiveProfile];

    
    // Buffering
    _bufferIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    _bufferIndicator.center = CGPointMake(_orionView.frame.size.width/2, _orionView.frame.size.height/2);
    [self.view addSubview:_bufferIndicator];
    
    [_bufferIndicator startAnimating];

    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) loadTwitterFeed
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    STARTBACKGROUNDTHREAD
    
    TWTRAPIClient *client = [[TWTRAPIClient alloc] init];
    NSString *statusesShowEndpoint = [NSString stringWithFormat:kTwitterFeedURL,_dataModel.twitterTags];
    
    NSError *clientError;
    
    NSURLRequest *request = [client URLRequestWithMethod:kURLMethodGET URL:statusesShowEndpoint parameters:nil error:&clientError];
    
    if (request) {
        [client sendTwitterRequest:request completion:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
            if (data) {
                // handle the response data e.g.
                NSError *jsonError;
                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
                
                [_dataArray removeAllObjects];
                
                for (NSDictionary *twitterfeed in [json objectForKey:kOrionPlayerKeyStatus]){
                    
                    OrionLiveTwitterDataModel *feedTwitter      = [[OrionLiveTwitterDataModel alloc] init];
                    feedTwitter.tweetText            = [twitterfeed objectForKey:kOrionPlayerKeyText];
                    feedTwitter.twitterHandle = [[twitterfeed objectForKey:kOrionPlayerKeyUser] objectForKey:kOrionPlayerKeyScreenName];
                    feedTwitter.username        = [[twitterfeed objectForKey:kOrionPlayerKeyUser] objectForKey:kOrionPlayerKeyName];
                    feedTwitter.profilePictureURL          = [[twitterfeed objectForKey:kOrionPlayerKeyUser] objectForKey:kOrionPlayerKeyProfileImage];
                    NSDateFormatter *df = [[NSDateFormatter alloc] init] ;
                    //Wed Dec 01 17:08:03 +0000 2010
                    [df setDateFormat:@"eee MMM dd HH:mm:ss ZZZZ yyyy"];
                    NSDate *date = [df dateFromString:[twitterfeed objectForKey:kOrionPlayerKeyCreatedAt]];
                    [df setDateFormat:@"hh:mm"];
                    NSString *dateStr = [df stringFromDate:date];
                    feedTwitter.tweetTime            = dateStr;
                    
                    [_dataArray addObject:feedTwitter];
                    
                }
                
                STARTMAINTHREAD
                
                [_tableView reloadData];
                
                ENDTHREAD
            }
            else {
                NSLog(@"Error: %@", connectionError);
            }
        }];
    }
    else {
        NSLog(@"Error: %@", clientError);
    }
    
    ENDTHREAD

}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) setUpTableView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    CGFloat x = 0;
    CGFloat y = 0;
    CGFloat width = self.view.frame.size.width * 0.60;
    CGFloat height = self.view.frame.size.height;
    CGRect tableFrame = CGRectMake(x, y, width, height);
    
    
    UITableView *tableView = [[UITableView alloc]initWithFrame:tableFrame style:UITableViewStylePlain];
    tableView.scrollEnabled = YES;
    tableView.userInteractionEnabled = YES;
    tableView.bounces = YES;
    [tableView setShowsHorizontalScrollIndicator:NO];
    [tableView setShowsVerticalScrollIndicator:NO];
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    tableView.backgroundColor = [UIColor blackColor];
    
    tableView.contentInset = UIEdgeInsetsMake(0, 0, 64, 0);
    
    if([tableView respondsToSelector:@selector(setCellLayoutMarginsFollowReadableWidth:)])
    {
        tableView.cellLayoutMarginsFollowReadableWidth = NO;
    }
    
    tableView.delegate = self;
    tableView.dataSource = self;
    
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView = tableView;
    
    [self.view addSubview:self.tableView];
    
    self.tableView.hidden = true;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) hideButtons
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    buttonBack.hidden = true;
    buttonAddFriends.hidden = true;
    seperatorViewLeft.hidden = true;
    buttonTwitter.hidden = true;
    seperatorViewCenter.hidden = true;
    buttonVR.hidden = true;
    seperatorViewRight.hidden = true;
    buttonRefresh.hidden = true;
    imageViewLiveChatIcon.hidden = true;
    textFeildComments.hidden = true;
    textFeildBorder.hidden = true;
    _imageViewLiveProfile.hidden = true;
    _controlsHidden = true;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) showButtons
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    buttonBack.hidden = false;
    buttonAddFriends.hidden = false;
    seperatorViewLeft.hidden = false;
    buttonTwitter.hidden = false;
    seperatorViewCenter.hidden = false;
    buttonVR.hidden = false;
    seperatorViewRight.hidden = false;
    buttonRefresh.hidden = false;
    imageViewLiveChatIcon.hidden = false;
    textFeildComments.hidden = false;
    textFeildBorder.hidden = false;
    _imageViewLiveProfile.hidden = false;
    _controlsHidden = false;
}

/**
 *  Function called when tap detected on the screen.
 */
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void)tapDetected:(UITapGestureRecognizer*) gr
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [self showHideControlBars:!_controlsHidden];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void)showHideControlBars:(BOOL)hide
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if (_controlsHidden)
    {
        [self showButtons];
    }
    
    else
    {
        [self hideButtons];
    }
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionCancel:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self.tableView.hidden = true;
    
    [self showButtons];
}

#pragma mark - UITableView DataSource Methods -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    OrionLiveTwitterTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kRIDOrionLiveTwitterTableViewCell];
    
    if (cell == nil) {
        cell = [[OrionLiveTwitterTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:kRIDOrionLiveTwitterTableViewCell];
        
    }
    
    OrionLiveTwitterDataModel * dataModel = (OrionLiveTwitterDataModel*)[_dataArray objectAtIndex:indexPath.row];
    
    cell.labelName.text = [NSString stringWithFormat:@"%@  @%@",dataModel.username,dataModel.twitterHandle];
    cell.labelTweet.text = dataModel.tweetText;
    cell.labelTime.text = dataModel.tweetTime;
    [cell.labelName changeSubstringFont:dataModel.username withFont:kFontSFUIDisplayBold withSize:18.0];
    
    __weak OrionLiveTwitterTableViewCell *weakCell = cell;
    
    [cell.imageViewPicture setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:dataModel.profilePictureURL]]
                                 placeholderImage:[UtilityClass imageFromColor:[UIColor blackColor]]
                                          success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                              
                                              weakCell.imageViewPicture.image = image;
                                              weakCell.imageViewPicture.layer.cornerRadius = weakCell.imageViewPicture.frame.size.width / 2;
                                              weakCell.imageViewPicture.clipsToBounds = YES;
                                              [weakCell setNeedsLayout];
                                              
                                          } failure:nil];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;

}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 50)];
    view.backgroundColor = [UIColor blackColor];
    
    // Button Share WhatsApp
    UIButton * buttonCancel = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonCancel addTarget:self
                     action:@selector(actionCancel:)
             forControlEvents:UIControlEventTouchUpInside];
    buttonCancel.frame = CGRectMake(tableView.frame.size.width - 70, 0, 50 , 50);
    [buttonCancel setImage:[UIImage imageNamed:@"cross"] forState:UIControlStateNormal];
    buttonCancel.backgroundColor = [UIColor clearColor];
    [buttonCancel scaleToFill];
    
    [view addSubview:buttonCancel];
    
    return view;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return [_dataArray count];
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return 1;
}

#pragma mark - UITableView Delegate Methods -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return 100;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return 50.0;
}

#pragma - Orino1View delegate functions

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)orion1ViewReadyToPlayVideo:(Orion1View*)orion1View
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [_orionView play:0.0f];
    [self showHideControlBars:YES];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)orion1ViewDidUpdateProgress:(Orion1View*)orion1View currentTime:(CGFloat)currentTime availableTime:(CGFloat)availableTime totalDuration:(CGFloat)totalDuration
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if (currentTime > 1.0 && [_bufferIndicator isAnimating]) {
        [_bufferIndicator stopAnimating];
    }
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)orion1ViewDidChangeBufferingStatus:(Orion1View*)orion1View
                                 buffering:(BOOL)buffering
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if (buffering && ![_bufferIndicator isAnimating])
    {
        [_bufferIndicator startAnimating];
    }
    else if (!buffering && [_bufferIndicator isAnimating])
    {
        
        [_bufferIndicator stopAnimating];
    }
}

@end
