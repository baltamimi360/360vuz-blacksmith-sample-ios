//
//  OrionRecordedViewController.h
//  360 VUZ
//
//  Created by Mosib on 8/21/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrionRecordedViewController : UIViewController

@property (nonatomic) CellTypeHomeViewController CellType;
@property (nonatomic) id modal;

@end
