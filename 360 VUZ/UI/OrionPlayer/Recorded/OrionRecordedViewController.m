//
//  OrionRecordedViewController.m
//  360 VUZ
//
//  Created by Mosib on 8/21/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "OrionRecordedViewController.h"
#import "SpinCountDetailVideoTableViewCell.h"
#import "RecommendedVideoDetailTableViewCell.h"
#import "VideoDescriptionVideoDetailTableViewCell.h"
#import "WhatsHotDataModel.h"
#import "TrendingVideosDataModel.h"

#define MARGINO                     10
#define BUTTON_SIZEO                40

@interface OrionRecordedViewController ()<Orion1ViewDelegate, UIGestureRecognizerDelegate>
{
    UIView *viewSeperator;
    CGFloat heightofDescrption;
    NSString *videoURLString;
    NSString * spinsCount;
    NSString *titleString;
    UIButton * buttonBack;
    ControlMode controlMode;
    BOOL tableDescriptionHidden;
    BOOL _isSeeking;
    BOOL _controlsHidden;
    UITapGestureRecognizer *tapGr;
}

@property (strong, nonatomic) UITableView *tableView;
@property (nonatomic) UILabel * labelSpins;
@property (nonatomic) UILabel *labelTitle;
@property (nonatomic) Orion1View* orionView;
@property (nonatomic) UISlider  *timeSlider;
@property (nonatomic) UIButton *playPauseButton;
@property (nonatomic) UIButton *vrButton;
@property (nonatomic) UILabel *timeLeftLabel;
@property (nonatomic) UILabel *timeTotalLabel;
@property (nonatomic) UIView *bottomBar;
@property (nonatomic) UIActivityIndicatorView *bufferIndicator;

@end

@implementation OrionRecordedViewController


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)viewDidLoad
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    // Set Up Controller Details
    [self setUpDetails];
    
    // Set Up Orion Player
    [self setUpOrionPlayer];
    
    // Prepare Orion Controls Overlay
    [self prepareView];
    
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) viewDidAppear:(BOOL)animated
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super viewDidAppear:animated];
    
    [self loadOrionVideo];
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonBack:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [AppDelegate appDelegate].allowedOrientation = UIInterfaceOrientationMaskPortrait;
    
    [self.navigationController dismissViewControllerAnimated:false completion:nil];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpDetails
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    switch (_CellType) {
        case WhatsHot:
        {
            WhatsHotDataModel *dataModel = (WhatsHotDataModel *) _modal;
            
            spinsCount = dataModel.viewCount;
            titleString = dataModel.videoTitle;
            videoURLString = dataModel.videoLink;
            
            break;
        }
            
        case Trending :
        {
            TrendingVideosDataModel *dataModel = (TrendingVideosDataModel *) _modal;
            
            spinsCount = dataModel.viewCount;
            titleString = dataModel.videoTitle;
            videoURLString = dataModel.videoLink;
            
            break;
        }
            
        default:
            break;
    }
}


#pragma mark - Private Methods -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpOrionPlayer
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    UIView *backGroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    backGroundView.backgroundColor = [UIColor blackColor];
    
    [self.view addSubview:backGroundView];
    
    _orionView = [[Orion1View alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    _orionView.delegate = self;
    _orionView.overrideSilentSwitch = YES;
    
    [self.view addSubview:_orionView];
    
    _bufferIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    _bufferIndicator.center = CGPointMake(_orionView.frame.size.width/2, _orionView.frame.size.height/2);
    [self.view addSubview:_bufferIndicator];
    
    [_bufferIndicator startAnimating];
    
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) loadOrionVideo
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    NSURL *videoUrl =[[UtilityClass sharedManger] getCDNURL:[NSURL URLWithString:videoURLString]];
    
    [_orionView initVideoWithUrl:videoUrl previewImageUrl:nil licenseFileUrl:[[UtilityClass sharedManger] orionLicenseURL]];
}


#pragma mark - Orino1View delegate functions -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)orion1ViewReadyToPlayVideo:(Orion1View*)orion1View
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [orion1View play:0.0f];
    [_timeSlider setMaximumValue:_orionView.totalDuration];
    [self showHideControlBars:YES];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)orion1ViewVideoDidReachEnd:(Orion1View*)orion1View
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [self actionButtonBack:nil];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)orion1ViewDidUpdateProgress:(Orion1View*)orion1View currentTime:(CGFloat)currentTime availableTime:(CGFloat)availableTime totalDuration:(CGFloat)totalDuration
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if (currentTime > 1.0 && [_bufferIndicator isAnimating]) {
        [_bufferIndicator stopAnimating];
    }
    
    if(_timeSlider.maximumValue != totalDuration)
    {
        [_timeSlider setMaximumValue:totalDuration];
    }
    
    if (!_isSeeking)
    {
        _timeSlider.value = currentTime;
        [self updateTimeLabel:(int)currentTime];
    }
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)orion1ViewDidChangeBufferingStatus:(Orion1View*)orion1View
                                 buffering:(BOOL)buffering
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if (buffering && ![_bufferIndicator isAnimating])
    {
        [_bufferIndicator startAnimating];
    }
    else if (!buffering && [_bufferIndicator isAnimating])
    {
        
        [_bufferIndicator stopAnimating];
    }
}

#pragma - Private Methods -
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) cellDescriptionValues:(VideoDescriptionVideoDetailTableViewCell *)cell
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
    switch (_CellType) {
        case WhatsHot:
        {
            
            WhatsHotDataModel *dataModel = (WhatsHotDataModel *) _modal;
            
            cell.labelShare.text = dataModel.videoDescription;
            
            break;
        }
            
        case Trending :
        {
            TrendingVideosDataModel *dataModel = (TrendingVideosDataModel *) _modal;
            
            cell.labelShare.text = dataModel.videoDescription;
            
            break;
        }
            
        default:
            break;
    }
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) prepareView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    // Bottom bar
    CGFloat bottomBarH = BUTTON_SIZEO + 1 * MARGINO;
    _bottomBar = [[UIView alloc] initWithFrame:CGRectMake(0, _orionView.frame.size.height - bottomBarH, _orionView.frame.size.width, bottomBarH)];
    _bottomBar.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_bottomBar];
    
    // Play/pause button
    _playPauseButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _playPauseButton.frame = CGRectMake(0, MARGINO, BUTTON_SIZEO, BUTTON_SIZEO);
    [_playPauseButton setImage:[UIImage imageNamed:@"playback_pause_stream"] forState:UIControlStateNormal];
    [_playPauseButton addTarget:self action:@selector(playPause:) forControlEvents:UIControlEventTouchUpInside];
    [_playPauseButton setContentEdgeInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    [_bottomBar addSubview:_playPauseButton];
    
    // Mute button
    _vrButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _vrButton.frame = CGRectMake(CGRectGetWidth(_bottomBar.frame) - BUTTON_SIZEO, _bottomBar.frame.origin.y + MARGINO, BUTTON_SIZEO, BUTTON_SIZEO);
    [_vrButton setImage:[UIImage imageNamed:@"select_VR"] forState:UIControlStateNormal];
    [_vrButton addTarget:self action:@selector(buttonVR:) forControlEvents:UIControlEventTouchUpInside];
    [_vrButton.imageView setContentMode:UIViewContentModeScaleAspectFit];
    [self.view addSubview:_vrButton];
    
    // Time left label
    _timeLeftLabel = [[UILabel alloc] initWithFrame:CGRectMake( BUTTON_SIZEO - 10 , MARGINO, BUTTON_SIZEO, BUTTON_SIZEO)];
    _timeLeftLabel.font = [UIFont fontWithName:kFontSFUIDisplayLight size:10];
    _timeLeftLabel.text = @"0:00";
    _timeLeftLabel.textColor = [UIColor whiteColor];
    _timeLeftLabel.textAlignment = NSTextAlignmentCenter;
    [_bottomBar addSubview:_timeLeftLabel];
    
    _timeTotalLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetWidth(_bottomBar.frame) - BUTTON_SIZEO - 20, MARGINO, BUTTON_SIZEO, BUTTON_SIZEO)];
    _timeTotalLabel.font = [UIFont fontWithName:kFontSFUIDisplayLight size:10];
    _timeTotalLabel.text = @"0:00";
    _timeTotalLabel.textAlignment = NSTextAlignmentLeft;
    _timeTotalLabel.textColor = [UIColor whiteColor];
    [_bottomBar addSubview:_timeTotalLabel];
    
    // Time slider
    int sliderX = _timeLeftLabel.frame.origin.x + _timeLeftLabel.frame.size.width - 5;
    int sliderY = MARGINO;
    int sliderH = BUTTON_SIZEO;
    int sliderW = _timeTotalLabel.frame.origin.x - sliderX - 5;
    
    _timeSlider = [[UISlider alloc] initWithFrame:CGRectMake(sliderX, sliderY, sliderW, sliderH)];
    
    [_timeSlider addTarget:self action:@selector(timeSliderDragExit:) forControlEvents:UIControlEventTouchUpInside];
    [_timeSlider addTarget:self action:@selector(timeSliderDragExit:) forControlEvents:UIControlEventTouchUpOutside];
    [_timeSlider addTarget:self action:@selector(timeSliderDragEnter:) forControlEvents:UIControlEventTouchDown];
    [_timeSlider addTarget:self action:@selector(timeSliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    [_timeSlider setBackgroundColor:[UIColor clearColor]];
    _timeSlider.minimumValue = 0.0;
    _timeSlider.maximumValue = 0.0;
    [_bottomBar addSubview:_timeSlider];
    
    UITapGestureRecognizer *sliderTapGr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sliderTapped:)];
    [_timeSlider addGestureRecognizer:sliderTapGr];
    
    
    // Tap gesture regocnizer
    tapGr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected:)];
    tapGr.delegate = self;
    [_orionView addGestureRecognizer:tapGr];
    
    
    // button Back
    buttonBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonBack addTarget:self
                   action:@selector(actionButtonBack:)
         forControlEvents:UIControlEventTouchUpInside];
    buttonBack.frame = CGRectMake(10, 10, 50 , 40);
    [buttonBack setImage:[UIImage imageNamed:@"back-button-White"] forState:UIControlStateNormal];
    
    [self.view addSubview:buttonBack];
    
    _labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetWidth(buttonBack.frame) + 10, 15, CGRectGetWidth(_orionView.frame) - (CGRectGetWidth(buttonBack.frame) + 10) , 20)];
    _labelTitle.font = [UIFont fontWithName:kFontSFUIDisplayRegular size:18];
    _labelTitle.textAlignment = NSTextAlignmentLeft;
    _labelTitle.textColor = [UIColor whiteColor];
    _labelTitle.text = titleString;
    [self.view addSubview:_labelTitle];
    
}


/**
 *  Function called when play/pause button selected.
 */
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void)playPause:(UIButton*)button
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if ([_orionView isPaused])
    {
        [_orionView play];
        [_playPauseButton setImage:[UIImage imageNamed:@"playback_pause_stream"] forState:UIControlStateNormal];
    }
    else
    {
        [_orionView pause];
        [_playPauseButton setImage:[UIImage imageNamed:@"playback_play_Stream"] forState:UIControlStateNormal];
    }
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (IBAction) timeSliderDragEnter:(id)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    _isSeeking = true;
}

/**
 *  Function called when time slider value changed (dragging).
 */
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (IBAction) timeSliderValueChanged:(id)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    UISlider *slider = sender;
    [self updateTimeLabel:(int)slider.value];
    
}

/**
 *  Function called when time slider dragging ended.
 */
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (IBAction) timeSliderDragExit:(id)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
    UISlider *slider = sender;
    [self.orionView seekTo:[slider value]];
    _isSeeking = false;
}

/**
 *  Function called when time slider has been tapped.
 */
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)sliderTapped:(UIGestureRecognizer *)g
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    UISlider* s = (UISlider*)g.view;
    CGPoint pt = [g locationInView: s];
    CGFloat percentage = pt.x / s.bounds.size.width;
    CGFloat delta = percentage * (s.maximumValue - s.minimumValue);
    CGFloat value = s.minimumValue + delta;
    [s setValue:value animated:YES];
    [self.orionView seekTo:value];
    _isSeeking = false;
    
    [self updateTimeLabel:(int)value];
}

/**
 *  Function to update time label.
 */
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)updateTimeLabel:(int)totalSeconds
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int durationSeconds = (int)_timeSlider.maximumValue % 60;
    int durationMinutes = ((int)_timeSlider.maximumValue/60) % 60;
    _timeLeftLabel.text = [NSString stringWithFormat:@"%d:%02d",minutes, seconds];
    _timeTotalLabel.text = [NSString stringWithFormat:@"%d:%02d", durationMinutes, durationSeconds];
}

/**
 *  Function called when tap detected on the screen.
 */
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void)tapDetected:(UITapGestureRecognizer*)gr
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [self showHideControlBars:!_controlsHidden];
}

/**
 *  Function to show or hide control bars.
 */
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void)showHideControlBars:(BOOL)hide
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if (hide == _controlsHidden)
    {
        return;
    }
    
    _controlsHidden = hide;
    [UIView animateWithDuration:0.3f animations:^(void){
        _bottomBar.hidden = _controlsHidden;
        buttonBack.hidden = _controlsHidden;
        _labelTitle.hidden = _controlsHidden;
        _vrButton.hidden = _controlsHidden;
    }];
}

/**
 *  Function called when mute button selected.
 */
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void)buttonVR:(UIButton*)button
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if (controlMode == SENSORS)
    {
        [self showHideControlBars:true];
        
        _orionView.vrModeEnabled = YES;
        controlMode = VR;
   
        _vrButton.hidden = false;
        tapGr.enabled = false;
    }
    
    else
    {
        [self showHideControlBars:false];
        
        _orionView.vrModeEnabled = false;
        controlMode = SENSORS;
        
        tapGr.enabled = true;
    }
}

@end
