//
//  LoginViewController.m
//  360 VUZ
//
//  Created by Mosib on 7/4/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "LoginViewController.h"
#import "SignUpViewController.h"
#import "IQKeyboardManager.h"
#import "IQKeyboardReturnKeyHandler.h"
#import "IQUIView+IQKeyboardToolbar.h"
#import "ForgotViewController.h"
#import "ServiceLogin.h"
#import "UserDetailsDataModel.h"

@interface LoginViewController ()
{
    
    UITextField *textFieldEmail;
    UITextField *textFieldPassword;
    CGRect deviceBounds;
    // Keyboard Manager
    IQKeyboardReturnKeyHandler *returnKeyHandler;
    __weak LoginViewController* weakSelf;
}
@end

@implementation LoginViewController

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)viewDidLoad
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super viewDidLoad];
    
    weakSelf = self;
    
    // Initialze View Controller Settings
    [self initialzeControllerSettings];
    
    // Set Up Frame for Screen
    [self setUpFrame];
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) viewWillAppear:(BOOL)animated
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super viewWillAppear:animated];
    
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)didReceiveMemoryWarning
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(BOOL)prefersStatusBarHidden
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return YES;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) initialzeControllerSettings
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    // Set White Background
    self.view.backgroundColor = [UIColor whiteColor];
    
    // Set Up Return KeyHandler
    returnKeyHandler = [[IQKeyboardReturnKeyHandler alloc] initWithViewController:self];
    
    // Initialze Instance Variable
    deviceBounds = [UtilityClass getDeviceBounds];
}

#pragma mark - Frame Handling -
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpFrame
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    // Button Cancel Frame
    UIButton *buttonBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonBack addTarget:self
                   action:@selector(actionButtonBack:)
         forControlEvents:UIControlEventTouchUpInside];
    CGSize sizeOfbuttonBack = CGSizeMake(60.0, 40.0);
    CGPoint PointOfbuttonBack = CGPointMake(15, 25);
    buttonBack.frame = CGRectMake(PointOfbuttonBack.x, PointOfbuttonBack.y, sizeOfbuttonBack.width, sizeOfbuttonBack.height);
    [buttonBack setImage:[UIImage imageNamed:@"back-button"] forState:UIControlStateNormal];
    CGPoint centerOfButtonBack = buttonBack.center;
    
    // Label Sign Up
    UILabel *labelSignUp = [[UILabel alloc] init];
    CGSize sizeOfSignUp = CGSizeMake(100.0, 35.0);
    CGPoint PointOfSignUp = CGPointMake(buttonBack.frame.origin.x + buttonBack.frame.size.width + 5, centerOfButtonBack.y - sizeOfSignUp.height/2);
    labelSignUp.frame = CGRectMake(PointOfSignUp.x, PointOfSignUp.y, sizeOfSignUp.width, sizeOfSignUp.height);
    labelSignUp.text = @"login";
    labelSignUp.font = [UIFont fontWithName:kFontSFUIDisplayBold size:26];
    labelSignUp.textColor =  UIColorFromRGB(0x13AEE5);
    
    // Label Welcome Frame
    UILabel *labelWelcome = [[UILabel alloc] init];
    CGSize sizeOflabelWelcome = CGSizeMake(140, 16);
    CGPoint pointOflabelWelcome = CGPointMake(PointOfSignUp.x + 20 , PointOfSignUp.y + sizeOfSignUp.height);
    labelWelcome.frame = CGRectMake(pointOflabelWelcome.x, pointOflabelWelcome.y, sizeOflabelWelcome.width, sizeOflabelWelcome.height);
    labelWelcome.text = @"welcome back dear";
    labelWelcome.font = [UIFont fontWithName:kFontSFUIDisplayRegular size:12];
    labelWelcome.textColor = UIColorFromRGB(0xD3D3D3);
    
    
    // Button Login Frame
    UIButton *buttonLogin = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonLogin addTarget:self
                    action:@selector(actionButtonLogin:)
          forControlEvents:UIControlEventTouchUpInside];
    [buttonLogin setTitle:@"Login" forState:UIControlStateNormal];
    buttonLogin.backgroundColor = UIColorFromRGB(0x29AEE3);
    CGSize sizeOfbuttonLogin = CGSizeMake(deviceBounds.size.width * 0.80, 55.0);
    CGPoint pointOfbuttonLogin = CGPointMake(deviceBounds.size.width/2 - sizeOfbuttonLogin.width/2, deviceBounds.size.height * 0.80);
    buttonLogin.frame = CGRectMake(pointOfbuttonLogin.x, pointOfbuttonLogin.y, sizeOfbuttonLogin.width, sizeOfbuttonLogin.height);
    [buttonLogin.titleLabel setFont:[UIFont fontWithName:kFontSFUIDisplayRegular size:14]];
    buttonLogin.layer.cornerRadius = 4;
    buttonLogin.clipsToBounds = YES;
    
    // Label Label Or Frame
    UILabel *labelOr = [[UILabel alloc] init];
    labelOr.textAlignment = NSTextAlignmentCenter;
    CGSize sizeOflabelOr = CGSizeMake(25, 16);
    CGPoint pointOflabelOr = CGPointMake(deviceBounds.size.width/2 - sizeOflabelOr.width/2, pointOfbuttonLogin.y - 220);
    labelOr.frame = CGRectMake(pointOflabelOr.x, pointOflabelOr.y, sizeOflabelOr.width, sizeOflabelOr.height);
    labelOr.text = @"or";
    labelOr.font = [UIFont fontWithName:kFontSFUIDisplayLight size:14];
    labelOr.textColor = UIColorFromRGB(0x29AEE3);
    
    CGPoint labelOrCenter = labelOr.center;
    
    // Label Welcome Frame
    CGSize sizeOfViewLeftOr = CGSizeMake(buttonLogin.frame.size.width/2 - 15, 2);
    CGPoint pointOfViewLeftOr = CGPointMake(labelOr.frame.origin.x - 5 - sizeOfViewLeftOr.width, labelOrCenter.y);
    UIView *viewLeftOr = [[UIView alloc] initWithFrame:CGRectMake(pointOfViewLeftOr.x, pointOfViewLeftOr.y, sizeOfViewLeftOr.width, sizeOfViewLeftOr.height)];
    viewLeftOr.backgroundColor = UIColorFromRGB(0x29AEE3);
    
    // Label Welcome Frame
    CGSize sizeOfViewRightOr = CGSizeMake(buttonLogin.frame.size.width/2 - 15, 2);
    CGPoint pointOfViewRightOr = CGPointMake(labelOr.frame.origin.x + labelOr.frame.size.width + 5, labelOrCenter.y);
    UIView *viewRightOr = [[UIView alloc] initWithFrame:CGRectMake(pointOfViewRightOr.x, pointOfViewRightOr.y, sizeOfViewRightOr.width, sizeOfViewRightOr.height)];
    viewRightOr.backgroundColor = UIColorFromRGB(0x29AEE3);
    
    
    // TextField Email
    textFieldEmail = [[UITextField alloc] init];
    CGSize sizeOfTextFieldEmail = CGSizeMake(deviceBounds.size.width * 0.80, 40.0);
    CGPoint PointTextFieldEmail = CGPointMake(deviceBounds.size.width/2 - sizeOfTextFieldEmail.width/2, pointOfbuttonLogin.y - 180);
    textFieldEmail.frame = CGRectMake(PointTextFieldEmail.x, PointTextFieldEmail.y, sizeOfTextFieldEmail.width, sizeOfTextFieldEmail.height);
    textFieldEmail.placeholder = @"email";
    [textFieldEmail applyCustomSettingWithUnderline];
    textFieldEmail.keyboardType = UIKeyboardTypeEmailAddress;
    
    
    // TextField Password
    textFieldPassword = [[UITextField alloc] init];
    CGSize sizeOfTextFieldPassword = CGSizeMake(deviceBounds.size.width * 0.80, 40.0);
    CGPoint PointTextFieldPassword = CGPointMake(deviceBounds.size.width/2 - sizeOfTextFieldPassword.width/2, PointTextFieldEmail.y + 75);
    textFieldPassword.frame = CGRectMake(PointTextFieldPassword.x, PointTextFieldPassword.y, sizeOfTextFieldPassword.width, sizeOfTextFieldPassword.height);
    textFieldPassword.placeholder = @"password";
    [textFieldPassword applyCustomSettingWithUnderline];
    textFieldPassword.secureTextEntry = YES;
    
    // Button Forgot Password Frame
    UIButton *buttonForgotPassword = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonForgotPassword addTarget:self
                             action:@selector(actionButtonForgotPassword:)
                   forControlEvents:UIControlEventTouchUpInside];
    CGSize sizeOfbuttonForgotPassword = CGSizeMake(140.0, 15.0);
    CGPoint PointOfbuttonForgotPassword = CGPointMake((deviceBounds.size.width - sizeOfTextFieldPassword.width)/2 +sizeOfTextFieldPassword.width - sizeOfbuttonForgotPassword.width  , PointTextFieldPassword.y + sizeOfTextFieldPassword.height + 10);
    buttonForgotPassword.frame = CGRectMake(PointOfbuttonForgotPassword.x, PointOfbuttonForgotPassword.y, sizeOfbuttonForgotPassword.width, sizeOfbuttonForgotPassword.height);
    [buttonForgotPassword setAttributedTitle:[self getAttributedStringForForgotPassword] forState:UIControlStateNormal];
    buttonForgotPassword.layer.cornerRadius = 4;
    buttonForgotPassword.clipsToBounds = YES;
    
    // Button Cancel Frame
    UIButton *buttonFacebook = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonFacebook addTarget:self
                       action:@selector(actionButtonFaceBook:)
             forControlEvents:UIControlEventTouchUpInside];
    CGSize sizeOfbuttonFacebook = CGSizeMake(60.0, 60.0);
    CGPoint PointOfbuttonFacebook = CGPointMake(deviceBounds.size.width/2 - sizeOfbuttonFacebook.width/2, pointOflabelOr.y - sizeOfbuttonFacebook.height - 35);
    buttonFacebook.frame = CGRectMake(PointOfbuttonFacebook.x, PointOfbuttonFacebook.y, sizeOfbuttonFacebook.width, sizeOfbuttonFacebook.height);
    [buttonFacebook scaleToFill];
    [buttonFacebook setImage:[UIImage imageNamed:@"facebook-icon"] forState:UIControlStateNormal];
    
    // Button Cancel Frame
    UIButton *buttonGoogle = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonGoogle addTarget:self
                     action:@selector(actionButtonGoogle:)
           forControlEvents:UIControlEventTouchUpInside];
    CGSize sizeOfbuttonGoogle = CGSizeMake(60.0, 60.0);
    CGPoint PointOfbuttonGoogle = CGPointMake(buttonFacebook.frame.origin.x - sizeOfbuttonGoogle.width - 35, PointOfbuttonFacebook.y);
    buttonGoogle.frame = CGRectMake(PointOfbuttonGoogle.x, PointOfbuttonGoogle.y, sizeOfbuttonGoogle.width, sizeOfbuttonGoogle.height);
    [buttonGoogle scaleToFill];
    [buttonGoogle setImage:[UIImage imageNamed:@"googleplus-icon"] forState:UIControlStateNormal];
    
    // Button Cancel Frame
    UIButton *buttonTwitter = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonTwitter addTarget:self
                      action:@selector(actionButtonTwitter:)
            forControlEvents:UIControlEventTouchUpInside];
    CGSize sizeOfbuttonTwitter = CGSizeMake(60.0, 60.0);
    CGPoint PointOfbuttonTwitter = CGPointMake(buttonFacebook.frame.origin.x + sizeOfbuttonGoogle.width + 35, PointOfbuttonFacebook.y);
    buttonTwitter.frame = CGRectMake(PointOfbuttonTwitter.x, PointOfbuttonTwitter.y, sizeOfbuttonTwitter.width, sizeOfbuttonTwitter.height);
    [buttonTwitter scaleToFill];
    [buttonTwitter setImage:[UIImage imageNamed:@"twitter-icon"] forState:UIControlStateNormal];
    
    // Button SignUp Frame
    UIButton *buttonSignUp = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonSignUp addTarget:self
                     action:@selector(actionButtonSignUp:)
           forControlEvents:UIControlEventTouchUpInside];
    [buttonSignUp setTitle:@"Sign Up" forState:UIControlStateNormal];
    CGSize sizeOfAlreadyAccount = CGSizeMake(200.0, 15.0);
    CGPoint PointOfAlreadyAccount = CGPointMake(deviceBounds.size.width/2 - sizeOfAlreadyAccount.width/2, deviceBounds.size.height * 0.95);
    buttonSignUp.frame = CGRectMake(PointOfAlreadyAccount.x, PointOfAlreadyAccount.y, sizeOfAlreadyAccount.width, sizeOfAlreadyAccount.height);
    [buttonSignUp setAttributedTitle:[self getAttributedStringForNoAccount] forState:UIControlStateNormal];
    buttonSignUp.layer.cornerRadius = 4;
    buttonSignUp.clipsToBounds = YES;
    
    // Adding Views to SuperView
    [self.view addSubview:buttonBack];
    [self.view addSubview:labelSignUp];
    [self.view addSubview:labelWelcome];
    [self.view addSubview:buttonGoogle];
    [self.view addSubview:buttonFacebook];
    [self.view addSubview:buttonTwitter];
    [self.view addSubview:labelOr];
    [self.view addSubview:viewLeftOr];
    [self.view addSubview:viewRightOr];
    [self.view addSubview:textFieldEmail];
    [self.view addSubview:textFieldPassword];
    [self.view addSubview:buttonForgotPassword];
    [self.view addSubview:buttonLogin];
    [self.view addSubview:buttonSignUp];
}

#pragma mark - Button Actions -
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonBack:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [self.navigationController popViewControllerAnimated:true];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonLogin:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if ([self validateTextfeilds])
    {
        if ([[AppDelegate appDelegate] isInternetReachable])
        {
        [self checkLoginCredentialsAPI:@{kAPIkeyEmail: textFieldEmail.text,
                                         kAPIkeyPassword:textFieldPassword.text}];
        }
        
        else
        {
            [[UtilityClass sharedManger] alertErrorWithMessage:@"Not Connected To Internet"];
        }
    }
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonSignUp:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [self.navigationController popViewControllerAnimated:false];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonFaceBook:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [[SocialClass sharedManger] logInWithFacebook:self];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonGoogle:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [[SocialClass sharedManger] logInWithGoogle];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonTwitter:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [[SocialClass sharedManger] logInWithTwitter];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonForgotPassword:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    ForgotViewController *forgotViewController = [[ForgotViewController alloc] init];
    [self.navigationController pushViewController:forgotViewController animated:true];
}


#pragma mark - Helper Methods -
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSAttributedString *) getAttributedStringForNoAccount
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    NSString *textForAlready = @"Don't have an account ? Sign Up";
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] init];
    [attString appendAttributedString:[[NSAttributedString alloc] initWithString:textForAlready  attributes:@{
                                                                                                              NSFontAttributeName:[UIFont fontWithName:kFontSFUIDisplayRegular size:12.0],
                                                                                                              NSForegroundColorAttributeName:UIColorFromRGB(0xD3D3D3)}]];
    NSRange range = [textForAlready rangeOfString:@"Sign Up"];
    [attString addAttribute:NSFontAttributeName value:[UIFont fontWithName:kFontSFUIDisplayHeavy size:12.0] range:range];
    [attString addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x29AEE3) range:range];
    
    return [attString copy];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSAttributedString *) getAttributedStringForForgotPassword
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    NSString *textForAlready = @"Forgot your password?";
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] init];
    [attString appendAttributedString:[[NSAttributedString alloc] initWithString:textForAlready  attributes:@{
                                                                                                              NSFontAttributeName:[UIFont fontWithName:kFontSFUIDisplayHeavy size:12.0],
                                                                                                              NSForegroundColorAttributeName:UIColorFromRGB(0x29AEE3)}]];
    return [attString copy];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (BOOL) validateTextfeilds
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if ([textFieldEmail.text length] == 0 || [textFieldPassword.text length] == 0)
    {
        [[UtilityClass sharedManger] alertErrorWithMessage:@"Feilds cannot be empty"];
        return false;
    }
    
    if (![[UtilityClass sharedManger] validateEmailWithString:textFieldEmail.text])
    {
        
        [[UtilityClass sharedManger] alertErrorWithMessage:@"Please enter a valid email"];
        
        return false;
    }
    
    return true;
}

#pragma mark - API Methods -
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) checkLoginCredentialsAPI:(NSDictionary *) paramerters
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    STARTBACKGROUNDTHREAD
    ServiceLogin *registerDevice = [[ServiceLogin alloc]init];
    
    [registerDevice executeRequest:paramerters
                           success:^(NSDictionary  *responseObject)
     {
         [weakSelf SuccessForloginCredentialsAPI:responseObject];
         
     }
                           failure:^(NSError * error) {
         
         [weakSelf FailureForLoginCredentialsAPI:error];
     }];
    ENDTHREAD
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) SuccessForloginCredentialsAPI:(NSDictionary *) responseObject
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if ([[responseObject objectForKey:kAPIkeyStatus] isEqualToString:kAPIkeySuccess])
    {
        
    [[Settings sharedManger] setUserLoggedIn:true];
        
    [[Settings sharedManger] setUserDetails:[self setUpUserDetails:[[responseObject objectForKey:kkeyData] objectForKey:kUserDetailKeyUser]]];
    
    // Set User Defaults
    [[Settings sharedManger] setUserID:[[responseObject objectForKey:kkeyData] objectForKey:kAPIkeyUserID]];
    [[Settings sharedManger] setServerToken:[[responseObject objectForKey:kkeyData] objectForKey:kAPIkeyServerToken]];
    
    STARTMAINTHREAD

    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    // Dismiss View Controller
    [weakSelf.navigationController dismissViewControllerAnimated:true completion:nil];
    
    // Set Up Tab Bar Controller
    [[AppDelegate appDelegate] setUpTabBarController];
    
    // Dismiss View Controller
    [AppDelegate appDelegate].isWelcomeNavigationOnTop = false;
    [AppDelegate appDelegate].topView = nil;
    
    [[AppDelegate appDelegate] makeTabBarAsRootViewController];
    
    ENDTHREAD
    }
    
    else
        
    {
        STARTMAINTHREAD
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        
        [[UtilityClass sharedManger] alertErrorWithMessage:@"Invalid Email/Password"];
        
        ENDTHREAD
    }
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) FailureForLoginCredentialsAPI:(NSError *)error
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    STARTMAINTHREAD
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [[UtilityClass sharedManger] alertErrorWithMessage:@"Something went wrong"];
    
    ENDTHREAD
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (UserDetailsDataModel *) setUpUserDetails:(NSDictionary *) user
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    UserDetailsDataModel * userDetails = [UserDetailsDataModel new];
    userDetails.bio = [user objectForKey:kUserDetailKeyBio];
    userDetails.credits = [NSString stringWithFormat:@"%@", [user objectForKey:kUserDetailKeyCredits]];
    userDetails.deviceID = [user objectForKey:kUserDetailKeyDeviceID];
    userDetails.emailID = [user objectForKey:kUserDetailKeyEmail];
    userDetails.emailVerified = [[user objectForKey:kUserDetailKeyEmailVerified] boolValue];
    userDetails.facebookID = [user objectForKey:kUserDetailKeyFacebookID];
    userDetails.firstName = [user objectForKey:kUserDetailKeyFirstName];
    userDetails.gender = [user objectForKey:kUserDetailKeyGender];
    userDetails.googlePlusID = [NSString stringWithFormat:@"%@", [user objectForKey:kUserDetailKeyGooglePlusID]];
    userDetails.userID = [NSString stringWithFormat:@"%@", [user objectForKey:kUserDetailKeyID]];
    userDetails.imageUrlPrimary = [user objectForKey:kUserDetailKeyImageURLP];
    userDetails.imageUrlSecondary = [user objectForKey:kUserDetailKeyImageURLS];
    userDetails.inviteCount = [NSString stringWithFormat:@"%@", [user objectForKey:kUserDetailKeyInviteCount]];
    userDetails.lastName = [user objectForKey:kUserDetailKeyLastName];
    userDetails.model = [user objectForKey:kUserDetailKeyModel];
    userDetails.phoneNumber = [user objectForKey:kUserDetailKeyPhoneNumber];
    userDetails.registrationType = [user objectForKey:kUserDetailKeyRegistrationType];
    userDetails.twitterId = [NSString stringWithFormat:@"%@", [user objectForKey:kUserDetailKeyTwitterID]];
    userDetails.username = [user objectForKey:kUserDetailKeyUsername];
    userDetails.website = [user objectForKey:kUserDetailKeyWebsite];
    userDetails.payPalAmount = [NSString stringWithFormat:@"%@", [user objectForKey:kUserDetailKeyPayPalAmount]];
    userDetails.likeCount = [NSString stringWithFormat:@"%@", [user objectForKey:kUserDetailKeyLikeCount]];
    userDetails.viewCount =  [NSString stringWithFormat:@"%@", [user objectForKey:kUserDetailKeyViewCount]];
    userDetails.videosCount =  [NSString stringWithFormat:@"%@", [user objectForKey:kUserDetailKeyVideosCount]];
    
    if([userDetails.username isEqual:[NSNull null]])
    {
        userDetails.username = [NSString stringWithFormat:@"%@ %@",userDetails.firstName,userDetails.lastName];
    }
    
    return userDetails;
}

@end
