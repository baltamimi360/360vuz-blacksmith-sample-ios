//
//  WelcomeViewController.m
//  360 VUZ
//
//  Created by Mosib on 7/2/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "WelcomeViewController.h"
#import "SignUpViewController.h"
#import "LoginViewController.h"
#import "ServiceLogin.h"


@interface WelcomeViewController ()
{
    CGRect deviceBounds;
    UIView *viewOption;
    AVPlayer *avplayer;
    UIImageView *backgroundImageView;
}
@end

@implementation WelcomeViewController

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)viewDidLoad
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super viewDidLoad];
    
    // Set Up Frame for Screen
    [self setUpFrame];
    
    // Start Up Video in Background
    [self setUpBackgroundVideo];
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) viewWillAppear:(BOOL)animated
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super viewWillAppear:animated];
    
    // Set Up Observers
    [self setUpObservers];
    
    // Start Playing the Video
    [avplayer play];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) viewWillDisappear:(BOOL)animated
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super viewWillDisappear:animated];
    
    // Set Up Observers
    [self removeObservers];
    
    // Start Playing the Video
    [avplayer pause];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)didReceiveMemoryWarning
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(BOOL)prefersStatusBarHidden
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return YES;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(BOOL)shouldAutorotate
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return NO;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(UIInterfaceOrientationMask)supportedInterfaceOrientations
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return UIInterfaceOrientationMaskPortrait;
}

#pragma mark - Observer Handling -
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpObservers
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    // AVPlayer Notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerItemDidReachEnd:)
                                                 name:AVPlayerItemDidPlayToEndTimeNotification
                                               object:[avplayer currentItem]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerStartPlaying)
                                                 name:UIApplicationDidBecomeActiveNotification object:nil];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) removeObservers
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:AVPlayerItemDidPlayToEndTimeNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIApplicationDidBecomeActiveNotification
                                                  object:nil];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) removeAllObservers
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Frame Handling -
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpFrame
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self.view.backgroundColor = [UIColor whiteColor];
    
    deviceBounds = [UtilityClass getDeviceBounds];
    
    // Background Image Frame
    backgroundImageView = [[UIImageView alloc] init];
    backgroundImageView.frame = [UtilityClass getDeviceBounds];
    backgroundImageView.image = [UIImage imageNamed:@"Welcome_Background"];
    
    // Button Cancel Frame
    UIButton *buttonCanel = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonCanel addTarget:self
                    action:@selector(actionButtonCancel:)
          forControlEvents:UIControlEventTouchUpInside];
    CGSize sizeOfbuttonCanel = CGSizeMake(60.0, 60.0);
    CGPoint PointOfbuttonCanel = CGPointMake(deviceBounds.size.width - 55, 15);
    buttonCanel.frame = CGRectMake(PointOfbuttonCanel.x, PointOfbuttonCanel.y, sizeOfbuttonCanel.width, sizeOfbuttonCanel.height);
    [buttonCanel setImage:[UIImage imageNamed:@"cross"] forState:UIControlStateNormal];
    
    // Button Cancel Frame
    UIButton *buttonFacebook = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonFacebook addTarget:self
                    action:@selector(actionButtonFaceBook:)
          forControlEvents:UIControlEventTouchUpInside];
    CGSize sizeOfbuttonFacebook = CGSizeMake(60.0, 60.0);
    CGPoint PointOfbuttonFacebook = CGPointMake(deviceBounds.size.width/2 - sizeOfbuttonFacebook.width/2, deviceBounds.size.height/2 - sizeOfbuttonFacebook.height/2);
    buttonFacebook.frame = CGRectMake(PointOfbuttonFacebook.x, PointOfbuttonFacebook.y, sizeOfbuttonFacebook.width, sizeOfbuttonFacebook.height);
    [buttonFacebook scaleToFill];
    [buttonFacebook setImage:[UIImage imageNamed:@"facebook-icon"] forState:UIControlStateNormal];
    
    // Button Cancel Frame
    UIButton *buttonGoogle = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonGoogle addTarget:self
                    action:@selector(actionButtonGoogle:)
          forControlEvents:UIControlEventTouchUpInside];
    CGSize sizeOfbuttonGoogle = CGSizeMake(60.0, 60.0);
    CGPoint PointOfbuttonGoogle = CGPointMake(buttonFacebook.frame.origin.x - sizeOfbuttonGoogle.width - 35, deviceBounds.size.height/2 - sizeOfbuttonFacebook.height/2);
    buttonGoogle.frame = CGRectMake(PointOfbuttonGoogle.x, PointOfbuttonGoogle.y, sizeOfbuttonGoogle.width, sizeOfbuttonGoogle.height);
    [buttonGoogle scaleToFill];
    [buttonGoogle setImage:[UIImage imageNamed:@"googleplus-icon"] forState:UIControlStateNormal];
    
    // Button Cancel Frame
    UIButton *buttonTwitter = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonTwitter addTarget:self
                    action:@selector(actionButtonTwitter:)
          forControlEvents:UIControlEventTouchUpInside];
    CGSize sizeOfbuttonTwitter = CGSizeMake(60.0, 60.0);
    CGPoint PointOfbuttonTwitter = CGPointMake(buttonFacebook.frame.origin.x + sizeOfbuttonGoogle.width + 35, deviceBounds.size.height/2 - sizeOfbuttonFacebook.height/2);
    buttonTwitter.frame = CGRectMake(PointOfbuttonTwitter.x, PointOfbuttonTwitter.y, sizeOfbuttonTwitter.width, sizeOfbuttonTwitter.height);
    [buttonTwitter scaleToFill];
    [buttonTwitter setImage:[UIImage imageNamed:@"twitter-icon"] forState:UIControlStateNormal];
    
    // Label Welcome Frame
    UILabel *labelWelcome = [[UILabel alloc] init];
    labelWelcome.textAlignment = NSTextAlignmentCenter;
    labelWelcome.frame = CGRectMake(0, buttonFacebook.frame.origin.y + buttonFacebook.frame.size.height + 15,deviceBounds.size.width, 20);
    labelWelcome.text = @"Login with a click";
    labelWelcome.font = [UIFont fontWithName:kFontSFUIDisplayRegular size:16];
    labelWelcome.textColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1/1.0];
    
    viewOption = [[UIView alloc] initWithFrame:CGRectMake(0, 0.65 * deviceBounds.size.height, deviceBounds.size.width, 180)];
    [self viewOrOption];
    
    // Label Copyright Frame
    UILabel *labelCopyright = [[UILabel alloc] init];
    labelCopyright.textAlignment = NSTextAlignmentCenter;
    CGSize labelSizeOfCopyright = CGSizeMake(300.0, 40.0);
    CGPoint labelPointOfCopyright = CGPointMake(deviceBounds.size.width/2 - labelSizeOfCopyright.width/2, 0.94 * deviceBounds.size.height);
    labelCopyright.frame = CGRectMake(labelPointOfCopyright.x, labelPointOfCopyright.y, labelSizeOfCopyright.width, labelSizeOfCopyright.height);
    labelCopyright.text = @"Copyright © 2017 360vuz.com . All  Rights Reserved";
    labelCopyright.font = [UIFont fontWithName:kFontSFUIDisplayRegular size:12];
    labelCopyright.textColor = [UIColor whiteColor];
    [labelCopyright changeSubstringFont:@"360vuz.com" withFont:kFontSFUIDisplayHeavy];
    
    // Adding Views to SuperView
    [self.view addSubview:backgroundImageView];
    [self.view addSubview:buttonCanel];
    [self.view addSubview:labelWelcome];
    [self.view addSubview:viewOption];
    [self.view addSubview:labelCopyright];
    [self.view addSubview:buttonFacebook];
    [self.view addSubview:buttonGoogle];
    [self.view addSubview:buttonTwitter];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) viewOrOption
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    // Label Label Or Frame
    UILabel *labelOr = [[UILabel alloc] init];
    labelOr.textAlignment = NSTextAlignmentCenter;
    CGSize sizeOflabelOr = CGSizeMake(25, 16);
    CGPoint pointOflabelOr = CGPointMake(deviceBounds.size.width/2 - sizeOflabelOr.width/2, 5);
    labelOr.frame = CGRectMake(pointOflabelOr.x, pointOflabelOr.y, sizeOflabelOr.width, sizeOflabelOr.height);
    labelOr.text = @"or";
    labelOr.font = [UIFont fontWithName:kFontSFUIDisplayLight size:14];
    labelOr.textColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:1/1.0];
    
    // Button GetStarted Frame
    UIButton *buttonSignUp = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonSignUp addTarget:self
                     action:@selector(actionButtonSignUp:)
           forControlEvents:UIControlEventTouchUpInside];
    [buttonSignUp setTitle:@"Sign Up" forState:UIControlStateNormal];
    buttonSignUp.backgroundColor = UIColorFromRGB(0x29AEE3);
    CGSize labelSizeOfGetStarted = CGSizeMake(deviceBounds.size.width * 0.85, 55.0);
    CGPoint labelPointOfGetStarted = CGPointMake(deviceBounds.size.width/2 - labelSizeOfGetStarted.width/2, 0.20 * viewOption.frame.size.height);
    buttonSignUp.frame = CGRectMake(labelPointOfGetStarted.x, labelPointOfGetStarted.y, labelSizeOfGetStarted.width, labelSizeOfGetStarted.height);
    [buttonSignUp.titleLabel setFont:[UIFont fontWithName:kFontSFUIDisplayRegular size:14]];
    buttonSignUp.layer.cornerRadius = 4;
    buttonSignUp.clipsToBounds = YES;
    
    // Button Login Frame
    UIButton *buttonLogin = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonLogin addTarget:self
                    action:@selector(actionButtonLogin:)
          forControlEvents:UIControlEventTouchUpInside];
    [buttonLogin setTitle:@"Login" forState:UIControlStateNormal];
    CGSize labelSizeOfLogin = CGSizeMake(deviceBounds.size.width * 0.85, 55.0);
    CGPoint labelPointOfLogin = CGPointMake(deviceBounds.size.width/2 - labelSizeOfLogin.width/2,  0.60 * viewOption.frame.size.height);
    buttonLogin.frame = CGRectMake(labelPointOfLogin.x, labelPointOfLogin.y, labelSizeOfLogin.width, labelSizeOfLogin.height);
    [buttonLogin.titleLabel setFont:[UIFont fontWithName:kFontSFUIDisplayRegular size:14]];
    buttonLogin.layer.borderWidth = 1.0f;
    buttonLogin.layer.borderColor = [UIColor whiteColor].CGColor;
    buttonLogin.layer.cornerRadius = 4; // this value vary as per your desire
    buttonLogin.clipsToBounds = YES;
    
    CGPoint labelOrCenter = labelOr.center;
    
    // Label Welcome Frame
    CGSize sizeOfViewLeftOr = CGSizeMake(buttonLogin.frame.size.width/2 - 30, 2);
    CGPoint pointOfViewLeftOr = CGPointMake(labelOr.frame.origin.x - 5 - sizeOfViewLeftOr.width, labelOrCenter.y);
    UIView *viewLeftOr = [[UIView alloc] initWithFrame:CGRectMake(pointOfViewLeftOr.x, pointOfViewLeftOr.y, sizeOfViewLeftOr.width, sizeOfViewLeftOr.height)];
    viewLeftOr.backgroundColor = UIColorFromRGB(0x29AEE3);
    
    // Label Welcome Frame
    CGSize sizeOfViewRightOr = CGSizeMake(buttonLogin.frame.size.width/2 - 30, 2);
    CGPoint pointOfViewRightOr = CGPointMake(labelOr.frame.origin.x + labelOr.frame.size.width + 5, labelOrCenter.y);
    UIView *viewRightOr = [[UIView alloc] initWithFrame:CGRectMake(pointOfViewRightOr.x, pointOfViewRightOr.y, sizeOfViewRightOr.width, sizeOfViewRightOr.height)];
    viewRightOr.backgroundColor = UIColorFromRGB(0x29AEE3);
    
    [viewOption addSubview:labelOr];
    [viewOption addSubview:viewLeftOr];
    [viewOption addSubview:viewRightOr];
    [viewOption addSubview:buttonSignUp];
    [viewOption addSubview:buttonLogin];
}

#pragma mark - Video Setup -
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpBackgroundVideo
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    //Not affecting background music playing
    NSError *sessionError = nil;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:&sessionError];
    [[AVAudioSession sharedInstance] setActive:YES error:&sessionError];
    
    //Set up player
    avplayer = [[AVPlayer alloc]initWithPlayerItem:[[AVPlayerItem alloc]initWithAsset:[AVAsset assetWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"videoLogin" ofType:@"mp4"]]]]];
    AVPlayerLayer *avPlayerLayer = [AVPlayerLayer playerLayerWithPlayer:avplayer];
    [avPlayerLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    [avPlayerLayer setFrame:[UtilityClass getDeviceBounds]];
    [backgroundImageView.layer addSublayer:avPlayerLayer];
    
    //Config player
    [avplayer seekToTime:kCMTimeZero];
    [avplayer setVolume:0.0f];
    [avplayer setActionAtItemEnd:AVPlayerActionAtItemEndNone];
}

#pragma mark - VideopPlayer Notifications Handler -
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)playerItemDidReachEnd:(NSNotification *)notification
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    AVPlayerItem *playerItem = [notification object];
    [playerItem seekToTime:kCMTimeZero];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)playerStartPlaying
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [avplayer play];
}

#pragma mark - Button Actions -
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) actionButtonCancel:(UIButton *) sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [self.navigationController dismissViewControllerAnimated:true completion:^{
        avplayer = nil;
    }];
    
    // Set Up Tab Bar Controller
    [[AppDelegate appDelegate] setUpTabBarController];
    
    [AppDelegate appDelegate].isWelcomeNavigationOnTop = false;
    [AppDelegate appDelegate].topView = nil;
    
    [[AppDelegate appDelegate] makeTabBarAsRootViewController];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonSignUp:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    SignUpViewController * sUpViewController = [[SignUpViewController alloc] init];
    [self.navigationController pushViewController:sUpViewController animated:true];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonLogin:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    LoginViewController * loginViewController = [[LoginViewController alloc] init];
    [self.navigationController pushViewController:loginViewController animated:true];
}

#pragma mark - Social Sign Up -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonFaceBook:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [[SocialClass sharedManger] logInWithFacebook:self];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonGoogle:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
     [[SocialClass sharedManger] logInWithGoogle];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonTwitter:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
   [[SocialClass sharedManger] logInWithTwitter];
}

@end
