//
//  SignUpViewController.m
//  360 VUZ
//
//  Created by Mosib on 7/4/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "SignUpViewController.h"
#import "IQKeyboardManager.h"
#import "IQKeyboardReturnKeyHandler.h"
#import "IQUIView+IQKeyboardToolbar.h"
#import "ServiceRegisterUser.h"

@interface SignUpViewController ()
{
    CGRect deviceBounds;
    // Keyboard Manager
    IQKeyboardReturnKeyHandler *returnKeyHandler;
    UIButton *buttonFemale;
    UIButton *buttonMale;
    UITextField *textFieldFirstName;
    UITextField *textFieldLastName;
    UITextField *textFieldEmail;
    UITextField *textFieldPassword;
    __weak SignUpViewController* weakSelf;
}
@end

@implementation SignUpViewController

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)viewDidLoad
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super viewDidLoad];
    
    weakSelf = self;
    
    // Initialze View Controller Settings
    [self initialzeControllerSettings];
    
    // Set Up Frame for Screen
    [self setUpFrame];

}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) viewWillAppear:(BOOL)animated
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super viewWillAppear:animated];
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) viewWillDisappear:(BOOL)animated
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super viewWillDisappear:animated];

}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)didReceiveMemoryWarning
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(BOOL)prefersStatusBarHidden
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return YES;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) initialzeControllerSettings
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    // Set White Background
    self.view.backgroundColor = [UIColor whiteColor];
    
    // Set Up Return KeyHandler
    returnKeyHandler = [[IQKeyboardReturnKeyHandler alloc] initWithViewController:self];
    
    // Initialze Instance Variable
    deviceBounds = [UtilityClass getDeviceBounds];
}

#pragma mark - Frame Handling -
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpFrame
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    // Button Cancel Frame
    UIButton *buttonBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonBack addTarget:self
                    action:@selector(actionButtonBack:)
          forControlEvents:UIControlEventTouchUpInside];
    CGSize sizeOfbuttonBack = CGSizeMake(60.0, 40.0);
    CGPoint PointOfbuttonBack = CGPointMake(15, 25);
    buttonBack.frame = CGRectMake(PointOfbuttonBack.x, PointOfbuttonBack.y, sizeOfbuttonBack.width, sizeOfbuttonBack.height);
    [buttonBack setImage:[UIImage imageNamed:@"back-button"] forState:UIControlStateNormal];
    CGPoint centerOfButtonBack = buttonBack.center;
    
    // Label Sign Up
    UILabel *labelSignUp = [[UILabel alloc] init];
    CGSize sizeOfSignUp = CGSizeMake(100.0, 35.0);
    CGPoint PointOfSignUp = CGPointMake(buttonBack.frame.origin.x + buttonBack.frame.size.width + 5, centerOfButtonBack.y - sizeOfSignUp.height/2);
    labelSignUp.frame = CGRectMake(PointOfSignUp.x, PointOfSignUp.y, sizeOfSignUp.width, sizeOfSignUp.height);
    labelSignUp.text = @"signup";
    labelSignUp.font = [UIFont fontWithName:kFontSFUIDisplayBold size:26];
    labelSignUp.textColor =  UIColorFromRGB(0x13AEE5);
    
    // Label Welcome Frame
    UILabel *labelWelcome = [[UILabel alloc] init];
    CGSize sizeOflabelWelcome = CGSizeMake(140, 16);
    CGPoint pointOflabelWelcome = CGPointMake(PointOfSignUp.x + 20 , PointOfSignUp.y + sizeOfSignUp.height);
    labelWelcome.frame = CGRectMake(pointOflabelWelcome.x, pointOflabelWelcome.y, sizeOflabelWelcome.width, sizeOflabelWelcome.height);
    labelWelcome.text = @"welcome to 360VUZ";
    labelWelcome.font = [UIFont fontWithName:kFontSFUIDisplayRegular size:12];
    labelWelcome.textColor = UIColorFromRGB(0xD3D3D3);
    
    // TextField First Name
    textFieldFirstName = [[UITextField alloc] init];
    CGSize sizeOfTextFieldFirstName = CGSizeMake(deviceBounds.size.width * 0.80, 40.0);
    CGPoint PointTextFieldFirstName = CGPointMake(deviceBounds.size.width/2 - sizeOfTextFieldFirstName.width/2, 100);
    textFieldFirstName.frame = CGRectMake(PointTextFieldFirstName.x, PointTextFieldFirstName.y, sizeOfTextFieldFirstName.width, sizeOfTextFieldFirstName.height);
    textFieldFirstName.placeholder = @"first name";
    [self setTextFeildAttributes:textFieldFirstName];
    
     // TextField Last  Name
    textFieldLastName = [[UITextField alloc] init];
    CGSize sizeOfTextFieldLastName = CGSizeMake(deviceBounds.size.width * 0.80, 40.0);
    CGPoint PointTextFieldLastName = CGPointMake(deviceBounds.size.width/2 - sizeOfTextFieldLastName.width/2, 175);
    textFieldLastName.frame = CGRectMake(PointTextFieldLastName.x, PointTextFieldLastName.y, sizeOfTextFieldLastName.width, sizeOfTextFieldLastName.height);
    textFieldLastName.placeholder = @"last name";
    [self setTextFeildAttributes:textFieldLastName];
    
     // TextField Email
    textFieldEmail = [[UITextField alloc] init];
    CGSize sizeOfTextFieldEmail = CGSizeMake(deviceBounds.size.width * 0.80, 40.0);
    CGPoint PointTextFieldEmail = CGPointMake(deviceBounds.size.width/2 - sizeOfTextFieldEmail.width/2, 250);
    textFieldEmail.frame = CGRectMake(PointTextFieldEmail.x, PointTextFieldEmail.y, sizeOfTextFieldEmail.width, sizeOfTextFieldEmail.height);
    textFieldEmail.placeholder = @"email";
    [self setTextFeildAttributes:textFieldEmail];
    textFieldEmail.keyboardType = UIKeyboardTypeEmailAddress;
    
     // TextField Password
    textFieldPassword = [[UITextField alloc] init];
    CGSize sizeOfTextFieldPassword = CGSizeMake(deviceBounds.size.width * 0.80, 40.0);
    CGPoint PointTextFieldPassword = CGPointMake(deviceBounds.size.width/2 - sizeOfTextFieldPassword.width/2, 325);
    textFieldPassword.frame = CGRectMake(PointTextFieldPassword.x, PointTextFieldPassword.y, sizeOfTextFieldPassword.width, sizeOfTextFieldPassword.height);
    textFieldPassword.placeholder = @"password";
    [self setTextFeildAttributes:textFieldPassword];
    textFieldPassword.secureTextEntry = YES;
    
    // Label Male
    UILabel *labelMale = [[UILabel alloc] init];
    CGSize sizeOfMale = CGSizeMake(80.0, 35.0);
    CGPoint PointOfMale = CGPointMake(deviceBounds.size.width/2 - sizeOfMale.width, PointTextFieldPassword.y + sizeOfTextFieldPassword.height + 25);
    labelMale.frame = CGRectMake(PointOfMale.x, PointOfMale.y, sizeOfMale.width, sizeOfMale.height);
    labelMale.text = @"Male";
    labelMale.font = [UIFont fontWithName:kFontSFUIDisplayRegular size:16];
    labelMale.textColor =  UIColorFromRGB(0xD3D3D3);
    
    // Button CheckMark Male
    buttonMale = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonMale addTarget:self
                   action:@selector(actionButtonMale:)
         forControlEvents:UIControlEventTouchUpInside];
    CGSize sizeOfbuttonMale = CGSizeMake(35.0, 35.0);
    CGPoint PointOfbuttonMale = CGPointMake(PointOfMale.x - sizeOfbuttonMale.width - 10, PointOfMale.y);
    buttonMale.frame = CGRectMake(PointOfbuttonMale.x, PointOfbuttonMale.y, sizeOfbuttonMale.width, sizeOfbuttonMale.height);
    [buttonMale setImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
    [buttonMale setSelected:true];
    buttonMale.layer.borderWidth = 2.0f;
    buttonMale.layer.borderColor = UIColorFromRGB(0xD3D3D3).CGColor;
    
    // Button CheckMark Female
    buttonFemale = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonFemale addTarget:self
                   action:@selector(actionButtonFemale:)
         forControlEvents:UIControlEventTouchUpInside];
    CGSize sizeOfbuttonFemale = CGSizeMake(35.0, 35.0);
    CGPoint PointOfbuttonFemale = CGPointMake(deviceBounds.size.width/2 + 10, PointOfMale.y);
    buttonFemale.frame = CGRectMake(PointOfbuttonFemale.x, PointOfbuttonFemale.y, sizeOfbuttonFemale.width, sizeOfbuttonFemale.height);
    buttonFemale.layer.borderWidth = 2.0f;
    buttonFemale.layer.borderColor = UIColorFromRGB(0xD3D3D3).CGColor;
    
    // Label Female
    UILabel *labelFemale = [[UILabel alloc] init];
    CGSize sizeOflabelFemale = CGSizeMake(80.0, 35.0);
    CGPoint PointOflabelFemale = CGPointMake(sizeOfbuttonFemale.width + PointOfbuttonFemale.x + 10, PointOfMale.y);
    labelFemale.frame = CGRectMake(PointOflabelFemale.x, PointOflabelFemale.y, sizeOflabelFemale.width, sizeOflabelFemale.height);
    labelFemale.text = @"Female";
    labelFemale.font = [UIFont fontWithName:kFontSFUIDisplayRegular size:16];
    labelFemale.textColor =  UIColorFromRGB(0xD3D3D3);

    // Button SignUp Frame
    UIButton *buttonSignUp = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonSignUp addTarget:self
                     action:@selector(actionButtonSignUp:)
           forControlEvents:UIControlEventTouchUpInside];
    [buttonSignUp setTitle:@"Sign Up" forState:UIControlStateNormal];
    buttonSignUp.backgroundColor = UIColorFromRGB(0x29AEE3);
    CGSize labelSizeOfGetStarted = CGSizeMake(deviceBounds.size.width * 0.80, 55.0);
    CGPoint labelPointOfGetStarted = CGPointMake(deviceBounds.size.width/2 - labelSizeOfGetStarted.width/2, deviceBounds.size.height * 0.80);
    buttonSignUp.frame = CGRectMake(labelPointOfGetStarted.x, labelPointOfGetStarted.y, labelSizeOfGetStarted.width, labelSizeOfGetStarted.height);
    [buttonSignUp.titleLabel setFont:[UIFont fontWithName:kFontSFUIDisplayRegular size:14]];
    buttonSignUp.layer.cornerRadius = 4;
    buttonSignUp.clipsToBounds = YES;
    
    // Button Login Frame
    UIButton *buttonLogin = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonLogin addTarget:self
                     action:@selector(actionButtonLogin:)
           forControlEvents:UIControlEventTouchUpInside];
    CGSize sizeOfbuttonLogin = CGSizeMake(200.0, 15.0);
    CGPoint PointOfbuttonLogin = CGPointMake(deviceBounds.size.width/2 - sizeOfbuttonLogin.width/2, deviceBounds.size.height * 0.95);
    buttonLogin.frame = CGRectMake(PointOfbuttonLogin.x, PointOfbuttonLogin.y, sizeOfbuttonLogin.width, sizeOfbuttonLogin.height);
    [buttonLogin setAttributedTitle:[self getAttributedStringForLogin] forState:UIControlStateNormal];
    buttonLogin.layer.cornerRadius = 4;
    buttonLogin.clipsToBounds = YES;
    
    // Adding Views to SuperView
    [self.view addSubview:buttonBack];
    [self.view addSubview:labelSignUp];
    [self.view addSubview:labelWelcome];
    [self.view addSubview:textFieldFirstName];
    [self.view addSubview:textFieldLastName];
    [self.view addSubview:textFieldEmail];
    [self.view addSubview:textFieldPassword];
    [self.view addSubview:buttonMale];
    [self.view addSubview:buttonFemale];
    [self.view addSubview:labelMale];
    [self.view addSubview:labelFemale];
    [self.view addSubview:buttonSignUp];
    [self.view addSubview:buttonLogin];
}

#pragma mark - Button Actions -
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonBack:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [self.navigationController popViewControllerAnimated:true];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonLogin:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [self.navigationController popViewControllerAnimated:false];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonSignUp:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    // Validate text Feilds
    if ([self validateTextfeilds])
    {
        // Is Internet Reacable
        if ([[AppDelegate appDelegate] isInternetReachable])
        {
            // API call for Login
            [self checkSignUpCredentialsAPI:@{kAPIkeyGender:[self getMaleOrFemale],
                                             kAPIkeyFirstName:textFieldFirstName.text,
                                             kAPIkeyLastName:textFieldLastName.text,
                                             kAPIkeyDeviceID:[[Settings sharedManger] getAPNSToken],
                                             kAPIkeyPhoneNumber:@"",
                                             kAPIkeyWebsite:@"",
                                             kAPIkeyBio:@"",
                                             kAPIkeyEmail:textFieldEmail.text,
                                             kAPIkeyPassword:textFieldPassword.text,
                                             kAPIkeyModel:[[UIDevice currentDevice] platformString]}];
        }
        
        else
        {
            // Show Error Message
            [[UtilityClass sharedManger] alertErrorWithMessage:@"Not Connected To Internet"];
        }
    }
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonTwitter:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonFemale:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if (!sender.isSelected)
    {
        [sender setSelected:true];
        [sender setImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
        
        [buttonMale setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [buttonMale setSelected:false];
        
    }
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonMale:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if (!sender.isSelected)
    {
        [sender setSelected:true];
        [sender setImage:[UIImage imageNamed:@"check"] forState:UIControlStateNormal];
        
        [buttonFemale setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [buttonFemale setSelected:false];
        
    }
}

#pragma mark - Helper Methods -
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSAttributedString *) getAttributedStringForLogin
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    NSString *textForAlready = @"Already have an account ? Login";
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] init];
    [attString appendAttributedString:[[NSAttributedString alloc] initWithString:textForAlready  attributes:@{
                                                                                                              NSFontAttributeName:[UIFont fontWithName:kFontSFUIDisplayRegular size:12.0],
                                                                                                              NSForegroundColorAttributeName:UIColorFromRGB(0xD3D3D3)}]];
    NSRange range = [textForAlready rangeOfString:@"Login"];
    [attString addAttribute:NSFontAttributeName value:[UIFont fontWithName:kFontSFUIDisplayHeavy size:12.0] range:range];
    [attString addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x29AEE3) range:range];
    
    return [attString copy];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setTextFeildAttributes:(UITextField *) textField
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    textField.borderStyle = UITextBorderStyleNone;
    textField.font = [UIFont fontWithName:kFontSFUIDisplayRegular size:16];
    textField.autocorrectionType = UITextAutocorrectionTypeNo;
    textField.keyboardType = UIKeyboardTypeDefault;
    textField.returnKeyType = UIReturnKeyDone;
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    
     [textField setBottomBorder:UIColorFromRGB(0xD3D3D3)];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (BOOL) validateTextfeilds
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if ([textFieldEmail.text length] == 0 || [textFieldPassword.text length] == 0 || [textFieldFirstName.text length] == 0 || [textFieldLastName.text length] == 0)
    {
        [[UtilityClass sharedManger] alertErrorWithMessage:@"Feilds cannot be empty"];
        return false;
    }
    
    if (![[UtilityClass sharedManger] validateEmailWithString:textFieldEmail.text])
    {
        
        [[UtilityClass sharedManger] alertErrorWithMessage:@"Please enter a valid email"];
        
        return false;
    }
    
    return true;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSString *) getMaleOrFemale
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
     if (buttonMale.isSelected) return @"Male";
      else return @"Female";
}


#pragma mark - API Methods -
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) checkSignUpCredentialsAPI:(NSDictionary *) paramerters
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    STARTBACKGROUNDTHREAD
    ServiceRegisterUser *registerUser = [[ServiceRegisterUser alloc]init];
    
    [registerUser executeRequest:paramerters
                           success:^(NSDictionary  *responseObject)
     {
         [weakSelf SuccessForSignUpCredentialsAPI:responseObject];
         
     }
                           failure:^(NSError * error) {
                               
                               [weakSelf FailureToGetResponceFromServer:error];
                           }];
    ENDTHREAD
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) SuccessForSignUpCredentialsAPI:(NSDictionary *) responseObject
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    NSString * responceObjectStatus = [responseObject objectForKey:kAPIkeyStatus];
    
    if ([responceObjectStatus isEqualToString:kAPIkeySuccess])
    {
        [self successInAPIResponce:responseObject];
    }
    else
    {
        [self failureInAPIResponce:responseObject];
    }
    
    STARTMAINTHREAD
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    ENDTHREAD
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) successInAPIResponce:(NSDictionary *) responseObject
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    STARTMAINTHREAD
    
     [[UtilityClass sharedManger] alertMessage:@"Success" withMessage:[responseObject objectForKey:kAPIkeyMessage]];
    
    [self.navigationController popToRootViewControllerAnimated:true];
    
    ENDTHREAD
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) failureInAPIResponce:(NSDictionary *) responseObject
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
     STARTMAINTHREAD
    
     [[UtilityClass sharedManger] alertErrorWithMessage:[NSString stringWithFormat:@"%@",[responseObject objectForKey:kAPIkeyMessage]]];
    
    // Pop to Root View Controller
    [weakSelf.navigationController popToRootViewControllerAnimated:true];
    
    ENDTHREAD
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) FailureToGetResponceFromServer:(NSError *)error
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    STARTMAINTHREAD
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [[UtilityClass sharedManger] alertErrorWithMessage:@"Something went wrong.Please try again later"];
    
    ENDTHREAD
}


@end
