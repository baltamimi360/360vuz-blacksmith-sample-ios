//
//  ForgotViewController.m
//  360 VUZ
//
//  Created by Mosib on 7/4/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "ForgotViewController.h"
#import "IQKeyboardManager.h"
#import "IQKeyboardReturnKeyHandler.h"
#import "IQUIView+IQKeyboardToolbar.h"
#import "ServicePasswordReset.h"

@interface ForgotViewController ()
{
    CGRect deviceBounds;
    // Keyboard Manager
    IQKeyboardReturnKeyHandler *returnKeyHandler;
    UITextField *textFieldEmail;
    __weak ForgotViewController* weakSelf;
}
@end

@implementation ForgotViewController

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)viewDidLoad
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super viewDidLoad];
    
    weakSelf = self;
    
    // Initialze View Controller Settings
    [self initialzeControllerSettings];
    
    // Set Up Frame for Screen
    [self setUpFrame];
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) viewWillAppear:(BOOL)animated
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super viewWillAppear:animated];
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) viewWillDisappear:(BOOL)animated
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super viewWillDisappear:animated];
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)didReceiveMemoryWarning
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(BOOL)prefersStatusBarHidden
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return YES;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) initialzeControllerSettings
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    // Set White Background
    self.view.backgroundColor = [UIColor whiteColor];
    
    // Set Up Return KeyHandler
    returnKeyHandler = [[IQKeyboardReturnKeyHandler alloc] initWithViewController:self];
    
    // Initialze Instance Variable
    deviceBounds = [UtilityClass getDeviceBounds];
}

#pragma mark - Frame Handling -
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpFrame
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    // Button Cancel Frame
    UIButton *buttonBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonBack addTarget:self
                   action:@selector(actionButtonBack:)
         forControlEvents:UIControlEventTouchUpInside];
    CGSize sizeOfbuttonBack = CGSizeMake(60.0, 40.0);
    CGPoint PointOfbuttonBack = CGPointMake(15, 25);
    buttonBack.frame = CGRectMake(PointOfbuttonBack.x, PointOfbuttonBack.y, sizeOfbuttonBack.width, sizeOfbuttonBack.height);
    [buttonBack setImage:[UIImage imageNamed:@"back-button"] forState:UIControlStateNormal];
    CGPoint centerOfButtonBack = buttonBack.center;
    
    // Label Sign Up
    UILabel *labelSignUp = [[UILabel alloc] init];
    CGSize sizeOfSignUp = CGSizeMake(220.0, 35.0);
    CGPoint PointOfSignUp = CGPointMake(buttonBack.frame.origin.x + buttonBack.frame.size.width + 5, centerOfButtonBack.y - sizeOfSignUp.height/2);
    labelSignUp.frame = CGRectMake(PointOfSignUp.x, PointOfSignUp.y, sizeOfSignUp.width, sizeOfSignUp.height);
    labelSignUp.text = @"Forgot Password";
    labelSignUp.font = [UIFont fontWithName:kFontSFUIDisplayBold size:26];
    labelSignUp.textColor =  UIColorFromRGB(0x13AEE5);
    
    
    // TextField Email
    textFieldEmail = [[UITextField alloc] init];
    CGSize sizeOfTextFieldEmail = CGSizeMake(deviceBounds.size.width * 0.80, 40.0);
    CGPoint PointTextFieldEmail = CGPointMake(deviceBounds.size.width/2 - sizeOfTextFieldEmail.width/2, PointOfSignUp.y + 100);
    textFieldEmail.frame = CGRectMake(PointTextFieldEmail.x, PointTextFieldEmail.y, sizeOfTextFieldEmail.width, sizeOfTextFieldEmail.height);
    textFieldEmail.placeholder = @"email";
    [self setTextFeildAttributes:textFieldEmail];
    textFieldEmail.keyboardType = UIKeyboardTypeEmailAddress;
    
    
    // Button SignUp Frame
    UIButton *buttonSubmit = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonSubmit addTarget:self
                    action:@selector(actionButtonSubmit:)
          forControlEvents:UIControlEventTouchUpInside];
    [buttonSubmit setTitle:@"Submit" forState:UIControlStateNormal];
    buttonSubmit.backgroundColor = UIColorFromRGB(0x29AEE3);
    CGSize sizeOfbuttonLogin = CGSizeMake(deviceBounds.size.width * 0.80, 55.0);
    CGPoint pointOfbuttonLogin = CGPointMake(deviceBounds.size.width/2 - sizeOfbuttonLogin.width/2, deviceBounds.size.height * 0.80);
    buttonSubmit.frame = CGRectMake(pointOfbuttonLogin.x, pointOfbuttonLogin.y, sizeOfbuttonLogin.width, sizeOfbuttonLogin.height);
    [buttonSubmit.titleLabel setFont:[UIFont fontWithName:kFontSFUIDisplayRegular size:14]];
    buttonSubmit.layer.cornerRadius = 4;
    buttonSubmit.clipsToBounds = YES;
    
    // Adding Views to SuperView
    [self.view addSubview:buttonBack];
    [self.view addSubview:labelSignUp];
    [self.view addSubview:textFieldEmail];
    [self.view addSubview:buttonSubmit];
}


#pragma mark - Button Actions -
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonBack:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [self.navigationController popViewControllerAnimated:true];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonSubmit:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if ([self validateTextfeilds])
    {
        if ([[AppDelegate appDelegate] isInternetReachable])
        {
            [self checkLoginCredentialsAPI:@{kAPIkeyEmail: textFieldEmail.text}];
        }
        
        else
        {
            [[UtilityClass sharedManger] alertErrorWithMessage:@"Not Connected To Internet"];
        }
    }
}

#pragma mark - Helper Methods -
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSAttributedString *) getAttributedStringForNoAccount
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    NSString *textForAlready = @"Don't have an account ? Sign Up";
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] init];
    [attString appendAttributedString:[[NSAttributedString alloc] initWithString:textForAlready  attributes:@{
                                                                                                              NSFontAttributeName:[UIFont fontWithName:kFontSFUIDisplayRegular size:12.0],
                                                                                                              NSForegroundColorAttributeName:UIColorFromRGB(0xD3D3D3)}]];
    NSRange range = [textForAlready rangeOfString:@"Sign Up"];
    [attString addAttribute:NSFontAttributeName value:[UIFont fontWithName:kFontSFUIDisplayHeavy size:12.0] range:range];
    [attString addAttribute:NSForegroundColorAttributeName value:UIColorFromRGB(0x29AEE3) range:range];
    
    return [attString copy];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSAttributedString *) getAttributedStringForForgotPassword
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    NSString *textForAlready = @"Forgot your password?";
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] init];
    [attString appendAttributedString:[[NSAttributedString alloc] initWithString:textForAlready  attributes:@{
                                                                                                              NSFontAttributeName:[UIFont fontWithName:kFontSFUIDisplayHeavy size:12.0],
                                                                                                              NSForegroundColorAttributeName:UIColorFromRGB(0x29AEE3)}]];
    return [attString copy];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setTextFeildAttributes:(UITextField *) textField
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    textField.borderStyle = UITextBorderStyleNone;
    textField.font = [UIFont fontWithName:kFontSFUIDisplayRegular size:16];
    textField.autocorrectionType = UITextAutocorrectionTypeNo;
    textField.keyboardType = UIKeyboardTypeDefault;
    textField.returnKeyType = UIReturnKeyDone;
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    
    
    [textField setBottomBorder:UIColorFromRGB(0xD3D3D3)];
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (BOOL) validateTextfeilds
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if ([textFieldEmail.text length] == 0 )
    {
        [[UtilityClass sharedManger] alertErrorWithMessage:@"Feilds cannot be empty"];
        return false;
    }
    
    if (![[UtilityClass sharedManger] validateEmailWithString:textFieldEmail.text])
    {
        
        [[UtilityClass sharedManger] alertErrorWithMessage:@"Please enter a valid email"];
        
        return false;
    }
    
    return true;
}

#pragma mark - API Methods -
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) checkLoginCredentialsAPI:(NSDictionary *) paramerters
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    STARTBACKGROUNDTHREAD
    ServicePasswordReset *passwordReset = [[ServicePasswordReset alloc]init];
    
    [passwordReset executeRequest:paramerters
                           success:^(NSDictionary  *responseObject)
     {
         [weakSelf SuccessForResetPasswordAPI:responseObject];
         
     }
                           failure:^(NSError * error) {
                               
                               [weakSelf FailureForResetPasswordAPI:error];
                           }];
    ENDTHREAD
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) SuccessForResetPasswordAPI:(NSDictionary *) responseObject
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    NSString * responceObjectStatus = [responseObject objectForKey:kAPIkeyStatus];
    
    if ([responceObjectStatus isEqualToString:kAPIkeySuccess])
    {
        [self successInAPIResponce:responseObject];
    }
    else
    {
        [self failureInAPIResponce:responseObject];
    }
    
    STARTMAINTHREAD
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    ENDTHREAD
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) successInAPIResponce:(NSDictionary *) responseObject
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    STARTMAINTHREAD
    
    [[UtilityClass sharedManger] alertMessage:@"Success" withMessage:[responseObject objectForKey:kAPIkeyMessage]];
    
    // Pop to Root View Controller
    [weakSelf.navigationController popToRootViewControllerAnimated:true];
    
    ENDTHREAD
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) failureInAPIResponce:(NSDictionary *) responseObject
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    STARTMAINTHREAD
    
    [[UtilityClass sharedManger] alertErrorWithMessage:[NSString stringWithFormat:@"%@",[responseObject objectForKey:kAPIkeyMessage]]];
    
    ENDTHREAD
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) FailureForResetPasswordAPI:(NSError *)error
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    STARTMAINTHREAD
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [[UtilityClass sharedManger] alertErrorWithMessage:@"Something went wrong.Please try again later"];
    
    ENDTHREAD
}

@end
