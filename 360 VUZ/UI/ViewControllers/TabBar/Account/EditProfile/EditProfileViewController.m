//
//  EditProfileViewController.m
//  360 VUZ
//
//  Created by Mosib on 8/1/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "EditProfileViewController.h"
#import "VideoTableViewCell.h"
#import "EditProfileDetailTableViewCell.h"
#import "EditProfileSocialTableViewCell.h"
#import "UserDetailsDataModel.h"
#import "EditProfileService.h"

typedef NS_ENUM(NSInteger, EditProfileSection)
{
    General,
    Personal,
    Connect
};

typedef NS_ENUM(NSInteger, GeneralSection)
{
    GeneralSectionTypeFacbook,
    GeneralSectionTypeTwitter,
    GeneralSectionTypeGoogle
};

@interface EditProfileViewController ()<UITableViewDataSource, UITableViewDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate>
{
    UserDetailsDataModel *user;
    UIImage *imageProfile;
    NSString * firstName;
    NSString * lastName;
    NSString * website;
    NSString * bio;
    NSString * phoneNumber;
}

@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSArray *sectionHeaderArray;
@property (strong, nonatomic) NSArray *placeHolderArray;
@property (strong, nonatomic) UIImageView *imageViewProfile;

@end

@implementation EditProfileViewController

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)viewDidLoad
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setUpTopView];
    
    // Initialize TableView
    [self setUpTableView];
    
    [self initializeObjects];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTestNotification:)
                                                 name:kNotificationTextFeildUpdatedInEditProfile
                                               object:nil];
    
    user =  [[Settings sharedManger] getUserDetails];
    
    [self updateProfile];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) viewWillAppear:(BOOL)animated
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super viewWillAppear:animated];

    user =  [[Settings sharedManger] getUserDetails];
        
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) viewWillDisappear:(BOOL)animated
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kNotificationTextFeildUpdatedInEditProfile object:nil];
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) updateProfile
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    __weak EditProfileViewController *weakSelf = self;
    
    UIImage * image = [UIImage imageNamed:@"profile-photo"];
    
    if (imageProfile != nil)
    {
        image = imageProfile;
    }
    
    [_imageViewProfile setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:user.imageUrlPrimary]]
                             placeholderImage:image
                                      success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                          
                                          weakSelf.imageViewProfile.image = image;
                                          imageProfile = image;
                                          weakSelf.imageViewProfile.layer.cornerRadius = weakSelf.imageViewProfile.frame.size.height /2;
                                          weakSelf.imageViewProfile.layer.masksToBounds = YES;
                                          weakSelf.imageViewProfile.layer.borderWidth = 0;
                                          
                                      } failure:nil];
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) receiveTestNotification:(NSNotification *) notification
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
    if ([[notification name] isEqualToString:kNotificationTextFeildUpdatedInEditProfile])
       
    {
            NSString * type =  [[notification userInfo] objectForKey:@"Type"];
           NSString *value = [[notification userInfo] objectForKey:@"value"];
        
          if ([type isEqualToString:@"FirstName"])
          {
              firstName = value;
          }
        
          else if ([type isEqualToString:@"LastName"])
          {
              lastName = value;
          }
    
          else if ([type isEqualToString:@"Website"])
          {
              website = value;
          }
        
          else if ([type isEqualToString:@"Bio"])
          {
              bio = value;
          }
        
          else if ([type isEqualToString:@"PhoneNumber"])
          {
              phoneNumber = value;
          }
    
    }
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonBack:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [self.navigationController popViewControllerAnimated:true];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionCamera:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    UIAlertController *alertController=[UIAlertController alertControllerWithTitle:@"" message:@"Change Profile image" preferredStyle:UIAlertControllerStyleActionSheet];
    
    // Take Photo
    UIAlertAction *takePhoto = [UIAlertAction actionWithTitle:@"Take Photo" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        
        picker.delegate = self;
        
        picker.allowsEditing = YES;
        
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        [self presentViewController:picker animated:YES completion:NULL];
        
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    [alertController addAction:takePhoto];
    
    // Choose Photo
    UIAlertAction *choosePhoto = [UIAlertAction actionWithTitle:@"Select From Photos" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        UIImagePickerController *pickerView = [[UIImagePickerController alloc] init];
        
        pickerView.allowsEditing = YES;
        
        pickerView.delegate = self;
        
        [pickerView setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        
       [self presentViewController:pickerView animated:YES completion:NULL];
        
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertController addAction:choosePhoto];
    
    UIAlertAction *actionCancel=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
        
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alertController addAction:actionCancel];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    
    NSData *imgData = UIImageJPEGRepresentation(chosenImage, 0); //1 it represents the quality of the image.
    NSLog(@"Size of Image(bytes):%lu",(unsigned long)[imgData length]);
    
    _imageViewProfile.image = [UIImage imageWithData:imgData];
    imageProfile = [[UtilityClass sharedManger] imageWithImage:[UIImage imageWithData:imgData] scaledToSize:CGSizeMake(100, 100)];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
    [picker dismissViewControllerAnimated:YES completion:NULL];

}

#pragma mark - UITableView DataSource Methods -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    switch (indexPath.section)
    {
            case General:
        {
            EditProfileDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kRIDEditProfileDetailTableViewCell];
            
            if (cell == nil) {
                cell = [[EditProfileDetailTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:kRIDEditProfileDetailTableViewCell];
                
                if (indexPath.row == 0)
                {
                    cell.textfeildEmail.text = user.firstName;
                    firstName = user.firstName;
                }
                
                else if (indexPath.row == 1)
                {
                    cell.textfeildEmail.text = user.lastName;
                    lastName = user.lastName;
                }
                
                else if (indexPath.row == 2)
                {
                    cell.textfeildEmail.text = user.website;
                    website = user.website;
                }
                
                else if (indexPath.row == 3)
                {
                    cell.textfeildEmail.text = user.bio;
                    bio = user.bio;
                }

            }
            
            cell.indexPath = indexPath;
            
            if (indexPath.row == 0)
            {
                cell.textfeildEmail.text = firstName;
            }
            
            else if (indexPath.row == 1)
            {
                cell.textfeildEmail.text = lastName;
            }
            
            else if (indexPath.row == 2)
            {
                cell.textfeildEmail.text = website;
            }
            
            else if (indexPath.row == 3)
            {
                cell.textfeildEmail.text = bio;
            }
            
         
               [self setPlaceHolder:cell.textfeildEmail atIndexPath:indexPath];
            
           
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            
            return cell;
            
            break;
        }
            
        case Personal:
        {
            EditProfileDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kRIDEditProfileDetailTableViewCell];
            
            if (cell == nil) {
                cell = [[EditProfileDetailTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:kRIDEditProfileDetailTableViewCell];
                
                if (indexPath.row == 0)
                {
                    cell.textfeildEmail.text = user.emailID;
                }
                
                else if (indexPath.row == 1)
                {
                    cell.textfeildEmail.text = user.phoneNumber;
                    phoneNumber = user.phoneNumber;
                }
            }
            
            cell.indexPath = indexPath;
            
            
            if (indexPath.row == 0)
            {
                cell.textfeildEmail.text = user.emailID;
                cell.textfeildEmail.enabled = false;
                
            }
            
            else if (indexPath.row == 1)
            {
                 cell.textfeildEmail.text = phoneNumber;
            }
            
           [self setPlaceHolder:cell.textfeildEmail atIndexPath:indexPath];
           
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            
            return cell;
            
            break;
        }
            
        case Connect:
        {
                EditProfileSocialTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kRIDEditProfileSocialTableViewCell];
            
                if (cell == nil) {
                    cell = [[EditProfileSocialTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:kRIDEditProfileSocialTableViewCell];
            
                }
            
                 [cell disableSwitch];
            
                [self updateData:cell atIndexPath:indexPath];
            
            
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                
                
                return cell;
            
            break;
        }
            
           
            default:
        {
            return nil;
        }
    }
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    switch (section)
    {
        case General:
        {
            return 4;
            
            break;
        }
            
        case Personal:
        {
            return 2;
            
            break;
        }
            
        case Connect:
        {
            return 3;
            
            break;
        }
            
        default:
        {
            return 0;
        }
    }
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return 3;
}

#pragma mark - UITableView Delegate Methods -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return 50;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if ([[_sectionHeaderArray objectAtIndex:section] isEqualToString:kEmptyString])
    {
        return nil;
    }
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 55)];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 25, tableView.frame.size.width, 22)];
    [label setFont:[UIFont fontWithName:kFontSFUIDisplayBold size:18]];
    label.textColor = UIColorFromRGB(0x29AEE3);
    
    NSString *string = [_sectionHeaderArray objectAtIndex:section];
    [label setText:string];
    [view addSubview:label];
    [view setBackgroundColor:[UIColor whiteColor]];
    return view;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if (section == 0)
    {
        return 0;
    }
    
    return 55.0;
}

#pragma mark - Private Methods -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpNavigationBar
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"back-button"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(actionButtonBack:)];
    
    self.navigationItem.leftBarButtonItem = backButton;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpTopView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    // Button Cancel Frame
    UIButton *buttonBack = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonBack addTarget:self
                   action:@selector(actionButtonBack:)
         forControlEvents:UIControlEventTouchUpInside];
    CGSize sizeOfbuttonBack = CGSizeMake(60.0, 40.0);
    CGPoint PointOfbuttonBack = CGPointMake(15, 25);
    buttonBack.frame = CGRectMake(PointOfbuttonBack.x, PointOfbuttonBack.y, sizeOfbuttonBack.width, sizeOfbuttonBack.height);
    [buttonBack setImage:[UIImage imageNamed:@"back-button"] forState:UIControlStateNormal];
    CGPoint centerOfButtonBack = buttonBack.center;
    
    // Label Edit Profile
    UILabel *labelEditProfile = [[UILabel alloc] init];
    CGSize sizeOfEditProfile = CGSizeMake(175.0, 35.0);
    CGPoint PointOfEditProfile = CGPointMake(buttonBack.frame.origin.x + buttonBack.frame.size.width + 5, centerOfButtonBack.y - sizeOfEditProfile.height/2);
    labelEditProfile.frame = CGRectMake(PointOfEditProfile.x, PointOfEditProfile.y, sizeOfEditProfile.width, sizeOfEditProfile.height);
    labelEditProfile.text = @"Edit Profile";
    labelEditProfile.font = [UIFont fontWithName:kFontSFUIDisplayBold size:26];
    labelEditProfile.textColor =  UIColorFromRGB(0x13AEE5);
    
    // Adding Views to SuperView
    [self.view addSubview:buttonBack];
    [self.view addSubview:labelEditProfile];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) setUpTableView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    CGFloat x = 0;
    CGFloat y = 70;
    CGFloat width = self.view.frame.size.width;
    CGFloat height = self.view.frame.size.height - y;
    CGRect tableFrame = CGRectMake(x, y, width, height);
    
    
    UITableView *tableView = [[UITableView alloc]initWithFrame:tableFrame style:UITableViewStylePlain];
    tableView.scrollEnabled = YES;
    tableView.userInteractionEnabled = YES;
    tableView.bounces = YES;
    [tableView setShowsHorizontalScrollIndicator:NO];
    [tableView setShowsVerticalScrollIndicator:NO];
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    
    tableView.contentInset = UIEdgeInsetsMake(0, 0, 64, 0);
    
    if([tableView respondsToSelector:@selector(setCellLayoutMarginsFollowReadableWidth:)])
    {
        tableView.cellLayoutMarginsFollowReadableWidth = NO;
    }
    
    tableView.delegate = self;
    tableView.dataSource = self;
    
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView = tableView;
    
    // Add Header View
    [self setUpTableViewHeader];
    
    // Add Footer View
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, 75)];
    
    // Button Save
    UIButton *buttonSave = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonSave addTarget:self
                   action:@selector(actionButtonSave:)
         forControlEvents:UIControlEventTouchUpInside];
    [buttonSave setTitle:@"Save" forState:UIControlStateNormal];
    buttonSave.backgroundColor = UIColorFromRGB(0x29AEE3);
    CGSize labelSizeOfGetStarted = CGSizeMake([UtilityClass getDeviceBounds].size.width * 0.85, 55.0);
    CGPoint labelPointOfGetStarted = CGPointMake([UtilityClass getDeviceBounds].size.width/2 - labelSizeOfGetStarted.width/2, 20);
    buttonSave.frame = CGRectMake(labelPointOfGetStarted.x, labelPointOfGetStarted.y, labelSizeOfGetStarted.width, labelSizeOfGetStarted.height);
    [buttonSave.titleLabel setFont:[UIFont fontWithName:kFontSFUIDisplayRegular size:20]];
    buttonSave.layer.cornerRadius = 4;
    buttonSave.clipsToBounds = YES;
    
    
    [footerView addSubview:buttonSave];
    
    self.tableView.tableFooterView = footerView;
    
    
    [self.view addSubview:self.tableView];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpTableViewHeader
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    UIView *tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 150)];
    
    CGSize sizeOfimageViewProfile = CGSizeMake(100.0, 100.0);
    CGPoint PointOfimageViewProfile = CGPointMake(self.view.frame.size.width/2 - sizeOfimageViewProfile.width/2, tableHeaderView.frame.size.height/2 - sizeOfimageViewProfile.height/2);
    
    _imageViewProfile = [[UIImageView alloc] initWithFrame:CGRectMake(PointOfimageViewProfile.x, PointOfimageViewProfile.y, sizeOfimageViewProfile.width, sizeOfimageViewProfile.height)];
    
    [_imageViewProfile setImage:[UIImage imageNamed:@"profile-photo"]];
    imageProfile = _imageViewProfile.image;
    [_imageViewProfile makeCircle];
    [_imageViewProfile.layer setBorderColor: [[UIColor blackColor] CGColor]];
    [_imageViewProfile.layer setBorderWidth: 0.5];
  
    [tableHeaderView addSubview:_imageViewProfile];
    
    
    // Button Camera
    UIButton *buttonCamera = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonCamera addTarget:self
                   action:@selector(actionCamera:)
         forControlEvents:UIControlEventTouchUpInside];
    CGSize sizeOfbuttonCamera = CGSizeMake(100.0, 100.0);
    CGPoint PointOfbuttonCamera = CGPointMake(self.view.frame.size.width/2 - sizeOfimageViewProfile.width/2, tableHeaderView.frame.size.height/2 - sizeOfimageViewProfile.height/2);
    buttonCamera.frame = CGRectMake(PointOfbuttonCamera.x, PointOfbuttonCamera.y, sizeOfbuttonCamera.width, sizeOfbuttonCamera.height);

     [tableHeaderView addSubview:buttonCamera];
    
    self.tableView.tableHeaderView = tableHeaderView;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonSave:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [self checkEditProfileAPI:@{@"first_name":firstName,@"last_name":lastName,@"gender":user.gender,@"profile_picture":[[UtilityClass sharedManger] encodeToBase64String:imageProfile],@"website":website,@"bio":bio,@"phone_number":phoneNumber}];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) initializeObjects
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    _sectionHeaderArray = [[NSArray alloc] initWithObjects:kEmptyString,@"PERSONAL INFORMATION",@"CONNECT YOUR ACCOUNT", nil];
    
    // Placeholder Array
    _placeHolderArray = @[@[@"First Name",@"Last Name",@"Website",@"Bio"],@[@"Email",@"Phone Number"]];
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setPlaceHolder:(UITextField *) textFeild atIndexPath:(NSIndexPath *) path
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    textFeild.attributedPlaceholder =
    [[NSAttributedString alloc]
     initWithString:[[_placeHolderArray objectAtIndex:path.section] objectAtIndex:path.row]
     attributes:@{NSFontAttributeName : [UIFont fontWithName:kFontSFUIDisplayRegular size:18]}];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) updateData:(EditProfileSocialTableViewCell *) cell atIndexPath:(NSIndexPath*) indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    switch (indexPath.row)
    {
        case GeneralSectionTypeFacbook:
        {
            cell.labelWhatsHot.text = @"Facebook";
            cell.labelWhatsHot.textColor = UIColorFromRGB(0x3b5998);
            
            if ([user.registrationType  isEqual: @"facebook"])
            {
                [cell enableSwitch];
            }
            
            break;
        }
            
        case GeneralSectionTypeTwitter:
        {
            cell.labelWhatsHot.text = @"Twitter";
            cell.labelWhatsHot.textColor = UIColorFromRGB(0x00ACED);//#00aced
            
            if ([user.registrationType  isEqual: @"twitter"])
            {
                [cell enableSwitch];
            }
            
            break;
        }
            
        case GeneralSectionTypeGoogle:
        {
            cell.labelWhatsHot.text = @"Google";
            cell.labelWhatsHot.textColor = UIColorFromRGB(0xDD4B39);//#dd4b39
            
            if ([user.registrationType  isEqual: @"google"])
            {
                [cell enableSwitch];
            }
            
            break;
        }
            
    }
}

#pragma mark - API Methods -
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) checkEditProfileAPI:(NSDictionary *) paramerters
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    __weak EditProfileViewController *weakSelf = self;
    
    STARTBACKGROUNDTHREAD
    EditProfileService *serviceUpdateProfile = [[EditProfileService alloc]init];
    
    [serviceUpdateProfile executeRequest:paramerters
                          success:^(NSDictionary  *responseObject)
     {
         [weakSelf SuccessForEditProfile:responseObject];
         
     }
                          failure:^(NSError * error) {
                              
                              [weakSelf FailureForEditProfile:error];
                          }];
    ENDTHREAD
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) SuccessForEditProfile:(NSDictionary *) responseObject
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    NSString * responceObjectStatus = [responseObject objectForKey:kAPIkeyStatus];
    
    if ([responceObjectStatus isEqualToString:kAPIkeySuccess])
    {
        [self successInAPIResponce:responseObject];
    }
    else
    {
        [self failureInAPIResponce:responseObject];
    }
    
    STARTMAINTHREAD
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    ENDTHREAD
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) successInAPIResponce:(NSDictionary *) responseObject
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    __weak EditProfileViewController *weakSelf = self;
    
    user =  [[Settings sharedManger] getUserDetails];
    
    [[UIImageView new] clearImageCacheForURL:[NSURL URLWithString:user.imageUrlPrimary]];
    
    STARTMAINTHREAD
    
    [[UtilityClass sharedManger] alertMessage:@"Success" withMessage:[responseObject objectForKey:kAPIkeyMessage]];
    
    // Pop to Root View Controller
    [weakSelf.navigationController popToRootViewControllerAnimated:true];
    
    ENDTHREAD
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) failureInAPIResponce:(NSDictionary *) responseObject
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    STARTMAINTHREAD
    
    [[UtilityClass sharedManger] alertErrorWithMessage:[NSString stringWithFormat:@"%@",[responseObject objectForKey:kAPIkeyMessage]]];
    
    ENDTHREAD
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) FailureForEditProfile:(NSError *)error
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    STARTMAINTHREAD
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [[UtilityClass sharedManger] alertErrorWithMessage:@"Something went wrong.Please try again later"];
    
    ENDTHREAD
}

@end
