//
//  ProfileViewController.m
//  360 VUZ
//
//  Created by Mosib on 7/6/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "ProfileViewController.h"
#import "PVCProfileBannerView.h"
#import "PVCNoVideosAdded.h"
#import "EditProfileViewController.h"
#import "OrionViewControllerLive.h"
#import "UserDetailsDataModel.h"


@interface ProfileViewController ()
{
    UserDetailsDataModel *user;
    UILabel *labelName;
    UILabel *labelFullName;
    UILabel *labelEmail;
    PVCProfileBannerView *profileBannerView;
}
@property (nonatomic) UIImageView *imageViewPicture;

@end

@implementation ProfileViewController

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)viewDidLoad
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super viewDidLoad];
    
    // Set Up Frame for Screen
    [self setUpFrame];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) viewWillAppear:(BOOL)animated
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super viewWillAppear:animated];
    
    user =  [[Settings sharedManger] getUserDetails];
    
    __weak ProfileViewController *weakSelf = self;
    
    [_imageViewPicture setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:user.imageUrlPrimary]]
                             placeholderImage:[UIImage imageNamed:@"profile-photo"]
                                      success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                          
                                          weakSelf.imageViewPicture.image = image;
                                          weakSelf.imageViewPicture.layer.cornerRadius = weakSelf.imageViewPicture.frame.size.height /2;
                                          weakSelf.imageViewPicture.layer.masksToBounds = YES;
                                          weakSelf.imageViewPicture.layer.borderWidth = 0;
                                          
                                      } failure:nil];
    
    labelName.text = [NSString stringWithFormat:@"%@ Profile",user.firstName];
    labelEmail.text = [[Settings sharedManger] getUserDetails].emailID;
    labelFullName.text = [[Settings sharedManger] getUserDetails].username;
    
    profileBannerView.label360sValue.text = user.videosCount;
    profileBannerView.labelCashValue.text = user.payPalAmount;
    profileBannerView.labelLivesValue.text = user.likeCount;
    profileBannerView.labelSpinsValue.text = user.viewCount;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpFrame
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    CGRect deviceBounds = [UtilityClass getDeviceBounds];
    
    // Label Name Title
    labelName = [[UILabel alloc] init];
    CGSize sizeOflabelName = CGSizeMake(deviceBounds.size.width, 20.0);
    CGPoint PointOflabelName = CGPointMake(0, 30);
    labelName.frame = CGRectMake(PointOflabelName.x, PointOflabelName.y, sizeOflabelName.width, sizeOflabelName.height);
    labelName.textAlignment = NSTextAlignmentCenter;
    labelName.minimumScaleFactor = 8;
    labelName.adjustsFontSizeToFitWidth = YES;
    labelName.font = [UIFont fontWithName:kFontSFUIDisplayBold size:14];
    labelName.textColor = UIColorFromRGB(0x4A4A4A);
    
    
    // ImageView Picture
    _imageViewPicture = [[UIImageView alloc] init];
    CGSize sizeOfimageViewPicture = CGSizeMake(100.0, 100.0);
    CGPoint PointOfimageViewPicture = CGPointMake(deviceBounds.size.width/2 - sizeOfimageViewPicture.width/2, PointOflabelName.y + sizeOflabelName.height + 15);
    _imageViewPicture.frame = CGRectMake(PointOfimageViewPicture.x, PointOfimageViewPicture.y, sizeOfimageViewPicture.width, sizeOfimageViewPicture.height);
    
     _imageViewPicture.image = [UIImage imageNamed:@"profile-photo"];
    

    // Button Invite
    UIButton *buttonInvite = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonInvite addTarget:self
                     action:@selector(actionButtonInvite:)
           forControlEvents:UIControlEventTouchUpInside];
    CGSize sizeOfbuttonInvite = CGSizeMake(50.0, 50.0);
    CGPoint PointOfbuttonInvite = CGPointMake(PointOfimageViewPicture.x - sizeOfbuttonInvite.width - 35, PointOfimageViewPicture.y + 50);
    buttonInvite.frame = CGRectMake(PointOfbuttonInvite.x, PointOfbuttonInvite.y, sizeOfbuttonInvite.width, sizeOfbuttonInvite.height);
    [buttonInvite scaleToFill];
    [buttonInvite setImage:[UIImage imageNamed:@"Invite-friends-icon"] forState:UIControlStateNormal];
    
    // Button Setting
    UIButton *buttonSetting = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonSetting addTarget:self
                      action:@selector(actionButtonSetting:)
            forControlEvents:UIControlEventTouchUpInside];
    CGSize sizeOfbuttonSetting = CGSizeMake(50.0, 50.0);
    CGPoint PointOfbuttonSetting = CGPointMake(PointOfimageViewPicture.x + sizeOfimageViewPicture.width + 35, PointOfimageViewPicture.y + 50);
    buttonSetting.frame = CGRectMake(PointOfbuttonSetting.x, PointOfbuttonSetting.y, sizeOfbuttonSetting.width, sizeOfbuttonSetting.height);
    [buttonSetting scaleToFill];
    [buttonSetting setImage:[UIImage imageNamed:@"settings-icon"] forState:UIControlStateNormal];
    
    // Label Full Name
    labelFullName = [[UILabel alloc] init];
    CGSize sizeOflabelFullName = CGSizeMake(deviceBounds.size.width, 26.0);
    CGPoint PointOflabelFullName = CGPointMake(0, PointOfimageViewPicture.y + sizeOfimageViewPicture.height + 35);
    labelFullName.frame = CGRectMake(PointOflabelFullName.x, PointOflabelFullName.y, sizeOflabelFullName.width, sizeOflabelFullName.height);
    labelFullName.textAlignment = NSTextAlignmentCenter;
    labelFullName.minimumScaleFactor = 8;
    labelFullName.adjustsFontSizeToFitWidth = YES;
    labelFullName.font = [UIFont fontWithName:kFontSFUIDisplayBold size:20];
    labelFullName.textColor = UIColorFromRGB(0x4A4A4A);
    
    // Label Email
    labelEmail = [[UILabel alloc] init];
    CGSize sizeOflabelEmail = CGSizeMake(deviceBounds.size.width, 18.0);
    CGPoint PointOflabelEmail = CGPointMake(0, PointOflabelFullName.y + sizeOflabelFullName.height + 3);
    labelEmail.frame = CGRectMake(PointOflabelEmail.x, PointOflabelEmail.y, sizeOflabelEmail.width, sizeOflabelEmail.height);
    labelEmail.textAlignment = NSTextAlignmentCenter;
    labelEmail.minimumScaleFactor = 8;
    labelEmail.adjustsFontSizeToFitWidth = YES;
    labelEmail.font = [UIFont fontWithName:kFontSFUIDisplayLight size:12];
    labelEmail.textColor = UIColorFromRGB(0x9B9B9B);
    
    // Label Invite Friends
    UILabel *labelInviteFriends = [[UILabel alloc] init];
    CGSize sizeOflabelInviteFriends = CGSizeMake(deviceBounds.size.width, 18.0);
    CGPoint PointOflabelInviteFriends = CGPointMake(0, PointOflabelEmail.y + sizeOflabelEmail.height + 10);
    labelInviteFriends.frame = CGRectMake(PointOflabelInviteFriends.x, PointOflabelInviteFriends.y, sizeOflabelInviteFriends.width, sizeOflabelInviteFriends.height);
    labelInviteFriends.textAlignment = NSTextAlignmentCenter;
    labelInviteFriends.minimumScaleFactor = 8;
    labelInviteFriends.adjustsFontSizeToFitWidth = YES;
    labelInviteFriends.text = @"Invite your friends and get free live credit.";
    labelInviteFriends.font = [UIFont fontWithName:kFontSFUIDisplayLight size:12];
    labelInviteFriends.textColor = UIColorFromRGB(0x13AEE5);
    
    // Banner View
    CGSize sizeOfprofileBannerView = CGSizeMake(deviceBounds.size.width, 60.0);
    CGPoint PointOfprofileBannerView = CGPointMake(0, PointOflabelInviteFriends.y + sizeOflabelInviteFriends.height + 10);
    profileBannerView = [[PVCProfileBannerView alloc] initWithFrame:CGRectMake(PointOfprofileBannerView.x, PointOfprofileBannerView.y, sizeOfprofileBannerView.width, sizeOfprofileBannerView.height)];
    
    // ImageView No Video Addes
    CGSize sizeOfNoVideosAddedView = CGSizeMake(deviceBounds.size.width, deviceBounds.size.height - PointOfprofileBannerView.y - sizeOfprofileBannerView.height);
    CGPoint PointOfNoVideosAddedView = CGPointMake(0,PointOfprofileBannerView.y + sizeOfprofileBannerView.height);
    PVCNoVideosAdded *noVideosAdded = [[PVCNoVideosAdded alloc] initWithFrame:CGRectMake(PointOfNoVideosAddedView.x, PointOfNoVideosAddedView.y, sizeOfNoVideosAddedView.width, sizeOfNoVideosAddedView.height)];
    
    
    
    [self.view addSubview:labelName];
    [self.view addSubview:_imageViewPicture];
    [self.view addSubview:buttonInvite];
    [self.view addSubview:buttonSetting];
    [self.view addSubview:labelFullName];
    [self.view addSubview:labelEmail];
    [self.view addSubview:labelInviteFriends];
    [self.view addSubview:profileBannerView];
    [self.view addSubview:noVideosAdded];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)didReceiveMemoryWarning
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonInvite:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
//    PayFortController *payFort = [[PayFortController alloc] initWithEnviroment:KPayFortEnviromentSandBox];
//    
//    payFort.IsShowResponsePage = YES;
//    payFort.HideLoading = YES;
//    
//    [payFort getUDID];
//    
//    NSMutableDictionary *request = [[NSMutableDictionary alloc]init];
//    [request setValue:@"10000" forKey:@"amount"];
//    [request setValue:@"AUTHORIZATION" forKey:@"command"];
//    [request setValue:@"USD" forKey:@"currency"];
//    [request setValue:@"email@domain.com" forKey:@"customer_email"]; [request setValue:@"en" forKey:@"language"];
//    [request setValue:@"112233682686" forKey:@"merchant_reference"];
//    [request setValue:@"token" forKey:@"sdk_token"];
//    [request setValue:@"VISA" forKey:@"payment_option"];
//    
//    [payFort callPayFortWithRequest:request currentViewController:self
//                            Success:^(NSDictionary *requestDic, NSDictionary *responeDic) {
//        NSLog(@"Success");
//        NSLog(@"requestDic=%@",requestDic);
//        NSLog(@"responeDic=%@",responeDic); }
//                           Canceled:^(NSDictionary *requestDic, NSDictionary *responeDic) { NSLog(@"Canceled");
//                               NSLog(@"requestDic=%@",requestDic); NSLog(@"responeDic=%@",responeDic);
//                           }
//                              Faild:^(NSDictionary *requestDic, NSDictionary *responeDic, NSString *message) {
//                                  NSLog(@"Faild"); NSLog(@"requestDic=%@",requestDic); NSLog(@"responeDic=%@",responeDic); NSLog(@"message=%@",message);
//                              }];
//    
//    [self.navigationController pushViewController:payFort animated:true];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonSetting:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:@"Settings" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        // Cancel button tappped.
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Edit Profile" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        EditProfileViewController * editProfileVC = [[EditProfileViewController alloc] init];
        
        [self.navigationController pushViewController:editProfileVC animated:true];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel Subscription" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [[UtilityClass sharedManger] UIApplicationOpenURLwithString:kURLCancelSubscription];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Contact Us" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        [[AppDelegate appDelegate] helpDesk];
        
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Terms and Conditions" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
         [[UtilityClass sharedManger] UIApplicationOpenURLwithString:kURLLegal];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Sign Out" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        // OK button tapped.
        
        [[Settings sharedManger] removeAllDefaults];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationSplashVideoFinished object:nil];
        
    }]];
    
    actionSheet.popoverPresentationController.sourceView = sender;
    actionSheet.popoverPresentationController.sourceRect = sender.bounds;
    
    // Present action sheet.
    [self presentViewController:actionSheet animated:YES completion:nil];
}

@end
