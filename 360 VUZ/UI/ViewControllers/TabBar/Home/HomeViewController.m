//
//  HomeViewController.m
//  360 VUZ
//
//  Created by Mosib on 7/6/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "HomeViewController.h"
#import "DetailVideoViewController.h"
#import "WhatsHotTableViewCell.h"
#import "CategoriesTableViewCell.h"
#import "Live360TableViewCell.h"
#import "TrendingHomeTableViewCell.h"
#import "SubscribeTableViewCell.h"
#import "WhatsHotCollectionViewCell.h"
#import "GlobalTrendingTableViewCell.h"
#import "VirtualTrendingTableViewCell.h"
#import "ServiceWhatsHot.h"
#import "WhatsHotDataModel.h"
#import "LiveVideoDataModel.h"
#import "SubscribeDataModel.h"
#import "TrendingVideosDataModel.h"
#import "CategoriesDataModel.h"
#import "GlobalTredingDataModel.h"
#import "VirtualTrendingDataModel.h"


@interface HomeViewController () <Orion1ViewDelegate,UITableViewDataSource, UITableViewDelegate>
{
    BOOL isLoaded;
    __weak HomeViewController* weakSelf;
}
@property (nonatomic) Orion1View* orionView;
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *dataArray;
@property (strong, nonatomic) NSDictionary *dataDictionary;

@end

@implementation HomeViewController

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)viewDidLoad
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    weakSelf = self;
    
    // Add Title Logo
    [self addTitleLogo];
    
    // Add Chat Icon to Navigation
    [self addChatIcon];
    
    // Initialize TableView
    [self setUpTableView];
    

    _dataArray = [[NSMutableArray alloc] initWithObjects:kHPKeyWhatsHot,kHPKeyLiveVideo,kHPKeyTrendingVirtualVideo,kHPKeyChannelList,kHPKeyTrendingVideo,kHPKeyYoutube,kHPKeyCategory, nil];
    
//    // Home Page API
//    [self homePageAPI];
    

}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) viewWillAppear:(BOOL)animated
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super viewWillAppear:animated];
    
     WhatsHotTableViewCell *cell = (WhatsHotTableViewCell *)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    if (cell.isDataLoaded)
    {          
          [cell.collectionView reloadData];
    }
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) viewWillDisappear:(BOOL)animated
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super viewWillDisappear:animated];
    
//    WhatsHotTableViewCell *cell = (WhatsHotTableViewCell *)[_tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
   
//    [cell stopTimer];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)didReceiveMemoryWarning
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - UITableView DataSource Methods -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    switch (indexPath.section)
    {
        case WhatsHot:
        {
            WhatsHotTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kRIDWhatsHotTableViewCell];
            
            if (cell == nil) {
                cell = [[WhatsHotTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:kRIDWhatsHotTableViewCell];
            
            }
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor = [UIColor blackColor];
            
            
            isLoaded = true;
            
            return cell;
            
            break;
        }
            
        case Live360:
        {
            Live360TableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kRIDLive360TableViewCell];
            
            if (cell == nil) {
                cell = [[Live360TableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:kRIDLive360TableViewCell];
                
            }
            
            cell.labelTitle.text = @"not to miss";
            cell.imageViewHeader.image = [UIImage imageNamed:@"360live"];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor = [UIColor whiteColor];
            
            return cell;
            
            break;
        }
            
        case Subscribe:
        {
            SubscribeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kRIDSubscribeTableViewCell];
            
            if (cell == nil) {
                cell = [[SubscribeTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:kRIDSubscribeTableViewCell];
                
              
                
            }
            
            cell.labelTitle.text = @"subscribe";
            cell.imageViewHeader.image = [UIImage imageNamed:@"360live"];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor = [UIColor whiteColor];
            
            
            return cell;
            
            break;
        }
            
        case Trending:
        {
            TrendingHomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kRIDTrendingHomeTableViewCell];
            
            if (cell == nil) {
                cell = [[TrendingHomeTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:kRIDTrendingHomeTableViewCell];
                
               
            }
            
            
            cell.labelTitle.text = @"trending 360 videos";
            cell.imageViewHeader.image = [UIImage imageNamed:@"hot-icon"];
            
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor = [UIColor whiteColor];
            
            return cell;
            
            break;
        }
            
        case Categories:
        {
            CategoriesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kRIDCategoriesTableViewCell];
            
            if (cell == nil) {
                cell = [[CategoriesTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kRIDCategoriesTableViewCell];
                
              
            }
            
            cell.labelTitle.text = @"360 categories";
            cell.imageViewHeader.image = [UIImage imageNamed:@"category-Selected"];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor = [UIColor whiteColor];
            
            
            return cell;
            
            break;
        }
            
        case GlobalTrending:
        {
            GlobalTrendingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kRIDGlobalHomeTableViewCell];
            
            if (cell == nil) {
                cell = [[GlobalTrendingTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:kRIDGlobalHomeTableViewCell];
                
            }
            
            
            cell.labelTitle.text = @"global trending";
            cell.imageViewHeader.image = [UIImage imageNamed:@"global_trending_icon"];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor = [UIColor whiteColor];
            
            return cell;
            
            break;
        }
            
        case VirtualToursTrending:
        {
            
            VirtualTrendingTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kRIDVirtualTrendingTableViewCell];
            
            if (cell == nil) {
                cell = [[VirtualTrendingTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:kRIDVirtualTrendingTableViewCell];
               
            }
            
            
            cell.labelTitle.text = @"trending virtual tours";
            cell.imageViewHeader.image = [UIImage imageNamed:@"trending-virtual-tours"];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.backgroundColor = [UIColor whiteColor];
            
            return cell;
            break;
        }
            
            default:
        {
            UITableViewCell *cell = [[UITableViewCell alloc] init];
            return cell;
        }
            
    }
    
}

///*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
//- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
///*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
//{
//    switch (indexPath.section)
//    {
//        case WhatsHot:
//        {
//            WhatsHotTableViewCell *cell = (WhatsHotTableViewCell *)[_tableView cellForRowAtIndexPath:indexPath];
//            
//            [cell stopTimer];
//            
//            break;
//        }
//            
//    }
//}
//
///*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
///*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
//{
//    switch (indexPath.section)
//    {
//        case WhatsHot:
//        {
//            WhatsHotTableViewCell *cell = (WhatsHotTableViewCell *)[_tableView cellForRowAtIndexPath:indexPath];
//            
//            [cell startTimer];
//            
//            break;
//        }
//            
//    }
//}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return 1;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return [_dataArray count];
}

#pragma mark - UITableView Delegate Methods -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    switch (indexPath.section)
    {
        case WhatsHot:
        {
            
            
            break;
        }
            
        case Live360:
        {
            
            break;
        }
            
        case Subscribe:
        {
            
            break;
        }
            
        case Trending:
        {
           
            
            break;
        }
            
        case Categories:
        {
           
            
            break;
        }
            
        case GlobalTrending:
        {
            
            break;
        }
            
        case VirtualToursTrending:
        {
            
           
            break;
        }
            
    }

}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    switch (indexPath.section)
    {
            case WhatsHot:
        {
            return RatioOfCellWhatsHot * [UtilityClass getDeviceBounds].size.height;
            break;
        }
            
        case Live360:
        {
            return 165;
            break;
        }
            
            case Subscribe:
        {
            return 235;
            break;
        }
            
            default:
        {
            return 165;
            break;
        }
            
    }
}

#pragma mark - Private Methods -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) setUpTableView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    CGFloat x = 0;
    CGFloat y = 0;
    CGFloat width = self.view.frame.size.width;
    CGFloat height = self.view.frame.size.height;
    CGRect tableFrame = CGRectMake(x, y, width, height);
    
    
    UITableView *tableView = [[UITableView alloc]initWithFrame:tableFrame style:UITableViewStylePlain];
    tableView.scrollEnabled = YES;
    tableView.userInteractionEnabled = YES;
    tableView.bounces = YES;
    [tableView setShowsHorizontalScrollIndicator:NO];
    [tableView setShowsVerticalScrollIndicator:NO];
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    
    tableView.contentInset = UIEdgeInsetsMake(0, 0., 90, 0);
    
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    if([tableView respondsToSelector:@selector(setCellLayoutMarginsFollowReadableWidth:)])
    {
        tableView.cellLayoutMarginsFollowReadableWidth = NO;
    }
    
    tableView.delegate = self;
    tableView.dataSource = self;
    
    self.tableView = tableView;
    
    [self.view addSubview:self.tableView];
}

@end
