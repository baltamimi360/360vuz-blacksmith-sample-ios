//
//  LiveHomeViewController.h
//  360 VUZ
//
//  Created by Mosib on 8/14/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LiveVideoDataModel.h"

@interface LiveHomeViewController : UIViewController

@property (nonatomic) LiveVideoDataModel *modal;

@end
