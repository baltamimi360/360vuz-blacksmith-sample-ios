//
//  LiveHomeViewController.m
//  360 VUZ
//
//  Created by Mosib on 8/14/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "LiveHomeViewController.h"
#import "UpComing360VUZ.h"


@interface LiveHomeViewController ()
{
    NSDate *liveEventDate;
    NSTimer *updateViewTimer;
}
@property (nonatomic) UpComing360VUZ *viewUpComing360;

@end

@implementation LiveHomeViewController

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)viewDidLoad
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Add Title Logo
    [self addTitleLogo];
    
    // Add Chat Icon to Navigation
    [self addChatIcon];
    
    // Set up Navigation Bar
    [self setUpNavigationBar];
    
    // UpComing 360 VUZ view
    [self setUpView];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) viewWillDisappear:(BOOL)animated
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super viewWillDisappear:animated];
    
    [self stopTimer];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)didReceiveMemoryWarning
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private Methods -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpNavigationBar
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"back-button"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(actionButtonBack:)];
    
    self.navigationItem.leftBarButtonItem = backButton;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonBack:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [self.navigationController popViewControllerAnimated:true];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    _viewUpComing360 = [[UpComing360VUZ alloc] initWithFrame:CGRectMake(0,0,[UtilityClass getDeviceBounds].size.width , [UtilityClass getDeviceBounds].size.height - 90)];
    
    __weak UpComing360VUZ *weakUpComing360VUZ = _viewUpComing360;
    
    [_viewUpComing360.imageMoviePoster setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_modal.image]]
                                             placeholderImage:[UtilityClass imageFromColor:[UIColor blackColor]]
                                                      success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                                          
                                                          weakUpComing360VUZ.imageMoviePoster.image = image;
                                                          
                                                      } failure:nil];
    _viewUpComing360.label360video.text = _modal.liveDescription;
    _viewUpComing360.labelTitle.text = _modal.title;
    
    liveEventDate =  [[UtilityClass sharedManger] dateFromString:_modal.datetime withDateFormat:kTimeFormatISO8601];
    
    _viewUpComing360.labelDate.text = [[UtilityClass sharedManger] stringFromDate:liveEventDate withDateFormat:kTimeFormatDayMonth];
    
    _viewUpComing360.labelTime.text = [[UtilityClass sharedManger] stringFromDate:liveEventDate withDateFormat:kTimeFormatHourSecsMeridian];
    
    _viewUpComing360.modal = _modal;
    _viewUpComing360.liveEventDate = liveEventDate;
    
    
    NSDate * now = [NSDate date];
    NSComparisonResult result = [now compare:liveEventDate];
    
    switch (result)
    {
        case NSOrderedAscending:
            
            [self startTimer];
            
            break;
            
        default:
            [_viewUpComing360.buttonLive setTitle:nil forState:UIControlStateNormal];
            [_viewUpComing360.buttonLive setImage:[UIImage imageNamed:@"360live"] forState:UIControlStateNormal];
            
            //             [self startTimer];
            
            break;
    }
    
    [self.view addSubview:_viewUpComing360];
    
    [_viewUpComing360 checkToShowAddToCalendar];

}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) startTimer
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    updateViewTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0 target: self
                                                        selector: @selector(callAfterEverySecond) userInfo: nil repeats: YES];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) stopTimer
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [updateViewTimer invalidate];
    updateViewTimer = nil;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) callAfterEverySecond
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
    NSDate * now = [NSDate date];
    NSComparisonResult result = [now compare:liveEventDate];
    
    switch (result)
    {
        case NSOrderedAscending:

            break;
            
        default:
           
            [self stopTimer];
            
            _viewUpComing360.viewTimeLeft.labelValueDays.text = @"00";
    
            _viewUpComing360.viewTimeLeft.labelValueHours.text = @"00";
            
            _viewUpComing360.viewTimeLeft.labelValueMinutes.text = @"00";
            
            _viewUpComing360.viewTimeLeft.labelValueSeconds.text = @"00";
            
            return;
            
            break;
    }
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond
                                                        fromDate:liveEventDate
                                                          toDate:[NSDate date]
                                                         options:0];
    
    _viewUpComing360.viewTimeLeft.labelValueDays.text = [NSString stringWithFormat:@"%ld",[components day]];
    
    _viewUpComing360.viewTimeLeft.labelValueHours.text = [NSString stringWithFormat:@"%ld",[components hour]];
    
    _viewUpComing360.viewTimeLeft.labelValueMinutes.text = [NSString stringWithFormat:@"%ld",[components minute]];
    
    _viewUpComing360.viewTimeLeft.labelValueSeconds.text = [NSString stringWithFormat:@"%ld",[components second]];
    
}

@end
