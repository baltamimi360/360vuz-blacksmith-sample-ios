//
//  VirtualHomeViewController.h
//  360 VUZ
//
//  Created by Mosib on 8/15/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VirtualTrendingDataModel.h"

@interface VirtualHomeViewController : UIViewController

@property (nonatomic) VirtualTrendingDataModel *dataModel;


@end
