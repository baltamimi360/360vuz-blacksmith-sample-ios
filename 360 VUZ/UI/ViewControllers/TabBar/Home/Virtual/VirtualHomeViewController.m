//
//  VirtualHomeViewController.m
//  360 VUZ
//
//  Created by Mosib on 8/15/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "VirtualHomeViewController.h"
#import "VirtualDetailTableViewCell.h"

@interface VirtualHomeViewController ()<UITableViewDataSource, UITableViewDelegate>
{
    NSArray *titlesArray;
    CGFloat heightofDescrption;
}
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) UIImageView *topImageView;

@end
@implementation VirtualHomeViewController

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)viewDidLoad
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    // Add Title Logo
    [self addTitleLogo];
    
    // Set up Navigation Bar
    [self setUpNavigationBar];
    
    // Initialize TableView
    [self setUpTableView];
    
    titlesArray = @[@"City",@"Category",@"Detail",@"Rate Video"];
    
     heightofDescrption = 44.0;
    
    // Set Up Orion Player
    [self setUpTopView];
    

}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonBack:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [self.navigationController popViewControllerAnimated:true];
}


#pragma mark - UITableView DataSource Methods -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
        VirtualDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kRIDVirtualDetailTableViewCell];
        
        if (cell == nil) {
            cell = [[VirtualDetailTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:kRIDVirtualDetailTableViewCell];
        }
        
        cell.labelTitle.text = [titlesArray objectAtIndex:indexPath.row];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
       [cell hideRatingView];
    
       switch (indexPath.row)
    {
            case 0:
        {
            cell.labelDescription.text = _dataModel.videoCity;
            
            break;
        }
            
        case 1:
        {
            
            cell.labelDescription.text = _dataModel.categoryName;
            
            break;
        }
            
        case 2:
        {
            
            cell.labelDescription.text = _dataModel.videoDescription;
            
            // Adjust Cell Frame
            [cell adjustFrame];
            
            heightofDescrption = cell.labelDescription.frame.size.height + 25 ;
            
            break;
        }
            
        case 3:
        {
            [cell showRatingView];
           
            break;
        }
            
            default:
        {
            break;
        }
    }
    
        return cell;
 
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
        return titlesArray.count;
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return 1;
}

#pragma mark - UITableView Delegate Methods -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if (indexPath.row == 3)
    {
        return 60;
    }
    
    else if (indexPath.row == 2)
    {
       return heightofDescrption;
    }
        return 44.0f;

}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
   
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 45)];
        
        // Label Spins
        UILabel *labeltTitle = [[UILabel alloc] init];
        labeltTitle.frame = CGRectMake(15, 0, tableView.frame.size.width - 30, 40);
        labeltTitle.textColor = UIColorFromRGB(0x13AEE5);
        labeltTitle.text = _dataModel.videoTitle;
        labeltTitle.font = [UIFont fontWithName:kFontSFUIDisplayMedium size:15];
        labeltTitle.numberOfLines = 2;
        
        [view addSubview:labeltTitle];
        
        view.backgroundColor = [UIColor whiteColor];
    
        return view;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if (section == 0)
    {
        return 45.0;
    }
    
    return 0.01;
}

#pragma mark - Private Methods -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpNavigationBar
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"back-button"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(actionButtonBack:)];
    
    self.navigationItem.leftBarButtonItem = backButton;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) setUpTableView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    CGFloat x = 0;
    CGFloat y = self.view.frame.size.height/2 - 64/2 - 94/2;
    CGFloat width = self.view.frame.size.width;
    CGFloat height = self.view.frame.size.height - y;
    CGRect tableFrame = CGRectMake(x, y, width, height);
    
    
    UITableView *tableView = [[UITableView alloc]initWithFrame:tableFrame style:UITableViewStyleGrouped];
    tableView.scrollEnabled = YES;
    tableView.userInteractionEnabled = YES;
    tableView.bounces = YES;
    [tableView setShowsHorizontalScrollIndicator:NO];
    [tableView setShowsVerticalScrollIndicator:NO];
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    tableView.backgroundColor = [UIColor lightGrayColor];
    
    tableView.contentInset = UIEdgeInsetsMake(0, 0, 94, 0);
    
    if([tableView respondsToSelector:@selector(setCellLayoutMarginsFollowReadableWidth:)])
    {
        tableView.cellLayoutMarginsFollowReadableWidth = NO;
    }
    
    tableView.delegate = self;
    tableView.dataSource = self;
    
    tableView.backgroundColor = [UIColor whiteColor];
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView = tableView;
    
    [self.view addSubview:_tableView];
    
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpTopView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    _topImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height/2 - 94/2 - 94/2)];
    
    _topImageView.backgroundColor = [UIColor blackColor];
    
    __weak VirtualHomeViewController *weakSelf = self;
    
    [_topImageView setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.dataModel.videoThumbnail]]
                                 placeholderImage:[UtilityClass imageFromColor:[UIColor blackColor]]
                                          success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                              
                                              weakSelf.topImageView.image = image;
                                             
                                              
                                          } failure:nil];
    
    [self.view addSubview:_topImageView];
    
   UIImageView *imageViewPlay = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
    imageViewPlay.image = [UIImage imageNamed:@"play-icon"];
    
    imageViewPlay.center = _topImageView.center;
    
    [self.view addSubview:imageViewPlay];
    
    UIButton * buttonVirtual = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonVirtual addTarget:self
                      action:@selector(actionButtonVirtual:)
            forControlEvents:UIControlEventTouchUpInside];
    buttonVirtual.frame = _topImageView.frame;
    
    [self.view addSubview:buttonVirtual];
    
}


#pragma - Button Actions -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) actionButtonVirtual:(UIButton *) button
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [[UtilityClass sharedManger] UIApplicationOpenURLwithString:[NSString stringWithFormat:@"%@", _dataModel.videoLink]];
}



@end
