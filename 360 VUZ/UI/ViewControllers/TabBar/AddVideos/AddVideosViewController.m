//
//  AddVideosViewController.m
//  360 VUZ
//
//  Created by Mosib on 7/6/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "AddVideosViewController.h"

@interface AddVideosViewController ()

@end

@implementation AddVideosViewController

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)viewDidLoad
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super viewDidLoad];
    
    // Set up UI
    [self setUpUI];
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)didReceiveMemoryWarning
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpUI
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self.view.backgroundColor = [UIColor whiteColor];
    
    CGRect deviceBounds = [UtilityClass getDeviceBounds];
    
    // Add Title Logo
    [self addTitleLogo];
    
    // Add Chat Icon
    [self addChatIcon];
    
    // Add Background View
    UIImageView *backgorundImage = [[UIImageView alloc] initWithFrame:self.view.bounds];
    backgorundImage.image = [UIImage imageNamed:@"AddVideosBackground"];
    
    // Add Button ImageView
    CGSize sizeOfimageViewButton = CGSizeMake(deviceBounds.size.width/2.5, deviceBounds.size.width/2.5);
    CGPoint pointOfImageViewButton = CGPointMake(deviceBounds.size.width/2 - sizeOfimageViewButton.width/2, deviceBounds.size.width/3);
    
    UIImageView *imageViewButton = [[UIImageView alloc] initWithFrame:CGRectMake(pointOfImageViewButton.x, pointOfImageViewButton.y, sizeOfimageViewButton.width, sizeOfimageViewButton.height)];
    imageViewButton.image = [UIImage imageNamed:@"bg_gallery"];
    
    UIButton * buttonAdd = [UIButton buttonWithType:UIButtonTypeCustom];
    [buttonAdd addTarget:self
                    action:@selector(actionButtonAdd:)
          forControlEvents:UIControlEventTouchUpInside];
    buttonAdd.frame = imageViewButton.frame;
    
    
    [self.view addSubview:backgorundImage];
    [self.view addSubview:imageViewButton];
    [self.view addSubview:buttonAdd];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) actionButtonAdd:(UIButton *) sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    NSLog(@"asdasd");
}

@end
