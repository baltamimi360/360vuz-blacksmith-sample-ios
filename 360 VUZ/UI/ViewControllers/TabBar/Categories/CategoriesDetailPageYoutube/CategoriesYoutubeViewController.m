//
//  CategoriesYoutubeViewController.m
//  360 VUZ
//
//  Created by Mosib on 8/16/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "CategoriesYoutubeViewController.h"
#import "CategoriesYoutubeTableViewCell.h"
#import "ServiceGlobalTredning.h"
#import "ContentDetailsGTDataModel.h"
#import "SnippetGTModel.h"
#import "ResourceIDGTSDataModel.h"
#import "ThumbnailsGTSDataModel.h"
#import "ThumbnailGTSDatModel.h"
#import "GlobalTredingDataModel.h"

@interface CategoriesYoutubeViewController ()<UITableViewDataSource, UITableViewDelegate>
{
    CGFloat heightofDescrption;
    NSString *pageToken;
    BOOL lastResultFetchedEmpty;
    __weak CategoriesYoutubeViewController * weakSelf;
}
@property (strong, nonatomic) UITableView *tableView;
@property (nonatomic) NSMutableArray *dataArray;

@end

@implementation CategoriesYoutubeViewController

static BOOL fetchInProgress;

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)viewDidLoad
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self initializeVariables];
        
    // Add Title Logo
    [self addTitleLogo];
    
    // Add Chat Icon to Navigation
    [self addChatIcon];
    
    // Set up Navigation Bar
    [self setUpNavigationBar];
    
    // Set Up View
    [self setUpView];
    
    // Initialize TableView
    [self setUpTableView];
    
    // Service API
    [self serviceGlobalTrendingAPI];
    
}


#pragma mark - API Methods -
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) serviceGlobalTrendingAPI
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    STARTBACKGROUNDTHREAD
    ServiceGlobalTredning *subscribe = [[ServiceGlobalTredning alloc]init];
    
    [subscribe executeRequest:@{@"next_page_token" : pageToken }
                      success:^(NSDictionary  *responseObject)
     {
         
         [weakSelf SuccessForHomePageAPI:responseObject];
         
     }
                      failure:^(NSError * error) {
                          
                          [weakSelf FailureForHomePageAPI:error];
                          
                          
                          
                      }];
    ENDTHREAD
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) SuccessForHomePageAPI:(NSDictionary *) responseObject
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
    if ([[[responseObject objectForKey:kkeyData] objectForKey:kkeyItems] count] == 0)
    {
        lastResultFetchedEmpty = true;
    }
    
    if (!lastResultFetchedEmpty)
    {
        STARTMAINTHREAD
        
        [_tableView beginUpdates];
        
        int resultsSize = (int)[self.dataArray count];
        
        pageToken = [[responseObject objectForKey:kkeyData] objectForKey:kkeNextPageToken];
        
        [self.dataArray addObjectsFromArray:[self  makeDataValuesForGlobalTrendingVideos:[[responseObject objectForKey:kkeyData] objectForKey:kkeyItems]]];
        
        NSMutableArray *arrayWithIndexPaths = [NSMutableArray array];
        
        for (int i = resultsSize; i < resultsSize + [[[responseObject objectForKey:kkeyData] objectForKey:kkeyItems] count]; i++)
            [arrayWithIndexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
        
        [self.tableView insertRowsAtIndexPaths:arrayWithIndexPaths withRowAnimation:UITableViewRowAnimationTop];
        
        [_tableView endUpdates];
        
        fetchInProgress = false;
        
        ENDTHREAD
        
    }
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) FailureForHomePageAPI:(NSError *)error
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    STARTMAINTHREAD
    
    fetchInProgress = false;
    
    ENDTHREAD
}


#pragma mark - Private Methods -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) initializeVariables
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    weakSelf = self;
    
    fetchInProgress = false;
    
    pageToken = @"";
    
    _dataArray = [[NSMutableArray alloc] init];
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if(self.tableView.contentOffset.y >= (self.tableView.contentSize.height - self.tableView.bounds.size.height)) {

        if (fetchInProgress)
            return;
        
        fetchInProgress = TRUE;
        
        // Service API
        [self serviceGlobalTrendingAPI];
    }
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSArray *) makeDataValuesForGlobalTrendingVideos:(NSArray *) dataArray
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    NSMutableArray *dataArrayWithDataModel = [[NSMutableArray alloc] init];
    
    for (NSDictionary * object in dataArray)
    {
        GlobalTredingDataModel *gobalTrendingVideosDataModel = [[GlobalTredingDataModel alloc] init];
        
        gobalTrendingVideosDataModel.kind = [object objectForKey:kGTVKeyID];
        gobalTrendingVideosDataModel.etag = [object objectForKey:kGTVKeyETag];
        gobalTrendingVideosDataModel.youtubeID = [object objectForKey:kGTVKeyVideoKind];
        
        NSDictionary *dicSnippets = [object objectForKey:kGTVKeySnippet];
        
        
        // Make Snippet Object
        SnippetGTModel *snippet = [[SnippetGTModel alloc] init];
        
        snippet.publishedAt = [dicSnippets objectForKey:kGTVKeyPublishedAt];
        snippet.channelId = [dicSnippets objectForKey:kGTVKeyChannedlID];
        snippet.title = [dicSnippets objectForKey:kGTVKeyTitle];
        snippet.videoDescription = [dicSnippets objectForKey:kGTVKeyDescription];
        snippet.channelTitle = [dicSnippets objectForKey:kGTVKeyChannelTitle];
        snippet.playlistId = [dicSnippets objectForKey:kGTVKeyPlayListID];
        snippet.position = [dicSnippets objectForKey:kGTVKeyPosition];
        
        NSDictionary *dicResourceId = [dicSnippets objectForKey:kGTVKeyResourceId];
        
        // Make Resource ID Object
        ResourceIDGTSDataModel *resourceId = [[ResourceIDGTSDataModel alloc] init];
        resourceId.kind = [dicResourceId objectForKey:kGTVKeyKind];
        resourceId.videoID = [dicResourceId objectForKey:kGTVKeyVideoID];
        
        NSDictionary *dicThumbnails = [dicSnippets objectForKey:kGTVKeyThumbnails];
        
        // Make Thumbnail Objects
        ThumbnailsGTSDataModel * thumbnails = [[ThumbnailsGTSDataModel alloc] init];
        
        NSDictionary *dicDefaultThumbnail = [dicThumbnails objectForKey:kGTVKeyDefault];
        
        // Make default Thumbnail Object
        ThumbnailGTSDatModel * defaultThumbnail = [[ThumbnailGTSDatModel alloc] init];
        defaultThumbnail.url = [dicDefaultThumbnail objectForKey:kGTVKeyURL];
        defaultThumbnail.width = [dicDefaultThumbnail objectForKey:kGTVKeyWidth];
        defaultThumbnail.height = [dicDefaultThumbnail objectForKey:kGTVKeyHeight];
        
        thumbnails.defaultThumbnail = defaultThumbnail;
        
        NSDictionary *dicmediumThumbnail = [dicThumbnails objectForKey:kGTVKeyMedium];
        
        // Make medium Thumbnail Object
        ThumbnailGTSDatModel * medium = [[ThumbnailGTSDatModel alloc] init];
        medium.url = [dicmediumThumbnail objectForKey:kGTVKeyURL];
        medium.width = [dicmediumThumbnail objectForKey:kGTVKeyWidth];
        medium.height = [dicmediumThumbnail objectForKey:kGTVKeyHeight];
        
        thumbnails.medium = medium;
        
        NSDictionary *dichighThumbnail = [dicThumbnails objectForKey:kGTVKeyHigh];
        
        // Make high Thumbnail Object
        ThumbnailGTSDatModel * high = [[ThumbnailGTSDatModel alloc] init];
        high.url = [dichighThumbnail objectForKey:kGTVKeyURL];
        high.width = [dichighThumbnail objectForKey:kGTVKeyWidth];
        high.height = [dichighThumbnail objectForKey:kGTVKeyHeight];
        
        thumbnails.high = high;
        
        NSDictionary *dicstandardThumbnail = [dicThumbnails objectForKey:kGTVKeyStandard];
        
        // Make standard Thumbnail Object
        ThumbnailGTSDatModel * standard = [[ThumbnailGTSDatModel alloc] init];
        standard.url = [dicstandardThumbnail objectForKey:kGTVKeyURL];
        standard.width = [dicstandardThumbnail objectForKey:kGTVKeyWidth];
        standard.height = [dicstandardThumbnail objectForKey:kGTVKeyHeight];
        
        thumbnails.standard = standard;
        
        NSDictionary *dicmaxresThumbnail = [dicThumbnails objectForKey:kGTVKeyMaxres];
        
        // Make maxres Thumbnail Object
        ThumbnailGTSDatModel * maxres = [[ThumbnailGTSDatModel alloc] init];
        maxres.url = [dicmaxresThumbnail objectForKey:kGTVKeyURL];
        maxres.width = [dicmaxresThumbnail objectForKey:kGTVKeyWidth];
        maxres.height = [dicmaxresThumbnail objectForKey:kGTVKeyHeight];
        thumbnails.maxres = maxres;
        
        snippet.thumbnails = thumbnails;
        snippet.resourceId = resourceId;
        
        NSDictionary *dicContentDetails = [object objectForKey:kGTVKeyContentDetails];
        
        // Make Content Details Object
        ContentDetailsGTDataModel * contentDetails = [[ContentDetailsGTDataModel alloc] init];
        contentDetails.videoID = [dicContentDetails objectForKey:kGTVKeyVideoID];
        contentDetails.videoPublishedAt = [dicContentDetails objectForKey:kGTVKeyVideoPublished];
        
        gobalTrendingVideosDataModel.snippet = snippet;
        gobalTrendingVideosDataModel.contentDetails = contentDetails;
        
        [dataArrayWithDataModel addObject:gobalTrendingVideosDataModel];
    }
    
    return [dataArrayWithDataModel copy];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)didReceiveMemoryWarning
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonBack:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [self.navigationController popViewControllerAnimated:true];
}

#pragma mark - UITableView DataSource Methods -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    CategoriesYoutubeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kRIDCategoriesYoutubeTableViewCell];
    
    if (cell == nil) {
        cell = [[CategoriesYoutubeTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:kRIDCategoriesYoutubeTableViewCell];
    }
    
    cell.dataModel = [self.dataArray objectAtIndex:indexPath.row];
    
    __weak CategoriesYoutubeTableViewCell *weakCell = cell;
    
    [cell.imageMoviePoster setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:cell.dataModel.snippet.thumbnails.medium.url]]
                                 placeholderImage:[UtilityClass imageFromColor:[UIColor blackColor]]
                                          success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                              
                                              weakCell.imageMoviePoster.image = image;
                                              [weakCell setNeedsLayout];
                                              
                                          } failure:nil];
    
    cell.labelTitle.text = cell.dataModel.snippet.title;
    
    return cell;
    
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return [_dataArray count];
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return 1;
}

#pragma mark - UITableView Delegate Methods -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    CategoriesYoutubeTableViewCell *cell = (CategoriesYoutubeTableViewCell *) [tableView cellForRowAtIndexPath:indexPath];
    
    [[UtilityClass sharedManger] UIApplicationOpenURLwithString:[NSString stringWithFormat:@"%@%@",kURLYoutube,cell.dataModel.snippet.resourceId.videoID]];
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return 120.0f;
}



#pragma mark - Private Methods -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) setUpTableView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    CGFloat x = 0;
    CGFloat y = 130;
    CGFloat width = self.view.frame.size.width;
    CGFloat height = self.view.frame.size.height - y;
    CGRect tableFrame = CGRectMake(x, y, width, height);
    
    
    UITableView *tableView = [[UITableView alloc]initWithFrame:tableFrame style:UITableViewStyleGrouped];
    tableView.scrollEnabled = YES;
    tableView.userInteractionEnabled = YES;
    tableView.bounces = YES;
    [tableView setShowsHorizontalScrollIndicator:NO];
    [tableView setShowsVerticalScrollIndicator:NO];
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    tableView.backgroundColor = [UIColor whiteColor];
    
    tableView.contentInset = UIEdgeInsetsMake(0, 0, 94, 0);
    
    if([tableView respondsToSelector:@selector(setCellLayoutMarginsFollowReadableWidth:)])
    {
        tableView.cellLayoutMarginsFollowReadableWidth = NO;
    }
    
    tableView.delegate = self;
    tableView.dataSource = self;
 
    self.tableView = tableView;
    
    
    [self.view addSubview:_tableView];
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    weakSelf = self;
    
    CGRect deviceBounds = [UtilityClass getDeviceBounds];
    
    UILabel * labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, deviceBounds.size.width, 20)];
    labelTitle.font = [UIFont fontWithName:kFontSFUIDisplayRegular size:18];
    labelTitle.textAlignment = NSTextAlignmentCenter;
    [labelTitle setTextColor:[UIColor colorWithRed:19/255.0 green:174/255.0 blue:229/255.0 alpha:1/1.0]];
    labelTitle.text = @"GLOBAL TRENDING";
    
    [self.view addSubview:labelTitle];
    
    UILabel * labelTrending = [[UILabel alloc] initWithFrame:CGRectMake(10, 50, deviceBounds.size.width - 20, 30)];
    labelTrending.font = [UIFont fontWithName:kFontSFUIDisplayBold size:25];
    labelTrending.text = @"Trending 360 Videos";
    
    [self.view addSubview:labelTrending];
    
    UILabel * labelHash = [[UILabel alloc] initWithFrame:CGRectMake(10, 80, deviceBounds.size.width - 20, 20)];
    labelHash.font = [UIFont fontWithName:kFontSFUIDisplayLight size:15];
    [labelHash setTextColor:[UIColor lightGrayColor]];
    labelHash.text = @"#360Video";
    
    [self.view addSubview:labelHash];
    
    UILabel * labelPopular = [[UILabel alloc] initWithFrame:CGRectMake(10, 100, deviceBounds.size.width - 20, 20)];
    labelPopular.font = [UIFont fontWithName:kFontSFUIDisplayLight size:15];
    [labelPopular setTextColor:[UIColor lightGrayColor]];;
    labelPopular.text = @"Today's most popular 360 videos";
    
    [self.view addSubview:labelPopular];
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpNavigationBar
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"back-button"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(actionButtonBack:)];
    
    self.navigationItem.leftBarButtonItem = backButton;
}


@end
