//
//  CategoriesDetailPageViewController.h
//  360 VUZ
//
//  Created by Mosib on 7/11/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoriesDetailPageViewController : UIViewController
@property (nonatomic) NSString *videoID;
@property (nonatomic) NSString *categoryTitle;

@end
