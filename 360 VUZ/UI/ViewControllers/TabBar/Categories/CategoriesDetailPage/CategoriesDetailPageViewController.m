//
//  CategoriesDetailPageViewController.m
//  360 VUZ
//
//  Created by Mosib on 7/11/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "CategoriesDetailPageViewController.h"
#import "VideoTableViewCell.h"
#import "DetailVideoViewController.h"
#import "CatrgoryDetailList.h"
#import "CategoryDetailDataModel.h"

@interface CategoriesDetailPageViewController ()<UITableViewDataSource, UITableViewDelegate>
{
    CGRect deviceBounds;
    int pageNumber;
    BOOL lastResultFetchedEmpty;
    __weak CategoriesDetailPageViewController * weakSelf;
}

@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *dataArray;

@end

@implementation CategoriesDetailPageViewController
static BOOL fetchInProgress;

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)viewDidLoad
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super viewDidLoad];
    
     //
     [self initializeVariables];
    
    // Add Title Logo
    [self addTitleLogo];
    
    // Set up Navigation Bar
    [self setUpNavigationBar];
    
    // Initialize TableView
    [self setUpTableView];
    
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0., 90, 0);
    
    // Service API
    [self serviceCategoryDetailList];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) viewWillAppear:(BOOL)animated
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super viewWillAppear:animated];
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonBack:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [self.navigationController popViewControllerAnimated:true];
}

#pragma mark - UITableView DataSource Methods -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    VideoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kRIDVideoTableViewCell];
    
    if (cell == nil) {
        cell = [[VideoTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:kRIDVideoTableViewCell];
        
    }
    
    cell.label360video.text = @"360° Video";

    cell.dataModel = [self.dataArray objectAtIndex:indexPath.row];
    
    __weak VideoTableViewCell *weakCell = cell;
    
    [cell.imageMoviePoster setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:cell.dataModel.videoThumbnail]]
                                 placeholderImage:[UtilityClass imageFromColor:[UIColor blackColor]]
                                          success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                              
                                              weakCell.imageMoviePoster.image = image;
                                              [weakCell setNeedsLayout];
                                              
                                          } failure:nil];
    
    cell.labelTitle.text = cell.dataModel.videoTitle;
    cell.labelSpins.text = cell.dataModel.likeCount;;
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
        return [_dataArray count];
}

#pragma mark - UITableView Delegate Methods -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    DetailVideoViewController * vc = [[DetailVideoViewController alloc] init];
    vc.CellType = WhatsHot;
    vc.modal = [self.dataArray objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:vc animated:true];
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return heightOfCellVideo;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 55)];

    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 25, tableView.frame.size.width, 22)];
    [label setFont:[UIFont fontWithName:kFontSFUIDisplayBold size:18]];
    label.textColor = [UIColor colorWithRed:74/255.0 green:74/255.0 blue:74/255.0 alpha:1/1.0];
    
    [label setText:_categoryTitle];
    [view addSubview:label];
    [view setBackgroundColor:[UIColor whiteColor]];
    return view;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return 55.0;
}

#pragma mark - Private Methods -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpNavigationBar
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"back-button"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(actionButtonBack:)];
    
    self.navigationItem.leftBarButtonItem = backButton;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) setUpTableView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    CGFloat x = 0;
    CGFloat y = 0;
    CGFloat width = self.view.frame.size.width;
    CGFloat height = self.view.frame.size.height;
    CGRect tableFrame = CGRectMake(x, y, width, height);
    

    UITableView *tableView = [[UITableView alloc]initWithFrame:tableFrame style:UITableViewStylePlain];
    tableView.scrollEnabled = YES;
    tableView.userInteractionEnabled = YES;
    tableView.bounces = YES;
    [tableView setShowsHorizontalScrollIndicator:NO];
    [tableView setShowsVerticalScrollIndicator:NO];
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
    
    tableView.contentInset = UIEdgeInsetsMake(0, 0, 64, 0);
    
    if([tableView respondsToSelector:@selector(setCellLayoutMarginsFollowReadableWidth:)])
    {
        tableView.cellLayoutMarginsFollowReadableWidth = NO;
    }
    
    tableView.delegate = self;
    tableView.dataSource = self;
    
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView = tableView;
    

    [self.view addSubview:self.tableView];
}

#pragma mark - API Methods -
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) serviceCategoryDetailList
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    STARTBACKGROUNDTHREAD
    CatrgoryDetailList *subscribe = [[CatrgoryDetailList alloc]init];
    
    [subscribe executeRequest:@{@"category_id" : _videoID ,@"limit" : @"5" , @"page_no": [@(pageNumber) stringValue]}
                      success:^(NSDictionary  *responseObject)
     {
         
         [weakSelf SuccessForHomePageAPI:responseObject];
         
     }
                      failure:^(NSError * error) {
                          
                          [weakSelf FailureForHomePageAPI:error];
                          
                          
                          
                      }];
    ENDTHREAD
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) SuccessForHomePageAPI:(NSDictionary *) responseObject
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
        
    if ([[responseObject objectForKey:@"data"] count] == 0)
    {
        lastResultFetchedEmpty = true;
    }
    
    else
    {
        pageNumber++;
    }
    

    
    if (!lastResultFetchedEmpty)
    {
        STARTMAINTHREAD
        
        [_tableView beginUpdates];
        
        int resultsSize = (int)[self.dataArray count];
        
        [self.dataArray addObjectsFromArray:[self  makeDataValuesForWhatsHot:[responseObject objectForKey:kkeyData]]];
        
        NSMutableArray *arrayWithIndexPaths = [NSMutableArray array];
        
        for (int i = resultsSize; i < resultsSize + [[responseObject objectForKey:kkeyData] count]; i++)
            [arrayWithIndexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
        
        [self.tableView insertRowsAtIndexPaths:arrayWithIndexPaths withRowAnimation:UITableViewRowAnimationTop];
        
        [_tableView endUpdates];
        
        fetchInProgress = false;
        
        ENDTHREAD
        
    }
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) FailureForHomePageAPI:(NSError *)error
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    STARTMAINTHREAD
    
    fetchInProgress = false;
    
    ENDTHREAD
}


#pragma mark - Private Methods -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) initializeVariables
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    weakSelf = self;
    
    fetchInProgress = false;
    
    pageNumber = 0;
    
    _dataArray = [[NSMutableArray alloc] init];
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if(self.tableView.contentOffset.y >= (self.tableView.contentSize.height - self.tableView.bounds.size.height)) {
        
        if (fetchInProgress)
            return;
        
        fetchInProgress = TRUE;
        
        // Service API
        [self serviceCategoryDetailList];
    }
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSArray *) makeDataValuesForWhatsHot:(NSArray *) dataArray
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    NSMutableArray *dataArrayWithDataModel = [[NSMutableArray alloc] init];
    
    for (NSDictionary * object in dataArray)
    {
        CategoryDetailDataModel *categoryDetailDataModel = [[CategoryDetailDataModel alloc] init];
        
        categoryDetailDataModel.videoID = [[object objectForKey:kWHKeyID] stringValue];
        categoryDetailDataModel.videoCity = [object objectForKey:kWHKeyVideoCity];
        categoryDetailDataModel.categoryID = [[object objectForKey:kWHKeyCategoryID] stringValue] ;
        categoryDetailDataModel.subCategoryID = [object objectForKey:kWHKeySubCategoryID];
        categoryDetailDataModel.videoTitle = [object objectForKey:kWHKeyVideoTitle];
        categoryDetailDataModel.videoDescription = [object objectForKey:kWHKeyVideoDescription];
        categoryDetailDataModel.websiteLink = [object objectForKey:kWHKeyWebsiteLink];
        categoryDetailDataModel.videoThumbnail = [object objectForKey:kWHKeyVideoThumbnail];
        categoryDetailDataModel.videoLink = [object objectForKey:kWHKeyVideoLink];
        categoryDetailDataModel.webVideoLink = [object objectForKey:kWHKeyWebVideoLink];
        categoryDetailDataModel.status = [object objectForKey:kWHKeyStatus];
        categoryDetailDataModel.createdDate = [object objectForKey:kWHKeyCreatedDate];
        categoryDetailDataModel.metaData = [object objectForKey:kWHKeyMetaData];
        categoryDetailDataModel.whatsHot = [object objectForKey:kWHKeyWhatsHot];
        categoryDetailDataModel.popular = [object objectForKey:kWHKeyPopular];
        categoryDetailDataModel.videotype = [object objectForKey:kWHKeyVideoType];
        categoryDetailDataModel.webVideoType = [object objectForKey:kWHKeyWebVideoType];
        categoryDetailDataModel.userID = [object objectForKey:kWHKeyUserID];
        categoryDetailDataModel.videoFeature = [object objectForKey:kWHKeyVideoFeature];
        categoryDetailDataModel.panoramaImage = [object objectForKey:kWHKeyPanoramImage];
        categoryDetailDataModel.notificationFlag = [object objectForKey:kWHKeyNotificationFlag];
        categoryDetailDataModel.isImage = [object objectForKey:kWHKeyIsImage];
        categoryDetailDataModel.rating = [object objectForKey:kWHKeyRating];
        categoryDetailDataModel.categoryName = [object objectForKey:kWHKeyCategoryID];
        categoryDetailDataModel.viewCount = [[object objectForKey:kWHKeyViewCount] stringValue];
        categoryDetailDataModel.likeCount = [[object objectForKey:kWHKeyLikeCount] stringValue];
        
        [dataArrayWithDataModel addObject:categoryDetailDataModel];
    }
    
    return [dataArrayWithDataModel copy];
}


@end
