//
//  CategoriesViewController.m
//  360 VUZ
//
//  Created by Mosib on 7/6/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "CategoriesViewController.h"
#import "CategoriesCollectionViewCell.h"
#import "CategoriesHeaderCollectionView.h"
#import "CategoriesDetailPageViewController.h"
#import "ServiceCategories.h"
#import "CategoriesYoutubeViewController.h"

@interface CategoriesViewController () <UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
{
    CGRect deviceBounds;
    NSArray *tableDataName;
    NSArray *tableDataImage;
    __weak CategoriesViewController * weakSelf;
}

@property (nonatomic, retain) UICollectionView *collectionView;
@property (nonatomic) NSArray *dataArray;

@end

@implementation CategoriesViewController

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)viewDidLoad
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    weakSelf = self;
    
    // Add Title Logo
    [self addTitleLogo];
    
    // Add Chat Icon to Navigation
    [self addChatIcon];
    
    // Add CollectionView to View
    [self setUpCollectionView];
    
    // Service API
    [self serviceCategoriesAPI];
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)didReceiveMemoryWarning
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Private Methods -


#pragma mark - API Methods -
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) serviceCategoriesAPI
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    STARTBACKGROUNDTHREAD
    
    ServiceCategories *categories = [[ServiceCategories alloc]init];
    
    [categories executeRequest:nil
                       success:^(NSDictionary  *responseObject)
     {
         
         
         [weakSelf SuccessForHomePageAPI:responseObject];
         
     }
                       failure:^(NSError * error) {
                           
                           [weakSelf FailureForHomePageAPI:error];
 
                       }];
    ENDTHREAD
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) SuccessForHomePageAPI:(NSDictionary *) responseObject
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
    self.dataArray = [self  makeDataValuesForCategory:[responseObject objectForKey:kkeyData]];
    
    STARTMAINTHREAD
    
    // Reload Data
    [_collectionView reloadData];
    
    ENDTHREAD
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) FailureForHomePageAPI:(NSError *)error
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    STARTMAINTHREAD
    
    //    [[UtilityClass sharedManger] alertErrorWithMessage:@"Something went wrong Please try again later"];
    
    ENDTHREAD
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpCollectionView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    deviceBounds = [UtilityClass getDeviceBounds];
    
    // Initialize Collection View
    _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, deviceBounds.size.width, deviceBounds.size.height) collectionViewLayout:[[UICollectionViewFlowLayout alloc] init]];
    
    _collectionView.contentInset = UIEdgeInsetsMake(0, 0., 90, 0);
    
    // Set DataSource and Delegate
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
    
    // Register Classes for Usage
    [_collectionView registerClass:[CategoriesCollectionViewCell class] forCellWithReuseIdentifier:kRIDCategoriesCollectionViewCell];
    [_collectionView registerClass:[CategoriesHeaderCollectionView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:kRIDCategoriesHeaderCollectionView];
    
    // Add CollectionView to View
    _collectionView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_collectionView];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSArray *) makeDataValuesForCategory:(NSArray *) dataArray
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    NSMutableArray *dataArrayWithDataModel = [[NSMutableArray alloc] init];
    
    for (NSDictionary * object in dataArray)
    {
        CategoriesDataModel *categoriesDataModel = [[CategoriesDataModel alloc] init];
        
        categoriesDataModel.videoID = [object objectForKey:kCKeyID];
        categoriesDataModel.image = [object objectForKey:kCKeyImage];
        categoriesDataModel.categoryName = [object objectForKey:kCKeyCategoryName];
        
        [dataArrayWithDataModel addObject:categoriesDataModel];
    }
    
    return [dataArrayWithDataModel copy];
}

#pragma mark - UICollectionViewDataSource -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    CategoriesCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kRIDCategoriesCollectionViewCell forIndexPath:indexPath];
    
    cell.dataModel = [self.dataArray objectAtIndex:indexPath.row];
    
    __weak CategoriesCollectionViewCell *weakCell = cell;
    
    [cell.imageViewBackground setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:cell.dataModel.image]]
                                    placeholderImage:[UtilityClass imageFromColor:[UIColor blackColor]]
                                             success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                                 
                                                 weakCell.imageViewBackground.image = image;
                                                 [weakCell setNeedsLayout];
                                                 
                                             } failure:nil];
    
    
    cell.labelTitle.text = cell.dataModel.categoryName;
    cell.labelSpins.text = @"";
    
    return cell;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    UICollectionReusableView *reusableview = nil;
    
    if (kind == UICollectionElementKindSectionHeader) {
        CategoriesHeaderCollectionView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"CategoriesHeaderCollectionView" forIndexPath:indexPath];
        
        reusableview = headerView;
    }
    
    return reusableview;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return [_dataArray count];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return 1;
}

#pragma mark - UICollectionViewDelegateFlowLayout - 

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return UIEdgeInsetsMake(5,10,0,10);  // top, left, bottom, right
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return CGSizeMake(deviceBounds.size.width/2 - 15, 100);
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return CGSizeMake(0, 50);
}

#pragma mark - UICollectionViewDelegate-
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    CategoriesCollectionViewCell *cell = (CategoriesCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    
    if ([cell.dataModel.videoID intValue]  == 33)
    {
        CategoriesYoutubeViewController *vc = [[CategoriesYoutubeViewController alloc] init];
        
        [self.navigationController pushViewController:vc animated:true];
    }
    else
    {
        // Normal Controller
        CategoriesDetailPageViewController * vc = [[CategoriesDetailPageViewController alloc] init];
        vc.videoID = [cell.dataModel.videoID stringValue];
        vc.categoryTitle = cell.labelTitle.text;
        [self.navigationController pushViewController:vc animated:true];
    }
}


@end
