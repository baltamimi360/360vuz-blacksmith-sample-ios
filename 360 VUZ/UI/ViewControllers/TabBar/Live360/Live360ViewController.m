//
//  Live360ViewController.m
//  360 VUZ
//
//  Created by Mosib on 7/6/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "Live360ViewController.h"

#import "EmailSubscriptionView.h"
#import "UpComing360VUZ.h"
#import "TopBarLive360View.h"
#import "Past360VUZView.h"
#import "IQKeyboardReturnKeyHandler.h"
#import "IQUIView+IQKeyboardToolbar.h"
#import "UpcomingLiveEventsService.h"

@interface Live360ViewController () <UIScrollViewDelegate>
{
    TopBarLive360View *topView;
    NSDictionary *dataDictionary;
    EmailSubscriptionView *emailSubscriptionView;
    UpComing360VUZ *viewUpComing360;
}

@property (nonatomic, retain) UIScrollView *scrollView;

@end

@implementation Live360ViewController

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)viewDidLoad
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super viewDidLoad];
    
    // Set Up UI
    [self setUpUI];
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpUI
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    self.view.backgroundColor = [UIColor whiteColor];
    
    // Add Title Logo
    [self addTitleLogo];
    
    // Add Chat Icon to Navigation
    [self addChatIcon];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    CGRect scrollViewFrame = CGRectMake(0, heightOfNavigationBar, [UtilityClass getDeviceBounds].size.width, [UtilityClass getDeviceBounds].size.height - heightOfNavigationBar - heightOfTabBar);
    self.scrollView = [[UIScrollView alloc] initWithFrame:scrollViewFrame] ;
    self.scrollView.delegate = self;
    [self.view addSubview:self.scrollView];
    CGSize scrollViewContentSize = CGSizeMake([UtilityClass getDeviceBounds].size.width * 2, [UtilityClass getDeviceBounds].size.height - heightOfNavigationBar - heightOfTabBar);
    [self.scrollView setContentSize:scrollViewContentSize];
    
    // Top Bar Page Control
    topView = [[TopBarLive360View alloc] initWithFrame:CGRectMake(0, heightOfNavigationBar, [UtilityClass getDeviceBounds].size.width, 50)];
    
    __weak Live360ViewController *weakSelf = self;
    topView.pageControlBlock = ^(int page) {
        
        [weakSelf scrollToPage:page];
    };
    [self.view addSubview:topView];
    
    // Past 360 View 
    Past360VUZView * viewPast360 = [[Past360VUZView alloc] initWithFrame:CGRectMake([UtilityClass getDeviceBounds].size.width,50,[UtilityClass getDeviceBounds].size.width , [UtilityClass getDeviceBounds].size.height - 94)];
    
    [self.scrollView addSubview:viewPast360];
    
    [self.scrollView setPagingEnabled:YES];
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.bounces = false;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) viewWillAppear:(BOOL)animated
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
        [self  serviceCategoriesAPI];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)didReceiveMemoryWarning
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    CGFloat pageWidth = self.scrollView.frame.size.width;
    float fractionalPage = self.scrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    [topView scrollByMainView:(int)page];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) scrollToPage:(int) page
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    STARTMAINTHREAD
    
    [UIView animateWithDuration:0.25
                          delay:0.0
                        options: UIViewAnimationOptionLayoutSubviews
                     animations:^{
                         _scrollView.contentOffset = CGPointMake(_scrollView.frame.size.width * page, _scrollView.contentOffset.y);
                     }completion:nil];
    
    ENDTHREAD
}

#pragma mark - API Methods -
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) serviceCategoriesAPI
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    __weak Live360ViewController *weakSelf = self;
    
    STARTBACKGROUNDTHREAD
    UpcomingLiveEventsService *live360 = [[UpcomingLiveEventsService alloc]init];
    
    [live360 executeRequest:@{@"limit" : @"5" , @"page_no": @"0"}
                    success:^(NSDictionary  *responseObject)
     {
         
         [weakSelf SuccessForHomePageAPI:responseObject];
         
     }
                    failure:^(NSError * error) {
                        
                        [weakSelf FailureForHomePageAPI:error];
                        
                        
                        
                    }];
    ENDTHREAD
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) SuccessForHomePageAPI:(NSDictionary *) responseObject
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
    STARTMAINTHREAD
    
    if ([[responseObject objectForKey:@"data"] count] == 0)
    {
        if (![emailSubscriptionView isDescendantOfView:self.view])
        {
            emailSubscriptionView = [[EmailSubscriptionView alloc] initWithFrame:CGRectMake(0,50,[UtilityClass getDeviceBounds].size.width , [UtilityClass getDeviceBounds].size.height - 150)];
            
            [self.scrollView addSubview:emailSubscriptionView];
        }
    }
    
    else
    {
        [emailSubscriptionView removeFromSuperview];
        
        dataDictionary = [[responseObject objectForKey:@"data"] objectAtIndex:0];
        
        // UpComing 360 VUZ view
        
        if (viewUpComing360 == nil)
        {
        viewUpComing360 = [[UpComing360VUZ alloc] initWithFrame:CGRectMake(0,50,[UtilityClass getDeviceBounds].size.width , [UtilityClass getDeviceBounds].size.height - 94)];
        }
        
        
        __weak UpComing360VUZ *weakSelf = viewUpComing360;
        
        [viewUpComing360.imageMoviePoster setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[dataDictionary objectForKey:@"image"]]]
                                     placeholderImage:[UtilityClass imageFromColor:[UIColor blackColor]]
                                              success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                                  
                                                  weakSelf.imageMoviePoster.image = image;
                                                  [weakSelf setNeedsLayout];
                                                  
                                              } failure:nil];
        
        viewUpComing360.label360video.text = @"360° Video";
        viewUpComing360.labelTitle.text = [dataDictionary objectForKey:@"title"];
        
        if (![viewUpComing360 isDescendantOfView:self.view])
        {
          [self.scrollView addSubview:viewUpComing360];
        }
    }
    
    ENDTHREAD
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) FailureForHomePageAPI:(NSError *)error
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    STARTMAINTHREAD
    
    if (![emailSubscriptionView isDescendantOfView:self.view] && ![viewUpComing360 isDescendantOfView:self.view])
    {
        emailSubscriptionView = [[EmailSubscriptionView alloc] initWithFrame:CGRectMake(0,50,[UtilityClass getDeviceBounds].size.width , [UtilityClass getDeviceBounds].size.height - 150)];
        
        [self.scrollView addSubview:emailSubscriptionView];
    }

    ENDTHREAD
}


@end
