//
//  DetailVideoViewController.m
//  360 VUZ
//
//  Created by Mosib on 8/3/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import "DetailVideoViewController.h"
#import "SpinCountDetailVideoTableViewCell.h"
#import "SocialShareDetailVideoTableViewCell.h"
#import "RecommendedVideoDetailTableViewCell.h"
#import "VideoDescriptionVideoDetailTableViewCell.h"
#import "WhatsHotDataModel.h"
#import "TrendingVideosDataModel.h"
#import "OrionRecordedViewController.h"
#import "RecommendedVideosService.h"


#define MARGIN                     10
#define BUTTON_SIZE                40

@interface DetailVideoViewController ()<UITableViewDataSource, UITableViewDelegate,Orion1ViewDelegate, UIGestureRecognizerDelegate>
{
    __weak DetailVideoViewController * weakSelf;
    UIView *viewSeperator;
    CGFloat heightofDescrption;
    NSString *videoURLString;
    NSString * spinsCount;
    NSString * videoID;
    NSString *titleString;
    BOOL tableDescriptionHidden;
    BOOL _isSeeking;
    BOOL _controlsHidden;
}

@property (strong, nonatomic) UITableView *tableView;
@property (nonatomic) UILabel * labelSpins;
@property (nonatomic) UILabel *labelTitle;
@property (nonatomic) Orion1View* orionView;
@property (nonatomic) UISlider  *timeSlider;
@property (nonatomic) UIButton *playPauseButton;
@property (nonatomic) UIButton *muteButton;
@property (nonatomic) UILabel *timeLeftLabel;
@property (nonatomic) UILabel *timeTotalLabel;
@property (nonatomic) UIView *bottomBar;
@property (nonatomic) NSMutableArray *dataArray;
@property (nonatomic) UIActivityIndicatorView *bufferIndicator;

@end

@implementation DetailVideoViewController

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)viewDidLoad
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    heightofDescrption = 44.0;
    
    weakSelf = self;
    
    _dataArray = [[NSMutableArray alloc] init];
    
    // Add Title Logo
    [self addTitleLogo];
    
    // Set up Navigation Bar
    [self setUpNavigationBar];
    
    // Set Up Orion Player
    [self setUpOrionPlayer];
  
    // Initialize TableView
    [self setUpTableView];
    
    // Set Up Controller Details
    [self setUpDetails];
    
    // Prepare Orion Controls Overlay
    [self prepareView];
    
    // Recommended Videos Service API
    [self serviceCategoriesAPI];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) viewDidAppear:(BOOL)animated
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [super viewDidAppear:animated];
    
    [self loadOrionVideo];
    
}

#pragma mark - API Methods -
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) serviceCategoriesAPI
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    STARTBACKGROUNDTHREAD
    
    RecommendedVideosService *recommaded = [[RecommendedVideosService alloc]init];
    
    [recommaded executeRequest:@{@"category_id":videoID}
                       success:^(NSDictionary  *responseObject)
     {
         
         
         [weakSelf SuccessForHomePageAPI:responseObject];
         
     }
                       failure:^(NSError * error) {
                           
                           [weakSelf FailureForHomePageAPI:error];
                           
                           
                           
                       }];
    ENDTHREAD
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) SuccessForHomePageAPI:(NSDictionary *) responseObject
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
   [self.dataArray removeAllObjects];

   [self.dataArray addObjectsFromArray:[self  makeDataValuesForWhatsHot:[responseObject objectForKey:kkeyData]]];
    
    STARTMAINTHREAD
    
    // Reload Data
    [_tableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationTop];
    
    ENDTHREAD
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) FailureForHomePageAPI:(NSError *)error
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    STARTMAINTHREAD
    
    //    [[UtilityClass sharedManger] alertErrorWithMessage:@"Something went wrong Please try again later"];
    
    ENDTHREAD
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) actionButtonBack:(UIButton*)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [self.navigationController popViewControllerAnimated:true];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpDetails
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    switch (_CellType) {
        case WhatsHot:
        {
            WhatsHotDataModel *dataModel = (WhatsHotDataModel *) _modal;
            
            spinsCount = dataModel.viewCount;
            titleString = dataModel.videoTitle;
            videoURLString = dataModel.videoLink;
            videoID = dataModel.categoryID;
            break;
        }
            
        case Trending :
        {
            TrendingVideosDataModel *dataModel = (TrendingVideosDataModel *) _modal;
            
            spinsCount = dataModel.viewCount;
            titleString = dataModel.videoTitle;
            videoURLString = dataModel.videoLink;
            videoID = dataModel.categoryID;
            
            break;
        }
            
        default:
            break;
    }
}

#pragma mark - UITableView DataSource Methods -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if (indexPath.section == 0)
    {
        VideoDescriptionVideoDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kRIDVideoDescriptionVideoDetailTableViewCell];
        
        if (cell == nil) {
            cell = [[VideoDescriptionVideoDetailTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:kRIDVideoDescriptionVideoDetailTableViewCell];
        }
        
        // Set Description
        [self cellDescriptionValues:cell];
      
        // Adjust Cell Frame
        [cell adjustFrame];
        
        heightofDescrption = cell.labelShare.frame.size.height ;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
   
        return cell;
    }
    
    else
    {
    if (indexPath.row == 0)
    {
    
    SocialShareDetailVideoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kRIDSocialShareDetailVideoTableViewCell];
    
    if (cell == nil) {
        cell = [[SocialShareDetailVideoTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:kRIDSocialShareDetailVideoTableViewCell];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
        
    }
    
    else
    {
        RecommendedVideoDetailTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kRIDRecommendedVideoDetailTableViewCell];
        
        if (cell == nil) {
            cell = [[RecommendedVideoDetailTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:kRIDRecommendedVideoDetailTableViewCell];
        }
        
        cell.dataModel = [self.dataArray objectAtIndex:indexPath.row];
        
        __weak RecommendedVideoDetailTableViewCell *weakCell = cell;
        
        [cell.imageMoviePoster setImageWithURLRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:cell.dataModel.videoThumbnail]]
                                     placeholderImage:[UtilityClass imageFromColor:[UIColor blackColor]]
                                              success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                                  
                                                  weakCell.imageMoviePoster.image = image;
                                                  [weakCell setNeedsLayout];
                                                  
                                              } failure:nil];
        
        cell.labelTitle.text = cell.dataModel.videoTitle;
        cell.labelDescription.text = cell.dataModel.websiteLink;;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
    }
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if (section == 0)
    {
        if (!tableDescriptionHidden)
        {
             return 0;
        }
        else
        {
             return 1;
        }
       
    }
    else
    {
         return [_dataArray count];
    }
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    return 2;
}

#pragma mark - UITableView Delegate Methods -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if (indexPath.section == 1)
    {
        RecommendedVideoDetailTableViewCell *cell = (RecommendedVideoDetailTableViewCell *)[self.tableView cellForRowAtIndexPath:indexPath];
        
        _modal = cell.dataModel;
        
        spinsCount = cell.dataModel.viewCount;
        titleString = cell.dataModel.videoTitle;
        videoURLString = cell.dataModel.videoLink;
        videoID = cell.dataModel.categoryID;
        
        NSURL *videoUrl =[[UtilityClass sharedManger] getCDNURL:[NSURL URLWithString:videoURLString]];
        
        [_orionView initVideoWithUrl:videoUrl previewImageUrl:nil licenseFileUrl:[[UtilityClass sharedManger] orionLicenseURL]];

        // Recommended Videos Service API
        [self serviceCategoriesAPI];
        
    }
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if (indexPath.section == 0)
    {
        if (!tableDescriptionHidden)
        {
            return 0;
        }
        else
        {
            return heightofDescrption + 15;
        }
    }
    
    else
    {
        if (indexPath.row == 0)
        {
            return 90;
        }
        
        return 110;
    }
    

}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if (section == 0)
    {
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 55)];
    
    CGRect  deviceBounds = [UtilityClass getDeviceBounds];
    
    // ImageView Spin
    UIImageView *imageSpin = [[UIImageView alloc] initWithFrame:CGRectMake(15, 7, 16, 15)];
    imageSpin.image = [UIImage imageNamed:@"spin-icon"];
    [view addSubview:imageSpin];
    
    // Label Spins
    _labelSpins = [[UILabel alloc] init];
    _labelSpins.frame = CGRectMake(imageSpin.frame.origin.x + imageSpin.frame.size.width + 5, 7, 50, 20);
    _labelSpins.textColor = UIColorFromRGB(0x13AEE5);
    _labelSpins.text = spinsCount;
    
    // Align Center
    CGPoint center = _labelSpins.center;
    center.y = imageSpin.center.y;
    _labelSpins.center = center;
    
    _labelSpins.font = [UIFont fontWithName:kFontSFUIDisplayRegular size:14];
    _labelSpins.backgroundColor = [UIColor clearColor];
    [view addSubview:_labelSpins];
    
    // ImageView Spin
    viewSeperator = [[UIView alloc] initWithFrame:CGRectMake(_labelSpins.frame.origin.x + _labelSpins.frame.size.width + 5, center.y - 0.5, deviceBounds.size.width - (_labelSpins.frame.origin.x + _labelSpins.frame.size.width + 10), 0.5)];
    viewSeperator.backgroundColor = [UIColor lightGrayColor];
    
    [view addSubview:viewSeperator];
        
        // Label Title
    _labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, _labelSpins.frame.origin.y + _labelSpins.frame.size.height + 5, deviceBounds.size.width - 40, 25)];
        _labelTitle.font = [UIFont fontWithName:kFontSFUIDisplayBold size:20];
        _labelTitle.textColor = UIColorFromRGB(0x4A4A4A);
        _labelTitle.text = titleString;
        
    [view addSubview:_labelTitle];
        
        // Button DropDown
        UIButton * buttonDropDown = [UIButton buttonWithType:UIButtonTypeCustom];
        [buttonDropDown addTarget:self
                        action:@selector(actionButtonDropDown:)
              forControlEvents:UIControlEventTouchUpInside];
        buttonDropDown.frame = CGRectMake(0, _labelTitle.frame.origin.y, deviceBounds.size.width , 50);
       
        [view addSubview:buttonDropDown];

        
    [self adjustFrameForSperator];
        
        view.backgroundColor = [UIColor whiteColor];
    
    return view;
    }
    
    return  nil;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if (section == 0)
    {
          return 60.0;
    }
    
    return 0.01;
}

#pragma mark - Private Methods -

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpNavigationBar
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"back-button"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(actionButtonBack:)];
    
    self.navigationItem.leftBarButtonItem = backButton;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) setUpTableView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    CGFloat x = 0;
    CGFloat y = _orionView.frame.size.height;
    CGFloat width = self.view.frame.size.width;
    CGFloat height = self.view.frame.size.height - y;
    CGRect tableFrame = CGRectMake(x, y, width, height);
    
    
    UITableView *tableView = [[UITableView alloc]initWithFrame:tableFrame style:UITableViewStylePlain];
    tableView.scrollEnabled = YES;
    tableView.userInteractionEnabled = YES;
    tableView.bounces = YES;
    [tableView setShowsHorizontalScrollIndicator:NO];
    [tableView setShowsVerticalScrollIndicator:NO];
    [tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    tableView.backgroundColor = [UIColor lightGrayColor];
    
    tableView.contentInset = UIEdgeInsetsMake(0, 0, 94, 0);
    
    if([tableView respondsToSelector:@selector(setCellLayoutMarginsFollowReadableWidth:)])
    {
        tableView.cellLayoutMarginsFollowReadableWidth = NO;
    }
    
    tableView.delegate = self;
    tableView.dataSource = self;
    
    tableView.backgroundColor = [UIColor whiteColor];

    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView = tableView;
    
    [self.view addSubview:_tableView];
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) adjustFrameForSperator
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    CGRect  deviceBounds = [UtilityClass getDeviceBounds];
    
    CGRect labelFrame = _labelSpins.frame;
    
    labelFrame.size.width = _labelSpins.intrinsicContentSize.width;
    
    _labelSpins.frame =  labelFrame;
    
    CGRect viewSeperatorFrame =  viewSeperator.frame;
    
    viewSeperatorFrame.origin.x = _labelSpins.frame.origin.x + _labelSpins.frame.size.width + 5,
    viewSeperatorFrame.size.width = deviceBounds.size.width - (_labelSpins.frame.origin.x + _labelSpins.frame.size.width + 10);
    
    viewSeperator.frame =  viewSeperatorFrame;
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) actionButtonDropDown:(UIButton *) button
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if (!tableDescriptionHidden)
    {
        tableDescriptionHidden = !tableDescriptionHidden;
    [_tableView beginUpdates];
    [_tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
    [_tableView endUpdates];
    }
    
    else
    {
        tableDescriptionHidden = !tableDescriptionHidden;
        [_tableView beginUpdates];
        [_tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
        [_tableView endUpdates];
    }
    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) setUpOrionPlayer
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    UIView *backGroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height/2)];
    
    backGroundView.backgroundColor = [UIColor blackColor];
    
    [self.view addSubview:backGroundView];
    
    _orionView = [[Orion1View alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height/2 )];
    _orionView.delegate = self;
    _orionView.overrideSilentSwitch = YES;
    
    [self.view addSubview:_orionView];
    
    _bufferIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    _bufferIndicator.center = CGPointMake(_orionView.frame.size.width/2, _orionView.frame.size.height/2);
    [self.view addSubview:_bufferIndicator];
    
    [_bufferIndicator startAnimating];

    
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) loadOrionVideo
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    NSURL *videoUrl =[[UtilityClass sharedManger] getCDNURL:[NSURL URLWithString:videoURLString]];
    
    [_orionView initVideoWithUrl:videoUrl previewImageUrl:nil licenseFileUrl:[[UtilityClass sharedManger] orionLicenseURL]];
}


#pragma - Orino1View delegate functions

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)orion1ViewReadyToPlayVideo:(Orion1View*)orion1View
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [orion1View play:0.0f];
    [_timeSlider setMaximumValue:_orionView.totalDuration];
    [self showHideControlBars:YES];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)orion1ViewDidUpdateProgress:(Orion1View*)orion1View currentTime:(CGFloat)currentTime availableTime:(CGFloat)availableTime totalDuration:(CGFloat)totalDuration
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if (currentTime > 1.0 && [_bufferIndicator isAnimating]) {
        [_bufferIndicator stopAnimating];
    }
    
    if(_timeSlider.maximumValue != totalDuration)
    {
        [_timeSlider setMaximumValue:totalDuration];
    }
    
    if (!_isSeeking)
    {
        _timeSlider.value = currentTime;
        [self updateTimeLabel:(int)currentTime];
    }
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)orion1ViewDidChangeBufferingStatus:(Orion1View*)orion1View
                                 buffering:(BOOL)buffering
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if (buffering && ![_bufferIndicator isAnimating])
    {
        [_bufferIndicator startAnimating];
    }
    else if (!buffering && [_bufferIndicator isAnimating])
    {
        
        [_bufferIndicator stopAnimating];
    }
}

#pragma - Private Methods
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void) cellDescriptionValues:(VideoDescriptionVideoDetailTableViewCell *)cell
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
   
    switch (_CellType) {
        case WhatsHot:
        {
            
            WhatsHotDataModel *dataModel = (WhatsHotDataModel *) _modal;
            
            cell.labelShare.text = dataModel.videoDescription;
            
            break;
        }
            
        case Trending :
        {
            TrendingVideosDataModel *dataModel = (TrendingVideosDataModel *) _modal;
            
            cell.labelShare.text = dataModel.videoDescription;
            
            break;
        }
            
        default:
            break;
    }
}


/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void) prepareView
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{    
    // Bottom bar
    CGFloat bottomBarH = BUTTON_SIZE + 1 * MARGIN;
    _bottomBar = [[UIView alloc] initWithFrame:CGRectMake(0, _orionView.frame.size.height - bottomBarH, _orionView.frame.size.width, bottomBarH)];
    _bottomBar.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_bottomBar];
    
    // Play/pause button
    _playPauseButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _playPauseButton.frame = CGRectMake(0, MARGIN, BUTTON_SIZE, BUTTON_SIZE);
    [_playPauseButton setImage:[UIImage imageNamed:@"playback_pause_stream"] forState:UIControlStateNormal];
    [_playPauseButton addTarget:self action:@selector(playPause:) forControlEvents:UIControlEventTouchUpInside];
    [_playPauseButton setContentEdgeInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
    [_bottomBar addSubview:_playPauseButton];
    

    // Mute button
    _muteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _muteButton.frame = CGRectMake(CGRectGetWidth(_bottomBar.frame) - BUTTON_SIZE, MARGIN, BUTTON_SIZE, BUTTON_SIZE);
    [[_muteButton imageView] setContentMode: UIViewContentModeScaleAspectFit];
//    [_muteButton setImage:[UIImage imageNamed:@"fullscreen-icon"] forState:UIControlStateNormal];
    [_muteButton addTarget:self action:@selector(mute:) forControlEvents:UIControlEventTouchUpInside];
    [_bottomBar addSubview:_muteButton];
    
    UIImageView *imageViewFullScreen = [[UIImageView alloc] initWithFrame:CGRectMake(12.5, 10, 15, 15)];
    imageViewFullScreen.image = [UIImage imageNamed:@"fullscreen-icon"];
    [_muteButton addSubview:imageViewFullScreen];
    
    // Time left label
    _timeLeftLabel = [[UILabel alloc] initWithFrame:CGRectMake( BUTTON_SIZE - 10 , MARGIN, BUTTON_SIZE, BUTTON_SIZE)];
    _timeLeftLabel.font = [UIFont fontWithName:kFontSFUIDisplayLight size:10];
    _timeLeftLabel.text = @"0:00";
    _timeLeftLabel.textColor = [UIColor whiteColor];
     _timeLeftLabel.textAlignment = NSTextAlignmentCenter;
    [_bottomBar addSubview:_timeLeftLabel];
    
    _timeTotalLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetWidth(_bottomBar.frame) - _muteButton.frame.origin.y - BUTTON_SIZE - 10, MARGIN, BUTTON_SIZE, BUTTON_SIZE)];
    _timeTotalLabel.font = [UIFont fontWithName:kFontSFUIDisplayLight size:10];
    _timeTotalLabel.text = @"0:00";
    _timeTotalLabel.textAlignment = NSTextAlignmentLeft;
    _timeTotalLabel.textColor = [UIColor whiteColor];
    [_bottomBar addSubview:_timeTotalLabel];
    
    // Time slider
    int sliderX = _timeLeftLabel.frame.origin.x + _timeLeftLabel.frame.size.width - 5;
    int sliderY = MARGIN;
    int sliderH = BUTTON_SIZE;
    int sliderW = _timeTotalLabel.frame.origin.x - sliderX - 5;
    
    _timeSlider = [[UISlider alloc] initWithFrame:CGRectMake(sliderX, sliderY, sliderW, sliderH)];
    
    [_timeSlider addTarget:self action:@selector(timeSliderDragExit:) forControlEvents:UIControlEventTouchUpInside];
    [_timeSlider addTarget:self action:@selector(timeSliderDragExit:) forControlEvents:UIControlEventTouchUpOutside];
    [_timeSlider addTarget:self action:@selector(timeSliderDragEnter:) forControlEvents:UIControlEventTouchDown];
    [_timeSlider addTarget:self action:@selector(timeSliderValueChanged:) forControlEvents:UIControlEventValueChanged];
    
    [_timeSlider setBackgroundColor:[UIColor clearColor]];
    _timeSlider.minimumValue = 0.0;
    _timeSlider.maximumValue = 0.0;
    [_bottomBar addSubview:_timeSlider];
    
    UITapGestureRecognizer *sliderTapGr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sliderTapped:)];
    [_timeSlider addGestureRecognizer:sliderTapGr];
    
    
    // Tap gesture regocnizer
    UITapGestureRecognizer *tapGr = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetected:)];
    tapGr.delegate = self;
    [_orionView addGestureRecognizer:tapGr];
    

}


/**
 *  Function called when play/pause button selected.
 */
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void)playPause:(UIButton*)button
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if ([_orionView isPaused])
    {
        [_orionView play];
        [_playPauseButton setImage:[UIImage imageNamed:@"playback_pause_stream"] forState:UIControlStateNormal];
    }
    else
    {
        [_orionView pause];
        [_playPauseButton setImage:[UIImage imageNamed:@"playback_play_Stream"] forState:UIControlStateNormal];
    }
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (IBAction) timeSliderDragEnter:(id)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    _isSeeking = true;
}

/**
 *  Function called when time slider value changed (dragging).
 */
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (IBAction) timeSliderValueChanged:(id)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    UISlider *slider = sender;
    [self updateTimeLabel:(int)slider.value];
    
}

/**
 *  Function called when time slider dragging ended.
 */
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (IBAction) timeSliderDragExit:(id)sender
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    
    UISlider *slider = sender;
    [self.orionView seekTo:[slider value]];
    _isSeeking = false;
}

/**
 *  Function called when time slider has been tapped.
 */
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)sliderTapped:(UIGestureRecognizer *)g
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    UISlider* s = (UISlider*)g.view;
    CGPoint pt = [g locationInView: s];
    CGFloat percentage = pt.x / s.bounds.size.width;
    CGFloat delta = percentage * (s.maximumValue - s.minimumValue);
    CGFloat value = s.minimumValue + delta;
    [s setValue:value animated:YES];
    [self.orionView seekTo:value];
    _isSeeking = false;
    
    [self updateTimeLabel:(int)value];
}

/**
 *  Function to update time label.
 */
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (void)updateTimeLabel:(int)totalSeconds
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int durationSeconds = (int)_timeSlider.maximumValue % 60;
    int durationMinutes = ((int)_timeSlider.maximumValue/60) % 60;
    _timeLeftLabel.text = [NSString stringWithFormat:@"%d:%02d",minutes, seconds];
    _timeTotalLabel.text = [NSString stringWithFormat:@"%d:%02d", durationMinutes, durationSeconds];
}

/**
 *  Function called when tap detected on the screen.
 */
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void)tapDetected:(UITapGestureRecognizer*)gr
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    [self showHideControlBars:!_controlsHidden];
}

/**
 *  Function to show or hide control bars.
 */
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void)showHideControlBars:(BOOL)hide
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    if (hide == _controlsHidden)
    {
        return;
    }
    
    _controlsHidden = hide;
    [UIView animateWithDuration:0.3f animations:^(void){
        _bottomBar.hidden = _controlsHidden;
    }];
}

/**
 *  Function called when mute button selected.
 */
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
-(void)mute:(UIButton*)button
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
      [AppDelegate appDelegate].allowedOrientation = UIInterfaceOrientationMaskLandscapeLeft;
    
    OrionRecordedViewController *vc = [[OrionRecordedViewController alloc] init];
    vc.modal = _modal;
    vc.CellType = _CellType;;
    
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:vc];
    [nc setNavigationBarHidden:YES animated:false];
    
    [_orionView pause];
    
    [self.navigationController presentViewController:nc animated:false completion:nil];
}

/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
- (NSArray *) makeDataValuesForWhatsHot:(NSArray *) dataArray
/*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*/
{
    NSMutableArray *dataArrayWithDataModel = [[NSMutableArray alloc] init];
    
    for (NSDictionary * object in dataArray)
    {
        WhatsHotDataModel *categoryDetailDataModel = [[WhatsHotDataModel alloc] init];
        
        categoryDetailDataModel.videoID = [[object objectForKey:kWHKeyID] stringValue];
        categoryDetailDataModel.videoCity = [object objectForKey:kWHKeyVideoCity];
        categoryDetailDataModel.categoryID = [[object objectForKey:kWHKeyCategoryID] stringValue];
        categoryDetailDataModel.subCategoryID = [object objectForKey:kWHKeySubCategoryID];
        categoryDetailDataModel.videoTitle = [object objectForKey:kWHKeyVideoTitle];
        categoryDetailDataModel.videoDescription = [object objectForKey:kWHKeyVideoDescription];
        categoryDetailDataModel.websiteLink = [object objectForKey:kWHKeyWebsiteLink];
        categoryDetailDataModel.videoThumbnail = [object objectForKey:kWHKeyVideoThumbnail];
        categoryDetailDataModel.videoLink = [object objectForKey:kWHKeyVideoLink];
        categoryDetailDataModel.webVideoLink = [object objectForKey:kWHKeyWebVideoLink];
        categoryDetailDataModel.status = [object objectForKey:kWHKeyStatus];
        categoryDetailDataModel.createdDate = [object objectForKey:kWHKeyCreatedDate];
        categoryDetailDataModel.metaData = [object objectForKey:kWHKeyMetaData];
        categoryDetailDataModel.whatsHot = [object objectForKey:kWHKeyWhatsHot];
        categoryDetailDataModel.popular = [object objectForKey:kWHKeyPopular];
        categoryDetailDataModel.videotype = [object objectForKey:kWHKeyVideoType];
        categoryDetailDataModel.webVideoType = [object objectForKey:kWHKeyWebVideoType];
        categoryDetailDataModel.userID = [object objectForKey:kWHKeyUserID];
        categoryDetailDataModel.videoFeature = [object objectForKey:kWHKeyVideoFeature];
        categoryDetailDataModel.panoramaImage = [object objectForKey:kWHKeyPanoramImage];
        categoryDetailDataModel.notificationFlag = [object objectForKey:kWHKeyNotificationFlag];
        categoryDetailDataModel.isImage = [object objectForKey:kWHKeyIsImage];
        categoryDetailDataModel.rating = [object objectForKey:kWHKeyRating];
        categoryDetailDataModel.categoryName = [object objectForKey:kWHKeyCategoryID];
        categoryDetailDataModel.viewCount = [[object objectForKey:kWHKeyViewCount] stringValue];
        categoryDetailDataModel.likeCount = [[object objectForKey:kWHKeyLikeCount] stringValue];
        
        [dataArrayWithDataModel addObject:categoryDetailDataModel];
    }
    
    return [dataArrayWithDataModel copy];
}


@end
