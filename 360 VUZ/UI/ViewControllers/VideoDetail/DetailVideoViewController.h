//
//  DetailVideoViewController.h
//  360 VUZ
//
//  Created by Mosib on 8/3/17.
//  Copyright © 2017 Khawaja Fahad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailVideoViewController : UIViewController

@property (nonatomic) CellTypeHomeViewController CellType;
@property (nonatomic) id modal;

@end
